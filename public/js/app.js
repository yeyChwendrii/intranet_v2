$(document).on('change', '#titularisation_direction, #titularisation_departementDirection', function () {
    let $field = $(this)
    let $directionField = $('#titularisation_direction')
    let $form = $field.closest('form')
    let data = {}
    data[$directionField.attr('name')] = $directionField.val()
    data[$field.attr('name')] = $field.val()
    $.post($form.attr('action'), data).then(function (data) {
        //chargement du champs service lié à une direction après selection de la direction
        let $inputdpt = $(data).find('#titularisation_departementDirection')
        $('#titularisation_departementDirection').replaceWith($inputdpt)

        //chargement du champs service lié à une direction après selection de la direction
        let $inputsvc = $(data).find('#titularisation_serviceDirection')
        $('#titularisation_serviceDirection').replaceWith($inputsvc)

        let $inputsvcdpt = $(data).find('#titularisation_serviceDepartementdirection')
        $('#titularisation_serviceDepartementdirection').replaceWith($inputsvcdpt)
    })

})