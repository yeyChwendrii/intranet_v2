<?php

namespace App\GlobalCall;


use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GlobalCall extends AbstractExtension
{
    private $messages;
    private $connectedUser;
    private $user;
    public function __construct(MessageRepository $messageRepository,
                                Security $security, UserRepository $userRepository, Environment $twi)
    {
        $this->messages = $messageRepository;
        $this->connectedUser = $security->getUser();
        $this->user = $this->connectedUser ? $userRepository->find($this->connectedUser->getId()) : null;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getMessagesForNotif', [$this, 'getMessagesForNotif']),
            new TwigFunction('getConnectedUser', [$this, 'getConnectedUser'])
        ];
    }

    public function getMessagesForNotif()
    {
        $messages = $this->messages->selectAllReceived($this->connectedUser->getId());
        return $messages;
    }
    public function getConnectedUser()
    {
        $user = $this->user;
        return $user;
    }
}