<?php

namespace App\Repository\AffairesSociales;

use App\Entity\AffairesSociales\EntiteMedicale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EntiteMedicale|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntiteMedicale|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntiteMedicale[]    findAll()
 * @method EntiteMedicale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntiteMedicaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntiteMedicale::class);
    }

    // /**
    //  * @return EntiteMedicale[] Returns an array of EntiteMedicale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EntiteMedicale
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
