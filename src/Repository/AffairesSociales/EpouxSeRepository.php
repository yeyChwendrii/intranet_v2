<?php

namespace App\Repository\AffairesSociales;

use App\Entity\AffairesSociales\EpouxSe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EpouxSe|null find($id, $lockMode = null, $lockVersion = null)
 * @method EpouxSe|null findOneBy(array $criteria, array $orderBy = null)
 * @method EpouxSe[]    findAll()
 * @method EpouxSe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EpouxSeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EpouxSe::class);
    }

    // /**
    //  * @return EpouxSe[] Returns an array of EpouxSe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * Compte le nombre d'epoux(ses) d'un agent
     * @param $user
     * @return mixed
     */
    public function countEpouxSesDunAgent($user)
    {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('count(e.id)')
            ->where('user = :user')
            ->setParameter('user' , $user)
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function countEpouxSes()
    {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EpouxSe
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
