<?php

namespace App\Repository\AffairesSociales;

use App\Entity\AffairesSociales\ChargeMedicale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ChargeMedicale|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChargeMedicale|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChargeMedicale[]    findAll()
 * @method ChargeMedicale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChargeMedicaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChargeMedicale::class);
    }

    // /**
    //  * @return ChargeMedicale[] Returns an array of ChargeMedicale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByEntiteMedicale($entiteMedicale)
    {
        return $this->createQueryBuilder('c')
            ->where('c.entiteMedicale = :entiteMedicale')
            ->setParameter('entiteMedicale', $entiteMedicale)
            ->orderBy('c.dateAt', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getSumByYear($entiteMedicale){
        return $this->createQueryBuilder('c')
            ->select('sum(c.couts) as sommeannuelle, SUBSTRING(c.dateAt, 1, 4) as year')
            ->where('c.entiteMedicale = :entiteMedicale')
            ->setParameter('entiteMedicale', $entiteMedicale)
            ->groupBy('year')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getAllSum($entiteMedicale){
        return $this->createQueryBuilder('c')
            ->select('sum(c.couts) as cout_total')
            ->where('c.entiteMedicale = :$entiteMedicale')
            ->setParameter('entiteMedicale', $entiteMedicale)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?ChargeMedicale
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
