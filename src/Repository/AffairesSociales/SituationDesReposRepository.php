<?php

namespace App\Repository\AffairesSociales;

use App\Entity\AffairesSociales\SituationDesRepos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SituationDesRepos|null find($id, $lockMode = null, $lockVersion = null)
 * @method SituationDesRepos|null findOneBy(array $criteria, array $orderBy = null)
 * @method SituationDesRepos[]    findAll()
 * @method SituationDesRepos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SituationDesReposRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SituationDesRepos::class);
    }

    // /**
    //  * @return SituationDesRepos[] Returns an array of SituationDesRepos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SituationDesRepos
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
