<?php

namespace App\Repository\AffairesSociales;

use App\Entity\AffairesSociales\Enfant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Enfant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enfant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enfant[]    findAll()
 * @method Enfant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnfantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Enfant::class);
    }

    // /**
    //  * @return Enfant[] Returns an array of Enfant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * Compte le nombre d'enfants d'un agent
     * @param $user
     * @return mixed
     */
    public function countEnfantsDunAgent($user)
    {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('count(e.id)')
            ->where('user = :user')
            ->setParameter('user' , $user)
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function countEnfants()
    {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?Enfant
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
