<?php

namespace App\Repository\AffairesSociales;

use App\Entity\AffairesSociales\SituationDuCarnet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SituationDuCarnet|null find($id, $lockMode = null, $lockVersion = null)
 * @method SituationDuCarnet|null findOneBy(array $criteria, array $orderBy = null)
 * @method SituationDuCarnet[]    findAll()
 * @method SituationDuCarnet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SituationDuCarnetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SituationDuCarnet::class);
    }

    // /**
    //  * @return SituationDuCarnet[] Returns an array of SituationDuCarnet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countCarnetsDunAgent($user)
    {
        $qb = $this->createQueryBuilder('c');
        return $qb
            ->select('count(c.id)')
            ->where('user = :user')
            ->setParameter('user' , $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?SituationDuCarnet
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
