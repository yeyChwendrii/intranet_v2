<?php

namespace App\Repository;

use App\Entity\ServiceDepartementdirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ServiceDepartementdirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceDepartementdirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceDepartementdirection[]    findAll()
 * @method ServiceDepartementdirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceDepartementdirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ServiceDepartementdirection::class);
    }

    /**
     * @param $service_id
     * @return mixed
     */
    public function findByIdService($service_id)
    {
        $qb = $this->createQueryBuilder('s');
        return $qb
            //->select('p.id')
            ->where('s.service = :service')
            ->setParameter('service', $service_id)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * Affiche les service de département lié a leur departement et direction
     * @return mixed
     */
    public function findSomeFields()
    {
        $fonctionnel = 1;
        $qb = $this->createQueryBuilder('s');

         $qb
            ->leftJoin('s.service', 'serv')
             ->leftJoin('s.departementdirection', 'dd')
             ->leftJoin('dd.departement', 'dep')
             ->leftJoin('dd.direction', 'dir')
             //->leftJoin('s.posteuserServicedepartementdirections', 'pu_svcdpt')
             ->leftJoin('s.agence', 'ag')
             ->andWhere('s.fonctionnel = :fonctionnel')
             ->setParameter('fonctionnel', $fonctionnel)
             ->select('s.id, serv.designation as serv_designation, serv.id as serv_id, s.tel as service_tel, ag.designation as ag_designation, ag.id as agence_id,
             dep.id as departement_id, dd.id as departementdirection_id, dd.tel as departementdirection_tel, dep.designation as dep_designation, 
             dir.id as direction_id, dir.designation as dir_designation, dir.tel as direction_tel')
             ->orderBy('serv.designation', 'DESC')

            ;
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Afficher les agents par service de département donné
     *
     * @return mixed
     */
    /*
    public function findAgentOfService()
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->leftJoin('s.service', 'serv')
            ->leftJoin('s.departementdirection', 'dd')
            ->leftJoin('dd.departement', 'dep')
            ->leftJoin('s.agence', 'ag')
            ->leftJoin('s.posteuserServicedepartementdirections', 'p_u_serdpt')
            ->leftJoin('p_u_serdpt.posteuser', 'p_u')
            ->leftJoin('p_u.user', 'u')
            ->leftJoin('p_u.poste', 'p')
            ->select('s.id, u.id as u_id, u.matricule, u.nom, u.prenom, p.designation as poste_designation')
            ->distinct()
        ;
        return $qb->getQuery()
            ->getResult();
    }
    */

    /**
     * Afficher les services de département
     *
     * @return mixed
     */

    public function findServiceOfDepartement()
    {
        $fonctionnel = 1;
        $qb = $this->createQueryBuilder('s_dpt');

        $qb
            ->leftJoin('s_dpt.service', 's')
            ->leftJoin('s_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->andWhere('s_dpt.fonctionnel = :fonctionnel')
            ->setParameter('fonctionnel', $fonctionnel)
            ->select('s_dpt.id as servicedepartementdirection_id, dpt_dir.id as departementdirection_id, s.designation as service_designation, s_dpt.tel')
            ->orderBy('s_dpt.id')
            ->distinct()
        ;
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Affiche les services de département lié a leur departement et direction d'une île donnée
     * @return mixed
     */
    public function findAllServicesDeptByIle($idIle)
    {
        $fonctionnel = 1;
        $qb = $this->createQueryBuilder('s');
        if ($idIle == 1) {
            $qb
                ->leftJoin('s.service', 'serv')
                ->leftJoin('s.departementdirection', 'dd')
                ->leftJoin('dd.departement', 'dep')
                ->leftJoin('dd.direction', 'dir')
                ->leftJoin('s.agence', 'ag')
                ->andWhere('s.fonctionnel = :fonctionnel')
                ->andWhere('dir.id != 8 and dir.id != 10')
                ->setParameter('fonctionnel', $fonctionnel)
                ->select('s.id, serv.designation as serv_designation, serv.id as serv_id, s.tel as service_tel, ag.designation as ag_designation, ag.id as agence_id,
                 dep.id as departement_id, dd.id as departementdirection_id, dd.tel as departementdirection_tel, dep.designation as dep_designation, 
                 dir.id as direction_id, dir.designation as dir_designation, dir.tel as direction_tel')
                ->orderBy('serv.designation', 'DESC')
            ;
        } elseif ($idIle == 2) {
            $qb
                ->leftJoin('s.service', 'serv')
                ->leftJoin('s.departementdirection', 'dd')
                ->leftJoin('dd.departement', 'dep')
                ->leftJoin('dd.direction', 'dir')
                ->leftJoin('s.agence', 'ag')
                ->andWhere('s.fonctionnel = :fonctionnel')
                ->andWhere('dir.id = 8')
                ->setParameter('fonctionnel', $fonctionnel)
                ->select('s.id, serv.designation as serv_designation, serv.id as serv_id, s.tel as service_tel, ag.designation as ag_designation, ag.id as agence_id,
                 dep.id as departement_id, dd.id as departementdirection_id, dd.tel as departementdirection_tel, dep.designation as dep_designation, 
                 dir.id as direction_id, dir.designation as dir_designation, dir.tel as direction_tel')
                ->orderBy('serv.designation', 'DESC')
            ;
        } elseif ($idIle == 3) {
            $qb
                ->leftJoin('s.service', 'serv')
                ->leftJoin('s.departementdirection', 'dd')
                ->leftJoin('dd.departement', 'dep')
                ->leftJoin('dd.direction', 'dir')
                ->leftJoin('s.agence', 'ag')
                ->andWhere('s.fonctionnel = :fonctionnel')
                ->andWhere('dir.id = 10')
                ->setParameter('fonctionnel', $fonctionnel)
                ->select('s.id, serv.designation as serv_designation, serv.id as serv_id, s.tel as service_tel, ag.designation as ag_designation, ag.id as agence_id,
                 dep.id as departement_id, dd.id as departementdirection_id, dd.tel as departementdirection_tel, dep.designation as dep_designation, 
                 dir.id as direction_id, dir.designation as dir_designation, dir.tel as direction_tel')
                ->orderBy('serv.designation', 'DESC')
            ;
        }

        return $qb->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?ServiceDepartementdirection
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
