<?php

namespace App\Repository;

use App\Entity\Sousmetier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Sousmetier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sousmetier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sousmetier[]    findAll()
 * @method Sousmetier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SousmetierRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sousmetier::class);
    }

    // /**
    //  * @return Sousmetier[] Returns an array of Sousmetier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sousmetier
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
