<?php

namespace App\Repository\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\BdsCotisation;
use App\Entity\BanqueDeSolidarite\BdsParametresdecotisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BdsCotisation|null find($id, $lockMode = null, $lockVersion = null)
 * @method BdsCotisation|null findOneBy(array $criteria, array $orderBy = null)
 * @method BdsCotisation[]    findAll()
 * @method BdsCotisation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BdsCotisationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BdsCotisation::class);
    }

    // /**
    //  * @return BdsCotisation[] Returns an array of BdsCotisation objects
    //  */

    public function findByAgent($user)
    {
        return $this->createQueryBuilder('b')
            ->leftJoin('b.BdsParametresdecotisation', 'bds_param')
            ->andWhere('bds_param.user = :user')
            ->setParameter('user', $user)
            ->orderBy('b.cotisationAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    public function getSumByYear($user){
        return $this->createQueryBuilder('b')
            ->select('sum(b.cotisationappliquee) as sommeannuelle, SUBSTRING(b.cotisationAt, 1, 4) as year')
            ->leftJoin('b.BdsParametresdecotisation', 'bds_param')
            ->andWhere('bds_param.user = :user')
            ->setParameter('user', $user)
            ->groupBy('year')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getAllSum($user){
        return $this->createQueryBuilder('b')
            ->select('sum(b.cotisationappliquee) as somme_totale')
            ->leftJoin('b.BdsParametresdecotisation', 'bds_param')
            ->andWhere('bds_param.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?BdsCotisation
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
