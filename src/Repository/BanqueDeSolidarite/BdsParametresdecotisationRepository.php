<?php

namespace App\Repository\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\BdsParametresdecotisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BdsParametresdecotisation|null find($id, $lockMode = null, $lockVersion = null)
 * @method BdsParametresdecotisation|null findOneBy(array $criteria, array $orderBy = null)
 * @method BdsParametresdecotisation[]    findAll()
 * @method BdsParametresdecotisation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BdsParametresdecotisationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BdsParametresdecotisation::class);
    }

    // /**
    //  * @return BdsParametresdecotisation[] Returns an array of BdsParametresdecotisation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByAgent($user): ?BdsParametresdecotisation
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
