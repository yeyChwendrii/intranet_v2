<?php

namespace App\Repository;

use App\Entity\Departement;
use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\UserSearch;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Departement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Departement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Departement[]    findAll()
 * @method Departement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Departement::class);
    }

    // Afficher le département d'une direction selon la direction choisie
    // Rechercher le département d'une direction
    /**
     * @return Query
     */
    public function findDeptDirByDir(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getDirection()){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.direction = :search')
                ->setParameter('search', $search->getDirection());
        }

        return $query->getQuery()->getResult();
    }

    //Afficher les départements selon la direction choisi
    /*
     * @return Query
     */
    public function findDeptDirByDirection(Direction $direction)
    {
        $qb = $this->createQueryBuilder('d');

        if ($direction){
            $qb
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.direction = :direction')
                ->setParameter('direction', $direction);
        }

        return $qb->getQuery()->getResult();
    }

    // Afficher le département d'une direction selon l'agence choisie
    // Rechercher le département d'une agence
    /**
     * @return Query
     */
    public function findDeptDirByAgence(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getAgence()){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.agence = :search')
                ->setParameter('search', $search->getAgence());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher les départements d'une île selon l'île choisie
    // Rechercher les départements d'une île
    /**
     * @return Query
     */
    public function findDeptByIle(Ville $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getIle() == 'Ngazidja'){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->andWhere('dir.id != 8 and dir.id != 10')
            ;
        } elseif ($search->getIle() == 'Ndzuani'){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->andWhere('dir.id = 10')
            ;
        } elseif ($search->getIle() == 'Mwali'){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->andWhere('dir.id = 8')
            ;
        }
        return $query->getQuery()->getResult();
    }

    public function findDeptByNgz()
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin('d.departementDirections', 'dd')
            ->addSelect('dd')
            ->leftJoin('dd.direction', 'dir')
            ->addSelect('dir')
            ->andWhere('dir.id != 8 and dir.id != 10')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findDeptByNdz()
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin('d.departementDirections', 'dd')
            ->addSelect('dd')
            ->leftJoin('dd.direction', 'dir')
            ->addSelect('dir')
            ->andWhere('dir.id = 10')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findDeptByMwa()
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin('d.departementDirections', 'dd')
            ->addSelect('dd')
            ->leftJoin('dd.direction', 'dir')
            ->addSelect('dir')
            ->andWhere('dir.id = 8')
        ;
        return $qb->getQuery()->getResult();
    }

    // Afficher le département selon l'agent choisie
    // Rechercher le département d'un agent
    /**
     * @return Query
     */
    public function findDeptByAgent(UserSearch $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getMatricule()){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.posteuserDepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.posteuser', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.user', 'u')
                ->addSelect('u')
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery()->getResult();
    }

    // On compte le nombre de département d'une île
    /*public function countDept($ile)
    {
        $qb = $this->createQueryBuilder('d');
        if ($ile == 1) {
            $qb->select('count(d.id)')
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->andWhere('dir.id != 8 and dir.id != 10')
            ;
        } elseif ($ile == 2) {
            $qb->select('count(d.id)')
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->andWhere('dir.id = 8')
            ;
        } elseif ($ile == 3) {
            $qb->select('count(d.id)')
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->andWhere('dir.id = 10')
            ;
        }
        return $qb->getQuery()->getSingleScalarResult();
    }*/

    // /**
    //  * @return Departement[] Returns an array of Departement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Departement
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
