<?php

namespace App\Repository;

use App\Entity\Agence;
use App\Entity\DepartementDirection;
use App\Entity\Ile;
use App\Entity\ServiceDepartementdirection;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Agence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Agence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Agence[]    findAll()
 * @method Agence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgenceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Agence::class);
    }

    // Afficher l'agence d'un service selon le service choisi
    // Rechercher l'agence d'un service
    /**
     * @return Query
     */
    public function findAgenceByServ(ServiceDepartementdirection $search)
    {
        $qb = $this->createQueryBuilder('a');
        $query = $qb
            ->orderBy('a.designation', 'ASC');
        if ($search->getService()){
            $query = $query
                ->leftJoin('a.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('a.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->andWhere('sdd.service = :search or sd.service = :search')
                ->setParameter('search', $search->getService());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher l'agence d'un département selon le département choisi
    // Rechercher l'agence d'un département
    /**
     * @return Query
     */
    public function findAgenceByDept(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('a');
        $query = $qb
            ->orderBy('a.designation', 'ASC');
        if ($search->getDepartement()){
            $query = $query
                ->leftJoin('a.departementDirections', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.departement = :search')
                ->setParameter('search', $search->getDepartement());
        }
        return $query->getQuery()->getResult();
    }

    // Afficher les agences d'une direction selon la direction choisie
    // Rechercher les agences d'une direction
    /**
     * @return Query
     */
    public function findAgenceByDir(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('a');
        $query = $qb
            ->orderBy('a.designation', 'ASC');
        if ($search->getDirection()){
            $query = $query
                ->leftJoin('a.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('a.serviceDirections', 'sd')
                ->addSelect('sd')
                ->andWhere('dd.direction = :search or sd.direction = :search')
                ->setParameter('search', $search->getDirection());
        }
        return $query->getQuery()->getResult();
    }

    // Afficher les agences d'une île selon l'île choisie
    // Rechercher les agences d'une île
    /**
     * @return Query
     */
    public function findAgenceByIle(Ville $search)
    {
        $qb = $this->createQueryBuilder('a');
        $query = $qb
            ->orderBy('a.designation', 'ASC');
        if ($search->getIle()){
            $query = $query
                ->leftJoin('a.ville', 'v')
                ->addSelect('v')
                ->andWhere('v.ile = :search')
                ->setParameter('search', $search->getIle());
        }
        return $query->getQuery()->getResult();
    }

    public function findAgenceByGetIle($ile)
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.ville', 'v')
            ->addSelect('v')
            ->leftJoin('v.ile', 'i')
            ->addSelect('i')
            ->andWhere('i.designation = :search')
            ->setParameter('search', $ile)
        ;
        return $qb->getQuery()->getResult();
    }

    public function findAllByIle($idIle)
    {
        $qb = $this->createQueryBuilder('ag');

        $qb
            ->leftJoin('ag.serviceDepartementdirections', 's_dptdir')
            ->leftJoin('ag.serviceDirections', 's_dir')
            ->leftJoin('ag.departementDirections', 'dptdir')
            ->leftJoin('ag.directions', 'dir')
            ->leftJoin('ag.ville', 'v')
            ->leftJoin('v.ile', 'i')
            ->andWhere('i.id = :search')
            ->setParameter('search', $idIle)
            ->select('ag.id, ag.designation, v.designation as ville_designation, i.designation as ile_designation ')
            ->orderBy('ag.designation', 'ASC')
        ;
        return $qb->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Agence[] Returns an array of Agence objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Agence
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /*
     $allIndispo = $qb
    ->select('a2.id, SUM(a2.quantity) as total')
    ->leftJoin('a2.dates', 'd')
    ->groupBy('a2.id')
    ->having('(a2.quantity - COUNT(d.quantity)) <= 0')
    ->orderBy('a2.id', 'ASC');
     */
    /*
    public function countHommes()
    {
        $sexe = 'M';
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->where('u.sexe = :sexe')
            ->setParameter('sexe', $sexe)
            ->getQuery()
            ->getSingleScalarResult();
    }
    */
     public function countServDeptNgz(){
         $ile = 1;
         $qb = $this->createQueryBuilder('a');
         return $qb
             ->select('count(a.id)')
             ->leftJoin('a.ville', 'v')
             ->where('v.ile = :ile')
             ->setParameter('ile', $ile)
             ->getQuery()
             ->getSingleScalarResult()
             ;
     }

    // On compte le nombre d'agence d'une île
    public function countAgence($ile)
    {
        $qb = $this->createQueryBuilder('a');
        return $qb
            ->select('count(a.id)')
            ->leftJoin('a.ville', 'v')
            ->where('v.ile = :ile')
            ->setParameter('ile', $ile)
            ->getQuery()->getSingleScalarResult();
    }

    /*=================================================
     *          YEYCHWENDRII
     ==================================================*/
    /**
     * Affiche les agences
     * @return mixed
     */
    public function findSomeFields()
    {
        $qb = $this->createQueryBuilder('ag');

        $qb
            ->leftJoin('ag.serviceDepartementdirections', 's_dptdir')
            ->leftJoin('ag.serviceDirections', 's_dir')
            ->leftJoin('ag.departementDirections', 'dptdir')
            ->leftJoin('ag.directions', 'dir')
            ->leftJoin('ag.ville', 'v')
            ->leftJoin('v.ile', 'i')
            ->select('ag.id, ag.designation, v.designation as ville_designation, i.designation as ile_designation ')
            ->orderBy('ag.designation', 'ASC')
        ;
        return $qb->getQuery()
            ->getResult();
    }
}
