<?php

namespace App\Repository;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\PosteUser;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PosteUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PosteUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PosteUser[]    findAll()
 * @method PosteUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PosteUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PosteUser::class);
    }

    // /**
    //  * @return PosteUser[] Returns an array of PosteUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function showAgentEnLigne(){
        $enfonction =  1;
        $delay = new \DateTime();
        $delay->setTimestamp(strtotime('2 minutes ago'));

        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 's')
            ->leftJoin('s_dir.direction', 'dir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.tel, u.enfonction, s.designation as service_designation, p_u1.nomEvenement,dir.id as direction_id, p.designation as poste_designation')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->andWhere('u.lastActivityAt > :delay')
            ->setParameter('delay', $delay)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }


    /**
     * Custom request for id PosteUser
     * @param $idEvenement
     * @return mixed
     */
    public function findByIdEvenement($idEvenement, $nomEvenement)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where('p.idEvenement = :idEvenement')
            ->setParameter('idEvenement', $idEvenement)
            ->andWhere('p.nomEvenement = :nomEvenement')
            ->setParameter('nomEvenement', $nomEvenement)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByIdUser($user){
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where(('p.user = :user'))
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /*
     * Affiche la derniere ligne de poste_user selon la date d'acquisition du poste
     */
    public function findLastByUser($user)
    {
        return $this->createQueryBuilder('p')
            //->select('p.poste')
            ->andWhere('p.user = :user')
            ->setParameter('user', $user)
            ->orderBy('p.dateEvenementAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return PosteUser[] Returns an array of PosteUser objects
     */
    public function findAllByLastPoste()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.dateEvenementAt', 'DESC')
            ->groupBy('p.user')
            //->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * Affiche tous les agents par statut (statigaire, contractuel, titulaire...) avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAllByStatut($statut_id){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->leftJoin('u.statut', 's')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation, s.id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->andWhere('s.id = :id')
            ->setParameter('id', $statut_id)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }


    /**
     * Affiche tous les agents des services direction par le service choisi avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAgentOfScdir(ServiceDirection $serviceDirection){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 's')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.tel, u.enfonction, s.designation as service_designation, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('p_u_s_dir.servicedirection = :servicedirection')
            ->setParameter('servicedirection', $serviceDirection)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    public function showAgentOfScdirSansParam(){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 's')
            ->leftJoin('s_dir.direction', 'dir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.tel, u.enfonction, s.designation as service_designation, p_u1.nomEvenement,dir.id as direction_id, p.designation as poste_designation')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche tous les agents des services département par le service choisi  avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAgentOfSvcdpt(ServiceDepartementdirection $serviceDepartementdirection){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dpt')
            ->leftJoin('p_u_s_dpt.servicedepartementdirection', 's_dpt')
            ->leftJoin('s_dpt.service', 's')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, s.designation as service_designation, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('p_u_s_dpt.servicedepartementdirection = :servicedepartementdirection')
            ->setParameter('servicedepartementdirection', $serviceDepartementdirection)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche tous les agents des services direction par le service choisi  avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAgentOfSvcdir(ServiceDirection $serviceDirection){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 's')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, s.designation as service_designation, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('p_u_s_dir.servicedirection = :servicedirection')
            ->setParameter('servicedirection', $serviceDirection)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }
    /**
     * Affiche tous les agents des services département Sans parametre (Seulement ceux qui sont en fonction)
     * @return mixed
     */
    public function showAgentOfSvcdptSansParams(){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dpt')
            ->leftJoin('p_u_s_dpt.servicedepartementdirection', 's_dpt')
            ->leftJoin('s_dpt.service', 's')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, s_dpt.id as service_id, s.designation as service_designation, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Sans les chefs de département
     * Affiche tous les agents des services département par  Dpt  avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAgentOfDiffenrentSvcOfDpt(DepartementDirection $departementdirection){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dpt')
            ->leftJoin('p_u_s_dpt.servicedepartementdirection', 's_dpt')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('s_dpt.departementdirection = :departementdirection')
            ->setParameter('departementdirection', $departementdirection)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Tout poste lié au département directement comme par exemple le chef de département
     * Affiche les agents liés au département indiqué en paramètre
     * @return mixed
     */
    public function showAgentOfDptOnly(DepartementDirection $departementdirection){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p_u1.dateEvenementAt, p.designation as poste_designation')
            ->andWhere('p_u_dpt.departementdirection = :departementdirection')
            ->setParameter('departementdirection', $departementdirection)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('p_u1.dateEvenementAt', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche les agents liés au département indiqué en paramètre
     * @return mixed
     */
    public function showAgentOfDptOnlySansParam(){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dit')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation, dpt_dit.id as departementdirection_id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Agent des services de département avec les chefs de département
     * @return mixed
     */
    public function showAgentOfDptdirSansParam(){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt_dir')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_sdpt')
            ->leftJoin('p_u_sdpt.servicedepartementdirection', 'sdpt')
            ->leftJoin('sdpt.service', 's')
            ->leftJoin('sdpt.departementdirection', 'dpt_dir2')
            ->leftJoin('p_u_dpt_dir.departementdirection', 'dpt_dir1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, p_u1.nomEvenement, p.designation as poste_designation, 
                dpt_dir1.id as departementdirection1_id, sdpt.id as servicedepartementdirection_id, s.designation as service_designation, dpt_dir2.id as departementdirection2_id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('sdpt.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche les agents de tout un département (tous les services du département mis en paramètre)
     * @return mixed
     */
    public function showAgentOfDptdir(DepartementDirection $departementdirection){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt_dir')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_sdpt')
            ->leftJoin('p_u_sdpt.servicedepartementdirection', 'sdpt')
            ->leftJoin('sdpt.service', 's')
            ->leftJoin('sdpt.departementdirection', 'dpt_dir2')
            ->leftJoin('p_u_dpt_dir.departementdirection', 'dpt_dir1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, p_u1.nomEvenement, p_u1.dateEvenementAt, p.designation as poste_designation,
                dpt_dir1.id as departementdirection1_id, sdpt.id as servicedepartementdirection_id, s.designation as service_designation, dpt_dir2.id as departementdirection2_id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->andWhere('sdpt.departementdirection = :departementdirection')
            //->orWhere('p_u_dpt_dir.departementdirection = :departementdirection')
            ->setParameter('departementdirection', $departementdirection)
            ->orderBy('p_u1.dateEvenementAt', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /*
     * ========================================================================================
     *                              DIRECTION
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Pour le directeur de la direction choisi et tout poste lié directement à la direction
     * @param Direction $direction
     * @return mixed
     */
    public function showAgentOfdir(Direction $direction){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('p_u_dir.direction = :direction')
            ->setParameter('direction', $direction)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Pour le directeur et tout poste lié directement à la direction
     * @param Direction $direction
     * @return mixed
     */
    public function showAgentOfdirSansParam(){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, u.tel, p_u1.nomEvenement, p.designation as poste_designation, dir.id as direction_id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche tous les agents des services direction par  Direction  avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAgentOfDiffenrentSvcOfDir(Direction $direction){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('s_dir.direction = :direction')
            ->setParameter('direction', $direction)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche les agents des départements de la direction indiquée en paramètre
     * @param Direction $direction
     * @return mixed
     */
    public function showAgentOfDiffenrentDptOfDir(Direction $direction){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt_dir')
            ->leftJoin('p_u_dpt_dir.departementdirection', 'dpt_dir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('dpt_dir.direction = :direction')
            ->setParameter('direction', $direction)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }


    public function showAgentOfDiffenrentSvcOfDptdir(Direction $direction){
        $enfonction =  1;
        return $this->createQueryBuilder('p_u1')
            ->select('p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.departementdirection', 'dptdir')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('p_u1.user', 'u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('dptdir.direction = :direction')
            ->setParameter('direction', $direction)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }



    /*
    public function findOneBySomeField($value): ?PosteUser
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
