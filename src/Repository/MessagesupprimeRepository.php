<?php

namespace App\Repository;

use App\Entity\Messagesupprime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Messagesupprime|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messagesupprime|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messagesupprime[]    findAll()
 * @method Messagesupprime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesupprimeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Messagesupprime::class);
    }

    // /**
    //  * @return Messagesupprime[] Returns an array of Messagesupprime objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countArchivedByUser($user)
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->where('m.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function showArchivedByUser($user)
    {
        return $this->createQueryBuilder('m')
            ->where('m.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Messagesupprime
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
