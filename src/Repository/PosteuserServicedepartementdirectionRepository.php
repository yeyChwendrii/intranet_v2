<?php

namespace App\Repository;

use App\Entity\PosteuserServicedepartementdirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PosteuserServicedepartementdirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method PosteuserServicedepartementdirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method PosteuserServicedepartementdirection[]    findAll()
 * @method PosteuserServicedepartementdirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PosteuserServicedepartementdirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PosteuserServicedepartementdirection::class);
    }

    // /**
    //  * @return PosteuserServicedepartementdirection[] Returns an array of PosteuserServicedepartementdirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByIdPosteUser($posteuser_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where('p.posteuser = :posteuser')
            ->setParameter('posteuser', $posteuser_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByIdService($service_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            ->where('p.servicedepartementdirection = :servicedepartementdirection')
            ->setParameter('servicedepartementdirection', $service_id)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?PosteuserServicedepartementdirection
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
