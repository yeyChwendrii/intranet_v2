<?php

namespace App\Repository;

use App\Entity\AlerteAdministrateur;
use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    // /**
    //  * @return Message[] Returns an array of Message objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    /**Selectionne tous les messages
     * @return mixed
     */
    public function selectAll()
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.dateEnvoiAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**Message reçus
     * @param $user
     * @return mixed
     */
    public function selectAllReceived($user)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.receivers', 'u')
            ->leftJoin('m.messagesvuses', 'mv')
            ->leftJoin('m.messagesupprimes', 'ms')
            ->leftJoin('ms.user', 'u_ms')
            ->where('u.id = :id')
            ->setParameter('id', $user)
            ->orderBy('m.dateEnvoiAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /** Affiche les messages supprimé par l'utilisateur donc archivés
     * @param $user
     * @return mixed
     */
    public function selectAllArchived($user)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.receivers', 'u')
            ->leftJoin('m.messagesvuses', 'mv')
            ->leftJoin('m.messagesupprimes', 'ms')
            ->leftJoin('ms.user', 'u_ms')
            ->where('u.id = :id')
            ->setParameter('id', $user)
            ->andWhere('u_ms.id = u.id')
            ->orderBy('m.dateEnvoiAt', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function countAllArchived($user)
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->leftJoin('m.receivers', 'u')
            ->leftJoin('m.messagesupprimes', 'ms')
            ->leftJoin('ms.user', 'u_ms')
            ->where('u.id = :id')
            ->setParameter('id', $user)
            ->andWhere('u_ms.id = :id')
            ->setParameter('id', $user)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**Compte le nombre de message reçus par l'utilisateur en paramètre
     * @param $user
     * @return mixed
     */
    public function countReceivedByUser($user)
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->leftJoin('m.receivers', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $user)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**Compte le nombre de message envoyé par l'uutilisateur en parametre
     * @param $user
     * @return mixed
     */
    public function countSentByUser($user)
    {
        return $this->createQueryBuilder('m')
            ->select('count(m.id)')
            ->leftJoin('m.userSender', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $user)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function showSentByUser($user)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.userSender', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $user)
            ->getQuery()
            ->getResult()
            ;
    }




    /*
    public function findOneBySomeField($value): ?Message
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
