<?php

namespace App\Repository;

use App\Entity\Messagesvus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Messagesvus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messagesvus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messagesvus[]    findAll()
 * @method Messagesvus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesvusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Messagesvus::class);
    }

    // /**
    //  * @return Messagesvus[] Returns an array of Messagesvus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param $message_id
     * @return mixed
     */
    public function selectAllByMessageId($message_id)
    {
        return $this->createQueryBuilder('mv')
            ->leftJoin('mv.message', 'm')
            ->where('m.id = :id')
            ->setParameter('id', $message_id)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Messagesvus
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
