<?php

namespace App\Repository;

use App\Entity\PosteuserServicedirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PosteuserServicedirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method PosteuserServicedirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method PosteuserServicedirection[]    findAll()
 * @method PosteuserServicedirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PosteuserServicedirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PosteuserServicedirection::class);
    }

    // /**
    //  * @return PosteuserServicedirection[] Returns an array of PosteuserServicedirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param $posteuser_id
     * @return mixed
     */
    public function findByIdPosteUser($posteuser_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where('p.posteuser = :posteuser')
            ->setParameter('posteuser', $posteuser_id)
            ->getQuery()
            //->getSingleScalarResult();
            ->getOneOrNullResult();
    }

    public function findByIdService($service_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where('p.servicedirection = :servicedirection')
            ->setParameter('servicedirection', $service_id)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?PosteuserServicedirection
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
