<?php

namespace App\Repository;

use App\Entity\PosteuserDepartementdirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PosteuserDepartementdirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method PosteuserDepartementdirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method PosteuserDepartementdirection[]    findAll()
 * @method PosteuserDepartementdirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PosteuserDepartementdirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PosteuserDepartementdirection::class);
    }

    // /**
    //  * @return PosteuserDepartementdirection[] Returns an array of PosteuserDepartementdirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByIdDepartement($departement_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            ->where('p.departementdirection = :departementdirection')
            ->setParameter('departementdirection', $departement_id)
            ->getQuery()
            ->getResult();
    }

    public function findByIdPosteUser($posteuser_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where('p.posteuser = :posteuser')
            ->setParameter('posteuser', $posteuser_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findOneBySomeField($value): ?PosteuserDepartementdirection
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
