<?php

namespace App\Repository;

use App\Entity\DepartementDirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DepartementDirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method DepartementDirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method DepartementDirection[]    findAll()
 * @method DepartementDirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartementDirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DepartementDirection::class);
    }

    // /**
    //  * @return DepartementDirection[] Returns an array of DepartementDirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * Affiche les service de département lié a leur departement et direction
     * @return mixed
     */
    public function findSomeFields()
    {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->leftJoin('d.departement', 'dpt')
            ->leftJoin('d.direction', 'dir')
            ->leftJoin('d.agence', 'ag')
            ->select('d.id, dpt.id as departement_id, dpt.designation as departement_designation, d.tel, dir.designation as direction_designation, dir.tel as direction_tel, 
            ag.designation as agence_designation, ag.id as agence_id')
            ->orderBy('dpt.designation', 'ASC')
        ;
        return $qb->getQuery()
            ->getResult();
    }

    /*
     * Affiche le departement avec les entités liées (c'est comme un findall qui est customizé pour éviter la lourdeur de la requête findall)
     */
    public function showDepartement(){
        $qb = $this->createQueryBuilder('d');

        $qb
            ->select('dpt', 'dir', 'd')
            ->join('d.departement', 'dpt')
            ->join('d.direction', 'dir')
            ;
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Afficher les départements d'une direction
     *
     * @return mixed
     */

    public function findDepartementOfDir()
    {
        $qb = $this->createQueryBuilder('dpt_dir');

        $qb
            ->leftJoin('dpt_dir.direction', 'dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->select('dpt_dir.id as departementdirection_id, dpt_dir.tel, dir.id as direction_id, dpt.designation as departement_designation')
            ->orderBy('dpt_dir.id')
            ->distinct()
        ;
        return $qb->getQuery()
            ->getResult();
    }

    public function isThereAservice(){
        $qb = $this->createQueryBuilder('dptdir');

        $qb
            ->leftJoin('dptdir.serviceDepartementdirections', 'sdpt')
            ->select('dptdir.id as departement_direction_id')
            ->addSelect('sdpt.id as service_id')
            ;
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Affiche les service de département lié a leur departement et direction selon l'île choisie
     * @return mixed
     */
    public function findAllDeptByIle($idIle)
    {
        $qb = $this->createQueryBuilder('d');
        if ($idIle == 1) {
            $qb
                ->leftJoin('d.departement', 'dpt')
                ->leftJoin('d.direction', 'dir')
                ->leftJoin('d.agence', 'ag')
                ->select('d.id, dpt.id as departement_id, dpt.designation as departement_designation, d.tel, dir.designation as direction_designation, dir.tel as direction_tel, 
                ag.designation as agence_designation, ag.id as agence_id')
                ->andWhere('dir.id != 8 and dir.id != 10')
                ->orderBy('dpt.designation', 'ASC');
        } elseif ($idIle == 2) {
            $qb
                ->leftJoin('d.departement', 'dpt')
                ->leftJoin('d.direction', 'dir')
                ->leftJoin('d.agence', 'ag')
                ->select('d.id, dpt.id as departement_id, dpt.designation as departement_designation, d.tel, dir.designation as direction_designation, dir.tel as direction_tel, 
                ag.designation as agence_designation, ag.id as agence_id')
                ->andWhere('dir.id = 8')
                ->orderBy('dpt.designation', 'ASC');
        } elseif ($idIle == 3) {
            $qb
                ->leftJoin('d.departement', 'dpt')
                ->leftJoin('d.direction', 'dir')
                ->leftJoin('d.agence', 'ag')
                ->select('d.id, dpt.id as departement_id, dpt.designation as departement_designation, d.tel, dir.designation as direction_designation, dir.tel as direction_tel, 
                ag.designation as agence_designation, ag.id as agence_id')
                ->andWhere('dir.id = 10')
                ->orderBy('dpt.designation', 'ASC');
            ;
        }
        return $qb->getQuery()
            ->getResult();
    }
    /*
    public function findOneBySomeField($value): ?DepartementDirection
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
