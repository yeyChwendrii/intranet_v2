<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcFormationuser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcFormationuser|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcFormationuser|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcFormationuser[]    findAll()
 * @method EcFormationuser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcFormationuserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcFormationuser::class);
    }

    // /**
    //  * @return EcFormationuser[] Returns an array of EcFormationuser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * Affiche les formation d'un utilisateur passé en paramètre
     *
     * @param $user
     * @return mixed
     */
    public function findByUser ($user){
        return $this->createQueryBuilder('ef')
            ->where('ef.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Compter le nombre dee formations académiques par l'utilisateur passé en paramètre
     *
     * @param $user
     * @return mixed
     */
    public function countFormationsByUser($user)
    {
        $qb = $this->createQueryBuilder('ef');
        return $qb
            ->select('count(ef.id)')
            ->where('ef.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EcFormationuser
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
