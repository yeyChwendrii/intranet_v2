<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcMetier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcMetier|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcMetier|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcMetier[]    findAll()
 * @method EcMetier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcMetierRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcMetier::class);
    }

    // /**
    //  * @return EcMetier[] Returns an array of EcMetier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EcMetier
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
