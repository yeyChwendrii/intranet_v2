<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcForrmationUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcForrmationUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcForrmationUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcForrmationUser[]    findAll()
 * @method EcForrmationUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcForrmationUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcForrmationUser::class);
    }

    // /**
    //  * @return EcForrmationUser[] Returns an array of EcForrmationUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EcForrmationUser
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
