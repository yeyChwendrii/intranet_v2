<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcSousmetier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcSousmetier|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcSousmetier|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcSousmetier[]    findAll()
 * @method EcSousmetier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcSousmetierRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcSousmetier::class);
    }

    // /**
    //  * @return EcSousmetier[] Returns an array of EcSousmetier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EcSousmetier
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
