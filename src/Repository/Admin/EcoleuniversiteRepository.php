<?php

namespace App\Repository\Admin;

use App\Entity\Admin\Ecoleuniversite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ecoleuniversite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ecoleuniversite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ecoleuniversite[]    findAll()
 * @method Ecoleuniversite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcoleuniversiteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ecoleuniversite::class);
    }

    // /**
    //  * @return Ecoleuniversite[] Returns an array of Ecoleuniversite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ecoleuniversite
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
