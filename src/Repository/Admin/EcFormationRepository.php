<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcFormation[]    findAll()
 * @method EcFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcFormationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcFormation::class);
    }

    // /**
    //  * @return EcFormation[] Returns an array of EcFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EcFormation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
