<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcExperienceAilleurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcExperienceAilleurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcExperienceAilleurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcExperienceAilleurs[]    findAll()
 * @method EcExperienceAilleurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcExperienceAilleursRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcExperienceAilleurs::class);
    }

    // /**
    //  * @return EcExperienceAilleurs[] Returns an array of EcExperienceAilleurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * Affiche les formation d'un utilisateur passé en paramètre
     *
     * @param $user
     * @return mixed
     */
    public function findByUser ($user){
        $type = 'formation';
        return $this->createQueryBuilder('ee')
            ->where('ee.user = :user')
            ->andWhere('ee.type = :type')
            ->setParameter('user', $user)
            ->setParameter('type', $type)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Compter le nombre dee formations académiques par l'utilisateur passé en paramètre
     *
     * @param $user
     * @return mixed
     */
    public function countFormationsByUser($user)
    {
        $type = 'formation';
        $qb = $this->createQueryBuilder('ee');
        return $qb
            ->select('count(ee.id)')
            ->where('ee.user = :user')
            ->andWhere('ee.type = :type')
            ->setParameter('user', $user)
            ->setParameter('type', $type)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Afficher d'autres experiences AUTRES que FORMATION
     *
     * @param $user
     * @return mixed
     */
    public function findAutresExperiencesByUser ($user){
        $type = 'formation';
        return $this->createQueryBuilder('ee')
            ->where('ee.user = :user')
            ->andWhere('ee.type != :type')
            ->setParameter('user', $user)
            ->setParameter('type', $type)
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * Compter le nombre dee formations académiques par l'utilisateur passé en paramètre
     *
     * @param $user
     * @return mixed
     */
    public function countAutresExperiencesByUser($user)
    {
        $type = 'formation';
        $qb = $this->createQueryBuilder('ee');
        return $qb
            ->select('count(ee.id)')
            ->where('ee.user = :user')
            ->andWhere('ee.type != :type')
            ->setParameter('user', $user)
            ->setParameter('type', $type)
            ->getQuery()
            ->getSingleScalarResult();
    }


    /*
    public function findOneBySomeField($value): ?EcExperienceAilleurs
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
