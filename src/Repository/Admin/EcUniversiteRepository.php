<?php

namespace App\Repository\Admin;

use App\Entity\Admin\EcUniversite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EcUniversite|null find($id, $lockMode = null, $lockVersion = null)
 * @method EcUniversite|null findOneBy(array $criteria, array $orderBy = null)
 * @method EcUniversite[]    findAll()
 * @method EcUniversite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcUniversiteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EcUniversite::class);
    }

    // /**
    //  * @return EcUniversite[] Returns an array of EcUniversite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EcUniversite
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
