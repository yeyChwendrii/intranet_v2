<?php

namespace App\Repository\Admin;

use App\Entity\Admin\Niveauqualification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Niveauqualification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Niveauqualification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Niveauqualification[]    findAll()
 * @method Niveauqualification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NiveauqualificationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Niveauqualification::class);
    }

    // /**
    //  * @return Niveauqualification[] Returns an array of Niveauqualification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Niveauqualification
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
