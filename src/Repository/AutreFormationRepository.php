<?php

namespace App\Repository;

use App\Entity\AutreFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AutreFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutreFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutreFormation[]    findAll()
 * @method AutreFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutreFormationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AutreFormation::class);
    }

    // /**
    //  * @return AutreFormation[] Returns an array of AutreFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AutreFormation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
