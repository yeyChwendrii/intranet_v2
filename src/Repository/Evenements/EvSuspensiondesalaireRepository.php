<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvSuspensiondesalaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvSuspensiondesalaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvSuspensiondesalaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvSuspensiondesalaire[]    findAll()
 * @method EvSuspensiondesalaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvSuspensiondesalaireRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvSuspensiondesalaire::class);
    }

    // /**
    //  * @return EvSuspensiondesalaire[] Returns an array of EvSuspensiondesalaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvSuspensiondesalaire
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
