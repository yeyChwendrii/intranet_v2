<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvStage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvStage|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvStage|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvStage[]    findAll()
 * @method EvStage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvStageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvStage::class);
    }

    // /**
    //  * @return EvStage[] Returns an array of EvStage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->leftJoin('ev.serviceDepartement', 'svcdpt')
            ->leftJoin('ev.serviceDirection', 'svcdir')
            ->select('ev.id', 'ev.reference', 'ev.dateDebutAt', 'ev.dateFinAt', 'ev.valide', 'svcdpt.id as svcdpt_id', 'svcdir.id as svcdir_id', 'u.matricule')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    /**
     * Retrouver le dernier stage enregistré
     *
     * @param $user
     * @return mixed
     */
    public function findLastByUserField($user)
    {
        return $this->createQueryBuilder('r')
            //->select('r.poste')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('r.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findByIdUser($user_id)
    {
        $qb = $this->createQueryBuilder('es');
        return $qb
            //->select('es.id')
            ->where('es.user = :user')
            ->setParameter('user', $user_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findOneBySomeField($value): ?EvStage
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
