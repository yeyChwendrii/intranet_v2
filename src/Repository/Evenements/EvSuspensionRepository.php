<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvSuspension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvSuspension|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvSuspension|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvSuspension[]    findAll()
 * @method EvSuspension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvSuspensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvSuspension::class);
    }

    // /**
    //  * @return EvSuspension[] Returns an array of EvSuspension objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->leftJoin('ev.departement', 'dptdir')
            ->select('ev.id', 'ev.reference', 'ev.dateSuspensionAt', 'ev.valide', 'dptdir.id as dptdir_id')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    public function findByLastUserField($user)
    {
        return $this->createQueryBuilder('s')
            //->select('s.poste')
            ->andWhere('s.user = :user')
            ->setParameter('user', $user)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?EvSuspension
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
