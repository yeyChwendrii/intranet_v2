<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvRecrutement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvRecrutement|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvRecrutement|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvRecrutement[]    findAll()
 * @method EvRecrutement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvRecrutementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvRecrutement::class);
    }

    // /**
    //  * @return EvRecrutement[] Returns an array of EvRecrutement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->leftJoin('ev.serviceDepartement', 'svcdpt')
            ->leftJoin('ev.serviceDirection', 'svcdir')
            ->select('ev.id', 'ev.reference', 'ev.dateRecrutementAt', 'ev.valide', 'svcdpt.id as svcdpt_id', 'svcdir.id as svcdir_id', 'u.matricule', 'ev.decisionAnnulee')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.id as user_id','u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findByIdUser($user_id)
    {
        $qb = $this->createQueryBuilder('r');
        return $qb
            //->select('r.id')
            ->where('r.user = :user')
            ->setParameter('user', $user_id)
            ->orderBy('r.dateRecrutementAt' ,'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findLastByUserField($user)
    {
        return $this->createQueryBuilder('r')
            //->select('r.poste')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('r.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?EvRecrutement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
