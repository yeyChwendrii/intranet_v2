<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvAnnulationdedecision;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvAnnulationdedecision|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvAnnulationdedecision|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvAnnulationdedecision[]    findAll()
 * @method EvAnnulationdedecision[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvAnnulationdedecisionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvAnnulationdedecision::class);
    }

    // /**
    //  * @return EvAnnulationdedecision[] Returns an array of EvAnnulationdedecision objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByIdUser($user_id)
    {
        $qb = $this->createQueryBuilder('es');
        return $qb
            //->select('es.id')
            ->where('es.user = :user')
            ->setParameter('user', $user_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvAnnulationdedecision
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
