<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvPromotioninterne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvPromotioninterne|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvPromotioninterne|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvPromotioninterne[]    findAll()
 * @method EvPromotioninterne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvPromotioninterneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvPromotioninterne::class);
    }

    // /**
    //  * @return EvPromotioninterne[] Returns an array of EvPromotioninterne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->leftJoin('ev.serviceDepartement', 'svcdpt')
            ->leftJoin('ev.serviceDirection', 'svcdir')
            ->leftJoin('ev.departement', 'dptdir')
            ->select('ev.id', 'ev.reference', 'ev.datePromotioninterneAt', 'ev.valide', 'svcdpt.id as svcdpt_id', 'svcdir.id as svcdir_id', 'dptdir.id as dptdir_id', 'u.matricule')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function findByIdUser($user_id)
    {
        $qb = $this->createQueryBuilder('pi');
        return $qb
            //->select('pi.id')
            ->where('pi.user = :user')
            ->setParameter('user', $user_id)
            ->orderBy('pi.datePromotioninterneAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findOneBySomeField($value): ?EvPromotioninterne
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
