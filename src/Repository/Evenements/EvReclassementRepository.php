<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvReclassement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvReclassement|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvReclassement|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvReclassement[]    findAll()
 * @method EvReclassement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvReclassementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvReclassement::class);
    }

    // /**
    //  * @return EvReclassement[] Returns an array of EvReclassement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->select('ev.id', 'ev.reference', 'ev.dateReclassementAt', 'ev.valide')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvReclassement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
