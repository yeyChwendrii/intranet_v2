<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvConge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvConge|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvConge|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvConge[]    findAll()
 * @method EvConge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvCongeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvConge::class);
    }

    // /**
    //  * @return EvConge[] Returns an array of EvConge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countCongeByType($natureConge)
    {
        $qb = $this->createQueryBuilder('c');
        return $qb
            ->select('count(c.id)')
            ->where('c.natureConge = :natureConge')
            ->setParameter('natureConge', $natureConge)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvConge
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
