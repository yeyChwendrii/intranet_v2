<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvLevedesuspension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvLevedesuspension|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvLevedesuspension|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvLevedesuspension[]    findAll()
 * @method EvLevedesuspension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvLevedesuspensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvLevedesuspension::class);
    }

    // /**
    //  * @return EvLevedesuspension[] Returns an array of EvLevedesuspension objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvLevedesuspension
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
