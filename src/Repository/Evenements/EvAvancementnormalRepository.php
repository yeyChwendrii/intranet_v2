<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvAvancementnormal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvAvancementnormal|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvAvancementnormal|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvAvancementnormal[]    findAll()
 * @method EvAvancementnormal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvAvancementnormalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvAvancementnormal::class);
    }

    // /**
    //  * @return EvAvancementnormal[] Returns an array of EvAvancementnormal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvAvancementnormal
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
