<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvTitularisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvTitularisation|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvTitularisation|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvTitularisation[]    findAll()
 * @method EvTitularisation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvTitularisationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvTitularisation::class);
    }

    // /**
    //  * @return EvTitularisation[] Returns an array of EvTitularisation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->leftJoin('ev.serviceDepartement', 'svcdpt')
            ->leftJoin('ev.serviceDirection', 'svcdir')
            ->select('ev.id', 'ev.reference', 'ev.dateTitularisationAt', 'ev.valide', 'svcdpt.id as svcdpt_id', 'svcdir.id as svcdir_id', 'u.matricule', 'ev.decisionAnnulee')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countEvenementByUser($user)
    {
        $qb = $this->createQueryBuilder('et');
        return $qb
            ->select('count(et.id)')
            ->where('et.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function findByIdUser($user_id)
    {
        $qb = $this->createQueryBuilder('n');
        return $qb
            //->select('n.id')
            ->where('n.user = :user')
            ->setParameter('user', $user_id)
            ->orderBy('n.dateTitularisationAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findLastByUserField($user)
    {
        return $this->createQueryBuilder('et')
            //->select('et.poste')
            ->andWhere('et.user = :user')
            ->setParameter('user', $user)
            ->orderBy('et.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?EvTitularisation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
