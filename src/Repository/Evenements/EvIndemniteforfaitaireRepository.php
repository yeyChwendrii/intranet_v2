<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvIndemniteforfaitaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvIndemniteforfaitaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvIndemniteforfaitaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvIndemniteforfaitaire[]    findAll()
 * @method EvIndemniteforfaitaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvIndemniteforfaitaireRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvIndemniteforfaitaire::class);
    }

    // /**
    //  * @return EvIndemniteforfaitaire[] Returns an array of EvIndemniteforfaitaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvIndemniteforfaitaire
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
