<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvRenouvellementdecontrat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvRenouvellementdecontrat|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvRenouvellementdecontrat|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvRenouvellementdecontrat[]    findAll()
 * @method EvRenouvellementdecontrat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvRenouvellementdecontratRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvRenouvellementdecontrat::class);
    }

    // /**
    //  * @return EvRenouvellementdecontrat[] Returns an array of EvRenouvellementdecontrat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findSomeFields(){
        return $this->createQueryBuilder('ev')
            ->leftJoin('ev.serviceDepartement', 'svcdpt')
            ->leftJoin('ev.serviceDirection', 'svcdir')
            ->select('ev.id', 'ev.reference', 'ev.dateRenouvellementdecontratAt', 'ev.valide', 'svcdpt.id as svcdpt_id', 'svcdir.id as svcdir_id', 'u.matricule')
            ->leftJoin('ev.user', 'u')
            ->addSelect('u.matricule', 'u.nom', 'u.prenom')
            ->getQuery()
            ->getResult();
    }

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvRenouvellementdecontrat
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
