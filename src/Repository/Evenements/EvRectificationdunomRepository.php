<?php

namespace App\Repository\Evenements;

use App\Entity\Evenements\EvRectificationdunom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvRectificationdunom|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvRectificationdunom|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvRectificationdunom[]    findAll()
 * @method EvRectificationdunom[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvRectificationdunomRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvRectificationdunom::class);
    }

    // /**
    //  * @return EvRectificationdunom[] Returns an array of EvRectificationdunom objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function countEvenement()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countNotValideEvent()
    {
        $qb = $this->createQueryBuilder('ce');
        return $qb
            ->select('count(ce.id)')
            ->where('ce.valide = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?EvRectificationdunom
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
