<?php

namespace App\Repository;

use App\Entity\DepartementDirection;
use App\Entity\PosteUser;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\ServiceDepartementdirection;
use App\Entity\User;
use App\Entity\UserSearch;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getActive()
    {
        // Comme vous le voyez, le délais est redondant ici, l'idéale serait de le rendre configurable via votre bundle
        $delay = new \DateTime();
        $delay->setTimestamp(strtotime('2 minutes ago'));

        $qb = $this->createQueryBuilder('u')
            ->where('u.lastActivityAt > :delay')
            ->setParameter('delay', $delay)
        ;

        return $qb->getQuery()->getResult();
    }

    public function findByUsernameAndMail($username, $mail)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->andWhere('u.mail = :mail')
            ->setParameter('mail', $mail);

        return $qb->getQuery()->getResult();
    }

    public function findByEnabled($enabled)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.enabled = :enabled')
            ->setParameter('enabled', $enabled);

        return $qb;
    }

    /**
     * ================================================
     *             personnel (gestion congé)
     * ================================================
     */
    public function showDernierConge()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('u.demandeConges', 'dc')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, dc.dateDebutAt, dc.dateFinAt, dc.nombreJour, dc.id as demande_conge_id')
            ->andWhere('u.enfonction = 1')
            ->orderBy('dc.dateFinAt', 'DESC')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

    public function showSituationConge()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.departementdirection', 'sdpt_dir')
            ->leftJoin('sdpt_dir.direction', 'sdptdir')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.direction', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.direction', 'dptdir')
            ->leftJoin('u.demandeConges', 'dc')
            ->leftJoin('dc.repriseService', 'rs')
            ->leftJoin('dc.modeleConge', 'mc')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, dc.dateDebutAt, dc.dateFinAt, dc.nombreJour, 
            dc.annee, mc.titre as titre_conge, rs.id as reprise_service_id, dc.id as demande_conge_id, sdptdir.designation as sdptdir_designation, 
            dptdir.designation as dptdir_designation, sdir.designation as sdir_designation, 
            dir.designation as dir_designation')
            ->orderBy('dc.dateFinAt', 'DESC')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

    public function findAgentEnFonction($user)
    {
        $qb = $this->createQueryBuilder('u');

        return $qb
            ->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
            u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
            u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
            opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
            opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
            sdpt.designation as sdpt_designation, s_dptdir.tel as s_dptdir_tel, sdir.designation as sdir_designation, 
            s_dir.tel as s_dir_tel, dpt.designation as dpt_designation, dpt_dir.tel as dpt_dir_tel, 
            dir.designation as dir_designation, dir.tel as dir_tel')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('u.statut', 'st')
            ->leftJoin('u.categorie', 'cat')
            ->leftJoin('u.userOptionVisibilite', 'opt_v')
            ->andWhere('u.enfonction = 1')
            ->andWhere('u.id = :id')
            ->setParameter('id', $user)
            ->getQuery()
            ->getResult()
            ;
    }
    /*================  fin personnel  ================*/

    public function findAllAgentsEnFonction()
    {
        $qb = $this->createQueryBuilder('u');

        return $qb
            ->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
            u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
            u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
            opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
            opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
            sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('u.statut', 'st')
            ->leftJoin('u.categorie', 'cat')
            ->leftJoin('u.userOptionVisibilite', 'opt_v')
            ->andWhere('u.enfonction = 1')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult()
            ;
    }

    public function whereConnectedThisMonth(QueryBuilder $queryBuilder)
    {
        $queryBuilder->andWhere('u.lastActivityAt BETWEEN :debut AND :fin')
                    ->setParameter('debut', new \DateTime(date('Y-m').'-01 00:00:00'))
                    ->setParameter('fin', new \DateTime(date('Y-m').'-31 23:59:59'))
        ;
        return $queryBuilder;
    }

    public function findWhoIsEnabledAndLoginThisMonth()
    {
        $qb = $this->findByEnabled(1);
        $qb = $this->whereConnectedThisMonth($qb);
        $qb->orderBy('u.username', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Personnal Query
     */
    public function countAgentsNgz()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(DISTINCT u.id)')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('dpt_dir.direction', 'd') ///
            ->leftJoin('s_dptdir.departementdirection', 'dd') ///
            ->leftJoin('dd.direction', 'dpt_d') ///
            ->leftJoin('s_dir.direction', 's_d') ///
            ->andWhere('(d.id != 8 and d.id != 10) or (dir.id != 8 and dir.id != 10) or (dpt_d.id != 8 and dpt_d.id != 10) or (s_d.id != 8 and s_d.id != 10)')
            ->andWhere('u.enfonction = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countAgentsMwa()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('dpt_dir.direction', 'd') ///
            ->leftJoin('s_dptdir.departementdirection', 'dd') ///
            ->leftJoin('dd.direction', 'dpt_d') ///
            ->leftJoin('s_dir.direction', 's_d') ///
            ->andWhere('(d.id = 8) or (dir.id = 8) or (dpt_d.id = 8) or (s_d.id = 8)')
            ->andWhere('u.enfonction = 1')
            ->select('count(DISTINCT u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countAgentsNdz()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('dpt_dir.direction', 'd') ///
            ->leftJoin('s_dptdir.departementdirection', 'dd') ///
            ->leftJoin('dd.direction', 'dpt_d') ///
            ->leftJoin('s_dir.direction', 's_d') ///
            ->andWhere('dir.id = 10 or d.id = 10 or dpt_d.id = 10 or s_d.id = 10')
            ->andWhere('u.enfonction = 1')
            ->select('count(DISTINCT u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function selectMatriculeNomPrenom()
    {
        $qb = $this->createQueryBuilder('i');
        return $qb
            ->select('i.matricule, i.nom, i.prenom')
            ->getQuery()
            ->getResult();
    }

    public function countAgents()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->andWhere('u.enfonction = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countAgentsHorsfonction()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->andWhere('u.enfonction = 0')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countFemmes()
    {
        $sexe = 'F';
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->where('u.sexe = :sexe')
            ->andWhere('u.enfonction = 1')
            ->setParameter('sexe', $sexe)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countHommes()
    {
        $sexe = 'M';
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->where('u.sexe = :sexe')
            ->andWhere('u.enfonction = 1')
            ->setParameter('sexe', $sexe)
            ->getQuery()
            ->getSingleScalarResult();
    }

    // On compte le nombre des cadres
    public function countCadre($categorie)
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->where('u.categorie = :categorie')
            ->andWhere('u.enfonction = 1')
            ->setParameter('categorie', $categorie)
            ->getQuery()->getSingleScalarResult();
    }

    // On compte le nombre des statuts
    public function countStatut($statut)
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->where('u.statut = :statut')
            ->andWhere('u.enfonction = 1')
            ->setParameter('statut', $statut)
            ->getQuery()->getSingleScalarResult();
    }

    // Afficher les utilisateurs par ordre des matricules
    // Rechercher un utilisateur soit par son nom/prénom/tel/e-mail ou soit par matricule
    /**
     * @return Query
     */
    public function findAllOrderBy(UserSearch $search) : Query
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.matricule', 'ASC');
        if ($search->getMatricule()){
            $query = $query
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery();
    }

    // Afficher les utilisateurs par ordre des matricules
    // Rechercher un utilisateur soit par son nom/prénom/tel/e-mail ou soit par matricule
    /**
     * @return Query
     */
    public function findAllExplicitEventOrderBy(UserSearch $search) : Query
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb;
        if ($search->getMatricule()){
            $query = $query
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                //->leftJoin('u.posteUsers', 'pu')
                //->orderBy('pu.dateEvenementAt', 'DESC')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery();
    }

    // Afficher les utilisateurs par ordre des matricules
    // Rechercher un utilisateur soit par son nom/prénom/tel/e-mail ou soit par matricule
    /**
     * @return Query
     */
    public function findAllSuspendusOrderBy(UserSearch $search) : Query
    {
        $enfonction = 0;
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->Where('u.enfonction = :enfonction')
            ->orderBy('u.matricule', 'ASC')
            ->setParameter('enfonction', $enfonction);
        if ($search->getMatricule()){
            $query = $query
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery();
    }

    // Afficher les utilisateurs par leur sexe
    /**
     * @return Query
     */
    /*public function findAllExplicitEventOrderBy(UserSearch $search) : Query
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb;
        if ($search->getMatricule()){
            $query = $query
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                //->leftJoin('u.posteUsers', 'pu')
                //->orderBy('pu.dateEvenementAt', 'DESC')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery();
    }*/

    // Afficher les utilisateurs par ordre des matricules
    // Rechercher un utilisateur soit par son nom/prénom/tel/e-mail ou soit par matricule
    /**
     * @return Query
     */
   /* public function findAllSuspendusOrderBy(UserSearch $search) : Query
    {
        $enfonction = 0;
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->Where('u.enfonction = :enfonction')
            ->orderBy('u.matricule', 'ASC')
            ->setParameter('enfonction', $enfonction);
        if ($search->getMatricule()){
            $query = $query
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery();
    }*/

    // Afficher les utilisateurs par leur sexe
    public function findAllBySexe($sexe)
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
            u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
            u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
            opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
            opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
            sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('u.statut', 'st')
            ->leftJoin('u.categorie', 'cat')
            ->leftJoin('u.userOptionVisibilite', 'opt_v')
            ->andWhere('u.sexe = :sexe')
            ->setParameter('sexe', $sexe)
            ->andWhere('u.enfonction = 1')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult()
        ;
    }

    // Afficher les utilisateurs par leur catégorie professionnel
    public function findAllByCategorie($categorie)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
            u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
            u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
            opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
            opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
            sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('u.statut', 'st')
            ->leftJoin('u.categorie', 'cat')
            ->leftJoin('u.userOptionVisibilite', 'opt_v')
            ->andWhere('u.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->andWhere('u.enfonction = 1')
            ->groupBy('u.id')
        ;

        return $query->getQuery()->getResult();
    }

    // Afficher les utilisateurs par leur statut
    public function findAllByStatut($statut)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
            u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
            u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
            opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
            opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
            sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('u.statut', 'st')
            ->leftJoin('u.categorie', 'cat')
            ->leftJoin('u.userOptionVisibilite', 'opt_v')
            ->andWhere('u.statut = :statut')
            ->setParameter('statut', $statut)
            ->andWhere('u.enfonction = 1')
            ->groupBy('u.id')
            ;

        return $query->getQuery()->getResult();
    }

    // Afficher les agents d'une île
    public function findAgentByIle($ile)
    {
        $qb = $this->createQueryBuilder('u');
        if ($ile == 'Ngazidja') {
            $qb->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
                u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
                u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
                opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
                opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
                sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
                ->leftJoin('u.posteUsers', 'p_u1')
                ->leftJoin('p_u1.poste', 'p')
                ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
                ->where('p_u2.user IS NULL')
                ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
                ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
                ->leftJoin('s_dptdir.service', 'sdpt')
                ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
                ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
                ->leftJoin('dpt_dir.departement', 'dpt')
                ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
                ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
                ->leftJoin('s_dir.service', 'sdir')
                ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
                ->leftJoin('p_u_dir.direction', 'dir')
                ->leftJoin('dpt_dir.direction', 'd') ///
                ->leftJoin('s_dptdir.departementdirection', 'dd') ///
                ->leftJoin('dd.direction', 'dpt_d') ///
                ->leftJoin('s_dir.direction', 's_d') ///
                ->leftJoin('u.statut', 'st')
                ->leftJoin('u.categorie', 'cat')
                ->leftJoin('u.userOptionVisibilite', 'opt_v')
                ->andWhere('(d.id != 8 and d.id != 10) or (dir.id != 8 and dir.id != 10) or (dpt_d.id != 8 and dpt_d.id != 10) or (s_d.id != 8 and s_d.id != 10)')
                ->andWhere('u.enfonction = 1')
                ->groupBy('u.id')
            ;
        } elseif ($ile == 'Mwali') {
            $qb->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
                u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
                u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
                opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
                opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
                sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
                ->leftJoin('u.posteUsers', 'p_u1')
                ->leftJoin('p_u1.poste', 'p')
                ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
                ->where('p_u2.user IS NULL')
                ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
                ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
                ->leftJoin('s_dptdir.service', 'sdpt')
                ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
                ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
                ->leftJoin('dpt_dir.departement', 'dpt')
                ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
                ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
                ->leftJoin('s_dir.service', 'sdir')
                ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
                ->leftJoin('p_u_dir.direction', 'dir')
                ->leftJoin('dpt_dir.direction', 'd') ///
                ->leftJoin('s_dptdir.departementdirection', 'dd') ///
                ->leftJoin('dd.direction', 'dpt_d') ///
                ->leftJoin('s_dir.direction', 's_d') ///
                ->leftJoin('u.statut', 'st')
                ->leftJoin('u.categorie', 'cat')
                ->leftJoin('u.userOptionVisibilite', 'opt_v')
                ->andWhere('(d.id = 8) or (dir.id = 8) or (dpt_d.id = 8) or (s_d.id = 8)')
                ->andWhere('u.enfonction = 1')
                ->groupBy('u.id')
            ;
        } elseif ($ile == 'Ndzuani') {
            $qb->select('u.id, u.username, u.email, u.lastActivityAt, u.matricule, u.nom, u.prenom, u.adresse, u.dateNaissanceAt, u.tel, u.indiceGroupe,
                u.indiceCategorie, u.indiceEchelon, u.image, p.designation as poste_designation, st.designation as statut, cat.designation as categorie, 
                u.enligne, opt_v.optEmail as optEmail, opt_v.optAdresse as optAdresse, opt_v.optDateNaissanceAt as optDateNaissanceAt, 
                opt_v.optIndiceGroupe as optIndiceGroupe, opt_v.optIndiceCategorieNiveau as optIndiceCategorieNiveau, 
                opt_v.optIndiceEchelon as optIndiceEchelon, opt_v.optEnligne as optEnligne, u.enfonction, 
                sdpt.designation as sdpt_designation, sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
                ->leftJoin('u.posteUsers', 'p_u1')
                ->leftJoin('p_u1.poste', 'p')
                ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
                ->where('p_u2.user IS NULL')
                ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
                ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
                ->leftJoin('s_dptdir.service', 'sdpt')
                ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
                ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
                ->leftJoin('dpt_dir.departement', 'dpt')
                ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
                ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
                ->leftJoin('s_dir.service', 'sdir')
                ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
                ->leftJoin('p_u_dir.direction', 'dir')
                ->leftJoin('dpt_dir.direction', 'd') ///
                ->leftJoin('s_dptdir.departementdirection', 'dd') ///
                ->leftJoin('dd.direction', 'dpt_d') ///
                ->leftJoin('s_dir.direction', 's_d') ///
                ->leftJoin('u.statut', 'st')
                ->leftJoin('u.categorie', 'cat')
                ->leftJoin('u.userOptionVisibilite', 'opt_v')
                ->andWhere('dir.id = 10 or d.id = 10 or dpt_d.id = 10 or s_d.id = 10')
                ->andWhere('u.enfonction = 1')
                ->groupBy('u.id');
        }
        return $qb->getQuery()->getResult();
    }

    // Afficher les agents d'une île selon l'île choisie
    // Rechercher les agents d'une île
    /*
     * @return Query
     *
    public function findAgentByIle(Ville $search)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.matricule', 'ASC');
        if ($search->getIle() == 'Ngazidja'){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('(d.id != 8 and d.id != 10) or (dir.id != 8 and dir.id != 10)')
            ;
        } elseif ($search->getIle() == 'Ndzuani'){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('dir.id = 10 or d.id = 10')
            ;
        } elseif ($search->getIle() == 'Mwali'){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('dir.id = 8 or d.id = 8')
            ;
        } else {
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('dir.id = 11 or d.id = 11')
            ;
        }
        return $query->getQuery()->getResult();
    }*/

    // On affiche les agents d'une île
    public function findByIle($ile)
    {
        $qb = $this->createQueryBuilder('u');
        if ($ile == 'Ngazidja') {
            $qb->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('(d.id != 8 and d.id != 10) or (dir.id != 8 and dir.id != 10)')
            ;
        } elseif ($ile == 'Mwali') {
            $qb->leftJoin('u.posteUsers', 'pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->where('(d.id = 8) or (dir.id = 8)')
            ;
        } elseif ($ile == 'Ndzuani') {
            $qb->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('(d.id = 10) or (dir.id = 10)')
            ;
        } else {
            $qb->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('(d.id = 11) or (dir.id = 11)')
            ;
        }
        return $qb->getQuery()->getResult();
    }

    // Afficher les agents d'un service selon le service choisi
    // Rechercher les agents d'un service
    /**
     * @return Query
     */
    public function findAgentsByServ(ServiceDepartementdirection $search)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.matricule', 'ASC');
        if ($search->getService()){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('pu.posteuserServicedirections', 'pusd')
                ->addSelect('pusd')
                ->leftJoin('pusd.servicedirection', 'sd')
                ->addSelect('sd')
                ->andWhere('sdd.service = :search or sd.service = :search')
                ->setParameter('search', $search->getService());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher les agents d'un département selon le département choisi
    // Rechercher les agents d'un département
    /**
     * @return Query
     */
    public function findAgentsByDept(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.matricule', 'ASC');
        if ($search->getDepartement()){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.departement = :search')
                ->setParameter('search', $search->getDepartement());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher les agents d'une direction selon la direction choisie
    // Rechercher les agents d'une direction
    /**
     * @return Query
     */
    public function findAgentsByDir(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.matricule', 'ASC');
        if ($search->getDirection()){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.direction = :search')
                ->setParameter('search', $search->getDirection());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher les agents d'un poste selon le poste choisi
    // Rechercher les agents d'un poste quelconque
    /**
     * @return Query
     */
    public function findAgentsByPoste(PosteUser $search)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.id', 'DESC');
        if ($search->getPoste()){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->andWhere('pu.poste = :search')
                ->setParameter('search', $search->getPoste())
//                ->select('MAX(pu.id)')
                ->groupBy('pu.user')
            ;
        }

        return $query->getQuery()->getResult();
    }

    public function findAgentsByPosteServDeptDir(PosteuserServicedepartementdirection $searchPoste)
    {
        $qb = $this->createQueryBuilder('u');
        $query = $qb
            ->orderBy('u.id', 'DESC');
        if ($searchPoste->getPosteuser()->getPoste()){
            $query = $query
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDepartementdirections', 'pudd')
                ->addSelect('pudd')
                ->leftJoin('pu.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.servicedepartementdirection', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->andWhere('pu.poste = :search')
                ->setParameter('search', $searchPoste->getPosteuser()->getPoste())
                ->groupBy('pu.user')
            ;
        }

        return $query->getQuery()->getResult();
    }

    // On compte le nombre d'agent d'une île
    /*public function countAgentsIle($ile)
    {
        $qb = $this->createQueryBuilder('u');
        if ($ile == 1) {
            $qb->select('count(u.id)')
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->where('d.id != 8 and d.id != 10')
            ;
        } elseif ($ile == 2) {
            $qb->select('count(u.id)')
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->where('d.id = 8')
            ;
        } elseif ($ile == 3) {
            $qb->select('count(u.id)')
                ->leftJoin('u.posteUsers', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.direction', 'd')
                ->addSelect('d')
                ->andWhere('d.id = 10')
            ;
        }
        return $qb->getQuery()->getSingleScalarResult();
    }*/

//    /**
//     * @return User[] Returns an array of User objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /*
     * ========================================================================================
     * MEDEL FILTRE
     * ========================================================================================
     */

    /**
     * Affiche tous les agents avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAllWithSomeFields()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
            ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
            ->leftJoin('s_dptdir.service', 'sdpt')
            ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
            ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
            ->leftJoin('dpt_dir.departement', 'dpt')
            ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
            ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
            ->leftJoin('s_dir.service', 'sdir')
            ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
            ->leftJoin('p_u_dir.direction', 'dir')
            ->leftJoin('p_u1.poste', 'p')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation, sdpt.designation as sdpt_designation, 
            sdir.designation as sdir_designation, dpt.designation as dpt_designation, dir.designation as dir_designation')
            ->orderBy('u.nom', 'ASC')
            ->distinct()
            ->groupBy('u.id')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Affiche les agents en service (enfonction) avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAllEnfonction(){
        return $this->createQueryBuilder('u')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.poste', 'p')
            ->andWhere('u.enfonction = 1')
            ->orderBy('u.matricule', 'ASC')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche les agents qui ont un compte actif
     * @return mixed
     */
    public function showAllEnfonctionWithActivAccount()
    {
        $enfonction = 1;
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->andWhere('u.enabled = 1')
            ->leftJoin('p_u1.poste', 'p')
            //->select('u.id, u.nom, u.prenom, u.matricule')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.matricule', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function showGcarAdmin($roles)
    {
        $enfonction = 1;

        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->andWhere('u.enabled = 1')
            ->leftJoin('p_u1.poste', 'p')
            //->select('u.id, u.nom, u.prenom, u.matricule')
            ->andWhere('u.enfonction = :enfonction')
            ->andwhere('u.roles LIKE :roles')
            ->orWhere('u.roles LIKE :roles')
            ->setParameter('enfonction', $enfonction)
            ->setParameter('roles', '%"'.$roles.'"%')
            ->orderBy('u.matricule', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche tous les agents hors-service (not in service) avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAllNotEnfonction(){
        $enfonction =  0;
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.poste', 'p')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }


    /**
     * Affiche tous les agents par poste avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAllByResponsabilite($poste_id){
        $enfonction =  1;
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.poste', 'p')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation, p.id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->andWhere('p.id = :id')
            ->setParameter('id', $poste_id)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    /**
     * Affiche tous les agents par statut (statigaire, contractuel, titulaire...) avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAllByStatut($statut_id){
        $enfonction =  1;
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('u.statut', 's')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation, s.id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->andWhere('s.id = :id')
            ->setParameter('id', $statut_id)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    //showAgentByCategorie
    /**
     * Affiche tous les agents par catégogie (Cadre dirigeant, cadre supérieur, cadre de gestion...) avec les champs : (id, matricule, nom, prenom et enfonction) de l'agent
     * @return mixed
     */
    public function showAgentByCategorie($categorie){
        $enfonction =  1;
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.poste', 'p')
            ->leftJoin('u.categorie', 'c')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, p_u1.nomEvenement, p.designation as poste_designation, c.id')
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->andWhere('u.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->orderBy('u.id', 'DESC')
            ->distinct()
            ->getQuery()
            ->getResult();
    }


    /**
     * ================================================
     *                  INTERVENTION
     * ================================================
     */
    /**
     * Affiche les agent du département ENERGIE pour le formtype interventionType
     *
     * @return mixed
     */
    public function selectAgentofDptenergie(){
        $dpt = 24;
        $enfonction =  1;
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dpt')
            ->leftJoin('p_u_s_dpt.servicedepartementdirection', 's_dpt')
            ->andWhere('s_dpt.departementdirection = :departementdirection')
            ->setParameter('departementdirection', $dpt)
            ->andWhere('u.enfonction = :enfonction')
            ->setParameter('enfonction', $enfonction)
            ->orderBy('u.id', 'DESC')
            ->getQuery()
            ->getResult();
        }
    /**
     * ================================================
     *                  affaires sociales
     * ================================================
     */
    public function showSituationFamiliale(){
        return $this->createQueryBuilder('u')
            ->leftJoin('u.posteUsers', 'p_u1')
            ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
            ->where('p_u2.user IS NULL')
            ->leftJoin('u.situationFamiliale', 'sf')
            ->leftJoin('u.evRecrutements', 'recrut')
            ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, sf.situation, sf.id as situationFamiliale_id, recrut.dateRecrutementAt, recrut.id as recrutement_id')
            ->andWhere('u.enfonction = 1')
            ->orderBy('u.id', 'DESC')
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }
}
