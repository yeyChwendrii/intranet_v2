<?php

namespace App\Repository;

use App\Entity\UserOptionVisibilite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserOptionVisibilite|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserOptionVisibilite|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserOptionVisibilite[]    findAll()
 * @method UserOptionVisibilite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserOptionVisibiliteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserOptionVisibilite::class);
    }

    // /**
    //  * @return UserOptionVisibilite[] Returns an array of UserOptionVisibilite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserOptionVisibilite
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
