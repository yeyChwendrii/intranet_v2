<?php

namespace App\Repository;

use App\Entity\ServiceDirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ServiceDirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceDirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceDirection[]    findAll()
 * @method ServiceDirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceDirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ServiceDirection::class);
    }

    public function findByIdService($service_id)
    {
        $qb = $this->createQueryBuilder('s');
        return $qb
            //->select('p.id')
            ->where('s.service = :service')
            ->setParameter('service', $service_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Affiche les service de direction lié a leur et direction
     * @return mixed
     */
    public function findSomeFields()
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->leftJoin('s.service', 'serv')
            ->leftJoin('s.direction', 'dir')
            ->leftJoin('s.agence', 'ag')
            ->select('s.id, serv.id as service_id,serv.designation as serv_designation, s.tel, s.fonctionnel, ag.designation as ag_designation, ag.id as agence_id, dir.id as direction_id, dir.designation as dir_designation')
            ->orderBy('serv.designation', 'DESC')

        ;
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Affiche les services d'une direction d'une île
     * @return mixed
     */
    public function findAllServicesDirByIle($idIle)
    {
        $fonctionnel = 1;
        $qb = $this->createQueryBuilder('s');
        if ($idIle == 1) {
            $qb
                ->leftJoin('s.service', 'serv')
                ->leftJoin('s.direction', 'dir')
                ->leftJoin('s.agence', 'ag')
                ->select('s.id, serv.id as service_id, serv.designation as serv_designation, s.tel, s.fonctionnel, ag.designation as ag_designation, ag.id as agence_id, dir.id as direction_id, dir.designation as dir_designation, dir.tel as tel_dir')
                ->andWhere('s.fonctionnel = :fonctionnel')
                ->andWhere('dir.id != 8 and dir.id != 10')
                ->setParameter('fonctionnel', $fonctionnel)
                ->orderBy('serv.designation', 'DESC')
            ;
        } elseif ($idIle == 2) {
            $qb
                ->leftJoin('s.service', 'serv')
                ->leftJoin('s.direction', 'dir')
                ->leftJoin('s.agence', 'ag')
                ->select('s.id, serv.id as service_id,serv.designation as serv_designation, s.tel, s.fonctionnel, ag.designation as ag_designation, ag.id as agence_id, dir.id as direction_id, dir.designation as dir_designation, dir.tel as tel_dir')
                ->andWhere('s.fonctionnel = :fonctionnel')
                ->andWhere('dir.id = 8')
                ->setParameter('fonctionnel', $fonctionnel)
                ->orderBy('serv.designation', 'DESC')
            ;
        } elseif ($idIle == 3) {
            $qb
                ->leftJoin('s.service', 'serv')
                ->leftJoin('s.direction', 'dir')
                ->leftJoin('s.agence', 'ag')
                ->select('s.id, serv.id as service_id,serv.designation as serv_designation, s.tel, s.fonctionnel, ag.designation as ag_designation, ag.id as agence_id, dir.id as direction_id, dir.designation as dir_designation, dir.tel as tel_dir')
                ->andWhere('s.fonctionnel = :fonctionnel')
                ->andWhere('dir.id = 10')
                ->setParameter('fonctionnel', $fonctionnel)
                ->orderBy('serv.designation', 'DESC')
            ;
        }

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Afficher les agents par service de direction donné
     *
     * @return mixed
     */
    /*
    public function findAgentOfService()
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->leftJoin('s.direction', 'sdir')
            ->leftJoin('s.agence', 'ag')
            ->leftJoin('s.posteuserServicedirections', 'p_u_serdir')
            ->leftJoin('p_u_serdir.posteuser', 'p_u')
            ->leftJoin('p_u.user', 'u')
            ->leftJoin('p_u.poste', 'p')
            ->select('s.id, u.id as u_id, u.matricule, u.nom, u.prenom, p.designation as poste_designation')
            ->distinct()
        ;
        return $qb->getQuery()
            ->getResult();
    }
    */

    // /**
    //  * @return ServiceDirection[] Returns an array of ServiceDirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    /*
    public function findOneBySomeField($value): ?ServiceDirection
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
