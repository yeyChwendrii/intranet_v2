<?php

namespace App\Repository;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\UserSearch;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Direction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Direction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Direction[]    findAll()
 * @method Direction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Direction::class);
    }

    // Afficher la direction d'un service selon le service choisi
    // Rechercher la direction d'un service
    /**
     * @return Query
     */
    public function findServDeptDirByServ(ServiceDepartementdirection $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getService()){
            $query = $query
                ->leftJoin('d.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('dd.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->andWhere('sdd.service = :search or sd.service = :search')
                ->setParameter('search', $search->getService());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher la direction d'un département selon le département choisi
    // Rechercher la direction d'un département
    /**
     * @return Query
     */
    public function findDeptDirByDept(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getDepartement()){
            $query = $query
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.departement = :search')
                ->setParameter('search', $search->getDepartement());
        }
        return $query->getQuery()->getResult();
    }

    // Afficher la direction d'une agence selon l'agence choisie
    // Rechercher la direction d'une agence
    /**
     * @return Query
     */
    public function findServDirByAgence(ServiceDirection $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getAgence()){
            $query = $query
                ->leftJoin('d.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('d.departementDirections', 'dd')
                ->addSelect('dd')
                ->andWhere('sd.agence = :search or dd.agence = :search')
                ->setParameter('search', $search->getAgence());
        }
        return $query->getQuery()->getResult();
    }

    // Afficher les directions d'une île selon l'île choisie
    // Rechercher les directions d'une île
    /**
     * @return Query
     */
    public function findDirByIle(Ville $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getIle() == 'Ngazidja'){
            $query = $query
                ->andWhere('d.id != 8 and d.id != 10')
            ;
        } elseif ($search->getIle() == 'Ndzuani'){
            $query = $query
                ->andWhere('d.id = 10')
            ;
        } elseif ($search->getIle() == 'Mwali'){
            $query = $query
                ->andWhere('d.id = 8')
            ;
        }

        return $query->getQuery()->getResult();
    }

    public function findDeptByNgz()
    {
        $qb = $this->createQueryBuilder('d')
            ->where('d.id != 8 and d.id != 10')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findDeptByNdz()
    {
        $qb = $this->createQueryBuilder('d')
            ->where('d.id = 10')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findDeptByMwa()
    {
        $qb = $this->createQueryBuilder('d')
            ->where('d.id = 8')
        ;
        return $qb->getQuery()->getResult();
    }

    // On compte le nombre de direction d'une île
    public function countDir($ile)
    {
        $qb = $this->createQueryBuilder('d');
        if ($ile == 1) {
            $qb->select('count(d.id)')
                ->where('d.id != 8 and d.id != 10')
            ;
        } elseif ($ile == 2) {
            $qb->select('count(d.id)')
                ->where('d.id = 8')
            ;
        } elseif ($ile == 3) {
            $qb->select('count(d.id)')
                ->andWhere('d.id = 10')
            ;
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    // Afficher la direction selon l'agent choisie
    // Rechercher la direction d'un agent
    /**
     * @return Query
     */
    public function findDirByAgent(UserSearch $search)
    {
        $qb = $this->createQueryBuilder('d');
        $query = $qb
            ->orderBy('d.designation', 'ASC');
        if ($search->getMatricule()){
            $query = $query
                ->leftJoin('d.posteuserDirections', 'pud')
                ->addSelect('pud')
                ->leftJoin('pud.posteuser', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.user', 'u')
                ->addSelect('u')
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery()->getResult();
    }


    /**
     * Affiche les service de département lié a leur departement et direction
     * @return mixed
     */
    public function findSomeFields()
    {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->leftJoin('d.agence', 'ag')
            ->select('d.id, d.designation, d.tel, ag.designation as agence_designation, ag.id as agence_id')
            ->orderBy('d.designation', 'ASC')
        ;
        return $qb->getQuery()
            ->getResult();
    }

    /**
     * Affiche les directions d'une île
     * @return mixed
     */
    public function findAllDirByIle($idIle)
    {
        $qb = $this->createQueryBuilder('d');
        if ($idIle == 1) {
            $qb
                ->leftJoin('d.agence', 'ag')
                ->select('d.id, d.designation, d.tel, ag.designation as agence_designation, ag.id as agence_id')
                ->andWhere('d.id != 8 and d.id != 10')
                ->orderBy('d.designation', 'ASC');
        } elseif ($idIle == 2) {
            $qb
                ->leftJoin('d.agence', 'ag')
                ->select('d.id, d.designation, d.tel, ag.designation as agence_designation, ag.id as agence_id')
                ->andWhere('d.id = 8')
                ->orderBy('d.designation', 'ASC');
        } elseif ($idIle == 3) {
            $qb
                ->leftJoin('d.agence', 'ag')
                ->select('d.id, d.designation, d.tel, ag.designation as agence_designation, ag.id as agence_id')
                ->andWhere('d.id = 10')
                ->orderBy('d.designation', 'ASC');
        }
        return $qb->getQuery()
            ->getResult();
    }



    /*
     * ====================================================
     *      yeyChwendrii
     * ====================================================
     */
    /**
     * Affiche les directions sans la direction d'Anjouan et de Mohéli
     * @return mixed
     */
    public function countProjetsNgz()
    {
        $qb = $this->createQueryBuilder('d');
        //$id1 = 8;
        //$id2 = 10;
        return $qb
            ->select('count(d.id)')
            ->andWhere('d.id != 8 and d.id != 10')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Affiche les directions de ngazidja
     * @return mixed
     */
    public function countDirNgz()
    {
        $qb = $this->createQueryBuilder('d');
        //$id1 = 8;
        //$id2 = 10;
        return $qb
            ->select('count(d.id)')
            ->andWhere('d.id != 8 and d.id != 10')
            ->getQuery()
            ->getSingleScalarResult();
    }
    // /**
    //  * @return Direction[] Returns an array of Direction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Direction
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
