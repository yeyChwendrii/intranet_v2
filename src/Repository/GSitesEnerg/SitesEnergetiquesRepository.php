<?php

namespace App\Repository\GSitesEnerg;

use App\Entity\GSitesEnerg\SitesEnergetiques;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SitesEnergetiques|null find($id, $lockMode = null, $lockVersion = null)
 * @method SitesEnergetiques|null findOneBy(array $criteria, array $orderBy = null)
 * @method SitesEnergetiques[]    findAll()
 * @method SitesEnergetiques[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SitesEnergetiquesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SitesEnergetiques::class);
    }

    // /**
    //  * @return SitesEnergetiques[] Returns an array of SitesEnergetiques objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function countSites()
    {
        $qb = $this->createQueryBuilder('se');
        return $qb
            ->select('count(se.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?SitesEnergetiques
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
