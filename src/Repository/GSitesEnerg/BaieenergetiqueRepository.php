<?php

namespace App\Repository\GSitesEnerg;

use App\Entity\GSitesEnerg\Baieenergetique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Baieenergetique|null find($id, $lockMode = null, $lockVersion = null)
 * @method Baieenergetique|null findOneBy(array $criteria, array $orderBy = null)
 * @method Baieenergetique[]    findAll()
 * @method Baieenergetique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaieenergetiqueRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Baieenergetique::class);
    }

    // /**
    //  * @return Baieenergetique[] Returns an array of Baieenergetique objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Baieenergetique
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
