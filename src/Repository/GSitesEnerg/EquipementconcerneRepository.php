<?php

namespace App\Repository\GSitesEnerg;

use App\Entity\GSitesEnerg\Equipementconcerne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Equipementconcerne|null find($id, $lockMode = null, $lockVersion = null)
 * @method Equipementconcerne|null findOneBy(array $criteria, array $orderBy = null)
 * @method Equipementconcerne[]    findAll()
 * @method Equipementconcerne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipementconcerneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Equipementconcerne::class);
    }

    // /**
    //  * @return Equipementconcerne[] Returns an array of Equipementconcerne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Equipementconcerne
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
