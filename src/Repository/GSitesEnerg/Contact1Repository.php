<?php

namespace App\Repository\GSitesEnerg;

use App\Entity\GSitesEnerg\Contact1;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Contact1|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact1|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact1[]    findAll()
 * @method Contact1[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Contact1Repository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contact1::class);
    }

    // /**
    //  * @return Contact1[] Returns an array of Contact1 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contact1
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
