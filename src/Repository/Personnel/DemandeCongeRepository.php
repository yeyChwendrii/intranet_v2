<?php

namespace App\Repository\Personnel;

use App\Entity\Personnel\CongeDataSearch;
use App\Entity\Personnel\DemandeConge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DemandeConge|null find($id, $lockMode = null, $lockVersion = null)
 * @method DemandeConge|null findOneBy(array $criteria, array $orderBy = null)
 * @method DemandeConge[]    findAll()
 * @method DemandeConge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemandeCongeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DemandeConge::class);
    }

    public function findByUserDemandesConges($user)
    {
        $qb = $this->createQueryBuilder('dc')
            ->where('dc.agent = :agent')
            ->setParameter('agent', $user);

        return $qb->getQuery()->getResult();
    }

    public function findAllCongeQuery(CongeDataSearch $search)
    {
        $query = $this->createQueryBuilder('dc')
                        ->leftJoin('dc.agent', 'u')
                        ->leftJoin('u.posteUsers', 'p_u1')
                        ->leftJoin('App\Entity\PosteUser', 'p_u2', 'WITH', 'p_u1.user = p_u2.user AND p_u1.dateEvenementAt < p_u2.dateEvenementAt')
                        ->where('p_u2.user IS NULL')
                        ->leftJoin('p_u1.posteuserServicedepartementdirections', 'p_u_s_dptdir')
                        ->leftJoin('p_u_s_dptdir.servicedepartementdirection', 's_dptdir')
                        ->leftJoin('s_dptdir.departementdirection', 'sdpt_dir')
                        ->leftJoin('sdpt_dir.direction', 'sdptdir')
                        ->leftJoin('p_u1.posteuserServicedirections', 'p_u_s_dir')
                        ->leftJoin('p_u_s_dir.servicedirection', 's_dir')
                        ->leftJoin('s_dir.direction', 'sdir')
                        ->leftJoin('p_u1.posteuserDirections', 'p_u_dir')
                        ->leftJoin('p_u_dir.direction', 'dir')
                        ->leftJoin('p_u1.posteuserDepartementdirections', 'p_u_dpt')
                        ->leftJoin('p_u_dpt.departementdirection', 'dpt_dir')
                        ->leftJoin('dpt_dir.direction', 'dptdir')
                        ->leftJoin('dc.repriseService', 'rs')
                        ->leftJoin('dc.modeleConge', 'mc')
                        ->select('u.id, u.nom, u.prenom, u.matricule, u.enfonction, dc.id as demande_conge_id, 
                        dc.dateDebutAt, dc.dateFinAt, dc.nombreJour, dc.annee, mc.titre as titre_conge, 
                        rs.id as reprise_service_id, sdptdir.designation as sdptdir_designation, 
                        dptdir.designation as dptdir_designation, sdir.designation as sdir_designation, 
                        dir.designation as dir_designation')
                        ;

        if ($search->getDateDebutAt() and !$search->getDateFinAt() and !$search->getModeleConge()) {
            $query = $query
                ->andWhere('dc.dateDebutAt >= :dateDebutAt')
                ->setParameter('dateDebutAt', $search->getDateDebutAt())
            ;
        } elseif ($search->getDateDebutAt() and $search->getModeleConge() and !$search->getDateFinAt()) {
            $query = $query
                ->andWhere('dc.dateDebutAt >= :dateDebutAt')
                ->setParameter('dateDebutAt', $search->getDateDebutAt())
                ->andWhere('dc.modeleConge = :modeleConge')
                ->setParameter('modeleConge', $search->getModeleConge())
            ;
        } elseif (!$search->getDateDebutAt() and !$search->getModeleConge() and $search->getDateFinAt()) {
            $query = $query
                ->andWhere('dc.dateDebutAt <= :dateFinAt')
                ->setParameter('dateFinAt', $search->getDateFinAt())
            ;
        } elseif (!$search->getDateDebutAt() and $search->getDateFinAt() and $search->getModeleConge()) {
            $query = $query
                ->andWhere('dc.dateDebutAt <= :dateFinAt')
                ->setParameter('dateFinAt', $search->getDateFinAt())
                ->andWhere('dc.modeleConge = :modeleConge')
                ->setParameter('modeleConge', $search->getModeleConge())
            ;
        } elseif ($search->getDateDebutAt() and $search->getDateFinAt() and !$search->getModeleConge()) {
            $query = $query
                ->andWhere('dc.dateDebutAt BETWEEN :dateDebutAt AND :dateFinAt')
                ->setParameter('dateDebutAt', $search->getDateDebutAt())
                ->setParameter('dateFinAt', $search->getDateFinAt())
            ;
        } elseif ($search->getDateDebutAt() and $search->getDateFinAt() and $search->getModeleConge()) {
            $query = $query
                ->andWhere('dc.dateDebutAt BETWEEN :dateDebutAt AND :dateFinAt')
                ->setParameter('dateDebutAt', $search->getDateDebutAt())
                ->setParameter('dateFinAt', $search->getDateFinAt())
                ->andWhere('dc.modeleConge = :modeleConge')
                ->setParameter('modeleConge', $search->getModeleConge())
            ;
        } elseif ($search->getModeleConge()) {
            $query = $query
                ->andWhere('dc.modeleConge = :modeleConge')
                ->setParameter('modeleConge', $search->getModeleConge())
            ;
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return DemandeConge[] Returns an array of DemandeConge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DemandeConge
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
