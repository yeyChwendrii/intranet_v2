<?php

namespace App\Repository\Personnel;

use App\Entity\Personnel\AgentDroitConge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgentDroitConge|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgentDroitConge|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgentDroitConge[]    findAll()
 * @method AgentDroitConge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgentDroitCongeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgentDroitConge::class);
    }

    // /**
    //  * @return AgentDroitConge[] Returns an array of AgentDroitConge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgentDroitConge
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
