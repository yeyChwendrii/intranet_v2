<?php

namespace App\Repository\Personnel;

use App\Entity\Personnel\CategorieConge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CategorieConge|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieConge|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieConge[]    findAll()
 * @method CategorieConge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieCongeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieConge::class);
    }

    // /**
    //  * @return CategorieConge[] Returns an array of CategorieConge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategorieConge
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
