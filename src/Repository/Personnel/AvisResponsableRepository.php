<?php

namespace App\Repository\Personnel;

use App\Entity\Personnel\AvisResponsable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AvisResponsable|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvisResponsable|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvisResponsable[]    findAll()
 * @method AvisResponsable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvisResponsableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AvisResponsable::class);
    }

    // /**
    //  * @return AvisResponsable[] Returns an array of AvisResponsable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AvisResponsable
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
