<?php

namespace App\Repository\Personnel;

use App\Entity\Personnel\MsgAccueilConge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MsgAccueilConge|null find($id, $lockMode = null, $lockVersion = null)
 * @method MsgAccueilConge|null findOneBy(array $criteria, array $orderBy = null)
 * @method MsgAccueilConge[]    findAll()
 * @method MsgAccueilConge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MsgAccueilCongeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MsgAccueilConge::class);
    }

    // /**
    //  * @return MsgAccueilConge[] Returns an array of MsgAccueilConge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MsgAccueilConge
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
