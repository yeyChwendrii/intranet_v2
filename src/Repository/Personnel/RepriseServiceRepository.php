<?php

namespace App\Repository\Personnel;

use App\Entity\Personnel\RepriseService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RepriseService|null find($id, $lockMode = null, $lockVersion = null)
 * @method RepriseService|null findOneBy(array $criteria, array $orderBy = null)
 * @method RepriseService[]    findAll()
 * @method RepriseService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepriseServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RepriseService::class);
    }

    // /**
    //  * @return RepriseService[] Returns an array of RepriseService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RepriseService
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
