<?php

namespace App\Repository;

use App\Entity\DepartementDirection;
use App\Entity\DirectionSearch;
use App\Entity\Service;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use App\Entity\UserSearch;
use App\Entity\Ville;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findAll()
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Service::class);
    }

    // Afficher les services selon la direction choisie
    // Rechercher les services d'une direction
    /**
     * @return Query
     */
    public function findServDeptDirByDir(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
            ->orderBy('s.designation', 'ASC');
        if ($search->getDirection()){
            $query = $query
                ->leftJoin('s.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('s.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.direction = :search or sd.direction = :search')
                ->setParameter('search', $search->getDirection());
        }

        return $query->getQuery()->getResult();
    }

    /**
     * Affiche les service
     * @return mixed
     */
    public function findSomeFields()
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->leftJoin('s.serviceDepartementdirections', 'sdpt')
            ->leftJoin('s.serviceDirections', 'sdir')
            ->select('s.id, s.designation as serv_designation')
            ->orderBy('s.id', 'DESC')

        ;
        return $qb->getQuery()
            ->getResult();
    }

    public function customFindAll(){
        $qb = $this->createQueryBuilder('s');
        $qb
            ->select('sdpt', 'sdir', 's')
            ->join('s.serviceDepartementdirections', 'sdpt')
            ->join('s.serviceDirections', 'sdir')
            ;
        return $qb->getQuery()->getResult();
    }

    public function serviceNotLinked(){
        $qb = $this->createQueryBuilder('s');
        $qb
            ->leftJoin('s.serviceDepartementdirections', 'sdpt')
            ->leftJoin('s.serviceDirections', 'sdir')
            ->where('sdpt.service is null')
            ->andWhere('sdir.service is null')
            ;
        return $qb->getQuery()->getResult();
    }

    // Afficher les services liés à un département selon le département choisi
    // Rechercher les services d'un département
    /**
     * @return Query
     */
    public function findServDeptDirByDept(DepartementDirection $search)
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
            ->orderBy('s.designation', 'ASC');
        if ($search->getDepartement()){
            $query = $query
                ->leftJoin('s.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->andWhere('dd.departement = :search')
                ->setParameter('search', $search->getDepartement());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher les services selon l'agence choisie
    // Rechercher les services d'une agence
    /**
     * @return Query
     */
    public function findServDeptDirByAgence(ServiceDepartementdirection $search)
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
            ->orderBy('s.designation', 'ASC');
        if ($search->getAgence()){
            $query = $query
                ->leftJoin('s.serviceDepartementdirections', 'dd')
                ->addSelect('dd')
                ->leftJoin('s.serviceDirections', 'sd')
                ->addSelect('sd')
                ->andWhere('dd.agence = :search or sd.agence = :search')
                ->setParameter('search', $search->getAgence());
        }

        return $query->getQuery()->getResult();
    }

    // Afficher le service selon l'agent choisie
    // Rechercher le service d'un agent
    /**
     * @return Query
     */
    public function findServiceByAgent(UserSearch $search)
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
            ->orderBy('s.designation', 'ASC');
        if ($search->getMatricule()){
            $query = $query
                ->leftJoin('s.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                /*->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('s.serviceDirections', 'sd')
                ->addSelect('sd')*/
                ->leftJoin('sdd.posteuserServicedepartementdirections', 'pusdd')
                ->addSelect('pusdd')
                ->leftJoin('pusdd.posteuser', 'pu')
                ->addSelect('pu')
                ->leftJoin('pu.user', 'u')
                ->addSelect('u')
                ->where('u.matricule LIKE :search')
                ->orWhere('u.nom LIKE :search')
                ->orWhere('u.prenom LIKE :search')
                ->orWhere('u.tel LIKE :search')
                ->orWhere('u.email LIKE :search')
                ->setParameter('search', '%'.$search->getMatricule().'%');
        }

        return $query->getQuery()->getResult();
    }

    // Afficher les services d'une île selon l'île choisie
    // Rechercher les services d'une île
    /**
     * @return Query
     */
    public function findServiceByIle(Ville $search)
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
            ->orderBy('s.designation', 'ASC');
        if ($search->getIle() == 'Ngazidja'){
            $query = $query
                ->leftJoin('s.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('s.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('sd.direction', 'd')
                ->addSelect('d')
                ->andWhere('(dir.id != 8 and dir.id != 10) or (d.id != 8 and d.id != 10)')
            ;
        } elseif ($search->getIle() == 'Ndzuani'){
            $query = $query
                ->leftJoin('s.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('s.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('sd.direction', 'd')
                ->addSelect('d')
                ->andWhere('dir.id = 10 or d.id = 10')
            ;
        } elseif ($search->getIle() == 'Mwali'){
            $query = $query
                ->leftJoin('s.serviceDepartementdirections', 'sdd')
                ->addSelect('sdd')
                ->leftJoin('sdd.departementdirection', 'dd')
                ->addSelect('dd')
                ->leftJoin('s.serviceDirections', 'sd')
                ->addSelect('sd')
                ->leftJoin('dd.direction', 'dir')
                ->addSelect('dir')
                ->leftJoin('sd.direction', 'd')
                ->addSelect('d')
                ->andWhere('dir.id = 8 or d.id = 8')
            ;
        }
        return $query->getQuery()->getResult();
    }

    public function findServiceByNgz()
    {
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.serviceDepartementdirections', 'sdd')
            ->addSelect('sdd')
            ->leftJoin('sdd.departementdirection', 'dd')
            ->addSelect('dd')
            ->leftJoin('s.serviceDirections', 'sd')
            ->addSelect('sd')
            ->leftJoin('dd.direction', 'dir')
            ->addSelect('dir')
            ->leftJoin('sd.direction', 'd')
            ->addSelect('d')
            ->andWhere('(dir.id != 8 and dir.id != 10) or (d.id != 8 and d.id != 10)')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findServiceByNdz()
    {
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.serviceDepartementdirections', 'sdd')
            ->addSelect('sdd')
            ->leftJoin('sdd.departementdirection', 'dd')
            ->addSelect('dd')
            ->leftJoin('s.serviceDirections', 'sd')
            ->addSelect('sd')
            ->leftJoin('dd.direction', 'dir')
            ->addSelect('dir')
            ->leftJoin('sd.direction', 'd')
            ->addSelect('d')
            ->andWhere('dir.id = 10 or d.id = 10')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findServiceByMwa()
    {
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.serviceDepartementdirections', 'sdd')
            ->addSelect('sdd')
            ->leftJoin('sdd.departementdirection', 'dd')
            ->addSelect('dd')
            ->leftJoin('s.serviceDirections', 'sd')
            ->addSelect('sd')
            ->leftJoin('dd.direction', 'dir')
            ->addSelect('dir')
            ->leftJoin('sd.direction', 'd')
            ->addSelect('d')
            ->andWhere('dir.id = 8 or d.id = 8')
        ;
        return $qb->getQuery()->getResult();
    }

    public function findServiceById($value): ?Service
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /*
     * ============================================================================
     *                      YEYCHWENDRII
     * ============================================================================
     */



    // /**
    //  * @return Service[] Returns an array of Service objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Service
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
