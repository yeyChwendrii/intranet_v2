<?php

namespace App\Repository;

use App\Entity\PosteuserDirection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PosteuserDirection|null find($id, $lockMode = null, $lockVersion = null)
 * @method PosteuserDirection|null findOneBy(array $criteria, array $orderBy = null)
 * @method PosteuserDirection[]    findAll()
 * @method PosteuserDirection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PosteuserDirectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PosteuserDirection::class);
    }

    // /**
    //  * @return PosteuserDirection[] Returns an array of PosteuserDirection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByIdPosteUser($posteuser_id)
    {
        $qb = $this->createQueryBuilder('p');
        return $qb
            //->select('p.id')
            ->where('p.posteuser = :posteuser')
            ->setParameter('posteuser', $posteuser_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findOneBySomeField($value): ?PosteuserDirection
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
