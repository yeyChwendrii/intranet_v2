<?php

namespace App\Controller\Admin;

use App\Entity\Admin\EcFormationuser;
use App\Entity\UserSearch;
use App\Form\Admin\EcFormationuserType;
use App\Form\UserSearchType;
use App\Repository\Admin\EcFormationuserRepository;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/ec_formationuser")
 */
class EcFormationuserController extends AbstractController
{
    /**
     * @Route("/", name="admin_ec_formationuser_index", methods={"GET"})
     */
    public function index(EcFormationuserRepository $ecFormationuserRepository): Response
    {
        return $this->render('admin/ec_formationuser/index.html.twig', [
            'ec_formationusers' => $ecFormationuserRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_ec_formationuser_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $ecFormationuser = new EcFormationuser();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //affectation de la valeur de l'utilisateur au formulaire
        $ecFormationuser = $ecFormationuser->setUser($user);

        $form = $this->createForm(EcFormationuserType::class, $ecFormationuser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($ecFormationuser);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ec_formationuser_index');
        }

        return $this->render('admin/ec_formationuser/new.html.twig', [
            'ec_formationuser' => $ecFormationuser,
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_ec_formationuser_show", methods={"GET"})
     */
    public function show(EcFormationuser $ecFormationuser): Response
    {
        return $this->render('admin/ec_formationuser/show.html.twig', [
            'ec_formationuser' => $ecFormationuser,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_ec_formationuser_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EcFormationuser $ecFormationuser): Response
    {
        $form = $this->createForm(EcFormationuserType::class, $ecFormationuser);
        $form->handleRequest($request);

        $ecFormationuser = $ecFormationuser->setEcoleuniversite($ecFormationuser->getEcoleuniversite());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ec_formationuser_index', [
                'id' => $ecFormationuser->getId(),
            ]);
        }

        return $this->render('admin/ec_formationuser/edit.html.twig', [
            'ec_formationuser' => $ecFormationuser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_ec_formationuser_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EcFormationuser $ecFormationuser): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecFormationuser->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecFormationuser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ec_formationuser_index');
    }

    /*=============================================================
     * CHOISIR AGENT
     ===============================================================*/

    /**
     * @Route("/choisir/agent", name="ecoleuniversite_choisir_agent")
     */
    public function choisirAgent(
        PaginatorInterface $paginator,
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        UserRepository $userRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $users = $paginator->paginate(
            $userRepository->findAllOrderBy($search),
            $request->query->getInt('page', 1),
            18
        );

        return $this->render('admin/choisir_agent.html.twig', [
            'users'         => $users,
            'formSearch'    => $formSearch->createView(),
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }
}
