<?php

namespace App\Controller\Admin;

use App\Entity\Admin\EcUniversite;
use App\Form\Admin\EcUniversiteType;
use App\Repository\Admin\EcUniversiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/universite")
 */
class EcUniversiteController extends AbstractController
{
    /**
     * @Route("/", name="ec_universite_index", methods={"GET"})
     */
    public function index(EcUniversiteRepository $ecUniversiteRepository): Response
    {
        return $this->render('admin/ec_universite/index.html.twig', [
            'ec_universites' => $ecUniversiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ec_universite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ecUniversite = new EcUniversite();
        $form = $this->createForm(EcUniversiteType::class, $ecUniversite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecUniversite);
            $entityManager->flush();
            //on récupere le dernier id de l'université enregistrée
            $last_id = $ecUniversite->getId();

            return $this->redirectToRoute('ec_universite_show',  array('id' => $last_id));
        }

        return $this->render('admin/ec_universite/new.html.twig', [
            'ec_universite' => $ecUniversite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ec_universite_show", methods={"GET"})
     */
    public function show(EcUniversite $ecUniversite): Response
    {
        return $this->render('admin/ec_universite/show.html.twig', [
            'ec_universite' => $ecUniversite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ec_universite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EcUniversite $ecUniversite): Response
    {
        $form = $this->createForm(EcUniversiteType::class, $ecUniversite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ec_universite_index', [
                'id' => $ecUniversite->getId(),
            ]);
        }

        return $this->render('admin/ec_universite/edit.html.twig', [
            'ec_universite' => $ecUniversite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ec_universite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EcUniversite $ecUniversite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecUniversite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecUniversite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ec_universite_index');
    }
}
