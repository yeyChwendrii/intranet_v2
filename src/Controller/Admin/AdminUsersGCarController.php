<?php

namespace App\Controller\Admin;

use App\Entity\Fonction;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\User;
use App\Form\FonctionType;
use App\Form\PosteType;
use App\Form\PosteUserDepartementType;
use App\Form\PosteUserDirectionType;
use App\Form\PosteUserServiceType;
use App\Form\PosteUserType;
use App\Form\UserType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminUsersGCarController extends AbstractController
{
    public function __construct( ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/moncompte/user/edit/{id}", name="admin.users_gcar.edit", requirements={"id": "\d+"})
     * @Security("has_role('ROLE_ADMIN_GCAR')")
     */
    public function index(
        User $user,
        Request $request
    )
    {


        //Formulaire
        $formUser = $this->createForm(UserType::class, $user);

        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()){

            $this->manager->flush();
            return $this->redirectToRoute('contacts_agents');
        }

        return $this->render('admin_users_g_car/edit_users_gcar.html.twig', [
            'user' => $user,
            'form_user' => $formUser->createView(),
        ]);
    }


    /**
     * @Route("/moncompte/user/add_userserv/{id}", name="admin.users_gcar.addUserServ", requirements={"id": "\d+"})
     * @Security("has_role('ROLE_ADMIN_GCAR')")
     */
    public function addUserDirDptServ(
        $id,
        Request $request,
        User $user,
        PosteUserRepository $posteUserRepository,
        UserRepository $userRepository
    )
    {

        $posteUserService = new PosteUserService();
        $posteUserDepartement = new PosteUserDepartement();
        $posteUserDirection = new PosteUserDirection();

        $user_id = $user->getId();
        $cur_user = $userRepository->find($user_id);

        //Récupération de l'ID PosteUser
        $cur_posteUser = $posteUserRepository->findOneBy(['user' => $cur_user]);

        //formulaire pour l'ajout de Service seulement
        $formPosteUserServ =$this->createForm(PosteUserServiceType::class, $posteUserService);
        $formPosteUserServ->handleRequest($request);
        //formulaire pour l'ajout de Département
        $formPosteUserDept = $this->createForm(PosteUserDepartementType::class, $posteUserDepartement);
        $formPosteUserDept->handleRequest($request);
        //formulaire pour l'ajout de Direction
        $formPosteUserDir = $this->createForm(PosteUserDirectionType::class, $posteUserDirection);
        $formPosteUserDir->handleRequest($request);

        // on affecte l'agent à un service
        if ($formPosteUserServ->isSubmitted() && $formPosteUserServ->isValid()){
            $posteUserService->setPosteUser($cur_posteUser);
            $this->manager->persist($posteUserService);
            $this->manager->flush();
            $this->addFlash('success', 'Service lié à l\'agent avec succès!');
            return $this->redirectToRoute('admin.users_gcar.edit', [
                'id' => $user->getId()
            ]);
        }

        // on affecte l'agent à un département
        if ($formPosteUserDept->isSubmitted() && $formPosteUserDept->isValid()){
            $posteUserDepartement->setUser($cur_user);
            $this->manager->persist($posteUserDepartement);
            $this->manager->flush();
            $this->addFlash('success', 'Département lié à l\'agent avec succès!');
            return $this->redirectToRoute('admin.users_gcar.addUserServ', [
                'id' => $user->getId()
            ]);
        }

        // on affecte l'agent à une direction
        if ($formPosteUserDir->isSubmitted() && $formPosteUserDir->isValid()){
            $posteUserDepartement->setUser($cur_user);
            $this->manager->persist($posteUserDirection);
            $this->manager->flush();
            $this->addFlash('success', 'Direction lié à l\'agent avec succès!');
            return $this->redirectToRoute('admin.users_gcar.addUserServ', [
                'id' => $user->getId()
            ]);
        }

        return $this->render('admin_users_g_car/add_userserv_gcar.html.twig', [
            'form_poste_userserv' => $formPosteUserServ->createView(),
            'form_poste_userdept' => $formPosteUserDept->createView(),
            'form_poste_userdir' => $formPosteUserDir->createView(),
            'cur_user' => $cur_user,
            'cur_posteUser' => $cur_posteUser
        ]);
    }

    /**
     * @Route("/moncompte/user/edit_userserv/{slug}-{id}", name="admin.users_gcar.editUserServ", requirements={"slug": "[a-z0-9\-]*", "id": "\d+"})
     * @Security("has_role('ROLE_ADMIN_GCAR')")
     */
    /*
    public function editUserServ(
        $id,
        $slug,
        Request $request,
        User $user,
        PosteUserRepository $posteUserRepository,
        UserRepository $userRepository,
        //appel des etité
        PosteuserServicedepartementdirectionvice $posteuserServicedept
    )
    {
        $cur_user = $posteuserServicedept->getPosteUser()->getUser();

        //Récupération de l'ID PosteUser
        $id_posteuser = $posteuserServicedept->getPosteUser();

        //$cur_posteUser = $posteUserRepository->findOneBy(['user' => $cur_user])->getId();

        //formulaire pour l'ajout de Service seulement
        $formPosteUserServ =$this->createForm(PosteUserServiceType::class, $posteuserServicedept);
        $formPosteUserServ->handleRequest($request);
        //formulaire pour l'ajout de Département

        // on affecte l'agent à un service
        if ($formPosteUserServ->isSubmitted() && $formPosteUserServ->isValid()){
            $posteuserServicedept->setPosteUser($id_posteuser);
            //$this->manager->persist($posteUserService);
            $this->manager->flush();
            $this->addFlash('success', 'Service lié à l\'agent avec succès!');
            return $this->redirectToRoute('admin.users_gcar.edit', [
                'id' => $posteuserServicedept->getPosteUser()->getUser()->getId()
            ]);
        }




        return $this->render('admin_users_g_car/edit_userserv_gcar.html.twig', [
            'form_poste_userserv' => $formPosteUserServ->createView(),
            'cur_user' => $cur_user,
            //'cur_posteUser' => $cur_posteUser
        ]);
    }
    */

    /**
     * MODIFICATION DE POSTE_USER ET AJOUT DE POSTE EN CAS D'INDISPONIBILITé DU POSTE
     * @Route("/moncompte/poste_user/edit/{slug}-{id}", name="admin.users_gcar.editPoste", requirements={"slug": "[a-z0-9\-]*", "id": "\d+"})
     * @Security("has_role('ROLE_ADMIN_GCAR')")
     */
    public function editPosteUser(
        Request $request,
        UserRepository $userRepository,
        PosteUserRepository $posteUserRepository,
        User $user,
        PosteType $posteType,
        PosteUser $posteUser,
        $id,
        $slug
    )
    {
        $poste = new Poste();
        $fonction = new Fonction();

        //formulaire pour l'ajout de POSTE seulement
        $formAddPoste =$this->createForm(PosteType::class, $poste);
        $formAddPoste->handleRequest($request);

        //formulaire pour l'ajout de FONCTION seulement
        $formAddFonction =$this->createForm(FonctionType::class, $fonction);
        $formAddFonction->handleRequest($request);

        //id user envoyé à la vue
        $poste_user_id = $posteUser->getId();
        $poste_user = $posteUserRepository->find($poste_user_id);

        $user = $posteUser->getUser();
        $user_id = $posteUser->getUser()->getId();

        //si on ajoute un poste car le poste était manquant dans la liste, on reste sur la meme page
        if ($formAddPoste->isSubmitted() && $formAddPoste->isValid()){
            $this->manager->persist($poste);
            $this->manager->flush();
            $this->addFlash('success', 'Poste ajouté avec succès!');
            return $this->redirectToRoute('admin.users_gcar.addPoste', [
                'id' => $user->getId()
            ]);
        }

        //si on ajoute une fonction car la fonction était manquant dans la liste, on reste sur la meme page
        if ($formAddFonction->isSubmitted() && $formAddFonction->isValid()){
            $this->manager->persist($fonction);
            $this->manager->flush();
            $this->addFlash('success', 'Fonction ajoutée avec succès!');
            return $this->redirectToRoute('admin.users_gcar.addPoste', [
                'id' => $user->getId()
            ]);
        }

        $formPosteUser = $this->createForm(PosteUserType::class, $posteUser);
        $formPosteUser->handleRequest($request);

         if ($formPosteUser->isSubmitted() && $formPosteUser->isValid()){
             $posteUser->setUser($user);
             $this->manager->flush();
             $this->addFlash('success', 'Poste modifié avec succès!');
             return $this->redirectToRoute('admin.users_gcar.edit', [
                 'id' => $user_id,
             ]);
         }


        return $this->render('admin_users_g_car/edit_userposte_gcar.html.twig',[
            'form_poste_user' => $formPosteUser->createView(),
            'form_add_poste' => $formAddPoste->createView(),
            'form_add_fonction' => $formAddFonction->createView(),
            'poste_user' => $poste_user
        ]);
    }

}
