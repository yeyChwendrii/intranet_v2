<?php

namespace App\Controller\Admin;

use App\Entity\Admin\EcFormation;
use App\Entity\Admin\EcMetier;
use App\Entity\Admin\Ecoleuniversite;
use App\Entity\Admin\EcSousmetier;
use App\Entity\Admin\EcUniversite;
use App\Entity\Admin\Niveauqualification;
use App\Form\Admin\EcFormationType;
use App\Form\Admin\EcMetierType;
use App\Form\Admin\EcoleuniversiteType;
use App\Form\Admin\EcSousmetierType;
use App\Form\Admin\NiveauqualificationType;
use App\Repository\Admin\EcFormationRepository;
use App\Repository\Admin\EcMetierRepository;
use App\Repository\Admin\EcoleuniversiteRepository;
use App\Repository\Admin\EcSousmetierRepository;
use App\Repository\Admin\EcUniversiteRepository;
use App\Repository\Admin\NiveauqualificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion")
 */
class EcoleuniversiteController extends AbstractController
{
    /**
     * @Route("/ecoleuniversite/", name="admin_ecoleuniversite_index", methods={"GET"})
     */
    public function index(EcoleuniversiteRepository $ecoleuniversiteRepository,
                          EcMetierRepository $ecMetierRepository,
                          EcSousmetierRepository $ecSousmetierRepository,
                          EcFormationRepository $ecFormationRepository,
                          EcUniversiteRepository $ecUniversiteRepository,
                          NiveauqualificationRepository $niveauqualificationRepository): Response
    {
        return $this->render('admin/ecoleuniversite/index.html.twig', [
            'ecoleuniversites' => $ecoleuniversiteRepository->findAll(),
            'niveauqualifications' => $niveauqualificationRepository->findAll(),
            'ec_metiers' => $ecMetierRepository->findAll(),
            'ec_sousmetiers' => $ecSousmetierRepository->findAll(),
            'ec_formations' => $ecFormationRepository->findAll(),
            'ec_universites' => $ecUniversiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ecoleuniversite/new", name="admin_ecoleuniversite_new", methods={"GET","POST"})
     */
    public function new(Request $request, EcUniversiteRepository $ecUniversiteRepository): Response
    {

        if ($request->get('id_universite')){
            $url_id = $request->get('id_universite');
            $universite =  $ecUniversiteRepository->find($url_id);
        }else{
            $url_id = '';
            $universite= '';
        }
        $ecoleuniversite = new Ecoleuniversite();

        if ($universite instanceof EcUniversite){
            $ecoleuniversite = $ecoleuniversite->setEcoleOuUniversite('Université');
        }else{
            $ecoleuniversite = $ecoleuniversite->setEcoleOuUniversite('Ecole');
        }
        $form = $this->createForm(EcoleuniversiteType::class, $ecoleuniversite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecoleuniversite);
            $entityManager->flush();

            if ($universite instanceof EcUniversite){
                return $this->redirectToRoute('ec_universite_show', array('id' => $universite->getId()));
            }else{
                return $this->redirectToRoute('admin_ecoleuniversite_index');
            }

        }

        return $this->render('admin/ecoleuniversite/new.html.twig', [
            'ecoleuniversite' => $ecoleuniversite,
            'form' => $form->createView(),
            'url_id' => $url_id,
        ]);
    }

    /**
     * @Route("/ecoleuniversite/{id}", name="admin_ecoleuniversite_show", methods={"GET"})
     */
    public function show(Ecoleuniversite $ecoleuniversite): Response
    {
        return $this->render('admin/ecoleuniversite/show.html.twig', [
            'ecoleuniversite' => $ecoleuniversite,
        ]);
    }

    /**
     * @Route("/ecoleuniversite/{id}/edit", name="admin_ecoleuniversite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ecoleuniversite $ecoleuniversite): Response
    {
        $form = $this->createForm(EcoleuniversiteType::class, $ecoleuniversite);
        $form->handleRequest($request);

        $type = $ecoleuniversite->getEcoleOuUniversite();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index', [
                'id' => $ecoleuniversite->getId(),
            ]);
        }

        return $this->render('admin/ecoleuniversite/edit.html.twig', [
            'ecoleuniversite' => $ecoleuniversite,
            'form' => $form->createView(),
            'type' => $type,
        ]);
    }

    /**
     * @Route("/ecoleuniversite/{id}", name="admin_ecoleuniversite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Ecoleuniversite $ecoleuniversite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecoleuniversite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecoleuniversite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ecoleuniversite_index');
    }





    /* #################################################################
     *                  NIVEAU QUALIFICATION
     */#################################################################

    /**
     * @Route("/niveauqualification/new", name="admin_niveauqualification_new", methods={"GET","POST"})
     */
    public function newNiveauqualification(Request $request): Response
    {
        $niveauqualification = new Niveauqualification();
        $form = $this->createForm(NiveauqualificationType::class, $niveauqualification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($niveauqualification);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index');
        }

        return $this->render('admin/niveauqualification/new.html.twig', [
            'niveauqualification' => $niveauqualification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/niveauqualification/{id}/edit", name="admin_niveauqualification_edit", methods={"GET","POST"})
     */
    public function editNiveauqualification(Request $request, Niveauqualification $niveauqualification): Response
    {
        $form = $this->createForm(NiveauqualificationType::class, $niveauqualification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index', [
                'id' => $niveauqualification->getId(),
            ]);
        }

        return $this->render('admin/niveauqualification/edit.html.twig', [
            'niveauqualification' => $niveauqualification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/niveauqualification/{id}", name="admin_niveauqualification_delete", methods={"DELETE"})
     */
    public function deleteNiveauqualification(Request $request, Niveauqualification $niveauqualification): Response
    {
        if ($this->isCsrfTokenValid('delete'.$niveauqualification->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($niveauqualification);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ecoleuniversite_index');
    }



    /* #################################################################
     *                  Ec METIER
     */#################################################################


    /**
     * @Route("/ec_metier/new", name="admin_ec_metier_new", methods={"GET","POST"})
     */
    public function newMetier(Request $request): Response
    {
        $ecMetier = new EcMetier();
        $form = $this->createForm(EcMetierType::class, $ecMetier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecMetier);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index');
        }

        return $this->render('admin/ec_metier/new.html.twig', [
            'ec_metier' => $ecMetier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ec_metier/{id}/edit", name="admin_ec_metier_edit", methods={"GET","POST"})
     */
    public function editMetier(Request $request, EcMetier $ecMetier): Response
    {
        $form = $this->createForm(EcMetierType::class, $ecMetier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index', [
                'id' => $ecMetier->getId(),
            ]);
        }

        return $this->render('admin/ec_metier/edit.html.twig', [
            'ec_metier' => $ecMetier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ec_metier/{id}", name="admin_ec_metier_delete", methods={"DELETE"})
     */
    public function deleteMetier(Request $request, EcMetier $ecMetier): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecMetier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecMetier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ecoleuniversite_index');
    }


    /* #################################################################
    *                  Ec SOUS-METIER
    */#################################################################

    /**
     * @Route("/ec_sousmetier/", name="admin_ec_sousmetier_index", methods={"GET"})
     */
    public function indexSousmetier(EcSousmetierRepository $ecSousmetierRepository): Response
    {
        return $this->render('admin/ec_sousmetier/index.html.twig', [
            'ec_sousmetiers' => $ecSousmetierRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ec_sousmetier/new", name="admin_ec_sousmetier_new", methods={"GET","POST"})
     */
    public function newSousmetier(Request $request): Response
    {
        $ecSousmetier = new EcSousmetier();
        $form = $this->createForm(EcSousmetierType::class, $ecSousmetier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecSousmetier);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index');
        }

        return $this->render('admin/ec_sousmetier/new.html.twig', [
            'ec_sousmetier' => $ecSousmetier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ec_sousmetier/{id}/edit", name="admin_ec_sousmetier_edit", methods={"GET","POST"})
     */
    public function editSousmetier(Request $request, EcSousmetier $ecSousmetier): Response
    {
        $form = $this->createForm(EcSousmetierType::class, $ecSousmetier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ec_sousmetier_index', [
                'id' => $ecSousmetier->getId(),
            ]);
        }

        return $this->render('admin/ec_sousmetier/edit.html.twig', [
            'ec_sousmetier' => $ecSousmetier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ec_sousmetier/{id}", name="admin_ec_sousmetier_delete", methods={"DELETE"})
     */
    public function deleteSousmetier(Request $request, EcSousmetier $ecSousmetier): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecSousmetier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecSousmetier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ec_sousmetier_index');
    }


    /* #################################################################
    *                  Ec Formation
    */#################################################################

    /**
     * @Route("/ec_formation/new", name="admin_ec_formation_new", methods={"GET","POST"})
     */
    public function newEcFormation(Request $request): Response
    {
        $ecFormation = new EcFormation();
        $form = $this->createForm(EcFormationType::class, $ecFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecFormation);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index');
        }

        return $this->render('admin/ec_formation/new.html.twig', [
            'ec_formation' => $ecFormation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ec_formation/{id}/edit", name="admin_ec_formation_edit", methods={"GET","POST"})
     */
    public function editEcFormation(Request $request, EcFormation $ecFormation): Response
    {
        $form = $this->createForm(EcFormationType::class, $ecFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ecoleuniversite_index', [
                'id' => $ecFormation->getId(),
            ]);
        }

        return $this->render('admin/ec_formation/edit.html.twig', [
            'ec_formation' => $ecFormation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ec_formation/{id}", name="admin_ec_formation_delete", methods={"DELETE"})
     */
    public function deleteEcFormation(Request $request, EcFormation $ecFormation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecFormation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecFormation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ecoleuniversite_index');
    }

}
