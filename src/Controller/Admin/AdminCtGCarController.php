<?php

namespace App\Controller\Admin;

use App\Entity\Agence;
use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Service;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\Ville;
use App\Form\AgenceType;
use App\Form\DepartementDirectionType;
use App\Form\DirectionType;
use App\Form\ServiceDepartementdirectionType;
use App\Form\ServiceDirectionType;
use App\Form\ServiceType;
use App\Form\VilleType;
use App\Repository\AgenceRepository;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DirectionRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\DepartementRepository;
use App\Repository\ServiceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/gestion/ct/svc")
 */
class AdminCtGCarController extends AbstractController
{
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/service", name="admin.ct_gcar.indexServ")
     *
     */
    public function indexServ(ServiceRepository $serviceRepository,
                          ServicedepartementdirectionRepository $servicedepartementRepository,
                          DepartementDirectionRepository $departementDirectionRepository,
                          DirectionRepository $directionRepository,
                          DepartementRepository $departementRepository,
                          Request $request)
    {

        //dd($serviceRepository->findSomeFields());
        return $this->render('admin_ct_g_car/index.html.twig', [
            'services' => $serviceRepository->findAll(),
            'departements' => $serviceRepository->findAll(),
            'directions' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/add_serv", name="admin.ct_gcar.addServ")
     * @Security("has_role('ROLE_ADMIN_GCAR')")
     */
    public function addServ(
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository
    )
    {
        $service = new Service();
        $direction = new Direction();
        $serviceDept = new ServiceDepartementdirection();
        $serviceDir = new ServiceDirection();
        $departementDir = new DepartementDirection();

        $servs = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $formAddServ = $this->createForm(ServiceType::class, $service);
        $formAddServ->handleRequest($request);

        $formServDept = $this->createForm(ServiceDepartementdirectionType::class, $serviceDept);
        $formServDept->handleRequest($request);

        $formServDir = $this->createForm(ServiceDirectionType::class, $serviceDir);
        $formServDir->handleRequest($request);

        $formAddDir = $this->createForm(DirectionType::class, $direction);
        $formAddDir->handleRequest($request);

        $formDeptDir = $this->createForm(DepartementDirectionType::class, $departementDir);
        $formDeptDir->handleRequest($request);

        //Ajouter un service
        if ($formAddServ->isSubmitted() && $formAddServ->isValid()){
            $this->manager->persist($service);
            $this->manager->flush();

            $this->addFlash('success', 'Service ajouté avec succès!');
            return $this->redirectToRoute('admin.ct_gcar.addServ');
        }

        //Ajouté une direction
        if ($formAddDir->isSubmitted() && $formAddDir->isValid()){
            $this->manager->persist($direction);
            $this->manager->flush();

            $this->addFlash('success', 'Direction ajoutée avec succès!');
            return $this->redirectToRoute('admin.ct_gcar.addServ');
        }

        //lié le service a un département
        if ($formServDept->isSubmitted() && $formServDept->isValid()){
            $this->manager->persist($serviceDept);
            $this->manager->flush();

            $this->addFlash('success', 'Service lié avec succès!');
            return $this->redirectToRoute('admin.ct_gcar.addServ');
        }

        //lié le service à une direction
        if ($formServDir->isSubmitted() && $formServDir->isValid()){
            $this->manager->persist($serviceDir);
            $this->manager->flush();

            $this->addFlash('success', 'Service lié avec succès!');
            return $this->redirectToRoute('admin.ct_gcar.addServ');
        }

        //lié le departement à une direction
        if ($formDeptDir->isSubmitted() && $formDeptDir->isValid()){
            $this->manager->persist($departementDir);
            $this->manager->flush();

            $this->addFlash('success', 'Département lié à une direction avec succès!');
            return $this->redirectToRoute('admin.ct_gcar.addServ');
        }

        return $this->render('admin_ct_g_car/add_serv.html.twig', [
            'form_add_serv' =>$formAddServ->createView(),
            'form_add_dir' =>$formAddDir->createView(),
            'form_serv_dept' =>$formServDept->createView(),
            'form_serv_dir' =>$formServDir->createView(),
            'form_dept_dir' =>$formDeptDir->createView(),
            'servs' =>$servs,
            'services' =>$servs,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }

    /**
     * @Route("/{id}/editServ", name="admin.ct_gcar.editServ")
     * @Security("has_role('ROLE_ADMIN_GCAR')")
     */
    public function editServ(Request $request,
                             Service $service,
                             ServiceDepartementdirectionRepository $serviceDepartementRepository,
                             ServiceDirectionRepository $serviceDirectionRepository,
                             DepartementRepository $departementRepository,
                             DirectionRepository $directionRepository,
                             ServiceRepository $serviceRepository){

        $servs = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        //Récupération de l'id du service
        $service_id = $service->getId();
        //id du service à partir de l'instance du service
        $id = $serviceRepository->find($service_id);

        /*
         * //Récupération de l'id du service
        $service_id = $service->getId();
        //id du service à partir de l'instance du service
        $id = $serviceRepository->find($service_id);
         */


        //formulaire de modification du service
        $form_serv = $this->createForm(ServiceType::class, $service);
        $form_serv->handleRequest($request);

        //formulaire d'ajout de liaison du service à un département
        $servDpt = new ServiceDepartementdirection();
        $servDpt->setService($service);
        $form_servDpt = $this->createForm(ServiceDepartementdirectionType::class, $servDpt);
        $form_servDpt->handleRequest($request);

        //formulaire d'ajout de liaison du service à une direction
        $servDir = new ServiceDirection();
        $servDir->setService($service);
        $form_servDir = $this->createForm(ServiceDirectionType::class, $servDir);
        $form_servDir->handleRequest($request);

        //récupération de l'entité manager
        $entityManager = $this->getDoctrine()->getManager();

        //EDIT
        //si le formulaire de midification du service est soumis
        if ($form_serv->isSubmitted() && $form_serv->isValid()) {

            //on flush la modif
            $entityManager->flush();

            $this->addFlash('success', 'Service modifié avec succès!');
            return $this->redirectToRoute('admin.ct_gcar.indexServ');
        }

        //AJOUT
        // si le formulaire D'AJOUT de liaison serviceDepartement est soumis
        if ($form_servDpt->isSubmitted() && $form_servDpt->isValid()){

            //Vérification si le service est déjà lié. Si c'est le cas on supprime sa précédente liaison avec la direction concernée
            //Récupération du service concerné
            $service_dir = $serviceDirectionRepository->findByIdService($service->getId());

            if ($service_dir){
                $entityManager->remove($service_dir);
            }

            $entityManager->persist($servDpt);
            $entityManager->flush();

            $this->addFlash('success', 'Service lié avec succès');
            return $this->redirectToRoute('admin.ct_gcar.indexServ');
        }

        //AJOUT
        // si le formulaire D'AJOUT de liaison serviceDirection est soumis
        if ($form_servDir->isSubmitted() && $form_servDir->isValid()){

            //Vérification si le service est déjà lié. Si c'est le cas on supprime sa précédente liaison avec le département concerné
            //Récupération du service concerné
            $service_dept = $serviceDepartementRepository->findByIdService($id);

            if ($service_dept){
                $entityManager->remove($service_dept);
            }

            $entityManager->persist($servDir);
            $entityManager->flush();

            $this->addFlash('success', 'Service lié avec succès');
            return $this->redirectToRoute('admin.ct_gcar.indexServ');
        }
        //ARCHIVAGE DU SERVICE
        //

        return $this->render('admin_ct_g_car/edit_serv.html.twig',[
            'form_editServ' => $form_serv->createView(),
            'form_addServDpt'  => $form_servDpt->createView(),
            'form_addServDir'  => $form_servDir->createView(),
            'servs' => $serviceRepository->findAll(),
            'current_serv' => $serviceRepository->find($service_id),
            'services' =>$servs,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }
}
