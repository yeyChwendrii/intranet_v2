<?php

namespace App\Controller\Admin;

use App\Entity\Admin\EcExperienceAilleurs;
use App\Entity\UserSearch;
use App\Form\Admin\EcExperienceAilleursType;
use App\Form\UserSearchType;
use App\Repository\Admin\EcExperienceAilleursRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/experience_ailleurs")
 */
class EcExperienceAilleursController extends AbstractController
{
    /**
     * @Route("/", name="admin_ec_experience_ailleurs_index", methods={"GET"})
     */
    public function index(EcExperienceAilleursRepository $ecExperienceAilleursRepository): Response
    {
        return $this->render('admin/ec_experience_ailleurs/index.html.twig', [
            'ec_experience_ailleurs' => $ecExperienceAilleursRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_ec_experience_ailleurs_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $ecExperienceAilleur = new EcExperienceAilleurs();

        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //affectation de la valeur de l'utilisateur au formulaire
        $ecExperienceAilleur = $ecExperienceAilleur->setUser($user);

        $form = $this->createForm(EcExperienceAilleursType::class, $ecExperienceAilleur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ecExperienceAilleur);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ec_experience_ailleurs_index');
        }

        return $this->render('admin/ec_experience_ailleurs/new.html.twig', [
            'ec_experience_ailleur' => $ecExperienceAilleur,
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_ec_experience_ailleurs_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EcExperienceAilleurs $ecExperienceAilleur): Response
    {
        $form = $this->createForm(EcExperienceAilleursType::class, $ecExperienceAilleur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_ec_experience_ailleurs_index', [
                'id' => $ecExperienceAilleur->getId(),
            ]);
        }

        return $this->render('admin/ec_experience_ailleurs/edit.html.twig', [
            'ec_experience_ailleur' => $ecExperienceAilleur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_ec_experience_ailleurs_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EcExperienceAilleurs $ecExperienceAilleur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ecExperienceAilleur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ecExperienceAilleur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_ec_experience_ailleurs_index');
    }

    /*=============================================================
     * CHOISIR AGENT
     ===============================================================*/

    /**
     * @Route("/choisir/agent", name="ec_experience_ailleurs_choisir_agent")
     */
    public function choisirAgent(
        PaginatorInterface $paginator,
        Request $request,

        UserRepository $userRepository
    )
    {

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $users = $paginator->paginate(
            $userRepository->findAllOrderBy($search),
            $request->query->getInt('page', 1),
            18
        );

        return $this->render('admin/ec_experience_ailleurs/choisir_agent.html.twig', [
            'users'         => $users,
            'formSearch'    => $formSearch->createView(),
        ]);
    }
}
