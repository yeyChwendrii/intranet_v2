<?php

namespace App\Controller;

use App\Entity\PosteUser;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use App\Entity\UserSearch;
use App\Form\UserSearchType;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\MessageRepository;
use App\Repository\CategorieRepository;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\UserRepository;
use App\Service\MercureCookieGenerator;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class InfoController extends AbstractController
{
    public function __construct(UserRepository $userRepository, ObjectManager $manager)
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
    }

    /**
     * @Route("moncompte/contacts", name="contacts_agents")
     */
    public function index(
        PaginatorInterface $paginator,
        Request $request,
        PosteUserRepository $posteUserRepository,
        MercureCookieGenerator $cookieGenerator
    )
    {
        $total_users = $this->userRepository->countAgents();

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $users = $paginator->paginate(
            $this->userRepository->findAllOrderBy($search),
            $request->query->getInt('page', 1),
            13
        );

        $response = $this->render('info/contacts_agents.html.twig', [
            'users'         => $users,
            'total_users'   => $total_users,
            'formSearch'    => $formSearch->createView(),
        ]);
        /** @var Response $response */
        $response->headers->set('set-cookie', $cookieGenerator->generate($this->getUser()));
        return $response;
    }

    /**
     * @Route("moncompte/contacts/ping/{user}", name="ping", methods={"POST"})
     */
    public function ping(MessageBusInterface $bus, ?User $user = null){
        $target = [];
        if ($user !== null){
            $target = ["http://comorestelecom.km/user/{$user->getId()}"];
        }
        $update = new Update( "http://comorestelecom.km/ping", "[]", $target);
        $bus->dispatch($update);
        return $this->redirectToRoute('contacts_agents');
    }

    /**
     * @Route("moncompte/contacts/hors_fonction", name="contacts_agents_hors_fonction")
     */
    public function agentsHorsFonction(
        PaginatorInterface $paginator,
        Request $request
    )
    {
        $total_users = $this->userRepository->countAgentsHorsfonction();

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $users = $paginator->paginate(
            $this->userRepository->findAllSuspendusOrderBy($search),
            $request->query->getInt('page', 1),
            13
        );

        return $this->render('info/contacts_agents_hors_fonction.html.twig', [
            'users'         => $users,
            'total_users'   => $total_users,
            'formSearch'    => $formSearch->createView(),
        ]);
    }

    /**
     * @Route("info/agent/{id}/profil", name="info_profile_agent")
     */
    public function infoProfileAgent(
        Request $request,
        PosteUserRepository $posteUserRepository,
        EvNominationRepository $evNominationRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvAffectationRepository $evAffectationRepository,
        EvTitularisationRepository $evTitularisationRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        UserRepository $userRepository
    )
    {
        $id = $request->get('id');
        $agent = $userRepository->find($id);
        $dernierPosteUser = $posteUserRepository->findLastByUser($agent);
        //dd($dernierPosteUser);
        /** @var PosteUser $dernierPosteUser */
        $nomEvenement = $dernierPosteUser ? $dernierPosteUser->getNomEvenement() : '';
        if ($nomEvenement!= ''){
            $idEvenement = $dernierPosteUser->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $evenement = $evRecrutementRepository->find($idEvenement);
                $poste = $evenement->getPoste();
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
            }
            elseif ($nomEvenement == 'Renouvellementdecontrat'){
                $evenement = $evRenouvellementdecontratRepository->find($idEvenement);
                $poste = $evenement->getPoste();
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
            }
            elseif ($nomEvenement == 'Titularisation'){
                $evenement = $evTitularisationRepository->find($idEvenement);

                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $poste = $evenement->getPoste();

                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
            }
            elseif ($nomEvenement == 'Affectation'){
                $evenement = $evAffectationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();

            }
            elseif ($nomEvenement == 'Promotioninterne'){
                $evenement = $evPromotioninterneRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Reintegration'){
                $evenement = $evReintegrationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Nomination'){
                $evenement = $evNominationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $evenement = $evDemisedefonctionRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
        }else{
            $service = 'Non-renseigné';
            $departement = 'Non-renseigné';
            $direction = 'Non-renseignée';
            $poste = 'Non-renseignée';

        }

        $retraite = $agent->getRetraite();

        return $this->render('info/info_profile_agent.html.twig', [
            'agent'                     => $agent,
            'service'                   => $service,
            'departement'               => $departement,
            'direction'                 => $direction,
            'poste'                     => $poste,
            'retraite'                  => $retraite,
            'nomEvenement'                  => $nomEvenement,
        ]);
    }


    /**
     * @Route("info/agent/{id}/profil.print", name="info_profile_agent_print")
     */
    public function infoProfileAgentPrint(
        Request $request,
        PosteUserRepository $posteUserRepository,
        EvNominationRepository $evNominationRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvAffectationRepository $evAffectationRepository,
        EvTitularisationRepository $evTitularisationRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        UserRepository $userRepository
    )
    {
        $id = $request->get('id');
        $agent = $userRepository->find($id);
        $dernierPosteUser = $posteUserRepository->findLastByUser($agent);
        //dd($dernierPosteUser);
        /** @var PosteUser $dernierPosteUser */
        $nomEvenement = $dernierPosteUser ? $dernierPosteUser->getNomEvenement() : '';
        if ($nomEvenement!= ''){
            $idEvenement = $dernierPosteUser->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Renouvellementdecontrat'){
                $evenement = $evRenouvellementdecontratRepository->find($idEvenement);
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Titularisation'){
                $evenement = $evTitularisationRepository->find($idEvenement);
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Affectation'){
                $evenement = $evAffectationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Promotioninterne'){
                $evenement = $evPromotioninterneRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Reintegration'){
                $evenement = $evReintegrationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Nomination'){
                $evenement = $evNominationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $evenement = $evDemisedefonctionRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
        }else{
            $service = 'Non-renseigné';
            $departement = 'Non-renseigné';
            $direction = 'Non-renseignée';
        }

        $retraite = $agent->getRetraite();

        return $this->render('info/info_profile_agent_print.html.twig', [
            'agent'            => $agent,
            'service'         => $service,
            'departement'         => $departement,
            'direction'         => $direction,
            'retraite'         => $retraite,
            'poste'            => $poste,
            'nomEvenement'     => $nomEvenement,
        ]);
    }


    /**
     * Affiche les evenements d'un agent
     * @Route("info/agent/{id}/evenement", name="info_evenement_agent")
     */
    public function infoEvenementAgent(
        Request $request,
        PosteUserRepository $posteUserRepository,
        EvNominationRepository $evNominationRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvAffectationRepository $evAffectationRepository,
        EvTitularisationRepository $evTitularisationRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        UserRepository $userRepository
    )
    {
        $id = $request->get('id');
        $agent = $userRepository->find($id);
        $dernierPosteUser = $posteUserRepository->findLastByUser($agent);
        /** @var PosteUser $dernierPosteUser */
        $nomEvenement = $dernierPosteUser ? $dernierPosteUser->getNomEvenement() : '';
        if ($nomEvenement!= ''){
            $idEvenement = $dernierPosteUser->getIdEvenement();

            if ($nomEvenement == 'Recrutement'){
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                //dd($evenement->getServiceDirection());
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Renouvellementdecontrat'){
                $evenement = $evRenouvellementdecontratRepository->find($idEvenement);
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Titularisation'){
                $evenement = $evTitularisationRepository->find($idEvenement);

                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();

                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Affectation'){
                $evenement = $evAffectationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();

            }
            elseif ($nomEvenement == 'Promotioninterne'){
                $evenement = $evPromotioninterneRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Reintegration'){
                $evenement = $evReintegrationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Nomination'){
                $evenement = $evNominationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $evenement = $evDemisedefonctionRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
        }else{
            $service = 'Non-renseigné';
            $departement = 'Non-renseigné';
            $direction = 'Non-renseignée';
        }

        $retraite = $agent->getRetraite();

        return $this->render('info/info_evenement_agent.html.twig', [
            'agent'             => $agent,
            'dernier_poste'     => $dernierPosteUser,
            'service'           => $service,
            'departement'       => $departement,
            'direction'         => $direction,
            'retraite'          => $retraite,
        ]);
    }


    /**
     * Filtre
     * Info sur les agents et filtre par (tout)
     * Information sur la variable "respoonsabilite" de l'URL:
     * 0=> Filtre par statut (en service, horsservice, titulaire, contractuel...)
     * 1=> Filtre par Fonction (Poste)
     * 2=> Filtre par service Direction
     * 3=> Filtre par service Département
     *
     * @Route("/societe/agents", name="info_liste_agents", methods={"GET"})
     */
    public function info(UserRepository $userRepository,
                         PosteRepository $posteRepository,
                         PosteUserRepository $posteUserRepository,
                         ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                         ServiceDirectionRepository $serviceDirectionRepository,
                         DepartementDirectionRepository $departementDirectionRepository,
                         DirectionRepository $directionRepository,
                         CategorieRepository $categorieRepository,
                         Request $request)
    {
        $users_online = $posteUserRepository->showAgentEnLigne();

        //variable de l'URL
        $filtre = $request->get('filtre');
        $responsabilite = $request->get('responsabilite');

        //variable $poste pour le menu
        $postes = $posteRepository->showSomeFields();

        //variable pour le menu des services de département et direction
        $servicesDpt = $serviceDepartementdirectionRepository->findSomeFields();
        $servicesDir = $serviceDirectionRepository->findSomeFields();
        $departement = $departementDirectionRepository->findSomeFields();
        $direction = $directionRepository->findSomeFields();
        $categorie =  $categorieRepository->showSomeFields();

        if ($responsabilite == 0){
            $titre2 = '';
            $last_posteusers2 = [];
            $last_posteusers3 = [];
            if ($filtre == 'tous'){
                $last_posteusers = $userRepository->showAllWithSomeFields();

                $chefs = [];

            }elseif ($filtre == 'enfonction'){
                $last_posteusers = $userRepository->showAllEnfonction();
                $chefs = [];
                //dd($last_posteusers);
            }elseif ($filtre == 'horsservice'){
                $last_posteusers = $userRepository->showAllNotEnfonction();
                $chefs = [];
            }else{ //par statut (stagiaire, contractuel, titulaire ...)
                /*
                 * 1 => titulaire
                 * 2 => contractuel
                 * 3 => staggiaire
                 * 4 => probatoire
                 * 5 => temporaire
                 */
                $last_posteusers = $userRepository->showAllByStatut($filtre);
                $chefs = [];
                //dd($filtre);
            }
        }elseif ($responsabilite == 1){ //Par poste (fonction)
            if ($filtre){
                $last_posteusers = $userRepository->showAllByResponsabilite($filtre);
                $chefs = [];
                $last_posteusers2 = [];
                $last_posteusers3 = [];
                $poste = $posteRepository->find($filtre);
                $titre2 = $poste->getDesignation();
            }
        }elseif($responsabilite== 2 ){ //Service de direction
            $serviceDirection = $serviceDirectionRepository->find($filtre);
            $last_posteusers = $posteUserRepository->showAgentOfScdir($serviceDirection);
            $svc = $serviceDirectionRepository->find($filtre);
            $titre2 = $svc->getService()->getDesignation();
            $chefs = [];
            $last_posteusers2 = [];
            $last_posteusers3 = [];
        }elseif($responsabilite== 3 ){ //Service de département
            $serviceDepartementdirection = $serviceDepartementdirectionRepository->find($filtre);
            $last_posteusers = $posteUserRepository->showAgentOfSvcdpt($serviceDepartementdirection);
            $svc = $serviceDepartementdirectionRepository->find($filtre);
            $titre2 = $svc->getService()->getDesignation();
            $chefs = [];
            $last_posteusers2 = [];
            $last_posteusers3 = [];
        }elseif ($responsabilite == 4){//departement
            $departementdirection = $departementDirectionRepository->find($filtre);
            $last_posteusers = $posteUserRepository->showAgentOfDiffenrentSvcOfDpt($departementdirection);
            $chefs = $posteUserRepository->showAgentOfDptOnly($departementdirection);
            $dpt = $departementDirectionRepository->find($filtre);
            $titre2 = $dpt->getDepartement()->getDesignation();
            $last_posteusers2 = [];
            $last_posteusers3 = [];
        }elseif ($responsabilite == 5){//direction
            $dir = $directionRepository->find($filtre);
            $titre2 = $dir->getDesignation();
            $last_posteusers = $posteUserRepository->showAgentOfDiffenrentSvcOfDir($dir);
            $last_posteusers2 = $posteUserRepository->showAgentOfDiffenrentSvcOfDptdir($dir);
            $last_posteusers3 = $posteUserRepository->showAgentOfDiffenrentDptOfDir($dir);
            $chefs = $posteUserRepository->showAgentOfdir($dir);
        }elseif ($responsabilite == 6){//Catégorie (Cadre de gestion, cadre supérieur, ...)
            $cat = $categorieRepository->find($filtre);
            $titre2 = $cat->getDesignation();
            $last_posteusers = $userRepository->showAgentByCategorie($cat);
            $last_posteusers2 = [];
            $last_posteusers3 = [];
            $chefs = [];
        }

        //dd($postes);
        return $this->render('info/info_liste_agents.html.twig',[
            'users_online'          => $users_online,
            'postes'                => $postes,
            'categorie'             => $categorie,
            'servicesDir'           => $servicesDir,
            'servicesDpt'           => $servicesDpt,
            'departement'           => $departement,
            'direction'             => $direction,
            'filtre'                =>$filtre,
            'last_posteusers'       => $last_posteusers,
            'last_posteusers2'      => $last_posteusers2,
            'last_posteusers3'      => $last_posteusers3,
            'chefs'                 => $chefs,
            'titre2'                => $titre2,
            'responsabilite'        => $request->get('responsabilite'),
        ]);
    }

}
