<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserOptionVisibilite;
use App\Form\UserOptionVisibiliteType;
use App\Repository\Admin\EcExperienceAilleursRepository;
use App\Repository\Admin\EcFormationRepository;
use App\Repository\Admin\EcFormationuserRepository;
use App\Repository\AgenceRepository;
use App\Repository\MessageRepository;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\FonctionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserOptionVisibiliteRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class MoncompteController extends AbstractController
{

    /**
     * Affiche le profil de l'agent de CT
     *
     * @Route("/moncompte", name="moncompte_index")
     */
    public function index(

        MessageRepository $alerteAdministrateurRepository,
        PosteUserRepository $posteUserRepository,
        Security $security,
        EcFormationuserRepository $ecFormationuserRepository,
        EcExperienceAilleursRepository $ecExperienceAilleursRepository,
        FonctionRepository $fonctionRepository,
        //PosteUserServiceRepository $posteUserServiceRepository,
        //PosteUserDepartementRepository $posteUserDepartementRepository,
        //PosteUserDirectionRepository $posteUserDirectionRepository,
        UserOptionVisibiliteRepository $userOptionVisibiliteRepository,
        ObjectManager $manager
    )
    {

        $user = $security->getUser();
        $poste = $posteUserRepository->findLastByUser($user);

        $alertes = $alerteAdministrateurRepository->findAll();

        $userOptionVisibilite = $userOptionVisibiliteRepository->findByUser($this->getUser());

        if (!$userOptionVisibilite)
        {
            $optionVisibilite = new UserOptionVisibilite();
            $optionVisibilite->setUser($this->getUser())
                            ->setOptUsername(1)
                            ->setOptEmail(1)
                            ->setOptEnabled(1)
                            ->setOptLastLogin(1)
                            ->setOptLastActivityAt(1)
                            ->setOptLoginCount(1)
                            ->setOptFirstLogin(1)
                            ->setOptMatricule(1)
                            ->setOptNom(1)
                            ->setOptPrenom(1)
                            ->setOptAdresse(0)
                            ->setOptSexe(1)
                            ->setOptDateNaissanceAt(0)
                            ->setOptPhoto(1)
                            ->setOptTel(0)
                            ->setOptIndiceGroupe(0)
                            ->setOptIndiceCategorieNiveau(0)
                            ->setOptIndiceEchelon(0)
                            ->setOptEnligne(1)
            ;

            $manager->persist($optionVisibilite);
            $manager->flush();
        }

        //Formation et expérience dans la page de profil
         $nbrFormationsAcademique = $ecFormationuserRepository->countFormationsByUser($user);
         $nbrFormationsNonAcademique = $ecExperienceAilleursRepository->countFormationsByUser($user);
         $nbrExperiencesAilleurs = $ecExperienceAilleursRepository->countAutresExperiencesByUser($user);

         //Tâches attribuées
        $tachesDeFonction = $fonctionRepository->findByUser($user);
        foreach ($tachesDeFonction as $value){
            $tachesDeFonction = $value;
        }

        //dd($tachesDeFonction);

        return $this->render('moncompte/profil.html.twig', [
            'alertes'                   => $alertes,
            'poste'                     => $poste,
            'user_option_visibilites'   => $userOptionVisibilite,
            'user'                      => $user,
            'nbrFormationsAcademique'   => $nbrFormationsAcademique,
            'nbrFormationsNonAcademique'=> $nbrFormationsNonAcademique,
            'nbrExperienceAilleurs'     => $nbrExperiencesAilleurs,
            'tachesDeFonction'     => $tachesDeFonction,
        ]);
    }

    /**
     * Affiche la page d'accueil du compte de l'agent de CT
     *
     * @Route("/moncompte/home", name="moncompte_home")
     */
    public function home(
        Request $request,
        MessageRepository $alerteAdministrateurRepository,
        UserRepository $userRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        AgenceRepository $agenceRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteUserRepository $posteUserRepository
    )
    {
        //Alertes
        $alertes = $alerteAdministrateurRepository->findAll();
        //Nombre d'Agences
        $AgencesNgz = $agenceRepository->countAgence(1);
        $AgencesMwa = $agenceRepository->countAgence(2);
        $AgencesNdz = $agenceRepository->countAgence(3);
        //Nombre des Agents de CT
        $nbrAgentsNgz = $userRepository->countAgentsNgz();
        $nbrAgentsMwa = $userRepository->countAgentsMwa();
        $nbrAgentsNdz = $userRepository->countAgentsNdz();
        $nbrFemmes = $userRepository->countFemmes();
        $nbrHommes = $userRepository->countHommes();
        $nbrAgents = $userRepository->countAgents();
//        $nbrAgentsNgz = $userRepository->countAgentsIle(1);
        //Nombre de Cadre
        $nbrCadreDirigeants = $userRepository->countCadre(1);
        $nbrCadreSuperieurs = $userRepository->countCadre(2);
        $nbrCadreGestions = $userRepository->countCadre(3);
        $nbrCadreExecutions = $userRepository->countCadre(4);
        //Nombre de Statut
        $statutTitulaires = $userRepository->countStatut(1);
        $statutContractuels = $userRepository->countStatut(2);
        $statutStagiaires = $userRepository->countStatut(3);
        $statutProbatoires = $userRepository->countStatut(4);
        $statutTemporaires = $userRepository->countStatut(5);
        //Services
        $services = $serviceRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $serviceDirections = $serviceDirectionRepository->findAll();
        //Departements
        $departements = $departementRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        //Nombre de Departements
//        $departementsNgz = $departementRepository->countDept(1);
        //Directions
        $directions = $directionRepository->findAll();
        //Nombre de Directions
        $directionsNgz = $directionRepository->countDir(1);
        $directionsMwa = $directionRepository->countDir(2);
        $directionsNdz = $directionRepository->countDir(3);
        $countProjetsWithoutId8Id10 = $directionRepository->countProjetsNgz();

        $users = ''; $agentsH = ''; $agentsF = ''; $cadreDirigeants = ''; $cadreSuperieurs = ''; $cadreGestions = '';
        $cadreExecutions = ''; $titulaires = ''; $contractuels = ''; $stagiaires = ''; $probatoires = ''; $temporaires = '';
        $agentsNgazidja = ''; $agentsMwali = ''; $agentsNdzuani = ''; $agencesNgazidja = ''; $agencesMwali = ''; $agencesNdzuani = '';
        $service_dpt_dir = ''; $service_dir = ''; $departement_dir = ''; $direction = ''; $s_dpt = ''; $directeur = '';
        $servicesDeptNgazidja = ''; $servicesDeptMwali = ''; $servicesDeptNdzuani = ''; $agent = ''; $chef_dpt = '';
        $servicesDirNgazidja = ''; $servicesDirMwali = ''; $servicesDirNdzuani = '';
        $deptsNgazidja = ''; $deptsMwali = ''; $deptsNdzuani = ''; $services_lies = '';
        $dirsNgazidja = ''; $dirMwali = ''; $dirNdzuani = ''; $departements_lies = '';

        //Delai considéré qu'un agent est online/offline
        $delai = new \DateTime('2 minutes ago');

        //Agents de CT
        if ($request->get('agents')) {
            if ($request->get('agents') == 'h') $agentsH = $userRepository->findAllBySexe('M');
            elseif ($request->get('agents') == 'f') $agentsF = $userRepository->findAllBySexe('F');
            else $users = $userRepository->findAllAgentsEnFonction();
        //Cadres professionnels
        } elseif ($request->get('cadres')) {
            if ($request->get('cadres') == 'dirigeants') $cadreDirigeants = $userRepository->findAllByCategorie(1);
            elseif ($request->get('cadres') == 'superieurs') $cadreSuperieurs = $userRepository->findAllByCategorie(2);
            elseif ($request->get('cadres') == 'gestions') $cadreGestions = $userRepository->findAllByCategorie(3);
            elseif ($request->get('cadres') == 'executions') $cadreExecutions = $userRepository->findAllByCategorie(4);
            else $users = $userRepository->findAllAgentsEnFonction();
        //Statuts professionnels
        } elseif ($request->get('statut')) {
            if ($request->get('statut') == 'titulaires') $titulaires = $userRepository->findAllByStatut(1);
            elseif ($request->get('statut') == 'contractuels') $contractuels = $userRepository->findAllByStatut(2);
            elseif ($request->get('statut') == 'stagiaires') $stagiaires = $userRepository->findAllByStatut(3);
            elseif ($request->get('statut') == 'probatoires') $probatoires = $userRepository->findAllByStatut(4);
            elseif ($request->get('statut') == 'temporaires') $temporaires = $userRepository->findAllByStatut(5);
            else $users = $userRepository->findAllAgentsEnFonction();
        //Agents d'une île
        } elseif ($request->get('ile_agent')) {
            if ($request->get('ile_agent') == 'Ngazidja') $agentsNgazidja = $userRepository->findAgentByIle('Ngazidja');
            elseif ($request->get('ile_agent') == 'Mwali') $agentsMwali = $userRepository->findAgentByIle('Mwali');
            elseif ($request->get('ile_agent') == 'Ndzuani') $agentsNdzuani = $userRepository->findAgentByIle('Ndzuani');
            else $users = $userRepository->findAllAgentsEnFonction();
        //Agences d'une île
        } elseif ($request->get('ile_agence')) {
            $service_dpt_dir = $serviceDepartementdirectionRepository->findSomeFields();
            $service_dir = $serviceDirectionRepository->findSomeFields();
            $departement_dir = $departementDirectionRepository->findSomeFields();
            $direction = $directionRepository->findSomeFields();

            if ($request->get('ile_agence') == 'Ngazidja') $agencesNgazidja = $agenceRepository->findAllByIle(1);
            elseif ($request->get('ile_agence') == 'Mwali') $agencesMwali = $agenceRepository->findAllByIle(2);
            elseif ($request->get('ile_agence') == 'Ndzuani') $agencesNdzuani = $agenceRepository->findAllByIle(3);
            else $users = $userRepository->findAllAgentsEnFonction();
        //Services/Dept d'une île
        } elseif ($request->get('ile_service_dept')) {
            $agent = $posteUserRepository->showAgentOfSvcdptSansParams();
            $s_dpt = $serviceDepartementdirectionRepository->findServiceOfDepartement();
            $chef_dpt = $posteUserRepository->showAgentOfDptOnlySansParam();
            $directeur = $posteUserRepository->showAgentOfdirSansParam();

            if ($request->get('ile_service_dept') == 'Ngazidja') $servicesDeptNgazidja = $serviceDepartementdirectionRepository->findAllServicesDeptByIle(1);
            elseif ($request->get('ile_service_dept') == 'Mwali') $servicesDeptMwali = $serviceDepartementdirectionRepository->findAllServicesDeptByIle(2);
            elseif ($request->get('ile_service_dept') == 'Ndzuani') $servicesDeptNdzuani = $serviceDepartementdirectionRepository->findAllServicesDeptByIle(3);
            else $users = $userRepository->findAllAgentsEnFonction();
        //Services/Dir d'une île
        } elseif ($request->get('ile_service_dir')) {
            $agent = $posteUserRepository->showAgentOfScdirSansParam();
            $s_dpt = $serviceDepartementdirectionRepository->findServiceOfDepartement();
            $chef_dpt = $posteUserRepository->showAgentOfDptOnlySansParam();
            $directeur = $posteUserRepository->showAgentOfdirSansParam();

            if ($request->get('ile_service_dir') == 'Ngazidja') $servicesDirNgazidja = $serviceDirectionRepository->findAllServicesDirByIle(1);
            elseif ($request->get('ile_service_dir') == 'Mwali') $servicesDirMwali = $serviceDirectionRepository->findAllServicesDirByIle(2);
            elseif ($request->get('ile_service_dir') == 'Ndzuani') $servicesDirNdzuani = $serviceDirectionRepository->findAllServicesDirByIle(3);
            else $users = $userRepository->findAllAgentsEnFonction();
        //Départements d'une île
        } elseif ($request->get('ile_dept')) {
            $agent = $posteUserRepository->showAgentOfDptdirSansParam();
            $services_lies = $serviceDepartementdirectionRepository->findServiceOfDepartement();

            if ($request->get('ile_dept') == 'Ngazidja') $deptsNgazidja = $departementDirectionRepository->findAllDeptByIle(1);
            elseif ($request->get('ile_dept') == 'Mwali') $deptsMwali = $departementDirectionRepository->findAllDeptByIle(2);
            elseif ($request->get('ile_dept') == 'Ndzuani') $deptsNdzuani = $departementDirectionRepository->findAllDeptByIle(3);
            else $users = $userRepository->findAllAgentsEnFonction();
        //Directions d'une île
        } elseif ($request->get('ile_dir')) {
            $agent = $posteUserRepository->showAgentOfdirSansParam();
            $departements_lies = $departementDirectionRepository->findDepartementOfDir();

            if ($request->get('ile_dir') == 'Ngazidja') $dirsNgazidja = $directionRepository->findAllDirByIle(1);
            elseif ($request->get('ile_dir') == 'Mwali') $dirMwali = $directionRepository->findAllDirByIle(2);
            elseif ($request->get('ile_dir') == 'Ndzuani') $dirNdzuani = $directionRepository->findAllDirByIle(3);
            else $users = $userRepository->findAllAgentsEnFonction();
        } else {
            $users = $userRepository->findAllAgentsEnFonction();
        }

        return $this->render('moncompte/home.html.twig', [
            'alertes' => $alertes,
            'nbrAgencesNgz' => $AgencesNgz,
            'nbrAgencesMwa' => $AgencesMwa,
            'nbrAgencesNdz' => $AgencesNdz,
            'nbrFemmes' => $nbrFemmes,
            'nbrHommes' => $nbrHommes,
            'nbrAgents' => $nbrAgents,
            'nbrAgentsNgz' => $nbrAgentsNgz,
            'nbrAgentsMwa' => $nbrAgentsMwa,
            'nbrAgentsNdz' => $nbrAgentsNdz,
            'nbrDirigeants' => $nbrCadreDirigeants,
            'nbrSuperieurs' => $nbrCadreSuperieurs,
            'nbrGestions' => $nbrCadreGestions,
            'nbrExecutions' => $nbrCadreExecutions,
            'nbrTitulaire' => $statutTitulaires,
            'nbrContractuel' => $statutContractuels,
            'nbrStagiaire' => $statutStagiaires,
            'nbrProbatoire' => $statutProbatoires,
            'nbrTemporaire' => $statutTemporaires,
//            'nbrDeptNgz' => $departementsNgz,
            'nbrDirNgz' => $directionsNgz,
            'nbrDirMwa' => $directionsMwa,
            'nbrDirNdz' => $directionsNdz,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirections,
            'departement_directions' => $departementDirections,
//            'agences' => $agenceRepository->findAll(),
//            'posteuser_directions' => $posteuserDirectionRepository->findAll(),
            'poste_users' => $posteUserRepository->findAll(),
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'countProjetsWithoutId8Id10' => $countProjetsWithoutId8Id10,
            'users' => $users,
            'agentsH' => $agentsH,
            'agentsF' => $agentsF,
            'cadreDirigeants' => $cadreDirigeants,
            'cadreSuperieurs' => $cadreSuperieurs,
            'cadreGestions' => $cadreGestions,
            'cadreExecutions' => $cadreExecutions,
            'titulaires' => $titulaires,
            'contractuels' => $contractuels,
            'stagiaires' => $stagiaires,
            'probatoires' => $probatoires,
            'temporaires' => $temporaires,
            'agentsNgazidja' => $agentsNgazidja,
            'agentsMwali' => $agentsMwali,
            'agentsNdzuani' => $agentsNdzuani,
            'delai' => $delai,
            'agencesNgazidja' => $agencesNgazidja,
            'agencesMwali' => $agencesMwali,
            'agencesNdzuani' => $agencesNdzuani,
            'service_dpt_dir' => $service_dpt_dir,
            'service_dir' => $service_dir,
            'departement_dir' => $departement_dir,
            'direction' => $direction,
            'servicesDeptNgazidja' => $servicesDeptNgazidja,
            'servicesDeptMwali' => $servicesDeptMwali,
            'servicesDeptNdzuani' => $servicesDeptNdzuani,
            'servicesDirNgazidja' => $servicesDirNgazidja,
            'servicesDirMwali' => $servicesDirMwali,
            'servicesDirNdzuani' => $servicesDirNdzuani,
            'deptsNgazidja' => $deptsNgazidja,
            'deptsMwali' => $deptsMwali,
            'deptsNdzuani' => $deptsNdzuani,
            'services_lies' => $services_lies,
            'dirsNgazidja' => $dirsNgazidja,
            'dirMwali' => $dirMwali,
            'dirNdzuani' => $dirNdzuani,
            'departements_lies' => $departements_lies,
            'agent' => $agent,
            's_dpt' => $s_dpt,
            'chef_dpt' => $chef_dpt,
            'directeur' => $directeur,
        ]);
    }

    /**
     * Permet de changer son statut soit en ligne ou hors ligne
     *
     * @Route("/enligne/{id}", name="moncompte_enligne")
     *
     * @param User $agent
     * @param ObjectManager $manager
     * @param UserRepository $userRepository
     * @return Response
     */
    public function changerStatut(User $agent, ObjectManager $manager, UserRepository $userRepository) : Response
    {
        $user = $this->getUser();
        if (!$user) return $this->json([
            'code' => 403,
            'message' => 'Unautorized'
        ], 403);

        if ($agent->isEnligne()){
            $agent_ct = $userRepository->findOneBy([
                'id' => $user]);

            $agent_ct->setEnligne(false);
            $manager->flush();

            return $this->json([
                'code' => 200,
                'message' => 'vous venez de vous mettre hors ligne!'
            ], 200);
        }

        $agent->setEnligne(true);

        $manager->persist($agent);
        $manager->flush();

        return $this->json([
            'code' => 200,
            'message' => 'vous venez de vous mettre en ligne!'
        ], 200);
    }


    /**
     * Permet de changer la visibilité des données personelles
     *
     * @Route("/confidentialite/{id}/edit", name="moncompte_visibilite")
     */
    public function changerVisibilite(Request $request, UserOptionVisibilite $userOptionVisibilite, ObjectManager $manager)
    {
        $formUserOptionVisibilites = $this->createForm(UserOptionVisibiliteType::class, $userOptionVisibilite);
        $formUserOptionVisibilites->handleRequest($request);

        if ($formUserOptionVisibilites->isSubmitted() && $formUserOptionVisibilites->isValid())
        {
                $manager->persist($userOptionVisibilite);
                $manager->flush();

                $this->addFlash('success', 'Modifications effectuées!');
                return $this->redirectToRoute('moncompte_index');
        }

        return $this->render('moncompte/confidentialite.html.twig', [
            'formUserOptionVisibilites' => $formUserOptionVisibilites->createView(),
            'user_option_visibilite' => $userOptionVisibilite,
        ]);
    }


    /**
     * Ecoles | Universités | Facultés
     *
     * @Route("/moncompte/mesformations", name="moncompte_mesformations", methods={"GET"})
     */
    public function mesFormationsAcademiques(Request $request,
                                                     EcFormationRepository $ecFormationRepository,
                                                     EcFormationuserRepository $ecFormationuserRepository,
                                                     UserRepository $userRepository, Security $security): Response
    {
        $user = $security->getUser();
        $formations = $ecFormationuserRepository->findByUser($user);
        return $this->render('moncompte/mes_formations_academiques.html.twig', [
            'user'          => $user,
            'formations'    => $formations,
        ]);
    }

    /**
     * Formations non académiques
     *
     * @Route("/moncompte/mesformations_non_academiques", name="moncompte_mesformations_non_academiques", methods={"GET"})
     */
    public function mesFormationsNonAcademiques(Request $request,
                                                EcExperienceAilleursRepository $ecExperienceAilleursRepository,
                                                UserRepository $userRepository,
                                                Security $security): Response
    {
        $user = $security->getUser();
        $formations = $ecExperienceAilleursRepository->findByUser($user);
        //dd($formations);
        return $this->render('moncompte/mes_formations_non_academiques.html.twig', [
            'user'          => $user,
            'formations'    => $formations,
        ]);
    }

    /**
     * Expériences Ailleurs (stage , seminaires /....)
     *
     * @Route("/moncompte/experiences_ailleurs", name="moncompte_experiences_ailleurs", methods={"GET"})
     */
    public function mesExperiencesAilleurs(Request $request,
                                            EcExperienceAilleursRepository $ecExperienceAilleursRepository,
                                            Security $security): Response
    {
        $user = $security->getUser();
        $formations = $ecExperienceAilleursRepository->findAutresExperiencesByUser($user);

        return $this->render('moncompte/mes_formations_non_academiques.html.twig', [
            'user'          => $user,
            'formations'    => $formations,
        ]);
    }

}
