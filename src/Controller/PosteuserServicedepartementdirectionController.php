<?php

namespace App\Controller;

use App\Entity\PosteuserServicedepartementdirection;
use App\Form\PosteuserServicedepartementdirectionType;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/posteuser/servicedepartementdirection")
 */
class PosteuserServicedepartementdirectionController extends AbstractController
{
    /**
     * @Route("/", name="posteuser_servicedepartementdirection_index", methods={"GET"})
     */
    public function index(PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository): Response
    {
        return $this->render('posteuser_servicedepartementdirection/index.html.twig', [
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirectionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="posteuser_servicedepartementdirection_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
        $form = $this->createForm(PosteuserServicedepartementdirectionType::class, $posteuserServicedepartementdirection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($posteuserServicedepartementdirection);
            $entityManager->flush();

            return $this->redirectToRoute('posteuser_servicedepartementdirection_index');
        }

        return $this->render('posteuser_servicedepartementdirection/new.html.twig', [
            'posteuser_servicedepartementdirection' => $posteuserServicedepartementdirection,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="posteuser_servicedepartementdirection_show", methods={"GET"})
     */
    public function show(PosteuserServicedepartementdirection $posteuserServicedepartementdirection): Response
    {
        return $this->render('posteuser_servicedepartementdirection/show.html.twig', [
            'posteuser_servicedepartementdirection' => $posteuserServicedepartementdirection,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="posteuser_servicedepartementdirection_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PosteuserServicedepartementdirection $posteuserServicedepartementdirection): Response
    {
        $form = $this->createForm(PosteuserServicedepartementdirectionType::class, $posteuserServicedepartementdirection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('posteuser_servicedepartementdirection_index', [
                'id' => $posteuserServicedepartementdirection->getId(),
            ]);
        }

        return $this->render('posteuser_servicedepartementdirection/edit.html.twig', [
            'posteuser_servicedepartementdirection' => $posteuserServicedepartementdirection,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="posteuser_servicedepartementdirection_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PosteuserServicedepartementdirection $posteuserServicedepartementdirection): Response
    {
        if ($this->isCsrfTokenValid('delete'.$posteuserServicedepartementdirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($posteuserServicedepartementdirection);
            $entityManager->flush();
        }

        return $this->redirectToRoute('posteuser_servicedepartementdirection_index');
    }
}
