<?php

namespace App\Controller;

use App\Entity\Departement;
use App\Entity\DepartementDirection;
use App\Form\DepartementDirectionType;
use App\Form\DepartementType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/ct/departements")
 */
class DepartementController extends AbstractController
{
    /**
     * @Route("/", name="departement_index", methods={"GET"})
     */
    public function index(DepartementRepository $departementRepository,
                          ServiceRepository $serviceRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('departement/index.html.twig', [
            'departements' => $departementRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="departement_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        DepartementRepository $departementRepository,
                        ServiceRepository $serviceRepository,
                        DirectionRepository $directionRepository): Response
    {
        $departement = new Departement();
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($departement);
            $entityManager->flush();

            $this->addFlash('success','Enregistrement effectué avec succès');
            return $this->redirectToRoute('departement_index');
        }

        return $this->render('departement/new.html.twig', [
            'departement' => $departement,
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="departement_show", methods={"GET"})
     */
    public function show(Departement $departement,
                         DepartementRepository $departementRepository,
                         ServiceRepository $serviceRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('departement/show.html.twig', [
            'departement' => $departement,
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'services' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="departement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository,
                         Departement $departement): Response
    {
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        $departementdirection = new DepartementDirection();
        //récupération de l'id département pour fixer le département a sa valeur par defaut
        $id_departement = $departementRepository->find($departement->getId());
        $departementdirection = $departementdirection->setDepartement($id_departement);

        $form_addDpt = $this->createForm(DepartementDirectionType::class, $departementdirection);
        $form_addDpt->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Modifications effectuées avec succès');
            return $this->redirectToRoute('departement_index', [
                'id' => $departement->getId(),
            ]);
        }
        if ($form_addDpt->isSubmitted() && $form_addDpt->isValid()){
            $this->getDoctrine()->getManager()->persist($departementdirection);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            return $this->redirectToRoute('departement_direction_index');
        }

        return $this->render('departement/edit.html.twig', [
            'departement' => $departement,
            'form_dpt' => $form->createView(),
            'form_addDpt' => $form_addDpt->createView(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'services' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="departement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Departement $departement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$departement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($departement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('departement_index');
    }
}
