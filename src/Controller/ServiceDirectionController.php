<?php

namespace App\Controller;

use App\Entity\ServiceDirection;
use App\Form\ServiceDirectionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\Evenements\EvStageRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ServiceDirectionController extends AbstractController
{
    /**
     * @Route("/admin/ct/svcdir/", name="service_direction_index", methods={"GET"})
     */
    public function index(ServiceDirectionRepository $serviceDirectionRepository,
                          EvNominationRepository $evNominationRepository,
                          EvRecrutementRepository $evRecrutementRepository,
                          EvTitularisationRepository $evTitularisationRepository,
                          EvDemisedefonctionRepository $evDemisedefonctionRepository,
                          EvPromotioninterneRepository $evPromotioninterneRepository,
                          EvLicenciementRepository $evLicenciementRepository,
                          EvStageRepository $evStageRepository,
                          EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
                          EvAffectationRepository $evAffectationRepository,
                          EvDemissionRepository $evDemissionRepository,
                          EvReintegrationRepository $evReintegrationRepository,
                          EvMiseapiedRepository $evMiseapiedRepository
    ): Response
    {
        return $this->render('service_direction/index.html.twig', [
            'service_directions'                => $serviceDirectionRepository->findSomeFields(),
            'ev_nomination'                     => $evNominationRepository->findSomeFields(),
            'ev_recrutement'                    => $evRecrutementRepository->findSomeFields(),
            'ev_titularisation'                 => $evTitularisationRepository->findSomeFields(),
            'ev_demisedefonction'               => $evDemisedefonctionRepository->findSomeFields(),
            'ev_promotioninterne'               => $evPromotioninterneRepository->findSomeFields(),
            //'ev_licenciement'                   => $evLicenciementRepository->findSomeFields(),
            'ev_stage'                          => $evStageRepository->findSomeFields(),
            'ev_renouvellementdecontrat'        => $evRenouvellementdecontratRepository->findSomeFields(),
            'ev_affectation'                    => $evAffectationRepository->findSomeFields(),
            'ev_demission'                      => $evDemissionRepository->findSomeFields(),
            'ev_reintegration'                  => $evReintegrationRepository->findSomeFields(),
            'ev_miseapied'                      => $evMiseapiedRepository->findSomeFields(),
        ]);
    }

    /**
     * @Route("/admin/ct/svcdir/new", name="service_direction_new", methods={"GET","POST"})
     */

    public function new(Request $request): Response
    {
        $serviceDirection = new ServiceDirection();
        $form = $this->createForm(ServiceDirectionType::class, $serviceDirection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($serviceDirection);
            $entityManager->flush();

            return $this->redirectToRoute('service_direction_index');
        }

        return $this->render('service_direction/new.html.twig', [
            'service_direction' => $serviceDirection,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/admin/ct/svcdir/{id}", name="service_direction_show", methods={"GET"})
     */
    public function show(ServiceDirection $serviceDirection,
                         ServiceRepository $serviceRepository,
                         DirectionRepository $directionRepository,
                         DepartementRepository $departementRepository): Response
    {
        return $this->render('service_direction/show.html.twig', [
            'service_direction' => $serviceDirection,
            'services' => $serviceRepository->findAll(),
            'departements' => $serviceRepository->findAll(),
            'directions' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/ct/svcdir/{id}/edit", name="service_direction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         ServiceDirection $serviceDirection,
                         ServiceRepository $serviceRepository,
                         DirectionRepository $directionRepository,
                         DepartementRepository $departementRepository): Response
    {
        $form = $this->createForm(ServiceDirectionType::class, $serviceDirection);
        $form->handleRequest($request);

        $current_serv = $serviceDirection->getService();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('service_direction_index', [
                'id' => $serviceDirection->getId(),
            ]);
        }

        return $this->render('service_direction/edit.html.twig', [
            'service_direction' => $serviceDirection,
            'current_serv' => $current_serv,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $serviceRepository->findAll(),
            'directions' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/ct/svcdir/{id}", name="service_direction_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ServiceDirection $serviceDirection): Response
    {
        if ($this->isCsrfTokenValid('delete'.$serviceDirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($serviceDirection);
            $entityManager->flush();
        }

        return $this->redirectToRoute('service_direction_index');
    }

    /**
     * @Route("admin/ct/svcdir/{id}", name="service_direction_archive", methods={"ARCHIVE"})
     */
    public function archive(Request $request, ServiceDirection $serviceDirection): Response
    {
        if ($this->isCsrfTokenValid('archive'.$serviceDirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $serviceDirection->setFonctionnel(0);
            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, le service n'existe plus");
        return $this->redirectToRoute('service_direction_index');
    }

    /**
     * @Route("societe/svcdir/info", name="service_direction_info", methods={"GET"})
     */
    public function info(ServiceDirectionRepository $serviceDirectionRepository,
                         DirectionRepository $directionRepository,
                         PosteUserRepository $posteUserRepository,
                         UserRepository $userRepository): Response
    {
        $agent = $posteUserRepository->showAgentOfScdirSansParam();
        //$s_dpt = $serviceDepartementdirectionRepository->findServiceOfDepartement();
        $directeur = $posteUserRepository->showAgentOfdirSansParam();
        //dd($agent);
        return $this->render('service_direction/info.html.twig', [
            'service_directions' => $serviceDirectionRepository->findSomeFields(),
            'agent'                         =>$agent,
            'directeur'                     =>$directeur,
        ]);
    }

    /**
     * Affiche les détails du service de direction
     *
     * @Route("societe/svcdir/info/{id}", name="service_direction_info_detail", methods={"GET"})
     */
    public function infoDetail(ServiceDirectionRepository $serviceDirectionRepository,
                               DepartementRepository $departementRepository,
                               DirectionRepository $directionRepository,
                               ServiceRepository $serviceRepository,
                               PosteUserRepository $posteUserRepository,
                               UserRepository $userRepository, Request $request): Response
    {
        $url_id = $request->get('id');
        $servicedirection = $serviceDirectionRepository->find($url_id);
        $agents = $posteUserRepository->showAgentOfSvcdir($servicedirection);
        return $this->render('service_direction/info_service.html.twig', [
            'servicedirection'                => $servicedirection,
            'agents'                          => $agents,
        ]);
    }
}
