<?php

namespace App\Controller\GSitesEnerg;

use App\Entity\GSitesEnerg\Intervention;
use App\Entity\GSitesEnerg\Materiel;
use App\Form\GSitesEnerg\InterventionType;
use App\Repository\DepartementDirectionRepository;
use App\Repository\GSitesEnerg\InterventionRepository;
use App\Repository\GSitesEnerg\MaterielRepository;
use App\Repository\PosteUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/gestion/sitesenergetiques/intervention")
 */
class InterventionController extends AbstractController
{
    /**
     * @Route("/", name="intervention_index", methods={"GET"})
     */
    public function index(InterventionRepository $interventionRepository): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/intervention_index.html.twig', [
            'interventions' => $interventionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="intervention_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        DepartementDirectionRepository $departementDirectionRepository,
                        Security $security,
                        MaterielRepository $materielRepository,
                        PosteUserRepository $posteUserRepository): Response
    {
        $intervention = new Intervention();

        //test de l'envoi d'une varriable dans l'url
        if ($request->get('id_materiel')){
            $materiel = $materielRepository->find($request->get('id_materiel'));
            $intervention = $intervention->addMateriel($materiel);
        }else{
            $materiel = '';
        }

        $intervention = $intervention->setSaisipar($security->getUser());
        $form = $this->createForm(InterventionType::class, $intervention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($intervention);
            $entityManager->flush();

            if ($materiel instanceof Materiel){
                return $this->redirectToRoute('sites_energetiques_show',  array('id' => $materiel->getBaieenergetique()->getSiteenergetique()->getId()));
            }else{
                return $this->redirectToRoute('intervention_index');
            }
        }

        return $this->render('g_sites_energ/sites_energetiques/intervention_new.html.twig', [
            'intervention' => $intervention,
            'form' => $form->createView(),
            'materiel' => $materiel,
        ]);
    }

    /**
     * @Route("/{id}", name="intervention_show", methods={"GET"})
     */
    public function show(Intervention $intervention): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/intervention_show.html.twig', [
            'intervention' => $intervention,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="intervention_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Intervention $intervention): Response
    {
        $form = $this->createForm(InterventionType::class, $intervention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('intervention_index', [
                'id' => $intervention->getId(),
            ]);
        }

        return $this->render('g_sites_energ/sites_energetiques/intervention_edit.html.twig', [
            'intervention' => $intervention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="intervention_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Intervention $intervention): Response
    {
        if ($this->isCsrfTokenValid('delete'.$intervention->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($intervention);
            $entityManager->flush();
        }

        return $this->redirectToRoute('intervention_index');
    }
}
