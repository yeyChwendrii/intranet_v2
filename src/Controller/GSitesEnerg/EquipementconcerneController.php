<?php

namespace App\Controller\GSitesEnerg;

use App\Entity\GSitesEnerg\Equipementconcerne;
use App\Form\GSitesEnerg\EquipementconcerneType;
use App\Repository\GSitesEnerg\EquipementconcerneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/sitesenergetiques/equipementconcerne")
 */
class EquipementconcerneController extends AbstractController
{
    /**
     * @Route("/", name="equipementconcerne_index", methods={"GET"})
     */
    public function index(EquipementconcerneRepository $equipementconcerneRepository): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/equipementconcerne_index.html.twig', [
            'equipementconcernes' => $equipementconcerneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="equipementconcerne_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $equipementconcerne = new Equipementconcerne();
        $form = $this->createForm(EquipementconcerneType::class, $equipementconcerne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($equipementconcerne);
            $entityManager->flush();

            return $this->redirectToRoute('equipementconcerne_index');
        }

        return $this->render('g_sites_energ/sites_energetiques/equipementconcerne_new.html.twig', [
            'equipementconcerne' => $equipementconcerne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="equipementconcerne_show", methods={"GET"})
     */
    public function show(Equipementconcerne $equipementconcerne): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/equipementconcerne_show.html.twig', [
            'equipementconcerne' => $equipementconcerne,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="equipementconcerne_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Equipementconcerne $equipementconcerne): Response
    {
        $form = $this->createForm(EquipementconcerneType::class, $equipementconcerne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('equipementconcerne_index', [
                'id' => $equipementconcerne->getId(),
            ]);
        }

        return $this->render('g_sites_energ/sites_energetiques/equipementconcerne_edit.html.twig', [
            'equipementconcerne' => $equipementconcerne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="equipementconcerne_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Equipementconcerne $equipementconcerne): Response
    {
        if ($this->isCsrfTokenValid('delete'.$equipementconcerne->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($equipementconcerne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('equipementconcerne_index');
    }
}
