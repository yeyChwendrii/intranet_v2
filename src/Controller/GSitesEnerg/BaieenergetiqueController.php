<?php

namespace App\Controller\GSitesEnerg;

use App\Entity\GSitesEnerg\Baieenergetique;
use App\Entity\GSitesEnerg\SitesEnergetiques;
use App\Form\GSitesEnerg\BaieenergetiqueType;
use App\Repository\GSitesEnerg\BaieenergetiqueRepository;
use App\Repository\GSitesEnerg\SitesEnergetiquesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/sitesenergetiques/baieenergetique")
 */
class BaieenergetiqueController extends AbstractController
{
    /**
     * @Route("/", name="baieenergetique_index", methods={"GET"})
     */
    public function index(BaieenergetiqueRepository $baieenergetiqueRepository): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/baieenergetique_index.html.twig', [
            'baieenergetiques' => $baieenergetiqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="baieenergetique_new", methods={"GET","POST"})
     */
    public function new(Request $request, SitesEnergetiquesRepository $sitesEnergetiquesRepository): Response
    {

        $baieenergetique = new Baieenergetique();

        //test de l'envoi d'une varriable dans l'url
        if ($request->get('id_site')){
            $site = $sitesEnergetiquesRepository->find($request->get('id_site'));
            $baieenergetique = $baieenergetique->setSiteenergetique($site);
        }else{
            $site = '';
        }
        $form = $this->createForm(BaieenergetiqueType::class, $baieenergetique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($baieenergetique);
            $entityManager->flush();

            if ($site instanceof SitesEnergetiques) {
                return $this->redirectToRoute('sites_energetiques_show', array('id' => $site->getId()));
            } else{
                return $this->redirectToRoute('baieenergetique_index');
            }
        }

        return $this->render('g_sites_energ/sites_energetiques/baieenergetique_new.html.twig', [
            'baieenergetique' => $baieenergetique,
            'form' => $form->createView(),
            'site' => $site,
        ]);
    }


    /**
     * @Route("/{id}", name="baieenergetique_show", methods={"GET"})
     */
    public function show(Baieenergetique $baieenergetique): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/baieenergetique_show.html.twig', [
            'baieenergetique' => $baieenergetique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="baieenergetique_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Baieenergetique $baieenergetique): Response
    {
        $form = $this->createForm(BaieenergetiqueType::class, $baieenergetique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('baieenergetique_index', [
                'id' => $baieenergetique->getId(),
            ]);
        }

        return $this->render('g_sites_energ/sites_energetiques/baieenergetique_edit.html.twig', [
            'baieenergetique' => $baieenergetique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="baieenergetique_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Baieenergetique $baieenergetique): Response
    {
        if ($this->isCsrfTokenValid('delete'.$baieenergetique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($baieenergetique);
            $entityManager->flush();
        }

        return $this->redirectToRoute('baieenergetique_index');
    }
}
