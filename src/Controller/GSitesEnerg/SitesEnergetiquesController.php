<?php

namespace App\Controller\GSitesEnerg;

use App\Entity\GSitesEnerg\SitesEnergetiques;
use App\Form\GSitesEnerg\SitesEnergetiquesType;
use App\Repository\DepartementDirectionRepository;
use App\Repository\GSitesEnerg\InterventionRepository;
use App\Repository\GSitesEnerg\SitesEnergetiquesRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/sitesenergetiques/sites")
 */
class SitesEnergetiquesController extends AbstractController
{
    /**
     * @Route("/", name="sites_energetiques_index", methods={"GET"})
     */
    public function index(SitesEnergetiquesRepository $sitesEnergetiquesRepository, DepartementDirectionRepository $departementDirectionRepository,
                          ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                          PosteUserRepository $posteUserRepository): Response
    {
        $nbr_sites_energetiques = $sitesEnergetiquesRepository->countSites();
        $departementdirection = $departementDirectionRepository->find(24);
        $services_lies_volovolo = $serviceDepartementdirectionRepository->findServiceOfDepartement();
        $service_regionale_ngz = $serviceDepartementdirectionRepository->find(168);
        $service_regionale_ndz = $serviceDepartementdirectionRepository->find(169);
        $service_regionale_mwl = $serviceDepartementdirectionRepository->find(170);

        $agents = $posteUserRepository->showAgentOfDptdirSansParam();

        //dd($departementdirection->getTel());
        return $this->render('g_sites_energ/sites_energetiques/index.html.twig', [
            'nbr_sites_energetiques'                => $nbr_sites_energetiques,
            'sites_energetiques'                    => $sitesEnergetiquesRepository->findAll(),
            'departementdirection'                  => $departementdirection,
            'services_lies_volovolo'                => $services_lies_volovolo,
            'service_regionale_ngz'                 => $service_regionale_ngz,
            'service_regionale_ndz'                 => $service_regionale_ndz,
            'service_regionale_mwl'                 => $service_regionale_mwl,
        ]);
    }

    /**
     * @Route("/new", name="sites_energetiques_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sitesEnergetique = new SitesEnergetiques();
        $form = $this->createForm(SitesEnergetiquesType::class, $sitesEnergetique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sitesEnergetique);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            return $this->redirectToRoute('sites_energetiques_index');
        }

        return $this->render('g_sites_energ/sites_energetiques/new.html.twig', [
            'sites_energetique' => $sitesEnergetique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sites_energetiques_show", methods={"GET"})
     */
    public function show(SitesEnergetiques $sitesEnergetique, InterventionRepository $interventionRepository): Response
    {
        $interventions = $interventionRepository->findAll();
        return $this->render('g_sites_energ/sites_energetiques/show.html.twig', [
            'sites_energetique' => $sitesEnergetique,
            'interventions' => $interventions,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sites_energetiques_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SitesEnergetiques $sitesEnergetique): Response
    {
        $form = $this->createForm(SitesEnergetiquesType::class, $sitesEnergetique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sites_energetiques_index', [
                'id' => $sitesEnergetique->getId(),
            ]);
        }

        return $this->render('g_sites_energ/sites_energetiques/edit.html.twig', [
            'sites_energetique' => $sitesEnergetique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sites_energetiques_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SitesEnergetiques $sitesEnergetique): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sitesEnergetique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sitesEnergetique);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sites_energetiques_index');
    }
}
