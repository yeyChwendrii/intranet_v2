<?php

namespace App\Controller\GSitesEnerg;

use App\Entity\GSitesEnerg\Baieenergetique;
use App\Entity\GSitesEnerg\Materiel;
use App\Form\GSitesEnerg\MaterielType;
use App\Repository\GSitesEnerg\BaieenergetiqueRepository;
use App\Repository\GSitesEnerg\MaterielRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("gestion/sitesenergetiques/materiel")
 */
class MaterielController extends AbstractController
{
    /**
     * @Route("/", name="materiel_index", methods={"GET"})
     */
    public function index(MaterielRepository $materielRepository): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/materiel_index.html.twig', [
            'materiels' => $materielRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="materiel_new", methods={"GET","POST"})
     */
    public function new(Request $request, BaieenergetiqueRepository $baieenergetiqueRepository): Response
    {
        $materiel = new Materiel();

        //test de l'envoi d'une varriable dans l'url
        if ($request->get('id_baie')){
            $baie = $baieenergetiqueRepository->find($request->get('id_baie'));
            $materiel = $materiel->setBaieenergetique($baie);
        }else{
            $baie = '';
        }
        $form = $this->createForm(MaterielType::class, $materiel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($materiel);
            $entityManager->flush();

            if ($baie instanceof Baieenergetique){
                return $this->redirectToRoute('baieenergetique_show', array('id' => $baie->getId()));
            }else{
                return $this->redirectToRoute('materiel_index');
            }
        }


        return $this->render('g_sites_energ/sites_energetiques/materiel_new.html.twig', [
            'materiel' => $materiel,
            'form' => $form->createView(),
            'baie' => $baie,
        ]);
    }

    /**
     * @Route("/{id}", name="materiel_show", methods={"GET"})
     */
    public function show(Materiel $materiel): Response
    {
        return $this->render('g_sites_energ/sites_energetiques/materiel_show.html.twig', [
            'materiel' => $materiel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="materiel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Materiel $materiel): Response
    {
        $form = $this->createForm(MaterielType::class, $materiel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('materiel_index', [
                'id' => $materiel->getId(),
            ]);
        }

        return $this->render('g_sites_energ/sites_energetiques/materiel_edit.html.twig', [
            'materiel' => $materiel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="materiel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Materiel $materiel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$materiel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($materiel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('materiel_index');
    }
}
