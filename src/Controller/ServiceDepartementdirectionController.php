<?php

namespace App\Controller;

use App\Entity\ServiceDepartementdirection;
use App\Form\ServiceDepartementdirectionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\Evenements\EvStageRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceDepartementdirectionController extends AbstractController
{
    /**
     * @Route("/admin/ct/svcdptdir/", name="service_departementdirection_index", methods={"GET"})
     */
    public function index(ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                          EvNominationRepository $evNominationRepository,
                          EvRecrutementRepository $evRecrutementRepository,
                          EvTitularisationRepository $evTitularisationRepository,
                          EvDemisedefonctionRepository $evDemisedefonctionRepository,
                          EvPromotioninterneRepository $evPromotioninterneRepository,
                          EvLicenciementRepository $evLicenciementRepository,
                          EvStageRepository $evStageRepository,
                          EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
                          EvAffectationRepository $evAffectationRepository,
                          EvDemissionRepository $evDemissionRepository,
                          EvReintegrationRepository $evReintegrationRepository,
                          EvMiseapiedRepository $evMiseapiedRepository,
                          PosteUserRepository $posteUserRepository): Response
    {
        //dd($posteUserRepository->showAgentOfSvcdptSansParams2());
        //dd($serviceDepartementdirectionRepository->findEvenementlie());
        return $this->render('service_departementdirection/index.html.twig', [
            'service_departementdirections'     => $serviceDepartementdirectionRepository->findSomeFields(),
            'ev_nomination'                     => $evNominationRepository->findSomeFields(),
            'ev_recrutement'                    => $evRecrutementRepository->findSomeFields(),
            'ev_titularisation'                 => $evTitularisationRepository->findSomeFields(),
            'ev_demisedefonction'               => $evDemisedefonctionRepository->findSomeFields(),
            'ev_promotioninterne'               => $evPromotioninterneRepository->findSomeFields(),
            'ev_licenciement'                   => $evLicenciementRepository->findSomeFields(),
            'ev_stage'                          => $evStageRepository->findSomeFields(),
            'ev_renouvellementdecontrat'        => $evRenouvellementdecontratRepository->findSomeFields(),
            'ev_affectation'                    => $evAffectationRepository->findSomeFields(),
            'ev_demission'                      => $evDemissionRepository->findSomeFields(),
            'ev_reintegration'                  => $evReintegrationRepository->findSomeFields(),
            'ev_miseapied'                      => $evMiseapiedRepository->findSomeFields(),
        ]);
    }

    /**
     * @Route("/admin/ct/svcdptdir/new", name="service_departementdirection_new", methods={"GET","POST"})
     */


    public function new(Request $request,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository,
                        ServiceRepository $serviceRepository): Response
    {
        $serviceDepartementdirection = new ServiceDepartementdirection();
        $form = $this->createForm(ServiceDepartementdirectionType::class, $serviceDepartementdirection);
        $form->handleRequest($request);
        //dd($serviceRepository->serviceNotLinked());
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($serviceDepartementdirection);
            $entityManager->flush();

            return $this->redirectToRoute('service_departementdirection_index');
        }

        return $this->render('service_departementdirection/new.html.twig', [
            'service_departementdirection' => $serviceDepartementdirection,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $serviceRepository->findAll(),
            'directions' => $serviceRepository->findAll(),
        ]);
    }


    /**
     * @Route("/admin/ct/svcdptdir/{id}/edit", name="service_departementdirection_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         ServiceDepartementdirection $serviceDepartementdirection,
                         ServiceRepository $serviceRepository,
                         DirectionRepository $directionRepository,
                         DepartementRepository $departementRepository): Response
    {
        $form = $this->createForm(ServiceDepartementdirectionType::class, $serviceDepartementdirection);
        $form->handleRequest($request);

        $current_serv = $serviceDepartementdirection->getService();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('service_departementdirection_index', [
                'id' => $serviceDepartementdirection->getId(),
            ]);
        }

        return $this->render('service_departementdirection/edit.html.twig', [
            'service_departementdirection' => $serviceDepartementdirection,
            'current_serv' => $current_serv,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $serviceRepository->findAll(),
            'directions' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/ct/svcdptdir/{id}", name="service_departementdirection_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ServiceDepartementdirection $serviceDepartementdirection): Response
    {
        if ($this->isCsrfTokenValid('delete'.$serviceDepartementdirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($serviceDepartementdirection);
            $entityManager->flush();
        }

        $this->addFlash('success', 'Liaison supprimer avec succès!');
        return $this->redirectToRoute('service_departementdirection_index');
    }

    /**
     * @Route("admin/ct/svcdptdir/{id}", name="service_departementdirection_archive", methods={"ARCHIVE"})
     */
    public function archive(Request $request, ServiceDepartementdirection $serviceDepartementdirection): Response
    {
        if ($this->isCsrfTokenValid('archive'.$serviceDepartementdirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $serviceDepartementdirection->setFonctionnel(0);
            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, le service n'existe plus");
        return $this->redirectToRoute('service_departementdirection_index');
    }

    /*==================================================
     *              INFOS
     ***************************************************/
    /**
     * @Route("societe/svcdptdir/info", name="service_departementdirection_info", methods={"GET"})
     */
    public function info(ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository,
                          ServiceRepository $serviceRepository,
                            PosteUserRepository $posteUserRepository,
                            UserRepository $userRepository): Response
    {
        $agent = $posteUserRepository->showAgentOfSvcdptSansParams();
        //dd($agent);
        $s_dpt = $serviceDepartementdirectionRepository->findServiceOfDepartement();
        //dd($s_dpt);
        $chef_dpt = $posteUserRepository->showAgentOfDptOnlySansParam();
        //dd($chef_dpt);
        $directeur = $posteUserRepository->showAgentOfdirSansParam();
        //dd($directeur);
        return $this->render('service_departementdirection/info.html.twig', [
            'service_departementdirections' => $serviceDepartementdirectionRepository->findSomeFields(),
            'agent'                         =>$agent,
            's_dpt'                         =>$s_dpt,
            'chef_dpt'                      => $chef_dpt,
            'directeur'                      => $directeur,
        ]);
    }

    /**
     * Affiche les détails du service de département
     *
     * @Route("societe/svcdptdir/info/{id}", name="service_departementdirection_info_detail", methods={"GET"})
     */
    public function infoDetail(ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository,
                         ServiceRepository $serviceRepository,
                         PosteUserRepository $posteUserRepository,
                         UserRepository $userRepository, Request $request): Response
    {
        $url_id = $request->get('id');
        $servicedepartement = $serviceDepartementdirectionRepository->find($url_id);
        $agents = $posteUserRepository->showAgentOfSvcdpt($servicedepartement);
        return $this->render('service_departementdirection/info_service.html.twig', [
            'servicedepartement'                => $servicedepartement,
            'agents'                            => $agents,
        ]);
    }
}
