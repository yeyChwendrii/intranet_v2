<?php

namespace App\Controller;

use App\Entity\AgenceServicedepartementdirection;
use App\Entity\Message;
use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Messagesupprime;
use App\Entity\Messagesvus;
use App\Entity\PosteUser;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\UserSearch;
use App\Form\MessageType;
use App\Form\DepartementDirectionType;
use App\Form\DirectionType;
use App\Form\ServiceDepartementdirectionType;
use App\Form\ServiceDirectionType;
use App\Form\UserSearchType;
use App\Form\UserType;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\MessageRepository;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvAnnulationdedecisionRepository;
use App\Repository\Evenements\EvAvancementnormalRepository;
use App\Repository\Evenements\EvAvertissementRepository;
use App\Repository\Evenements\EvCongeRepository;
use App\Repository\Evenements\EvDecesRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvDisponibiliteRepository;
use App\Repository\Evenements\EvIndemniteforfaitaireRepository;
use App\Repository\Evenements\EvInterimRepository;
use App\Repository\Evenements\EvLevedesuspensionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvReclassementRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvRectificationdunomRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\Evenements\EvReprisedefonctionRepository;
use App\Repository\Evenements\EvRetraiteRepository;
use App\Repository\Evenements\EvStageRepository;
use App\Repository\Evenements\EvSuspensiondesalaireRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\MessagesupprimeRepository;
use App\Repository\MessagesvusRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\TypeMessageRepository;
use App\Repository\UserRepository;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Repository\PosteUserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;


class MoncompteAdministrationController extends AbstractController
{
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/moncompte/admin", name="moncompte_administration")
     */
    public function index(
        PosteUserRepository $posteUserRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository,
        PosteuserDirectionRepository $posteUserDirectionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementRepository,
        ServiceRepository $serviceRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvAffectationRepository $evAffectationRepository,
        EvNominationRepository $evNominationRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        EvTitularisationRepository $evTitularisationRepository,
        UserRepository $userRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        UserInterface $user,
        EvReclassementRepository $evReclassementRepository,
        EvAvancementnormalRepository $evAvancementnormalRepository,
        EvAvertissementRepository $evAvertissementRepository,

        EvCongeRepository $evCongeRepository,
        EvDecesRepository $evDecesRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository,
        EvDemissionRepository $evDemissionRepository,
        EvDisponibiliteRepository $evDisponibiliteRepository,
        EvIndemniteforfaitaireRepository $evIndemniteforfaitaireRepository,
        EvInterimRepository $evInterimRepository,
        EvLevedesuspensionRepository $evLevedesuspensionRepository,
        EvLicenciementRepository $evLicenciementRepository,
        EvMiseapiedRepository $evMiseapiedRepository,
        EvRectificationdunomRepository $evRectificationdunomRepository,
        EvReprisedefonctionRepository $evReprisedefonctionRepository,
        EvRetraiteRepository $evRetraiteRepository,
        EvStageRepository $evStageRepository,
        EvSuspensionRepository $evSuspensionRepository,
        EvSuspensiondesalaireRepository $evSuspensiondesalaireRepository
    )
    {
        $user = $user->getId();

        $roles = $userRepository->find($user)->getRoles();
        //Permission de voir les notifications
        $allow_to_see_notif = $this->isGranted('ROLE_ADMIN_GCAR');
        //comptage des evenements
        $nbr_event_invalide =
            $evAffectationRepository->countNotValideEvent()+
            $evAvancementnormalRepository->countNotValideEvent()+
            $evAvertissementRepository->countNotValideEvent()+
            $evCongeRepository->countNotValideEvent()+
            $evDecesRepository->countNotValideEvent()+
            $evDemisedefonctionRepository->countNotValideEvent()+
            $evDemissionRepository->countNotValideEvent()+
            $evDisponibiliteRepository->countNotValideEvent()+
            $evIndemniteforfaitaireRepository->countNotValideEvent()+
            $evInterimRepository->countNotValideEvent()+
            $evLevedesuspensionRepository->countNotValideEvent()+
            $evLicenciementRepository->countNotValideEvent()+
            $evMiseapiedRepository->countNotValideEvent()+
            $evNominationRepository->countNotValideEvent()+
            $evPromotioninterneRepository->countNotValideEvent()+
            $evRecrutementRepository->countNotValideEvent()+
            $evRectificationdunomRepository->countNotValideEvent()+
            $evReintegrationRepository->countNotValideEvent()+
            $evRenouvellementdecontratRepository->countNotValideEvent()+
            $evReprisedefonctionRepository->countNotValideEvent()+
            $evRetraiteRepository->countNotValideEvent()+
            $evStageRepository->countNotValideEvent()+
            $evSuspensionRepository->countNotValideEvent()+
            $evSuspensiondesalaireRepository->countNotValideEvent()+
            $evTitularisationRepository->countNotValideEvent()+
            $evReclassementRepository->countNotValideEvent();

        // postes de l'agent
        /** @var PosteUser $posteUser */
        $posteUser = $posteUserRepository->findLastByUser($user);
        if ($posteUser){
            $poste_user = $posteUser->getPoste()->getDesignation();
            $nom_evenement = $posteUser->getNomEvenement();

            if ($posteuserServicedepartementdirectionRepository->findByIdPosteUser($posteUser)->getServicedepartementdirection() != null){
                $entite = 'sdptdir';
                /** @var ServiceDepartementdirection $serviceDepartementdirection */
                $serviceDepartementdirection = $posteuserServicedepartementdirectionRepository->findByIdPosteUser($posteUser)
                    ->getServicedepartementdirection();
                $localisation_agent = $serviceDepartementdirection->getService()->getDesignation();
                $localisation_agent_id = $serviceDepartementdirection->getId();
                $tel_bureau = $serviceDepartementdirection->getTel();
                $hierarchie = $serviceDepartementdirection->getDepartementdirection()->getDepartement()->getDesignation();
                $tel_hierarchie = $serviceDepartementdirection->getDepartementdirection()->getTel();
                $charge = [];
                if ($posteUser->getPoste()->getDesignation()=='Chef de service'){
                    $responsabilite = 'chef_s';
                    $service_user = $posteuserServicedepartementdirectionRepository->findByIdPosteUser($posteUser);
                    $charges = $posteUserRepository->showAgentOfSvcdpt($service_user->getServicedepartementdirection());
                    //dd($charges);
                }else{
                    $charges = [];
                    $responsabilite = 'aucun';
                }

            }
            elseif ($posteuserServicedirectionRepository->findByIdPosteUser($posteUser)->getServicedirection() != null){
                $entite = 'sdir';
                /** @var ServiceDirection $serviceDirection */
                $serviceDirection = $posteuserServicedirectionRepository->findByIdPosteUser($posteUser)->getServicedirection();
                $localisation_agent = $serviceDirection->getService()->getDesignation();
                $localisation_agent_id = $serviceDirection->getId();
                $tel_bureau = $serviceDirection->getTel();
                $hierarchie = $serviceDirection->getDirection()->getDesignation();
                $tel_hierarchie = $serviceDirection->getDirection()->getTel();

                if ($posteUser->getPoste()->getDesignation()=='Chef de service'){
                    $responsabilite = 'chef_s';
                    $service_user = $posteuserServicedirectionRepository->findByIdPosteUser($posteUser);
                    $charges = $posteUserRepository->showAgentOfScdir($service_user->getServicedirection());
                    //dd($charges);
                }else{
                    $charges = [];
                    $responsabilite = 'aucun';
                }

            }
            elseif ($posteuserDepartementdirectionRepository->findByIdPosteUser($posteUser)->getDepartementdirection() != null){
                $entite = 'dpt';
                /** @var DepartementDirection $departementdirection */
                $departementdirection = $posteuserDepartementdirectionRepository->findByIdPosteUser($posteUser)->getDepartementdirection();
                $localisation_agent = $departementdirection->getDepartement()->getDesignation();
                $localisation_agent_id = $departementdirection->getId();

                $tel_bureau = $departementdirection->getTel();
                $hierarchie = $departementdirection->getDirection()->getDesignation();
                $tel_hierarchie = $departementdirection->getDirection()->getTel();

                if ($posteUser->getPoste()->getDesignation() =='Chef de département'){
                    $responsabilite = 'chef_d';
                    $departement_user = $posteuserDepartementdirectionRepository->findByIdPosteUser($posteUser);
                    $charges = $serviceRepository->findServDeptDirByDept($departement_user->getDepartementdirection());
                    //dd($charges);
                }

            }
            elseif ($posteUserDirectionRepository->findByIdPosteUser($posteUser)->getDirection() !=null){
                $entite = 'dir';
                /** @var Direction $direction */
                $direction = $posteUserDirectionRepository->findByIdPosteUser($posteUser)->getDirection();
                $localisation_agent = $direction->getDesignation();
                $localisation_agent_id = $direction->getId();
                $tel_bureau = $direction->getTel();
                $tel_hierarchie = 'Aucun';

                if ($posteUser->getPoste()->getDesignation() == 'Directeur'){
                    $responsabilite = 'directeur';
                    $direction_user = $posteUserDirectionRepository->findByIdPosteUser($posteUser);
                    if ($direction_user->getDirection() == 'Générale'){
                        $hierarchie = 'Aucun';
                        $charges = [];

                    }else{
                        $hierarchie = 'Générale';
                        //$user = $direction_user->getPosteUser()->getUser();
                        $charges = $departementRepository->findDeptDirByDirection($direction_user->getDirection());

                    }
                }



            }


        }
        //Si aucun poste n'est défini
        else{
            $entite = 'aucun';
            $localisation_agent = 'Non défini';
            $localisation_agent_id = NULL;
            $poste_user = 'Non défini';
            $charges = [];
            $tel_bureau = 'Non défini';
            $tel_hierarchie = 'Aucun';
            $nom_evenement = 'Non défini';
            $hierarchie = 'Non défini';
            $responsabilite = 'Aucun';
        }


        //Direction

        $recrutement = $evRecrutementRepository->findByIdUser($user);
        $titularisation = $evTitularisationRepository->findByIdUser($user);

        return $this->render('moncompte_administration/index.html.twig', [
            'entite'                => $entite,
            'poste_user'            => $poste_user,
            'responsabilite'        => $responsabilite,
            'nom_evenement'         => $nom_evenement,
            'localisation_agent'    => $localisation_agent,
            'localisation_agent_id' => $localisation_agent_id,
            'hierarchie'            => $hierarchie,
            'tel_hierarchie'        => $tel_hierarchie,
            'tel_bureau'            => $tel_bureau,
            'charges'                =>$charges,
            'recrutement'           => $recrutement,
            'titularisation'        => $titularisation,
            'roles'                 => $roles,
            'nbr_event_invalide'    => $nbr_event_invalide,
            'allow_to_see_notif'    => $allow_to_see_notif,
        ]);
    }

    /**
     * LISTE DES AGENTS DU DEPARTEMENT vu par le chef de département
     * @Route("/moncompte/admin/event_agents_dpt", name="moncompte_event_agents")
     */
    public function adminDataOfagentsDpt(ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                                      DepartementDirectionRepository $departementDirectionRepository,
                                      DirectionRepository $directionRepository,
                                      PosteUserRepository $posteUserRepository,
                                      UserRepository $userRepository, Request $request){

        $url_id = $request->get('dpt_id');
        $departementdirection = $departementDirectionRepository->find($url_id);
        $services_lies = $serviceDepartementdirectionRepository->findServiceOfDepartement();
        $agents = $posteUserRepository->showAgentOfDptdirSansParam();

        return $this->render('moncompte_administration/evenements/agents_dpt.html.twig', [
            'departementdirection'                => $departementdirection,
            'services_lies'                       => $services_lies,
            'agents'                              => $agents,
        ]);
    }

    /**
     * Affiche les détails du service de département
     *
     * @Route("moncompte/admin/event_agents_svc", name="moncompte_event_agents_svc", methods={"GET"})
     */
    public function adminDataOfagentSvc(ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                               ServiceDirectionRepository $serviceDirectionRepository,
                               DepartementRepository $departementRepository,
                               DirectionRepository $directionRepository,
                               ServiceRepository $serviceRepository,
                               PosteUserRepository $posteUserRepository,
                               UserRepository $userRepository, Request $request): Response
    {
        $entite = $request->get('entite');
        if ($entite and $entite=='sdir'){
            $service = $serviceDirectionRepository->find($request->get('svc_id'));
            $agents = $posteUserRepository->showAgentOfSvcdir($service);
        }elseif ($entite and $entite == 'sdptdir'){
            $service = $serviceDepartementdirectionRepository->find($request->get('svc_id'));
            $agents = $posteUserRepository->showAgentOfSvcdpt($service);
        }

        return $this->render('moncompte_administration/evenements/agents_svc.html.twig', [
            'service'                => $service,
            'agents'                 => $agents,
            'entite'                 => $entite,
        ]);
    }



    /**
     * Détail des evenements de l'agent vu par le chef de département ou le chef de service (UN SEUL AGENT)
     * @Route("/moncompte/admin/event_agent", name="moncompte_event_agent")
     */
    public function adminEventOfagent(
        Request $request,
        PosteUserRepository $posteUserRepository,
        EvNominationRepository $evNominationRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvAffectationRepository $evAffectationRepository,
        EvTitularisationRepository $evTitularisationRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        UserRepository $userRepository,
        Security $security
    )
    {
        $user_connected =  $security->getUser();
        $poste_user_connected = $posteUserRepository->findLastByUser($user_connected);
        $id_evenement = $poste_user_connected->getIdEvenement();
        $nom_evenement = $poste_user_connected->getNomEvenement();
        $dernier_poste_user_connected = $posteUserRepository->findByIdEvenement($id_evenement, $nom_evenement);

        $id = $request->get('id');
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id') : null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id') : null;
        $entite = $request->get('entite') ? $request->get('entite'): null;

        if ($dpt_id){
            $lien = 'dpt_id';
        }else{
            $lien = 'svc_id';
        }

        $agent = $userRepository->find($id);
        $dernierPosteUser = $posteUserRepository->findLastByUser($agent);
        /** @var PosteUser $dernierPosteUser */
        $nomEvenement = $dernierPosteUser ? $dernierPosteUser->getNomEvenement() : '';
        if ($nomEvenement!= ''){
            $idEvenement = $dernierPosteUser->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Renouvellementdecontrat'){
                $evenement = $evRenouvellementdecontratRepository->find($idEvenement);
                //$evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Titularisation'){
                $evenement = $evTitularisationRepository->find($idEvenement);

                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();

                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Affectation'){
                $evenement = $evAffectationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();

            }
            elseif ($nomEvenement == 'Promotioninterne'){
                $evenement = $evPromotioninterneRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Reintegration'){
                $evenement = $evReintegrationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Nomination'){
                $evenement = $evNominationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $evenement = $evDemisedefonctionRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
        }else{
            $service = 'Non-renseigné';
            $departement = 'Non-renseigné';
            $direction = 'Non-renseignée';
        }

        $retraite = $agent->getRetraite();

        return $this->render('moncompte_administration/evenements/info_evenement_agent.html.twig', [
            'agent'                             => $agent,
            'dernier_poste_user_connected'      => $dernier_poste_user_connected->getPoste()->getDesignation(),
            'service'                           => $service,
            'departement'                       => $departement,
            'direction'                         => $direction,
            'retraite'                          => $retraite,
            'dpt_id'                            => $dpt_id,
            'svc_id'                            => $svc_id,
            'lien'                              => $lien,
            'entite'                            => $entite,
        ]);
    }

    /**
     * Profil agent vu par le Chef de département
     * @Route("/moncompte/admin/{id}/profil", name="moncompte_profile_agent")
     */
    public function infoProfileAgent(
        Request $request,
        PosteUserRepository $posteUserRepository,
        EvNominationRepository $evNominationRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvAffectationRepository $evAffectationRepository,
        EvTitularisationRepository $evTitularisationRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        UserRepository $userRepository
    )
    {
        $id = $request->get('id');
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id') : null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id') : null;
        $entite = $request->get('entite') ? $request->get('entite'): null;

        if ($dpt_id){
            $lien = 'dpt_id';
        }else{
            $lien = 'svc_id';
        }
        $agent = $userRepository->find($id);
        $dernierPosteUser = $posteUserRepository->findLastByUser($agent);
        //dd($dernierPosteUser);
        /** @var PosteUser $dernierPosteUser */
        $nomEvenement = $dernierPosteUser ? $dernierPosteUser->getNomEvenement() : '';
        if ($nomEvenement!= ''){
            $idEvenement = $dernierPosteUser->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $poste = $evenement->getPoste();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Renouvellementdecontrat'){
                $evenement = $evRenouvellementdecontratRepository->find($idEvenement);
                //$evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $poste = $evenement->getPoste();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Titularisation'){
                $evenement = $evTitularisationRepository->find($idEvenement);

                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $poste = $evenement->getPoste();

                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }elseif($service instanceof  ServiceDirection){
                    $departement = '';
                    $direction = $service->getDirection();
                }else{
                    $departement = null;
                    $direction = null;
                }
            }
            elseif ($nomEvenement == 'Affectation'){
                $evenement = $evAffectationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();

            }
            elseif ($nomEvenement == 'Promotioninterne'){
                $evenement = $evPromotioninterneRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Reintegration'){
                $evenement = $evReintegrationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
            elseif ($nomEvenement == 'Nomination'){
                $evenement = $evNominationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $evenement = $evDemisedefonctionRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
                $poste = $evenement->getPoste();
            }
        }else{
            $service = 'Non-renseigné';
            $departement = 'Non-renseigné';
            $direction = 'Non-renseignée';
            $poste = 'Non-renseignée';

        }

        $retraite = $agent->getRetraite();

        return $this->render('moncompte_administration/evenements/info_profile_agent.html.twig', [
            'agent'                     => $agent,
            'service'                   => $service,
            'departement'               => $departement,
            'direction'                 => $direction,
            'poste'                     => $poste,
            'retraite'                  => $retraite,
            'nomEvenement'              => $nomEvenement,
            'dpt_id'                    => $dpt_id,
            'svc_id'                    => $svc_id,
            'lien'                      => $lien,
            'entite'                    => $entite,
        ]);
    }

    /**
     * @Route("/moncompte/admin/{slug}-{id}", name="moncompte_edit_infos", requirements={"slug": "[a-z0-9\-]*"})
     * @param $slug
     * @param $id
     * @param User $user
     * @param Request $request
     * @return Response
     */
    public function userEdit(
        $slug, $id,
        User $user,
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository
    )
    {
        if ($user->getSlug() !== $slug){
            return $this->redirectToRoute('moncompte_edit_infos', [
                'id' => $user->getId(),
                'slug' => $user->getSlug()
            ], 301);
        }

        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->manager->flush();
            $this->addFlash('success', 'Modifications effectuées avec succès!');
            return $this->redirectToRoute('moncompte_administration');
        }

        return $this->render('moncompte_administration/edit_infos_perso.html.twig',[
            'user' => $user,
            'form' => $form->createView(),
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }

    //================ TEL SERVICE DEPARTEMENT =============================

    /**
     * @Route("/moncompte/admin/edit_telsdpt/{id}", name="moncompte_edit_tel_sdpt")
     */
    public function editTelServiceDepartement(
        Request $request,
        ServiceDepartementdirection $servicedepartementdirection,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository
    )
    {
        $serviceDepartementdirs = $serviceDepartementdirectionRepository->findAll();
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $form_telservicedept = $this->createForm(ServiceDepartementdirectionType::class, $servicedepartementdirection);
        $form_telservicedept->handleRequest($request);

        if ($form_telservicedept->isSubmitted() && $form_telservicedept->isValid()){
            $this->manager->flush();
            $this->addFlash('success', 'Modifications effectuées avec succès!');
            return $this->redirectToRoute('moncompte_administration');
        }

        return $this->render('moncompte_administration/telsrvdpt_edit.html.twig',[
            'form_telservicedept' => $form_telservicedept->createView(),
            'service_departements' => $serviceDepartementdirs,
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }

    //================ TEL SERVICE DIRECTION =============================

    /**
     * @Route("/moncompte/admin/edit_telsdir/{id}", name="moncompte_edit_tel_sdir")
     */
    public function editTelServiceDirection(
        Request $request,
        ServiceDirection $serviceDirection,
        ServiceDirectionRepository $serviceDirectionRepository,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository
    )
    {
        $serviceDirections = $serviceDirectionRepository->findAll();
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $form_telservicedir = $this->createForm(ServiceDirectionType::class, $serviceDirection);
        $form_telservicedir->handleRequest($request);

        if ($form_telservicedir->isSubmitted() && $form_telservicedir->isValid()){
            $this->manager->flush();
            $this->addFlash('success', 'Modifications effectuées avec succès!');
            return $this->redirectToRoute('moncompte_administration');
        }

        return $this->render('moncompte_administration/telsrvdir_edit.html.twig',[
            'form_telservicedir' => $form_telservicedir->createView(),
            'service_directions' => $serviceDirections,
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }

    //================ TEL DEPARTEMENT =============================

    /**
     * @Route("/moncompte/admin/edit_teldptdir/{id}", name="moncompte_edit_tel_dptdir")
     */
    public function editTelDepartementDirection(
        Request $request,
        DepartementDirection $departementDirection,
        DepartementDirectionRepository $departementDirectionRepository,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository
    )
    {
        $departementDirections = $departementDirectionRepository->findAll();
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $form_teldepartementdir = $this->createForm(DepartementDirectionType::class, $departementDirection);
        $form_teldepartementdir->handleRequest($request);

        if ($form_teldepartementdir->isSubmitted() && $form_teldepartementdir->isValid()){
            $this->manager->flush();
            $this->addFlash('success', 'Modifications effectuées avec succès!');
            return $this->redirectToRoute('moncompte_administration');
        }

        return $this->render('moncompte_administration/teldptdir_edit.html.twig',[
            'form_teldepartementdir' => $form_teldepartementdir->createView(),
            'departement_directions' => $departementDirections,
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }

    //================ TEL SERVICE DIRECTION =============================

    /**
     * @Route("/moncompte/admin/edit_teldir/{id}", name="moncompte_edit_tel_dir")
     */
    public function editTelDirection(
        Request $request,
        Direction $direction,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();

        $form_teldirection = $this->createForm(DirectionType::class, $direction);
        $form_teldirection->handleRequest($request);

        if ($form_teldirection->isSubmitted() && $form_teldirection->isValid()){
            $this->manager->flush();
            $this->addFlash('success', 'Modifications effectuées avec succès!');
            return $this->redirectToRoute('moncompte_administration');
        }

        return $this->render('moncompte_administration/teldir_edit.html.twig',[
            'form_teldirection' => $form_teldirection->createView(),
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
        ]);
    }

    //=======================================================================
    //                      MESSAGES
    //================ ALERTER L'ADMINISTRATEUR =============================

    /**
     * @Route("/moncompte/message/new", name="message_new")
     */
    public function newMessage(
        Request $request,
        Security $security,
        UserRepository $userRepository,
        MessageRepository $messageRepository,
        MessagesupprimeRepository $messagesupprimeRepository,
        TypeMessageRepository $typeMessageRepository
    )
    {
        $user = $security->getUser();
        if ($request->get('typeMessage')){
            $urlTypeMessage = $request->get('typeMessage');
            if ($urlTypeMessage == 'donnees'){
                $typeMessage = $typeMessageRepository->find(1);
            }elseif ($urlTypeMessage == 'systeme'){
                $typeMessage  = $typeMessageRepository->find(2);
            }elseif ($urlTypeMessage == 'reclamations'){
                $typeMessage = $typeMessageRepository->find(4);
            }elseif ($urlTypeMessage == 'infos'){
                $typeMessage = $typeMessageRepository->find(5);
            }

        }else{
            $typeMessage = $typeMessageRepository->find(3);
        }

        //dd($recipients);

        $message = new Message();
        //si typeMessage est défini dans l'URL
        if ($request->get('typeMessage')){
            if ($urlTypeMessage == 'donnees'){
                $adminGcar = 'ROLE_ADMIN_GCAR';
                $admin = 'ROLE_ADMIN';
                $superAdmin = 'ROLE_SUPER_ADMIN';
                //on attribue les utilisateur des différents roles
                foreach ($userRepository->showGcarAdmin($adminGcar) as $adminGcar ){
                    $message->addReceiver($adminGcar);
                }
                foreach ($userRepository->showGcarAdmin($superAdmin) as $superAdmin ){
                    $message->addReceiver($superAdmin);
                }
                foreach ($userRepository->showGcarAdmin($admin) as $admin ){
                    $message->addReceiver($admin);
                }
            }
        }

        $form_message = $this->createForm(MessageType::class, $message);

        $form_message->handleRequest($request);

        $expediteur = $security->getUser();

        if ($form_message->isSubmitted() && $form_message->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            //envoi de la date
            $message->setDateEnvoiAt(new \DateTime())
                    ->setTypeMessage($typeMessage)
                    ->setUserSender($expediteur);



            $entityManager->persist($message);
            $entityManager->flush();
            return $this->redirectToRoute('message_index');
        }

        return $this->render('moncompte_administration/messages/message_new.html.twig', [
            'form_message'                  => $form_message->createView(),
            'nbrMessages'                   => $messageRepository->countReceivedByUser($user->getId()),
            'nbrMessagesEnvoyes'            => $messageRepository->countSentByUser($user->getId()),
            'typeMessage'                   =>$typeMessage,
            'nbr_msg_archived'              => $messagesupprimeRepository->countArchivedByUser($user),
        ]);
    }

    /**
     * @Route("/moncompte/message/response/{id}", name="message_response")
     */
    public function responseMessage(Request $request,
                                    Security $security,
                                    UserRepository $userRepository,
                                    MessagesupprimeRepository $messagesupprimeRepository,
                                    MessageRepository $messageRepository)
    {
        // contenu du message à répondre //
        if ($request->get('id')){
            $message_a_repondre = $messageRepository->find($request->get('id'));
            $sender = $message_a_repondre->getUserSender();
            $sujet = $message_a_repondre->getSujet();
            $contenu = $message_a_repondre->getContenu();
            $dateEnvoieAt = $message_a_repondre->getDateEnvoiAt();
            $typeMessage = $message_a_repondre->getTypeMessage();
            //dd($sender);
        }
        //---Fin contenu du message à répondre ---//

        $message = new Message();
        $message = $message->setUserSender($request->getUser())

            ->setContenu($message->getContenu().'<br/><span class="text-danger">
            ----------------------------------------------------------------------------------------------------------
            </span><br/>
            <i><span class="text-navy"><b>'.$sujet.'</b><br/>'.$contenu.'<br/>
             Envoyé le '.$dateEnvoieAt->format('d/m/Y').'</span></i>
            ')
            ->setSujet($sujet)
            ->setTypeMessage($typeMessage)
            ->addReceiver($userRepository->find($message_a_repondre->getUserSender()->getId()))
            ->setUserSender($security->getUser())
            ->setDateEnvoiAt(new \DateTime())
            ;

        $form_message = $this->createForm(MessageType::class, $message);
        $form_message->handleRequest($request);

        if ($form_message->isSubmitted() && $form_message->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();
            return $this->redirectToRoute('message_index');
        }

        return $this->render('moncompte_administration/messages/message_response.html.twig', [
            'form_message'                  => $form_message->createView(),
            'nbrMessages'                   => $messageRepository->countReceivedByUser($security->getUser()->getId()),
            'nbrMessagesEnvoyes'            => $messageRepository->countSentByUser($security->getUser()->getId()),
            'typeMessage'                   => $typeMessage,
            'sender'                        => $sender,
            'nbr_msg_archived'              => $messagesupprimeRepository->countArchivedByUser($security->getUser()),
        ]);

    }


    /**
     * @Route("/moncompte/message/show/{slug}-{id}", name="message_show", requirements={"slug": "[a-z0-9\-]*"})
     */

    public function messageShow(
        $slug,
        $id,
        Security $security,
        Message $message,
        MessagesvusRepository $messagesvusRepository,
        MessagesupprimeRepository $messagesupprimeRepository,
        MessageRepository $messageRepository
    )
    {
        if ($message->getSlug() !== $slug){
            return $this->redirectToRoute('moncompte_voir_message', [
                'id' => $message->getId(),
                'slug' => $message->getSlug()
            ], 301);
        }

        $messagesvus =  $messagesvusRepository->selectAllByMessageId($message->getId());

        //dd($messagesvus);

        $user = $security->getUser();

        $message = $messageRepository->find($id);

        $messagevus = new Messagesvus();
        $messagevus = $messagevus->setUser($user)
            ->setMessage($message)
            ->setVuAt(new \DateTime());
        $this->getDoctrine()->getManager()->persist($messagevus);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('moncompte_administration/messages/message_show.html.twig', [
            'message' => $message,
            'nbrMessages' => $messageRepository->countReceivedByUser($user->getId()),
            'nbrMessagesEnvoyes' => $messageRepository->countSentByUser($user->getId()),
            'nbr_msg_archived'      => $messagesupprimeRepository->countArchivedByUser($user),
        ]);
    }


    /**
     * @Route("/moncompte/messages", name="message_index")
     */

    public function messageIndex(MessageRepository $messageRepository, Security $security,
                                 MessagesupprimeRepository $messagesupprimeRepository){
        $user = $security->getUser();

        $messages = $messageRepository->selectAllReceived($user->getId());
        $nbr_msg_archived = $messagesupprimeRepository->countArchivedByUser($user);

        //dd($messages);
        return $this->render('moncompte_administration/messages/message_index.html.twig', [
            'messages'  => $messages,
            'user'      => $user,
            'nbrMessages'      => $messageRepository->countReceivedByUser($user->getId()),
            'nbrMessagesEnvoyes' => $messageRepository->countSentByUser($user->getId()),
            'nbr_msg_archived' => $nbr_msg_archived,
        ]);
    }

    /**
     * @Route("/moncompte/messages/archived", name="message_archived")
     */

    public function messageArchived(MessageRepository $messageRepository,
                                    MessagesupprimeRepository $messagesupprimeRepository,
                                    Security $security){
        $user = $security->getUser();

        $messages = $messagesupprimeRepository->showArchivedByUser($user);
        $nbr_msg_archived = $messagesupprimeRepository->countArchivedByUser($user);

        //dd($messages);
        return $this->render('moncompte_administration/messages/message_archived.html.twig', [
            'messages'  => $messages,
            'user'      => $user,
            'nbrMessages'      => $messageRepository->countReceivedByUser($user->getId()),
            'nbrMessagesEnvoyes' => $messageRepository->countSentByUser($user->getId()),
            'nbr_msg_archived' => $nbr_msg_archived,
        ]);
    }

    /**
     * @Route("/moncompte/messages/sent", name="message_sent")
     */

    public function messageSent(MessageRepository $messageRepository, MessagesupprimeRepository $messagesupprimeRepository, Security $security){
        $user = $security->getUser();

        //dd($messageRepository->showSentByUser($user->getId()));
        return $this->render('moncompte_administration/messages/message_sent.html.twig', [
            'user'      => $user,
            'nbrMessages' => $nbrMessages = $messageRepository->countReceivedByUser($user->getId()),
            'nbrMessagesEnvoyes' => $messageRepository->countSentByUser($user->getId()),
            'messagesEnvoyes' => $messageRepository->showSentByUser($user->getId()),
            'nbr_msg_archived'      => $messagesupprimeRepository->countArchivedByUser($user),
        ]);
    }

    /**
     * @Route("/moncompte/messages/todelete/{id}", name="message_todelete", methods={"ARCHIV"})
     */

    public function messageTodelete(MessageRepository $messageRepository,
                                    Security $security,
                                    Message $message, Request $request): Response
    {
        $user = $security->getUser();

        if ($this->isCsrfTokenValid('archiv'.$message->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $messagetodelete = new Messagesupprime();
            $messagetodelete = $messagetodelete->setSupprimeAt(new \DateTime())
                                                ->setUser($user)
                                                ->setMessage($messageRepository->find($message->getId()));
            $entityManager->persist($messagetodelete);
            $entityManager->flush();
        }

        return $this->redirectToRoute('message_index');
    }

    /**
     * @Route("/moncompte/messages/delete/{id}", name="message_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           MessagesvusRepository $messagesvusRepository,
                           Message $message): Response
    {
        if ($this->isCsrfTokenValid('delete'.$message->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $messagesvus =  $messagesvusRepository->selectAllByMessageId($message->getId());

            //on supprime les messages_vus d'abord avant de supprimer le messaage principal
            foreach ($messagesvus as $message_vu){
                /** @var Messagesvus $message_vu */
                $entityManager->remove($message_vu);
            }
            $entityManager->remove($message);


            $entityManager->flush();
        }

        return $this->redirectToRoute('message_index');
    }
}
