<?php

namespace App\Controller;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Form\DepartementDirectionType;
use App\Form\DirectionType;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDecesRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;


class DepartementDirectionController extends AbstractController
{
    /**
     * @Route("/admin/ct/dptdir/", name="departement_direction_index", methods={"GET"})
     */
    public function index(DepartementDirectionRepository $departementDirectionRepository,
                          EvNominationRepository $evNominationRepository,
                          EvAffectationRepository $evAffectationRepository,
                          EvDemisedefonctionRepository $evDemisedefonctionRepository,
                          EvPromotioninterneRepository $evPromotioninterneRepository,
                          EvLicenciementRepository $evLicenciementRepository,
                          EvDecesRepository $evDecesRepository,
                          EvMiseapiedRepository $evMiseapiedRepository,
                          EvDemissionRepository $evDemissionRepository,
                          EvSuspensionRepository $evSuspensionRepository,
                          EvReintegrationRepository $evReintegrationRepository,
                          PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository): Response
    {

        //$poste_departement = $posteuserDepartementdirectionRepository->showDepartementPoste();
        //dd($departementDirectionRepository->isThereAservice());
        return $this->render('departement_direction/index.html.twig', [
            'departement_directions'    => $departementDirectionRepository->findSomeFields(),
            'ev_nomination'             => $evNominationRepository->findSomeFields(),
            'ev_affectation'            => $evAffectationRepository->findSomeFields(),
            'ev_demisedefonction'       => $evDemisedefonctionRepository->findSomeFields(),
            'ev_promotioninterne'       => $evPromotioninterneRepository->findSomeFields(),
            'ev_licenciement'           => $evLicenciementRepository->findSomeFields(),
            'ev_deces'                  => $evDecesRepository->findSomeFields(),
            'ev_miseapied'              => $evMiseapiedRepository->findSomeFields(),
            'ev_demission'              => $evDemissionRepository->findSomeFields(),
            'ev_suspension'             => $evSuspensionRepository->findSomeFields(),
            'ev_reintegration'          => $evReintegrationRepository->findSomeFields(),
            'services_lies'             => $departementDirectionRepository->isThereAservice(),
        ]);
    }

    /**
     * @Route("/admin/ct/dptdir/new", name="departement_direction_new", methods={"GET","POST"})
     */

    public function new(Request $request): Response
    {
        $departementDirection = new DepartementDirection();
        $form = $this->createForm(DepartementDirectionType::class, $departementDirection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($departementDirection);
            $entityManager->flush();

            $this->addFlash('success','Modifications effectuées avec succès');
            return $this->redirectToRoute('departement_direction_index');
        }

        return $this->render('departement_direction/new.html.twig', [
            'departement_direction' => $departementDirection,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/admin/ct/dptdir/{id}", name="departement_direction_show", methods={"GET"})
     */
    /*
    public function show(DepartementDirection $departementDirection): Response
    {
        return $this->render('departement_direction/show.html.twig', [
            'departement_direction' => $departementDirection,
        ]);
    }
    */

    /**
     * @Route("/admin/ct/dptdir/{id}/edit", name="departement_direction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         DepartementDirection $departementDirection,
                         ServiceRepository $serviceRepository,
                         DirectionRepository $directionRepository,
                         DepartementRepository $departementRepository): Response
    {
        $form = $this->createForm(DepartementDirectionType::class, $departementDirection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Modifications effectuées avec succès');
            return $this->redirectToRoute('departement_direction_index', [
                'id' => $departementDirection->getId(),
            ]);
        }

        return $this->render('departement_direction/edit.html.twig', [
            'departement_direction' => $departementDirection,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/ct/dptdir/{id}", name="departement_direction_archive", methods={"ARCHIVE"})
     */
    public function archive(Request $request, DepartementDirection $departementDirection): Response
    {
        if ($this->isCsrfTokenValid('archive'.$departementDirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $departementDirection->setFonctionnel(0);
            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, le departement n'existe plus");
        return $this->redirectToRoute('departement_direction_index');
    }

    /**
     * @Route("/admin/ct/dptdir/{id}", name="departement_direction_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DepartementDirection $departementDirection): Response
    {
        if ($this->isCsrfTokenValid('delete'.$departementDirection->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($departementDirection);
            $entityManager->flush();
        }

        return $this->redirectToRoute('departement_direction_index');
    }

    /*
     * PARTIE DIRECTION
     */
    /**
     * @Route("/admin/ct/dir/", name="direction_index", methods={"GET"})
     */
    public function indexDirection(DirectionRepository $directionRepository): Response
    {
        return $this->render('departement_direction/index_dir.html.twig', [
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/ct/dir/new", name="direction_new", methods={"GET","POST"})
     */

    public function newDirection(Request $request): Response
    {
        $direction = new Direction();
        $form = $this->createForm(DirectionType::class, $direction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($direction);
            $entityManager->flush();

            $this->addFlash('success','Modifications effectuées avec succès');
            return $this->redirectToRoute('direction_index');
        }

        return $this->render('departement_direction/new_dir.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/ct/dir/{id}", name="direction_show", methods={"GET"})
     */

    public function show(Direction $direction, PosteUserRepository $posteUserRepository): Response
    {
        $poste_users = $posteUserRepository->showAgentOftdir($direction);
        return $this->render('departement_direction/show_dir.html.twig', [
            'direction' => $direction,
            'poste_users' => $poste_users,
        ]);
    }


    /**
     * @Route("/admin/ct/dir/{id}/edit", name="direction_edit", methods={"GET","POST"})
     */
    public function edit_dir(Request $request,
                         Direction $direction,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(DirectionType::class, $direction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Modifications effectuées avec succès');
            return $this->redirectToRoute('direction_index', [
                'id' => $direction->getId(),
            ]);
        }

        return $this->render('departement_direction/edit_dir.html.twig', [
            'direction' => $direction,
            'directions' => $directionRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/ct/dir/{id}", name="direction_delete", methods={"DELETE"})
     */
    public function delete_dir(Request $request, Direction $direction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$direction->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($direction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('direction_index');
    }


    /**
     * @Route("societe/dir/info", name="direction_info", methods={"GET"})
     */
    public function infoDir(DepartementDirectionRepository $departementDirectionRepository,
                         DirectionRepository $directionRepository,
                         PosteUserRepository $posteUserRepository
    ): Response
    {
        $agent = $posteUserRepository->showAgentOfdirSansParam();
        //dd($agent);
        //dd( $directionRepository->findSomeFields());
        $departements_lies = $departementDirectionRepository->findDepartementOfDir();
        //dd($departements_lies);
        return $this->render('departement_direction/info_dir.html.twig', [
            'directions'                    => $directionRepository->findSomeFields(),
            'agent'                         => $agent,
            'departements_lies'             => $departements_lies,
        ]);
    }


    /**
     * Affiche tous les départements avec certains détails
     *
     * @Route("societe/dptdir/info", name="departementdirection_info", methods={"GET"})
     */
    public function infoDpt(DepartementDirectionRepository $departementDirectionRepository,
                            DepartementRepository $departementRepository,
                            ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                            DirectionRepository $directionRepository,
                            PosteUserRepository $posteUserRepository
    ): Response
    {
        $agent = $posteUserRepository->showAgentOfDptdirSansParam();
        $services_lies = $serviceDepartementdirectionRepository->findServiceOfDepartement();



        //dd($services_lies);
        return $this->render('departement_direction/info_dpt.html.twig', [
            'departementdirections'         => $departementDirectionRepository->findSomeFields(),
            'agent'                         =>$agent,
            'services_lies'                 => $services_lies,
        ]);
    }

    /**
     * Affiche les détails du département
     *
     * @Route("societe/dptdir/info/{id}", name="departementdirection_info_detail", methods={"GET"})
     */
    public function infoDetail(ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                               DepartementDirectionRepository $departementDirectionRepository,
                               DirectionRepository $directionRepository,
                               PosteUserRepository $posteUserRepository,
                               UserRepository $userRepository,
                               Security $security, Request $request): Response
    {
        $url_id = $request->get('id');
        $departementdirection = $departementDirectionRepository->find($url_id);
        $services_lies = $serviceDepartementdirectionRepository->findServiceOfDepartement();
        $agents = $posteUserRepository->showAgentOfDptdirSansParam();

        $poste_user_connected = $posteUserRepository->findLastByUser($security->getUser());

        //$agents = $posteUserRepository->showAgentOfDptdir($departementdirection);
        //$agentsDpt = $posteUserRepository->showAgentOfDptOnly($departementdirection);
        //dd($agents);
        return $this->render('departement_direction/info_dpt_detail.html.twig', [
            'departementdirection'                => $departementdirection,
            'services_lies'                       => $services_lies,
            'agents'                              => $agents,
            'poste_user_connected'                => $poste_user_connected,
        ]);
    }


}
