<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvAvancementnormal;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvAvancementnormalType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAvancementnormalRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/avancementnormal")
 */
class EvAvancementnormalController extends AbstractController
{
    /**
     * @Route("/", name="evenements_avancementnormal_index", methods={"GET"})
     */
    public function index(EvAvancementnormalRepository $evAvancementnormalRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_avancementnormal/index.html.twig', [
            'ev_avancementnormals' => $evAvancementnormalRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_avancementnormal_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        PosteUserRepository $posteUserRepository,
                        PosteRepository $posteRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evAvancementnormal = new EvAvancementnormal();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //Récupération de l'évenement de titularisation (pour tester si l'agent a une titularisation enregistrée)
        /** @var EvTitularisation $user_titularisation */
        $user_titularisation = $evTitularisationRepository->findByIdUser($url_id);
        //récupération de l'utilisateur en question, via son id envoyé via url
        $info_titularisation = 0;
        if ($user_titularisation){
            $info_titularisation = 1;
        }
        $user = $userRepository->find($url_id);

        //récupération de l'id_postUser pour afficher son poste actuel (current)
        /** @var PosteUser $posteUser */
        $posteUser = $posteUserRepository->findLastByUser($url_id);
        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);

        $evAvancementnormal = $evAvancementnormal->setUser($user)
                                        ->setPoste($posteUser ? $posteUser->getPoste() : $aucun)
                                        ->setIndiceGroupe($user->getIndiceGroupe() ? $user->getIndiceGroupe() : '')
                                        ->setIndiceCategorie($user->getIndiceCategorie() ? $user->getIndiceCategorie() : '')
                                        ->setIndiceEchelon($user->getIndiceEchelon() ? $user->getIndiceEchelon() : '')
        ;

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evAvancementnormal = $evAvancementnormal->setValide(1);
        }else{
            $evAvancementnormal = $evAvancementnormal->setValide(0);
        }

        $form = $this->createForm(EvAvancementnormalType::class, $evAvancementnormal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent via son id
            $user = $evAvancementnormal->getUser();
            
            //on récupère l'indiceGroupe, indiceCategorie, indiceEchelon pour les insérer dans USER
            $indiceGroupe = $evAvancementnormal->getIndiceGroupe();
            $indiceCategorie = $evAvancementnormal->getIndiceCategorie();
            $indiceEchelon = $evAvancementnormal->getIndiceEchelon();

            $indices_pro = $user->setIndiceGroupe($indiceGroupe)
                                 ->setIndiceCategorie($indiceCategorie)
                                 ->setIndiceEchelon($indiceEchelon);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal EvAvancementnormal
            $entityManager->persist($evAvancementnormal);

            //objet2 User
            $entityManager->persist($indices_pro);

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_avancementnormal_index');
            }
        }

        return $this->render('evenements/ev_avancementnormal/new.html.twig', [
            'ev_avancementnormal' => $evAvancementnormal,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'info_titularisation' => $info_titularisation
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_avancementnormal_show", methods={"GET"})
     */
    public function show(EvAvancementnormal $evAvancementnormal,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_avancementnormal/show.html.twig', [
            'ev_avancementnormal' => $evAvancementnormal,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_avancementnormal_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvAvancementnormal $evAvancementnormal): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evAvancementnormal->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evAvancementnormal->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_avancementnormal_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_avancementnormal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvAvancementnormal $evAvancementnormal): Response
    {
        $form = $this->createForm(EvAvancementnormalType::class, $evAvancementnormal);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $user = $evAvancementnormal->getUser();
            //On récupère les indices pour les injecter à l'utilisateur (fos_user)
            $indiceGroupe = $evAvancementnormal->getIndiceGroupe();
            $indiceCategorie = $evAvancementnormal->getIndiceCategorie();
            $indiceEchelon = $evAvancementnormal->getIndiceEchelon();
            $user->setIndiceGroupe($indiceGroupe)
                 ->setIndiceCategorie($indiceCategorie)
                 ->setIndiceEchelon($indiceEchelon);

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evAvancementnormal->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_avancementnormal_index');
            }
        }

        return $this->render('evenements/ev_avancementnormal/edit.html.twig', [
            'ev_avancementnormal' => $evAvancementnormal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_avancementnormal_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvAvancementnormal $evAvancementnormal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evAvancementnormal->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evAvancementnormal);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_avancementnormal_index');
    }
}
