<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvTitularisationType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/titularisation")
 */
class EvTitularisationController extends AbstractController
{
    /**
     * @Route("/", name="evenements_titularisation_index", methods={"GET"})
     */
    public function index(EvTitularisationRepository $evTitularisationRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_titularisation/index.html.twig', [
            'ev_titularisations' => $evTitularisationRepository->findSomeFields(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_titularisation_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        DirectionRepository $directionRepository): Response
    {
        //recherche de la valeur titulaire dans le StatutRepository
        /*
             titulaire = 1
             contractuel = 2
             stagiaire = 3
         */
        $statut_val = $statutRepository->find(1);

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $evTitularisation = new EvTitularisation();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //Récupération des données de recrutement si elles existent
        /** @var EvRecrutement $user_recrutement */
        $user_recrutement = $evRecrutementRepository->findByIdUser($url_id);
        /** @var EvTitularisation $user_titularisation */
        $user_titularisation = $evTitularisationRepository->findByIdUser($url_id);
        $info_titularisation = 0;
        if ($user_titularisation){
            $info_titularisation = 1;
        }
        //inittialisation des variables
        $info_matricule = 1;
        $info_recrutement = 0;
        //Récupération de la valeur persistentCollection POSTE
        if ($user_recrutement){
            $info_recrutement = 1;
            $poste = $user_recrutement->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $evTitularisation->addPoste($value);
            }
            //si user_recrutement existe | on récupère son matricule, son poste
            $evTitularisation = $evTitularisation->setAncienMatricule($user_recrutement->getNouveauMatricule())
                                                ->setServiceDepartement($user_recrutement->getServiceDepartement())
                                                ->setServiceDirection($user_recrutement->getServiceDirection());
        }else{
            $evTitularisation = $evTitularisation->setAncienMatricule(NULL);
            $info_matricule = 0;
        }
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $evTitularisation = $evTitularisation->setUser($user)
                                             ->setDecisionAnnulee(1);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evTitularisation = $evTitularisation->setValide(1);
        }else{
            $evTitularisation = $evTitularisation->setValide(0);
        }
        $form = $this->createForm(EvTitularisationType::class, $evTitularisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent via son id

            $user = $evTitularisation->getUser();
            //On récupère le matricule pour l'injecter à l'utilisateur
            $nouveau_matricule = $evTitularisation->getNouveauMatricule();
            $matricule = $user->setMatricule($nouveau_matricule);
            //on injecte le statut a l'agent concerné
            $user_statut = $user->setStatut($statut_val);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evTitularisation);
            //objet2: le statut de l'agent dans fos_user
            $entityManager->persist($user_statut);
            //objet3: le nouveau matricule on l'injecte dans fos_user
            $entityManager->persist($matricule);
            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvTitularisation pour pouvoir récupérer son dernier id
             */
            $posteUser = new PosteUser();

            //Récupération de la valeur persistentCollection POSTE
            $posteUser = new PosteUser();
            $poste = $evTitularisation->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }
            $posteUser->setUser($user)
                ->setNomEvenement('Titularisation')
                ->setIdEvenement($evTitularisation->getId())
                ->setDateEvenementAt($evTitularisation->getDateTitularisationAt())
            ;

            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evTitularisation->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evTitularisation->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Puis au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evTitularisation->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evTitularisation->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_titularisation_index');
            }
        }

        return $this->render('evenements/ev_titularisation/new.html.twig', [
            'ev_titularisation' => $evTitularisation,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'info_matricule' => $info_matricule,
            'info_recrutement' => $info_recrutement,
            'info_titularisation' => $info_titularisation,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_titularisation_show", methods={"GET"})
     */
    public function show(EvTitularisation $evTitularisation,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_titularisation/show.html.twig', [
            'ev_titularisation' => $evTitularisation,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_titularisation_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvTitularisation $evTitularisation): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evTitularisation->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evTitularisation->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_titularisation_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_titularisation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvTitularisation $evTitularisation,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvTitularisationType::class, $evTitularisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evTitularisation);

            //objet2: le statut et matricule de l'agent dans fos_user
            //récupération de l'agent
            $user = $evTitularisation->getUser();

            //On récupère le matricule pour l'injecter à l'utilisateur
            $matricule = $evTitularisation->getNouveauMatricule();
            $user->setMatricule($matricule);
            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvTitularisation pour pouvoir récupérer son dernier id
             */
            $id_evenement = $evTitularisation->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Titularisation');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            //Récupération de la valeur persistentCollection POSTE
            $poste = $evTitularisation->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evTitularisation->getDateTitularisationAt());
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evTitularisation->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evTitularisation->getServiceDepartement());

                //Puis au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evTitularisation->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evTitularisation->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evTitularisation->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_titularisation_index');
            }
        }

        return $this->render('evenements/ev_titularisation/edit.html.twig', [
            'ev_titularisation' => $evTitularisation,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_titularisation_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvTitularisation $evTitularisation,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evTitularisation->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evTitularisation->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Titularisation');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
            /** @var PosteuserServicedepartementdirection $posteUser_servicedpt */
            //Supression du service département associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedpt);
            //Supression du service direction associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedir);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser);

            //Puis dernierement suppression de la ligne de titularisation (evTitularisation) concerné
            $entityManager->remove($evTitularisation);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_titularisation_index');
    }
}
