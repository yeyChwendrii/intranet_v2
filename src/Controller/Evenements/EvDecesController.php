<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvDeces;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvDecesType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvDecesRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/deces")
 */
class EvDecesController extends AbstractController
{
    /**
     * @Route("/", name="evenements_deces_index", methods={"GET"})
     */
    public function index(EvDecesRepository $evDecesRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_deces/index.html.twig', [
            'ev_decess' => $evDecesRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_deces_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        EvDecesRepository $evDecesRepository,
                        ServiceRepository $serviceRepository,
                        UserRepository $userRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evDeces = new EvDeces();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $id = $userRepository->find($url_id);
        $evDeces = $evDeces->setUser($id);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evDeces = $evDeces->setValide(1);
        }else{
            $evDeces = $evDeces->setValide(0);
        }

        $form = $this->createForm(EvDecesType::class, $evDeces);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent
            $user = $evDeces->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal
            $entityManager->persist($evDeces);
            $entityManager->flush();

            //objet2: objet secondaire : on desactive le compte de l'utilisateur puisqu'il est licencié et on met le champs "enabled à 0"
            $dernierIdEvenement = $evDeces->getId();
            $compte_activation = $user
                ->setEnabled(0)
                ->setEnfonction(0)
                ->setEnfonctionData($dernierIdEvenement . ",Décès");

            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_deces_index');
            }
        }

        return $this->render('evenements/ev_deces/new.html.twig', [
            'ev_deces' => $evDeces,
            'form' => $form->createView(),
            'ev_decess' => $evDecesRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_deces_show", methods={"GET"})
     */
    public function show(EvDeces $evDeces,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_deces/show.html.twig', [
            'ev_deces' => $evDeces,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_deces_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvDeces $evDeces): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evDeces->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evDeces->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_deces_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_deces_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvDeces $evDeces,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(EvDecesType::class, $evDeces);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evDeces->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_deces_index');
            }
        }

        return $this->render('evenements/ev_deces/edit.html.twig', [
            'ev_deces' => $evDeces,
            'form' => $form->createView(),
            'ev_deces' => $evDeces,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_deces_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvDeces $evDeces): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evDeces->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evDeces);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_deces_index');
    }
}
