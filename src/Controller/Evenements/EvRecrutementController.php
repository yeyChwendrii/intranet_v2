<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvStage;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvRecrutementType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvStageRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/recrutement")
 */
class EvRecrutementController extends AbstractController
{
    /**
     * @Route("/", name="evenements_recrutement_index", methods={"GET"})
     */
    public function index(EvRecrutementRepository $evRecrutementRepository): Response
    {

        return $this->render('evenements/ev_recrutement/index.html.twig', [
            'ev_recrutements' => $evRecrutementRepository->findSomeFields(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_recrutement_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository, PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        EvStageRepository $evStageRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        DirectionRepository $directionRepository,
                        UserRepository $userRepository): Response
    {
        //recherche de la valeur contractuel dans le StatutRepository
        /*
             titulaire = 1
             contractuel = 2
             stagiaire = 3
         */
        $statut_val = $statutRepository->find(2);

        $evRecrutement = new EvRecrutement();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        //Récupération des données de stage si elles existent
        /** @var EvStage $user_stage */
        $user_stage = $evStageRepository->findByIdUser($url_id);
        $user_recrutement = $evRecrutementRepository->findByIdUser($url_id);
        $info_matricule = 1;
        $info_recrutement = 0;
        if ($user_stage){
            $evRecrutement = $evRecrutement->setAncienMatricule($user_stage->getMatricule());
        }else{
            $evRecrutement = $evRecrutement->setAncienMatricule(NULL);
            $info_matricule = 0;
        }
        if ($user_recrutement){
            $info_recrutement = 1;
        }
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $evRecrutement = $evRecrutement->setUser($user)
                        ->setDecisionAnnulee(1)
                        ->setCategorie($user->getCategorie())
                        ->setIndiceGroupe($user->getIndiceGroupe() ? $user->getIndiceGroupe() : '')
                        ->setIndiceCategorie($user->getIndiceCategorie() ? $user->getIndiceCategorie() : '')
                        ->setIndiceEchelon($user->getIndiceEchelon() ? $user->getIndiceEchelon() : '');
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evRecrutement = $evRecrutement->setValide(1);
        }else{
            $evRecrutement = $evRecrutement->setValide(0);
        }

        $form = $this->createForm(EvRecrutementType::class, $evRecrutement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'agent via son id

            $user = $evRecrutement->getUser();
            //on récupère l'indiceGroupe, indiceCategorie, indiceEchelon pour les insérer dans USER
            $indiceGroupe = $evRecrutement->getIndiceGroupe();
            $indiceCategorie = $evRecrutement->getIndiceCategorie();
            $indiceEchelon = $evRecrutement->getIndiceEchelon();
            $categorie = $evRecrutement->getCategorie();

            //On récupère le matricule pour l'injecter à l'utilisateur
            $nouveau_matricule = $evRecrutement->getNouveauMatricule();

            $donneesUser = $user->setIndiceGroupe($indiceGroupe)
                ->setIndiceCategorie($indiceCategorie)
                ->setIndiceEchelon($indiceEchelon)
                ->setMatricule($nouveau_matricule)
                ->setEnfonction(1)
                ->setStatut($statut_val)
                ->setCategorie($categorie);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evRecrutement);
            //objet2 Les indices dans fos_user
            $entityManager->persist($donneesUser);

            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvRecrutement pour pouvoir récupérer son dernier id
             */
            //Récupération de la valeur persistentCollection POSTE
            $posteUser = new PosteUser();
            $poste = $evRecrutement->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }

            $posteUser->setUser($user)
                ->setNomEvenement('Recrutement')
                ->setIdEvenement($evRecrutement->getId())
                ->setDateEvenementAt($evRecrutement->getDateRecrutementAt());

            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evRecrutement->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evRecrutement->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Puis au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evRecrutement->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evRecrutement->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_recrutement_index');
            }
        }

        return $this->render('evenements/ev_recrutement/new.html.twig', [
            'ev_recrutement' => $evRecrutement,
            'user_recrutement' => $user_recrutement,
            'form' => $form->createView(),
            'info_matricule' => $info_matricule,
            'info_recrutement' => $info_recrutement
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_recrutement_show", methods={"GET"})
     */
    public function show(EvRecrutement $evRecrutement,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_recrutement/show.html.twig', [
            'ev_recrutement' => $evRecrutement,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_recrutement_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvRecrutement $evRecrutement): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evRecrutement->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evRecrutement->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_recrutement_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_recrutement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvRecrutement $evRecrutement,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvRecrutementType::class, $evRecrutement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evRecrutement);
            //objet2: le statut et matricule de l'agent dans fos_user
            //récupération de l'id de l'agent
            $user_id = $evRecrutement->getUser()->getId();
            $user = new User();
            //récupération de l'agent via son id
            $user->getId($user_id);
            $user = $evRecrutement->getUser();
            //On récupère les indices pour les injecter à l'utilisateur (fos_user)
            $indiceGroupe = $evRecrutement->getIndiceGroupe();
            $indiceCategorie = $evRecrutement->getIndiceCategorie();
            $indiceEchelon = $evRecrutement->getIndiceEchelon();
            $categorie = $evRecrutement->getCategorie();

            //On récupère le matricule pour l'injecter à l'utilisateur
            $nouveauMatricule = $evRecrutement->getNouveauMatricule();


            $user->setIndiceGroupe($indiceGroupe)
                ->setIndiceCategorie($indiceCategorie)
                ->setIndiceEchelon($indiceEchelon)
                ->setMatricule($nouveauMatricule)
                ->setCategorie($categorie);

            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvRecrutement pour pouvoir récupérer son dernier id
             */
            $id_evenement = $evRecrutement->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Recrutement');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $poste = $evRecrutement->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evRecrutement->getDateRecrutementAt());
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evRecrutement->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evRecrutement->getServiceDepartement());

                //Puis au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evRecrutement->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evRecrutement->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evRecrutement->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_recrutement_index');
            }
        }

        return $this->render('evenements/ev_recrutement/edit.html.twig', [
            'ev_recrutement' => $evRecrutement,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_recrutement_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvRecrutement $evRecrutement,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evRecrutement->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evRecrutement->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Recrutement');
            //vérification de l'existance de posteUser
            if ($posteUser){
                /** @var PosteUser $posteUser */
                $id_posteUser = $posteUser->getId();
                $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                /** @var PosteuserServicedepartementdirection $posteUser_servicedpt */
                //Supression du service département associé (reconnu par l'id_posteUSer)
                $entityManager->remove($posteUser_servicedpt);
                //Supression du service direction associé (reconnu par l'id_posteUSer)
                $entityManager->remove($posteUser_servicedir);
                //suppression du poste user associé (recconnu par l'id_evenement)
                $entityManager->remove($posteUser);
            }

            //Puis dernierement suppression de la ligne de recrutement (evrecrutement) concerné
            $entityManager->remove($evRecrutement);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_recrutement_index');
    }
}
