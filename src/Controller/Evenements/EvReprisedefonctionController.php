<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvReprisedefonction;
use App\Entity\PosteUser;

use App\Entity\User;
use App\Form\Evenements\EvReprisedefonctionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvReprisedefonctionRepository;
use App\Repository\Evenements\EvRetraiteRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/reprisedefonction")
 */
class EvReprisedefonctionController extends AbstractController
{
    /**
     * @Route("/", name="evenements_reprisedefonction_index", methods={"GET"})
     */
    public function index(EvReprisedefonctionRepository $evReprisedefonctionRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_reprisedefonction/index.html.twig', [
            'ev_reprisedefonctions' => $evReprisedefonctionRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_reprisedefonction_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        PosteRepository $posteRepository,
                        DirectionRepository $directionRepository,
                        EvLicenciementRepository $evLicenciementRepository,
                        EvMiseapiedRepository $evMiseapiedRepository,
                        EvRetraiteRepository $evRetraiteRepository,
                        EvSuspensionRepository $evSuspensionRepository,
                        EvDemissionRepository $evDemissionRepository): Response
    {
        $evReprisedefonction = new EvReprisedefonction();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        //Récupération du poste
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);

        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);
        $service = '';
        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                //$service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }
        }
        else{
            //si non on met "aucun' dans poste
            $poste = $posteRepository->find(13);
            $service = 'Aucun';
            $user_evenement = 0;
        }

        $evReprisedefonction = $evReprisedefonction->setUser($user)
                                            ->setPoste($poste_user ? $poste_user->getPoste() : $aucun)
                                            ->setServiceDepartementDirection($service);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evReprisedefonction = $evReprisedefonction->setValide(1);
        }else{
            $evReprisedefonction = $evReprisedefonction->setValide(0);
        }

        $form = $this->createForm(EvReprisedefonctionType::class, $evReprisedefonction);
        $form->handleRequest($request);


        /*
         * Vérifications d'une valeur de suspension ou d'arret de travail avant réintégration
         */
        //Vérification de démission
        $demission = $evDemissionRepository->findByLastUserField($user);
        $info_demission = 0;
        if ($demission){
            $info_demission = 1;
        }
        //Vérification de licencicement
        $licenciement = $evLicenciementRepository->findByLastUserField($user);
        $info_licenciement = 0;
        if ($licenciement){
            $info_licenciement = 1;
        }
        //Vérification de mise à pied
        $miseapied = $evMiseapiedRepository->findByLastUserField($user);
        $info_miseapied = 0;
        if ($miseapied){
            $info_miseapied = 1;
        }
        //Vérification de retraite
        $retraite = $evRetraiteRepository->findByLastUserField($user);
        $info_retraite = 0;
        if ($retraite){
            $info_retraite = 1;
        }
        //Vérification de suspension
        $suspension = $evSuspensionRepository->findByLastUserField($user);
        $info_suspension = 0;
        if ($suspension){
            $info_suspension = 1;
        }

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de la catégorie
            $user = $evReprisedefonction->getUser();
            //on change la valeur du compte pour l'activer
            $compte_activation = $user->setEnabled(1);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evReprisedefonction);
            //objet2: fos_user

            $entityManager->flush();
            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_reprisedefonction_index');
            }
        }

        return $this->render('evenements/ev_reprisedefonction/new.html.twig', [
            'ev_reprisedefonction' => $evReprisedefonction,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'info_demission' => $info_demission,
            'info_licenciement' =>$info_licenciement,
            'info_miseapied' => $info_miseapied,
            'info_retraite' => $info_retraite,
            'info_suspension' => $info_suspension,
            'event_name' => $event_name
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_reprisedefonction_show", methods={"GET"})
     */
    public function show(EvReprisedefonction $evReprisedefonction,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_reprisedefonction/show.html.twig', [
            'ev_reprisedefonction' => $evReprisedefonction,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_reprisedefonction_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvReprisedefonction $evReprisedefonction): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evReprisedefonction->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evReprisedefonction->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_reprisedefonction_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_reprisedefonction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvReprisedefonction $evReprisedefonction,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvReprisedefonctionType::class, $evReprisedefonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager et in flush
             $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evReprisedefonction->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_reprisedefonction_index');
            }
        }

        return $this->render('evenements/ev_reprisedefonction/edit.html.twig', [
            'ev_reprisedefonction' => $evReprisedefonction,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_reprisedefonction_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvReprisedefonction $evReprisedefonction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evReprisedefonction->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //Puis dernierement suppression de la ligne de Reprisedefonction (evReprisedefonction) concerné
            $entityManager->remove($evReprisedefonction);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_reprisedefonction_index');
    }
}
