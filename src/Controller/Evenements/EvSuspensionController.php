<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvSuspension;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvSuspensionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/suspension")
 */
class EvSuspensionController extends AbstractController
{
    /**
     * @Route("/", name="evenements_suspension_index", methods={"GET"})
     */
    public function index(EvSuspensionRepository $evSuspensionRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_suspension/index.html.twig', [
            'ev_suspensions' => $evSuspensionRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_suspension_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        UserRepository $userRepository,
                        PosteUserRepository $posteUserRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository): Response
    {
        $evSuspension = new EvSuspension();
        //************************************************
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $last_post = $posteUserRepository->findLastByUser($user);
        $info_evenement = 1;
        $event_name = '';
        if ($last_post->getNomEvenement() == 'Nomination') {
            $event_name = 'Nomination';

            $user_nomination = $evNominationRepository->findByIdUser($url_id);
            //si $user_nomination existe | on récupère son poste
            $poste = $user_nomination->getPoste();
            foreach ($poste as $value) {
                $poste = $value;
            }

            $evSuspension = $evSuspension->setPoste($poste)
                ->setServiceDepartementDirection($user_nomination->getServiceDepartement() ? $user_nomination->getServiceDepartement()->getService(): '' .'  /'.
                    $user_nomination->getServiceDirection().'  /'.
                    $user_nomination->getDepartement()->getDepartement()->getDesignation().'  /'.$user_nomination->getDirection());

        }elseif ($last_post->getNomEvenement() == 'Titularisation'){
            $event_name = 'Titularisation';

            $user_titularisation = $evTitularisationRepository->findLastByUserField($url_id);
            $poste = $user_titularisation->getPoste();
            foreach ($poste as $value){
                $poste = $value;
            }
            //si $user_titularisation existe | on récupère son matricule, son poste
            $evSuspension = $evSuspension->setPoste($poste)
                ->setServiceDepartementDirection($user_titularisation->getServiceDepartement() ? $user_titularisation->getServiceDepartement()->getService(): '' .'  /'.
                    $user_titularisation->getServiceDirection());
        }elseif ($last_post->getNomEvenement() == 'Recrutement'){
            $user_recrutement = $evRecrutementRepository->findByIdUser($url_id);
            $event_name = 'Recrutement';
            //si user_recrutement existe | on récupère son matricule, son poste
            $evSuspension = $evSuspension->setPoste($user_recrutement->getPoste())
                ->setServiceDepartementDirection($user_recrutement->getServiceDepartement()->getService().''.$user_recrutement->getServiceDirection());
        }elseif ($last_post->getNomEvenement() == 'Reintegration') {
            $user_reintegration = $evReintegrationRepository->findByIdUser($url_id);
            $event_name = 'Reintegration';
            //si $user_nomination existe | on récupère son poste
            $poste = $user_reintegration->getPoste();
            foreach ($poste as $value) {
                $poste = $value;
            }

            $evSuspension = $evSuspension->setPoste($poste)
                ->setServiceDepartementDirection($user_reintegration->getServiceDepartement() ? $user_reintegration->getServiceDepartement()->getService(): '' .'  /'.
                    $user_reintegration->getServiceDirection().'  /'.
                    $user_reintegration->getDepartement()->getDepartement()->getDesignation().'  /'.$user_reintegration->getDirection());

        }else{
            $evSuspension = $evSuspension->setServiceDepartementDirection(' ');
            $info_evenement = 0;
        }

        $evSuspension = $evSuspension->setUser($user);
        //************************************************
        $evSuspension = $evSuspension->setUser($user);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evSuspension = $evSuspension->setValide(1);
        }else{
            $evSuspension = $evSuspension->setValide(0);
        }

        $form = $this->createForm(EvSuspensionType::class, $evSuspension);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent via son id
            $user = $evSuspension->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal
            $entityManager->persist($evSuspension);

            $compte_activation = $user->setEnabled(0);
            //objet2: objet secondaire : on desactive le compte de l'utilisateur puisqu'il est licencié et on met le champs "enabled à 0"
            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $request->get('dpt_id')]);
            }else {
                return $this->redirectToRoute('evenements_suspension_index');
            }
        }

        return $this->render('evenements/ev_suspension/new.html.twig', [
            'ev_suspension'     => $evSuspension,
            'form'              => $form->createView(),
            'event_name'        => $event_name
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_suspension_show", methods={"GET"})
     */
    public function show(EvSuspension $evSuspension,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_suspension/show.html.twig', [
            'ev_suspension' => $evSuspension,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_suspension_validate", methods={"VALIDATE"})
     * @param Request $request
     * @param EvSuspension $evSuspension
     * @return Response
     */
    public function validate(Request $request,
                             EvSuspension $evSuspension): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evSuspension->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evSuspension->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_suspension_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_suspension_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvSuspension $evSuspension): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvSuspensionType::class, $evSuspension);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evSuspension->getUser()->getId(), 'dpt_id' => $request->get('dpt_id')]);
            }else {
                return $this->redirectToRoute('evenements_suspension_index');
            }
        }

        return $this->render('evenements/ev_suspension/edit.html.twig', [
            'ev_suspension' => $evSuspension,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_suspension_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvSuspension $evSuspension): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evSuspension->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //Récupérattion de l'utilisateur pour pouvoir activer son compte qui était désactivé lors de la saisie de suspension
            $user = $evSuspension->getUser();
            $compte_activation = $user->setEnabled(1);
            $entityManager->persist($compte_activation);
            //Maintenant on supprime les données
            $entityManager->remove($evSuspension);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_suspension_index');
    }
}
