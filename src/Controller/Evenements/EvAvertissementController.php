<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvAvertissement;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvAvertissementType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAvertissementRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/avertissement")
 */
class EvAvertissementController extends AbstractController
{
    /**
     * @Route("/", name="evenements_avertissement_index", methods={"GET"})
     */
    public function index(EvAvertissementRepository $evAvertissementRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_avertissement/index.html.twig', [
            'ev_avertissements' => $evAvertissementRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_avertissement_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        PosteUserRepository $posteUserRepository,
                        PosteRepository $posteRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evAvertissement = new EvAvertissement();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question, via son id envoyé via url
        $id = $userRepository->find($url_id);

        //récupération de l'id_postUser pour afficher son poste actuel (current)
        /** @var PosteUser $posteUser */
        $posteUser = $posteUserRepository->findLastByUser($url_id);
        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);

        $evAvertissement = $evAvertissement->setUser($id)
                                            ->setPoste($posteUser ? $posteUser->getPoste() : $aucun);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evAvertissement = $evAvertissement->setValide(1);
        }else{
            $evAvertissement = $evAvertissement->setValide(0);
        }

        $form = $this->createForm(EvAvertissementType::class, $evAvertissement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            $entityManager->persist($evAvertissement);

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $evAvertissement->getUser()->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evAvertissement->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_avertissement_index');
            }
        }

        return $this->render('evenements/ev_avertissement/new.html.twig', [
            'ev_avertissement' => $evAvertissement,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_avertissement_show", methods={"GET"})
     */
    public function show(EvAvertissement $evAvertissement,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_avertissement/show.html.twig', [
            'ev_avertissement' => $evAvertissement,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_avertissement_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvAvertissement $evAvertissement): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evAvertissement->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evAvertissement->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_avertissement_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_avertissement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvAvertissement $evAvertissement): Response
    {
        $form = $this->createForm(EvAvertissementType::class, $evAvertissement);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evAvertissement->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_avertissement_index');
            }
        }

        return $this->render('evenements/ev_avertissement/edit.html.twig', [
            'ev_avertissement' => $evAvertissement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_avertissement_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvAvertissement $evAvertissement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evAvertissement->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evAvertissement);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_avertissement_index');
    }
}
