<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvRenouvellementdecontrat;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvRenouvellementdecontratType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/renouvellementdecontrat")
 */
class EvRenouvellementdecontratController extends AbstractController
{
    /**
     * @Route("/", name="evenements_renouvellementdecontrat_index", methods={"GET"})
     */
    public function index(EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_renouvellementdecontrat/index.html.twig', [
            'ev_renouvellementdecontrats' => $evRenouvellementdecontratRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_renouvellementdecontrat_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository, PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        DirectionRepository $directionRepository): Response
    {
        //recherche de la valeur contractuel dans le StatutRepository
        /*
             titulaire = 1
             contractuel = 2
             stagiaire = 3
         */
        $statut_val = $statutRepository->find(2);

        $evRenouvellementdecontrat = new EvRenouvellementdecontrat();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        //Récupération des données de recrutement si elles existent
        /** @var EvRecrutement $user_recrutement */
        $user_recrutement = $evRecrutementRepository->findByIdUser($url_id);
        $info_recrutement = 1;
        if ($user_recrutement){
            $poste = $user_recrutement->getPoste(); //récupération de poste
            foreach ($poste as $value){
                /** @var Poste $value */
                $evRenouvellementdecontrat->addPoste($value);
            }
            //si user_recrutement existe | on récupère son matricule, son poste
            $evRenouvellementdecontrat = $evRenouvellementdecontrat->setMatricule($user_recrutement->getNouveauMatricule())
                //->setPoste($user_recrutement->getPoste())
                ->setServiceDepartement($user_recrutement->getServiceDepartement())
                ->setServiceDirection($user_recrutement->getServiceDirection());
        }else{
            $evRenouvellementdecontrat = $evRenouvellementdecontrat->setMatricule(NULL);
            $info_recrutement = 0;
        }
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $evRenouvellementdecontrat = $evRenouvellementdecontrat->setUser($user);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evRenouvellementdecontrat = $evRenouvellementdecontrat->setValide(1);
        }else{
            $evRenouvellementdecontrat = $evRenouvellementdecontrat->setValide(0);
        }

        $form = $this->createForm(EvRenouvellementdecontratType::class, $evRenouvellementdecontrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'agent via son id
            $user = $evRenouvellementdecontrat->getUser();

            //On récupère le matricule pour l'injecter à l'utilisateur
            $nouveau_matricule = $evRenouvellementdecontrat->getMatricule();
            $donnees_user = $user->setMatricule($nouveau_matricule)
                ->setStatut($statut_val);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReenouvellementdecontrat
            $entityManager->persist($evRenouvellementdecontrat);
            //objet2: le statut et le matricule de l'agent dans fos_user
            $entityManager->persist($donnees_user);

            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvRenouvellementdecontrat pour pouvoir récupérer son dernier id
             */
            $posteUser = new PosteUser();

            $poste = $evRenouvellementdecontrat->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }
            $posteUser->setUser($user)
                ->setNomEvenement('Renouvellementdecontrat')
                ->setIdEvenement($evRenouvellementdecontrat->getId())
                ->setDateEvenementAt($evRenouvellementdecontrat->getDateRenouvellementdecontratAt())
            ;

            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evRenouvellementdecontrat->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evRenouvellementdecontrat->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Puis au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evRenouvellementdecontrat->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evRenouvellementdecontrat->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_renouvellementdecontrat_index');
            }
        }

        return $this->render('evenements/ev_renouvellementdecontrat/new.html.twig', [
            'ev_renouvellementdecontrat' => $evRenouvellementdecontrat,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'info_recrutement' => $info_recrutement,
            'user_recrutement' => $user_recrutement,

        ]);
    }

    /**
     * @Route("/{id}", name="evenements_renouvellementdecontrat_show", methods={"GET"})
     */
    public function show(EvRenouvellementdecontrat $evRenouvellementdecontrat,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_renouvellementdecontrat/show.html.twig', [
            'ev_renouvellementdecontrat' => $evRenouvellementdecontrat,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_renouvellementdecontrat_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvRenouvellementdecontrat $evRenouvellementdecontrat): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evRenouvellementdecontrat->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evRenouvellementdecontrat->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_renouvellementdecontrat_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_renouvellementdecontrat_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvRenouvellementdecontrat $evRenouvellementdecontrat,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvRenouvellementdecontratType::class, $evRenouvellementdecontrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReenouvellementdecontrat
            $entityManager->persist($evRenouvellementdecontrat);
            //objet2: le statut et matricule de l'agent dans fos_user
            //récupération de l'id de l'agent
            $user_id = $evRenouvellementdecontrat->getUser()->getId();
            $user = new User();
            //récupération de l'agent via son id
            $user->getId($user_id);
            $user = $evRenouvellementdecontrat->getUser();
            //On récupère le matricule pour l'injecter à l'utilisateur
            $matricule = $evRenouvellementdecontrat->getMatricule();
            $user->setMatricule($matricule);
            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvRenouvellementdecontrat pour pouvoir récupérer son dernier id
             */
            $id_evenement = $evRenouvellementdecontrat->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Renouvellementdecontrat');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $poste = $evRenouvellementdecontrat->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evRenouvellementdecontrat->getDateRenouvellementdecontratAt());
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evRenouvellementdecontrat->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evRenouvellementdecontrat->getServiceDepartement());

                //Puis au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evRenouvellementdecontrat->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evRenouvellementdecontrat->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evRenouvellementdecontrat->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_renouvellementdecontrat_index');
            }
        }

        return $this->render('evenements/ev_renouvellementdecontrat/edit.html.twig', [
            'ev_renouvellementdecontrat' => $evRenouvellementdecontrat,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_renouvellementdecontrat_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvRenouvellementdecontrat $evRenouvellementdecontrat,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evRenouvellementdecontrat->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evRenouvellementdecontrat->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Renouvellementdecontrat');
            //vérification de l'existance de posteUser
            if ($posteUser){
                /** @var PosteUser $posteUser */
                $id_posteUser = $posteUser->getId();
                $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                /** @var PosteuserServicedepartementdirection $posteUser_servicedpt */
                //Supression du service département associé (reconnu par l'id_posteUSer)
                $entityManager->remove($posteUser_servicedpt);
                //Supression du service direction associé (reconnu par l'id_posteUSer)
                $entityManager->remove($posteUser_servicedir);
                //suppression du poste user associé (recconnu par l'id_evenement)
                $entityManager->remove($posteUser);
            }

            //Puis dernierement suppression de la ligne de renouvellementdecontrat (evRenouvellementdecontrat) concerné
            $entityManager->remove($evRenouvellementdecontrat);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_renouvellementdecontrat_index');
    }
}
