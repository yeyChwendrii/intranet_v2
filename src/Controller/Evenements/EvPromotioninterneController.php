<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvPromotioninterneType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/promotioninterne")
 */
class EvPromotioninterneController extends AbstractController
{
    /**
     * @Route("/", name="evenements_promotioninterne_index", methods={"GET"})
     */
    public function index(EvPromotioninterneRepository $evPromotioninterneRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_promotioninterne/index.html.twig', [
            'ev_promotioninternes' => $evPromotioninterneRepository->findSomeFields(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_promotioninterne_new", methods={"GET","POST"})
     */
    public function new(Request $request, PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvPromotioninterneRepository $evPromotioninterneRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvAffectationRepository $evAffectationRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        PosteRepository $posteRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evPromotioninterne = new EvPromotioninterne();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');

        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        //Récupération de PosteUser
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);
        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);
        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            $usr = $poste_user->getUser();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $evPromotioninterne = $evPromotioninterne->setServiceDepartement($service->getServiceDepartement());
                $evPromotioninterne = $evPromotioninterne->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $evPromotioninterne = $evPromotioninterne->setServiceDepartement($service->getServiceDepartement());
                $evPromotioninterne = $evPromotioninterne->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                /** @var EvNomination $ev_nomination */
                $ev_nomination = $evNominationRepository->findByIdUser($usr);

                //$service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evPromotioninterne = $evPromotioninterne->setServiceDepartement($ev_nomination->getServiceDepartement());

                $evPromotioninterne = $evPromotioninterne->setServiceDirection($ev_nomination->getServiceDirection());

                $evPromotioninterne = $evPromotioninterne->setDepartement($ev_nomination ? $ev_nomination->getDepartement() : null);

                $evPromotioninterne = $evPromotioninterne->setDirection($ev_nomination->getDirection());
            }elseif ($nomEvenement == 'Promotioninterne'){
                $event_name = 'Promotioninterne';
                $user_evenement = 1;
                $service = $evPromotioninterneRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evPromotioninterne = $evPromotioninterne->setServiceDepartement($service->getServiceDepartement());

                $evPromotioninterne = $evPromotioninterne->setServiceDirection($service->getServiceDirection());

                $evPromotioninterne = $evPromotioninterne->setDepartement($service ? $service->getDepartement() : null);

                $evPromotioninterne = $evPromotioninterne->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evPromotioninterne = $evPromotioninterne->setServiceDepartement($service->getServiceDepartement());

                $evPromotioninterne = $evPromotioninterne->setServiceDirection($service->getServiceDirection());

                $evPromotioninterne = $evPromotioninterne->setDepartement($service ? $service->getDepartement() : null);

                $evPromotioninterne = $evPromotioninterne->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                $evPromotioninterne = $evPromotioninterne->setServiceDepartement($service->getServiceDepartement());
                $evPromotioninterne = $evPromotioninterne->setServiceDirection($service->getServiceDirection());
            }
        }
        $evPromotioninterne = $evPromotioninterne->setUser($user)
            ->setCategorie($user->getCategorie())
            ->setIndiceGroupe($user->getIndiceGroupe() ? $user->getIndiceGroupe() : '')
            ->setIndiceCategorie($user->getIndiceCategorie() ? $user->getIndiceCategorie() : '')
            ->setIndiceEchelon($user->getIndiceEchelon() ? $user->getIndiceEchelon() : '')
            ->addPoste($poste_user ? $poste_user->getPoste() : $aucun);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evPromotioninterne = $evPromotioninterne->setValide(1);
        }else{
            $evPromotioninterne = $evPromotioninterne->setValide(0);
        }

        $form = $this->createForm(EvPromotioninterneType::class, $evPromotioninterne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'agent via son id
            $user = $evPromotioninterne->getUser();

            //on récupère l'indiceGroupe, indiceCategorie, indiceEchelon pour les insérer dans USER
            $indiceGroupe = $evPromotioninterne->getIndiceGroupe();
            $indiceCategorie = $evPromotioninterne->getIndiceCategorie();
            $indiceEchelon = $evPromotioninterne->getIndiceEchelon();

            $indices_pro = $user->setIndiceGroupe($indiceGroupe)
                ->setIndiceCategorie($indiceCategorie)
                ->setIndiceEchelon($indiceEchelon);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evPromotioninterne);
            //objet2 Les indices dans fos_user
            $entityManager->persist($indices_pro);

            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvPromotioninterne pour pouvoir récupérer son dernier id
             */
            $posteUser = new PosteUser();
            $poste = $evPromotioninterne->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }

            $posteUser->setUser($user)
                ->setNomEvenement('Promotioninterne')
                ->setIdEvenement($evPromotioninterne->getId());
            ;
            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance PosteUser avant Promotioninterne des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant insertion des données
            if ($evPromotioninterne->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evPromotioninterne->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evPromotioninterne->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evPromotioninterne->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evPromotioninterne->getDepartement() != null){
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection($evPromotioninterne->getDepartement());
                $entityManager->persist($posteuserDepartement);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evPromotioninterne->getDirection() != null){
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evPromotioninterne->getDirection());
                $entityManager->persist($posteuserDirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_promotioninterne_index');
            }
        }

        return $this->render('evenements/ev_promotioninterne/new.html.twig', [
            'ev_promotioninterne' => $evPromotioninterne,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'user_evenement' => $user_evenement,
            'event_name' => $event_name,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_promotioninterne_show", methods={"GET"})
     */
    public function show(EvPromotioninterne $evPromotioninterne,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_promotioninterne/show.html.twig', [
            'ev_promotioninterne' => $evPromotioninterne,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_promotioninterne_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvPromotioninterne $evPromotioninterne): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evPromotioninterne->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evPromotioninterne->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_promotioninterne_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_promotioninterne_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvPromotioninterne $evPromotioninterne,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        //définition des url pour les redirections
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvPromotioninterneType::class, $evPromotioninterne);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $user = $evPromotioninterne->getUser();
            //On récupère les indices pour les injecter à l'utilisateur (fos_user)
            $indiceGroupe = $evPromotioninterne->getIndiceGroupe();
            $indiceCategorie = $evPromotioninterne->getIndiceCategorie();
            $indiceEchelon = $evPromotioninterne->getIndiceEchelon();
            $user->setIndiceGroupe($indiceGroupe)
                ->setIndiceCategorie($indiceCategorie)
                ->setIndiceEchelon($indiceEchelon);

            //On flush tous les objets
            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvPromotioninterne pour pouvoir récupérer son id
             */
            $id_evenement = $evPromotioninterne->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Promotioninterne');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            //Récupération de la valeur persistentCollection POSTE
            $poste = $evPromotioninterne->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evPromotioninterne->getDatePromotioninterneAt())
                ;
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant Promotioninterne des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evPromotioninterne->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evPromotioninterne->getServiceDepartement());

                //Au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evPromotioninterne->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evPromotioninterne->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evPromotioninterne->getDepartement() != null){
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection($evPromotioninterne->getDepartement());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evPromotioninterne->getDirection());
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evPromotioninterne->getDirection() != null){
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evPromotioninterne->getDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evPromotioninterne->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_promotioninterne_index');
            }
        }

        return $this->render('evenements/ev_promotioninterne/edit.html.twig', [
            'ev_promotioninterne' => $evPromotioninterne,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_promotioninterne_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvPromotioninterne $evPromotioninterne,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                           PosteuserDirectionRepository $posteuserDirectionRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evPromotioninterne->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evPromotioninterne->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Promotioninterne');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
            $posteUser_departement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_direction = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
            /**
             * @var PosteuserServicedepartementdirection $posteUser_servicedpt
             */
            //Supression du posteUser_service_département associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedpt);
            //Supression du posteUser_service_direction associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedir);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_departement);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_direction);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser);

            //Puis dernierement suppression de la ligne de Promotioninterne (evPromotioninterne) concerné
            $entityManager->remove($evPromotioninterne);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_promotioninterne_index');
    }
}
