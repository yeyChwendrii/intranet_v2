<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvAffectation;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvAffectationType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/affectation")
 */
class EvAffectationController extends AbstractController
{
    /**
     * @Route("/", name="evenements_affectation_index", methods={"GET"})
     */
    public function index(EvAffectationRepository $evAffectationRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_affectation/index.html.twig', [
            'ev_affectations' => $evAffectationRepository->findSomeFields(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_affectation_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository,
                        UserRepository $userRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        PosteRepository $posteRepository,
                        PosteUserRepository $posteUserRepository): Response
    {
        $evAffectation = new EvAffectation();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //Récupération de PosteUser
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);
        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);
        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $evAffectation = $evAffectation->setServiceDepartement($service->getServiceDepartement());
                $evAffectation = $evAffectation->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $evAffectation = $evAffectation->setServiceDepartement($service->getServiceDepartement());
                $evAffectation = $evAffectation->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evAffectation = $evAffectation->setServiceDepartement($service->getServiceDepartement());

                $evAffectation = $evAffectation->setServiceDirection($service->getServiceDirection());

                $evAffectation = $evAffectation->setDepartement($service ? $service->getDepartement() : null);

                $evAffectation = $evAffectation->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evAffectation = $evAffectation->setServiceDepartement($service->getServiceDepartement());

                $evAffectation = $evAffectation->setServiceDirection($service->getServiceDirection());

                $evAffectation = $evAffectation->setDepartement($service ? $service->getDepartement() : null);

                $evAffectation = $evAffectation->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Demisedefonction'){
                $event_name = 'Demisedefonction';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evAffectation = $evAffectation->setServiceDepartement($service->getServiceDepartement());
                $evAffectation = $evAffectation->setServiceDirection($service->getServiceDirection());

            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                $evAffectation = $evAffectation->setServiceDepartement($service->getServiceDepartement());
                $evAffectation = $evAffectation->setServiceDirection($service->getServiceDirection());
            }
        }
        $evAffectation = $evAffectation->setUser($user)
            ->addPoste($poste_user ? $poste_user->getPoste() : $aucun);

        $evAffectation = $evAffectation->setUser($user);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evAffectation = $evAffectation->setValide(1);
        }else{
            $evAffectation = $evAffectation->setValide(0);
        }

        $form = $this->createForm(EvAffectationType::class, $evAffectation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent via son id

            $user = $evAffectation->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evAffectation);

            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvNomination pour pouvoir récupérer son dernier id
             */
            $posteUser = new PosteUser();
            $poste = $evAffectation->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }

            $posteUser->setUser($user)
                ->setNomEvenement('Affectation')
                ->setIdEvenement($evAffectation->getId())
                ->setDateEvenementAt($evAffectation->getDateAffectationAt());
            ;
            ;
            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance PosteUser avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant insertion des données
            if ($evAffectation->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evAffectation->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evAffectation->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evAffectation->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evAffectation->getDepartement() != null){
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection($evAffectation->getDepartement());
                $entityManager->persist($posteuserDepartement);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evAffectation->getDirection() != null){
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evAffectation->getDirection());
                $entityManager->persist($posteuserDirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_affectation_index');
            }
        }

        return $this->render('evenements/ev_affectation/new.html.twig', [
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'ev_affectation' => $evAffectation,
            'form' => $form->createView(),
            'event_name' => $event_name,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_affectation_show", methods={"GET"})
     */
    public function show(EvAffectation $evAffectation): Response
    {
        return $this->render('evenements/ev_affectation/show.html.twig', [
            'ev_affectation' => $evAffectation,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_affectation_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvAffectation $evAffectation): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evAffectation->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evAffectation->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_affectation_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_affectation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvAffectation $evAffectation,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        $form = $this->createForm(EvAffectationType::class, $evAffectation);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On flush tous les objets
            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvAffectation pour pouvoir récupérer son id
             */
            $id_evenement = $evAffectation->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Affectation');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            //Récupération de la valeur persistentCollection POSTE
            $poste = $evAffectation->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evAffectation->getDateAffectationAt());
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant affectation des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evAffectation->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evAffectation->getServiceDepartement());

                //Au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evAffectation->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evAffectation->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evAffectation->getDepartement() != null){
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setServicedirection($evAffectation->getDepartement());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evAffectation->getDirection() != null){
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setServicedirection($evAffectation->getDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evAffectation->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_affectation_index');
            }
        }

        return $this->render('evenements/ev_affectation/edit.html.twig', [
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'ev_affectation' => $evAffectation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_affectation_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvAffectation $evAffectation,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                           PosteuserDirectionRepository $posteuserDirectionRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evAffectation->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evAffectation->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Affectation');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
            $posteUser_departement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_direction = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
            /**
             * @var PosteuserServicedepartementdirection $posteUser_servicedpt
             */
            //Supression du posteUser_service_département associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedpt);
            //Supression du posteUser_service_direction associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedir);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_departement);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_direction);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser);

            //Puis dernierement suppression de la ligne de Affectation (evAffectation) concerné
            $entityManager->remove($evAffectation);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_affectation_index');
    }
}
