<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvSuspensiondesalaire;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\PosteUser;
use App\Entity\User;
use App\Form\Evenements\EvSuspensiondesalaireType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvSuspensiondesalaireRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/suspensiondesalaire")
 */
class EvSuspensiondesalaireController extends AbstractController
{
    /**
     * @Route("/", name="evenements_suspensiondesalaire_index", methods={"GET"})
     */
    public function index(EvSuspensiondesalaireRepository $evSuspensiondesalaireRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_suspensiondesalaire/index.html.twig', [
            'ev_suspensiondesalaires' => $evSuspensiondesalaireRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_suspensiondesalaire_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        PosteRepository $posteRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evSuspensiondesalaire = new EvSuspensiondesalaire();
        //************************************************************
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $users = $userRepository->findAll();

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $last_post = $posteUserRepository->findLastByUser($user);
        $info_evenement = 1;
        $event_name = '';
        if ($last_post->getNomEvenement() == 'Nomination') {
            $event_name = 'Nomination';

            $user_nomination = $evNominationRepository->findByIdUser($url_id);
            //si $user_nomination existe | on récupère son poste
            $poste = $user_nomination->getPoste();
            foreach ($poste as $value) {
                $poste = $value;
            }

            $evSuspensiondesalaire = $evSuspensiondesalaire->setPoste($poste)
                ->setServiceDepartementDirection($user_nomination->getServiceDepartement() ? $user_nomination->getServiceDepartement()->getService(): '' .'  /'.
                    $user_nomination->getServiceDirection().'  /'.
                    $user_nomination->getDepartement()->getDepartement()->getDesignation().'  /'.$user_nomination->getDirection());

        }elseif ($last_post->getNomEvenement() == 'Titularisation'){
            $event_name = 'Titularisation';

            $user_titularisation = $evTitularisationRepository->findLastByUserField($url_id);
            $poste = $user_titularisation->getPoste();
            foreach ($poste as $value){
                $poste = $value;
            }
            //si $user_titularisation existe | on récupère son matricule, son poste
            $evSuspensiondesalaire = $evSuspensiondesalaire->setPoste($poste)
                ->setServiceDepartementDirection($user_titularisation->getServiceDepartement() ? $user_titularisation->getServiceDepartement()->getService(): '' .'  /'.
                    $user_titularisation->getServiceDirection());;
        }elseif ($last_post->getNomEvenement() == 'Recrutement'){
            $user_recrutement = $evRecrutementRepository->findByIdUser($url_id);
            $event_name = 'Recrutement';
            //si user_recrutement existe | on récupère son matricule, son poste
            $evSuspensiondesalaire = $evSuspensiondesalaire->setPoste($user_recrutement->getPoste())
                ->setServiceDepartementDirection($user_recrutement->getServiceDepartement()->getService().''.$user_recrutement->getServiceDirection());
        }elseif ($last_post->getNomEvenement() == 'Reintegration') {
            $user_reintegration = $evReintegrationRepository->findByIdUser($url_id);
            $event_name = 'Reintegration';
            //si $user_nomination existe | on récupère son poste
            $poste = $user_reintegration->getPoste();
            foreach ($poste as $value) {
                $poste = $value;
            }

            $evSuspensiondesalaire = $evSuspensiondesalaire->setPoste($poste)
                ->setServiceDepartementDirection($user_reintegration->getServiceDepartement() ? $user_reintegration->getServiceDepartement()->getService(): '' .'  /'.
                    $user_reintegration->getServiceDirection().'  /'.
                    $user_reintegration->getDepartement()->getDepartement()->getDesignation().'  /'.$user_reintegration->getDirection());

        }else{
            $evSuspensiondesalaire = $evSuspensiondesalaire->setServiceDepartementDirection(' ');
            $info_evenement = 0;
        }

        $evSuspensiondesalaire = $evSuspensiondesalaire->setUser($user);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evSuspensiondesalaire = $evSuspensiondesalaire->setValide(1);
        }else{
            $evSuspensiondesalaire = $evSuspensiondesalaire->setValide(0);
        }

        //************************************************************************************

        //************************************************************************************


        $form = $this->createForm(EvSuspensiondesalaireType::class, $evSuspensiondesalaire);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            $entityManager->persist($evSuspensiondesalaire);
            //objet2: fos_user

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_suspensiondesalaire_index');
            }
        }

        return $this->render('evenements/ev_suspensiondesalaire/new.html.twig', [
            'ev_suspensiondesalaire' => $evSuspensiondesalaire,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'event_name' => $event_name,
            'users' => $users,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_suspensiondesalaire_show", methods={"GET"})
     */
    public function show(EvSuspensiondesalaire $evSuspensiondesalaire,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_suspensiondesalaire/show.html.twig', [
            'ev_suspensiondesalaire' => $evSuspensiondesalaire,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_suspensiondesalaire_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvSuspensiondesalaire $evSuspensiondesalaire): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evSuspensiondesalaire->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evSuspensiondesalaire->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_suspensiondesalaire_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_suspensiondesalaire_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvSuspensiondesalaire $evSuspensiondesalaire,
                         UserRepository $userRepository,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvSuspensiondesalaireType::class, $evSuspensiondesalaire);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager et in flush
             $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evSuspensiondesalaire->getUser()->getId(), 'dpt_id' => $dpt_id,'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_suspensiondesalaire_index');
            }
        }

        return $this->render('evenements/ev_suspensiondesalaire/edit.html.twig', [
            'ev_suspensiondesalaire' => $evSuspensiondesalaire,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'users' => $users = $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_suspensiondesalaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvSuspensiondesalaire $evSuspensiondesalaire): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evSuspensiondesalaire->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //Puis dernierement suppression de la ligne de Suspensiondesalaire (evSuspensiondesalaire) concerné
            $entityManager->remove($evSuspensiondesalaire);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_suspensiondesalaire_index');
    }
}
