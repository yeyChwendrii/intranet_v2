<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvDemission;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvDemissionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/demission")
 */
class EvDemissionController extends AbstractController
{
    /**
     * @Route("/", name="evenements_demission_index", methods={"GET"})
     */
    public function index(EvDemissionRepository $evDemissionRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_demission/index.html.twig', [
            'ev_demissions' => $evDemissionRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_demission_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        UserRepository $userRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository): Response
    {

        $evDemission = new EvDemission();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $id = $userRepository->find($url_id);
        $evDemission = $evDemission->setUser($id);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evDemission = $evDemission->setValide(1);
        }else{
            $evDemission = $evDemission->setValide(0);
        }

        $form = $this->createForm(EvDemissionType::class, $evDemission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent
            $user = $evDemission->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal
            $entityManager->persist($evDemission);
            $entityManager->flush();

            //objet2: objet secondaire : on desactive le compte de l'utilisateur puisqu'il est licencié et on met le champs "enabled à 0"
            $dernierIdEvenement = $evDemission->getId();
            $compte_activation = $user
                ->setEnabled(0)
                ->setEnfonction(0)
                ->setEnfonctionData($dernierIdEvenement . ",Démission");

            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_demission_index');
            }
        }

        return $this->render('evenements/ev_demission/new.html.twig', [
            'ev_demission' => $evDemission,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_demission_show", methods={"GET"})
     */
    public function show(EvDemission $evDemission,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_demission/show.html.twig', [
            'ev_demission' => $evDemission,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_demission_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvDemission $evDemission): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evDemission->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evDemission->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_demission_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_demission_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvDemission $evDemission,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(EvDemissionType::class, $evDemission);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evDemission->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_demission_index');
            }
        }

        return $this->render('evenements/ev_demission/edit.html.twig', [
            'ev_demission' => $evDemission,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_demission_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvDemission $evDemission): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evDemission->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evDemission);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_demission_index');
    }
}
