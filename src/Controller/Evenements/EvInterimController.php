<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvInterim;
use App\Entity\PosteUser;

use App\Entity\User;
use App\Form\Evenements\EvInterimType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvInterimRepository;
use App\Repository\Evenements\EvRetraiteRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/interim")
 */
class EvInterimController extends AbstractController
{
    /**
     * @Route("/", name="evenements_interim_index", methods={"GET"})
     */
    public function index(EvInterimRepository $evInterimRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_interim/index.html.twig', [
            'ev_interims' => $evInterimRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_interim_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        PosteRepository $posteRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evInterim = new EvInterim();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $users = $userRepository->selectMatriculeNomPrenom();

        //Récupération du poste
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);

        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);
        $service = '';
        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                //$service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }
        }
        else{
            //si non on met "aucun' dans poste
            $service = 'Aucun';
            $user_evenement = 0;
        }

        $evInterim = $evInterim->setUser($user)
                                            ->setPoste($poste_user ? $poste_user->getPoste() : $aucun)
                                            ->setServiceDepartementDirection($service);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evInterim = $evInterim->setValide(1);
        }else{
            $evInterim = $evInterim->setValide(0);
        }

        $form = $this->createForm(EvInterimType::class, $evInterim);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de la catégorie
            $user = $evInterim->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            $entityManager->persist($evInterim);

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_interim_index');
            }
        }

        return $this->render('evenements/ev_interim/new.html.twig', [
            'ev_interim' => $evInterim,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'event_name' => $event_name,
            'users' => $users,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_interim_show", methods={"GET"})
     */
    public function show(EvInterim $evInterim,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_interim/show.html.twig', [
            'ev_interim' => $evInterim,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_interim_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvInterim $evInterim): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evInterim->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evInterim->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_interim_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_interim_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvInterim $evInterim,
                         UserRepository $userRepository,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {

        $form = $this->createForm(EvInterimType::class, $evInterim);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager et in flush
             $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evInterim->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_interim_index');
            }
        }

        return $this->render('evenements/ev_interim/edit.html.twig', [
            'ev_interim' => $evInterim,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'users' => $users = $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_interim_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvInterim $evInterim): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evInterim->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //Puis dernierement suppression de la ligne de Interim (evInterim) concerné
            $entityManager->remove($evInterim);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_interim_index');
    }
}
