<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvLicenciement;
use App\Entity\PosteUser;

use App\Entity\User;
use App\Form\Evenements\EvLicenciementType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;

use App\Repository\PosteUserRepository;

use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/licenciement")
 */
class EvLicenciementController extends AbstractController
{
    /**
     * @Route("/", name="evenements_licenciement_index", methods={"GET"})
     */
    public function index(EvLicenciementRepository $evLicenciementRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_licenciement/index.html.twig', [
            'ev_licenciements' => $evLicenciementRepository->findSomeFields(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_licenciement_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        UserRepository $userRepository,
                        PosteRepository $posteRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvDemisedefonctionRepository $evDemisedefonctionRepository,
                        EvNominationRepository $evNominationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evLicenciement = new EvLicenciement();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //Récupération de PosteUser
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);
        //Recherche de l'evenement associé au poste

        $service = '';
        $user_evenement = 1;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            $poste = $poste_user->getPoste();
            if ($nomEvenement == 'Recrutement'){
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Titularisation'){
                $service = $evTitularisationRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $service = $evDemisedefonctionRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Reintegration'){
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                //$service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }
        }else{
            //si non on met "aucun' dans poste
            $poste = $posteRepository->find(13);
            $service = 'Aucun';
            $user_evenement = 0;
        }

        $evLicenciement = $evLicenciement->setUser($user)
            ->setPoste($poste)
            ->setServiceDepartementDirection($service);
        $form = $this->createForm(EvLicenciementType::class, $evLicenciement);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evLicenciement = $evLicenciement->setValide(1);
        }else{
            $evLicenciement = $evLicenciement->setValide(0);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $evLicenciement->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal
            $entityManager->persist($evLicenciement);
            $entityManager->flush();

            //objet2: objet secondaire : on desactive le compte de l'utilisateur puisqu'il est licencié et on met le champs "enabled à 0"
            $dernierIdEvenement = $evLicenciement->getId();
            $compte_activation = $user
                ->setEnabled(0)
                ->setEnfonction(0)
                ->setEnfonctionData($dernierIdEvenement . ",Licenciement");

            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_licenciement_index');
            }
        }

        return $this->render('evenements/ev_licenciement/new.html.twig', [
            'ev_licenciement' => $evLicenciement,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'user_evenement' => $user_evenement,
            'event_name' => $event_name,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_licenciement_show", methods={"GET"})
     */
    public function show(EvLicenciement $evLicenciement, ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_licenciement/show.html.twig', [
            'ev_licenciement' => $evLicenciement,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_licenciement_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvLicenciement $evLicenciement): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evLicenciement->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evLicenciement->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_licenciement_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_licenciement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvLicenciement $evLicenciement,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(EvLicenciementType::class, $evLicenciement);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evLicenciement->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id', 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_licenciement_index');
            }
        }

        return $this->render('evenements/ev_licenciement/edit.html.twig', [
            'ev_licenciement' => $evLicenciement,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_licenciement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvLicenciement $evLicenciement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evLicenciement->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evLicenciement);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_licenciement_index');
    }
}
