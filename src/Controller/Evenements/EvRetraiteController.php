<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvRetraite;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvRetraiteType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRetraiteRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/retraite")
 */
class EvRetraiteController extends AbstractController
{
    /**
     * @Route("/", name="evenements_retraite_index", methods={"GET"})
     */
    public function index(EvRetraiteRepository $evRetraiteRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_retraite/index.html.twig', [
            'ev_retraites' => $evRetraiteRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_retraite_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        UserRepository $userRepository,
                        PosteRepository $posteRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvDemisedefonctionRepository $evDemisedefonctionRepository,
                        EvNominationRepository $evNominationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $evRetraite = new EvRetraite();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //Récupération de PosteUser
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);
        //Recherche de l'evenement associé au poste

        $service = '';
        $user_evenement = 1;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            $poste = $poste_user->getPoste();
            if ($nomEvenement == 'Recrutement'){
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Titularisation'){
                $service = $evTitularisationRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Demisedefonction'){
                $service = $evDemisedefonctionRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Reintegration'){
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                //$service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }
        }else{
            //si non on met "aucun' dans poste
            $poste = $posteRepository->find(13);
            $service = 'Aucun';
            $user_evenement = 0;
        }

        $evRetraite = $evRetraite->setUser($user)
                                ->setPoste($poste)
                                ->setServiceDepartementDirection($service);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evRetraite = $evRetraite->setValide(1);
        }else{
            $evRetraite = $evRetraite->setValide(0);
        }

        $form = $this->createForm(EvRetraiteType::class, $evRetraite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $evRetraite->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal
            $entityManager->persist($evRetraite);
            $entityManager->flush();

            //objet2: objet secondaire : on desactive le compte de l'utilisateur puisqu'il est licencié et on met le champs "enabled à 0"
            $dernierIdEvenement = $evRetraite->getId();
            $compte_activation = $user
                ->setEnabled(0)
                ->setEnfonction(0)
                ->setEnfonctionData($dernierIdEvenement . ",Retraite");

            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id' => $svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_retraite_index');
            }
        }

        return $this->render('evenements/ev_retraite/new.html.twig', [
            'ev_retraite' => $evRetraite,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'user_evenement' => $user_evenement,
            'event_name' => $event_name,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_retraite_show", methods={"GET"})
     */
    public function show(EvRetraite $evRetraite, ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_retraite/show.html.twig', [
            'ev_retraite' => $evRetraite,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_retraite_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvRetraite $evRetraite): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evRetraite->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evRetraite->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_retraite_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_retraite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvRetraite $evRetraite,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(EvRetraiteType::class, $evRetraite);
        $form->handleRequest($request);

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evRetraite->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id' => $svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_retraite_index');
            }
        }

        return $this->render('evenements/ev_retraite/edit.html.twig', [
            'ev_retraite' => $evRetraite,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_retraite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvRetraite $evRetraite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evRetraite->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evRetraite);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_retraite_index');
    }
}
