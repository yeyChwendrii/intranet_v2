<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvMiseapied;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvMiseapiedType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/miseapied")
 */
class EvMiseapiedController extends AbstractController
{
    /**
     * @Route("/", name="evenements_miseapied_index", methods={"GET"})
     */
    public function index(EvMiseapiedRepository $evMiseapiedRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_miseapied/index.html.twig', [
            'ev_miseapieds' => $evMiseapiedRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_miseapied_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evMiseapied = new EvMiseapied();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $id = $userRepository->find($url_id);
        $evMiseapied = $evMiseapied->setUser($id);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evMiseapied = $evMiseapied->setValide(1);
        }else{
            $evMiseapied = $evMiseapied->setValide(0);
        }

        $form = $this->createForm(EvMiseapiedType::class, $evMiseapied);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent
            $user = $evMiseapied->getUser();
            //on change la valeur du compte pour le desactiver
            $compte_activation = $user->setEnabled(0);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal
            $entityManager->persist($evMiseapied);
            //objet2: objet secondaire : on desactive le compte de l'utilisateur puisqu'il est licencié et on met le champs "enabled à 0"
            $entityManager->persist($compte_activation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_miseapied_index');
            }
        }

        return $this->render('evenements/ev_miseapied/new.html.twig', [
            'ev_miseapied' => $evMiseapied,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_miseapied_show", methods={"GET"})
     */
    public function show(EvMiseapied $evMiseapied,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_miseapied/show.html.twig', [
            'ev_miseapied' => $evMiseapied,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_miseapied_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvMiseapied $evMiseapied): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evMiseapied->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evMiseapied->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_miseapied_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_miseapied_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvMiseapied $evMiseapied,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(EvMiseapiedType::class, $evMiseapied);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evMiseapied->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_miseapied_index');
            }
        }

        return $this->render('evenements/ev_miseapied/edit.html.twig', [
            'ev_miseapied' => $evMiseapied,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_miseapied_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvMiseapied $evMiseapied): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evMiseapied->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evMiseapied);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_miseapied_index');
    }
}
