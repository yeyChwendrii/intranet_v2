<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvAnnulationdedecision;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\User;
use App\Form\Evenements\EvAnnulationdedecisionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAnnulationdedecisionRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/annulationdedecision")
 */
class EvAnnulationdedecisionController extends AbstractController
{
    /**
     * @Route("/", name="evenements_annulationdedecision_index", methods={"GET"})
     */
    public function index(EvAnnulationdedecisionRepository $evAnnulationdedecisionRepository): Response
    {
        return $this->render('evenements/ev_annulationdedecision/index.html.twig', [
            'ev_annulationdedecisions' => $evAnnulationdedecisionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_annulationdedecision_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        UserRepository $userRepository): Response
    {
        $evAnnulationdedecision = new EvAnnulationdedecision();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        $enregistrement_impossible = 0;
        $recrutement = $evRecrutementRepository->findLastByUserField($user);
        $info_recrutement = 0;
        if ($recrutement){
            $evAnnulationdedecision = $evAnnulationdedecision->setMatricule($user->getMatricule())
                                                            ->setDecisionAAnnuler('Recrutement');
            $info_recrutement = 1;
            $enregistrement_impossible = $enregistrement_impossible + 1;
        }

        $titularisation = $evTitularisationRepository->findLastByUserField($user);
        $info_titularisation = 0;
        if ($titularisation){
            $evAnnulationdedecision = $evAnnulationdedecision->setMatricule($user->getMatricule())
                ->setDecisionAAnnuler('Titularisation');
            $info_titularisation = 1;
            $enregistrement_impossible = $enregistrement_impossible + 1;
        }

        $evAnnulationdedecision = $evAnnulationdedecision->setUser($user);

        $form = $this->createForm(EvAnnulationdedecisionType::class, $evAnnulationdedecision);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent via son id
            $user = $evAnnulationdedecision->getUser();

            //On récupère la décision a annuler pour l'injecter à l'evenelent concerné
            if ($evAnnulationdedecision->getDecisionAAnnuler()== 'Recrutement'){
                /** @var EvRecrutement $evenement */
                $evenement = $evRecrutementRepository->findByIdUser($user);
                $decision_info = $evenement->setDecisionAnnulee(0);
            }else{
                /** @var EvTitularisation $evenement */
                $evenement = $evTitularisationRepository->findByIdUser($user);
                $decision_info = $evenement->setDecisionAnnulee(0);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($evAnnulationdedecision);

            $entityManager->persist($decision_info);

            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt')){
                return $this->redirectToRoute('info_evenement_agent', ['id'=>$user->getId()]);
            }else {
                return $this->redirectToRoute('evenements_annulationdedecision_index');
            }
        }

        return $this->render('evenements/ev_annulationdedecision/new.html.twig', [
            'ev_annulationdedecision' => $evAnnulationdedecision,
            'form' => $form->createView(),
            'info_recrutement' => $info_recrutement,
            'info_titularisation' => $info_titularisation,
            'enregistrement_impossible' => $enregistrement_impossible
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_annulationdedecision_show", methods={"GET"})
     */
    public function show(EvAnnulationdedecision $evAnnulationdedecision,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_annulationdedecision/show.html.twig', [
            'ev_annulationdedecision' => $evAnnulationdedecision,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="evenements_annulationdedecision_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         EvAnnulationdedecision $evAnnulationdedecision,
                         EvRecrutementRepository $evRecrutementRepository,
                         EvTitularisationRepository $evTitularisationRepository): Response
    {
        $form = $this->createForm(EvAnnulationdedecisionType::class, $evAnnulationdedecision);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'agent via son id
            $user = $evAnnulationdedecision->getUser();

            //On récupère la décision a annuler pour l'injecter à l'evenelent concerné
            if ($evAnnulationdedecision->getDecisionAAnnuler()== 'Recrutement'){
                /** @var EvRecrutement $evenement */
                $evenement = $evRecrutementRepository->findByIdUser($user);
                $evenement->setDecisionAnnulee(0);
            }else{
                /** @var EvTitularisation $evenement */
                $evenement = $evTitularisationRepository->findByIdUser($user);
                $evenement->setDecisionAnnulee(0);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');
            return $this->redirectToRoute('evenements_annulationdedecision_index', [
                'id' => $evAnnulationdedecision->getId(),
            ]);
        }

        return $this->render('evenements/ev_annulationdedecision/edit.html.twig', [
            'ev_annulationdedecision' => $evAnnulationdedecision,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_annulationdedecision_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvAnnulationdedecision $evAnnulationdedecision): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evAnnulationdedecision->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evAnnulationdedecision);
            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_annulationdedecision_index');
    }
}
