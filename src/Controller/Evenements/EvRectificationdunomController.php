<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvRectificationdunom;
use App\Entity\PosteUser;
use App\Entity\User;
use App\Form\Evenements\EvRectificationdunomType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRectificationdunomRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/rectificationdunom")
 */
class EvRectificationdunomController extends AbstractController
{
    /**
     * @Route("/", name="evenements_rectificationdunom_index", methods={"GET"})
     */
    public function index(EvRectificationdunomRepository $evRectificationdunomRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_rectificationdunom/index.html.twig', [
            'ev_rectificationdunoms' => $evRectificationdunomRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_rectificationdunom_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvAffectationRepository $evAffectationRepository,
                        PosteRepository $posteRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evRectificationdunom = new EvRectificationdunom();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $users = $userRepository->findAll();

        //Récupération du poste
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);

        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);
        $service = '';
        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                //$service = $service ? $service->getServiceDepartement() : $service->getServiceDirection();
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné
                $serviceDept = $service->getServiceDepartement();
                $serviceDir = $service->getServiceDirection();
                $departement = $service->getDepartement();
                $direction = $service->getDirection();
                if ($serviceDept){
                    $service = $serviceDept;
                }elseif ($serviceDir){
                    $service = $serviceDir;
                }elseif ($departement){
                    $service = $departement;
                }elseif ($direction){
                    $service = $direction;
                }
            }
        }
        else{
            //si non on met "aucun' dans poste
            $service = 'Aucun';
            $user_evenement = 0;
        }

        $evRectificationdunom = $evRectificationdunom->setUser($user)
                                            ->setAncienNom($user->getNom())
                                            ->setAncienPrenom($user->getPrenom())
                                            ->setPoste($poste_user ? $poste_user->getPoste() : $aucun)
                                            ->setServiceDepartementDirection($service);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evRectificationdunom = $evRectificationdunom->setValide(1);
        }else{
            $evRectificationdunom = $evRectificationdunom->setValide(0);
        }

        $form = $this->createForm(EvRectificationdunomType::class, $evRectificationdunom);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de la catégorie
            $user = $evRectificationdunom->getUser();
            //on change la valeur du nom et prénom au niveau de fos_user
            $nouveauNom = $evRectificationdunom->getNouveauNom();
            $nouveauPrenom = $evRectificationdunom->getNouveauPrenom();
            $user_info = $user->setNom($nouveauNom)
                            ->setPrenom($nouveauPrenom);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evRectificationdunom);
            //objet2: fos_user
            $entityManager->flush();

            $entityManager->persist($user_info);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_rectificationdunom_index');
            }
        }

        return $this->render('evenements/ev_rectificationdunom/new.html.twig', [
            'ev_rectificationdunom' => $evRectificationdunom,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'event_name' => $event_name,
            'users' => $users,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_rectificationdunom_show", methods={"GET"})
     */
    public function show(EvRectificationdunom $evRectificationdunom,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_rectificationdunom/show.html.twig', [
            'ev_rectificationdunom' => $evRectificationdunom,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_rectificationdunom_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvRectificationdunom $evRectificationdunom): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evRectificationdunom->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evRectificationdunom->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_rectificationdunom_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_rectificationdunom_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvRectificationdunom $evRectificationdunom,
                         UserRepository $userRepository,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvRectificationdunomType::class, $evRectificationdunom);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user = $evRectificationdunom->getUser();
            //récupération de l'entité manager et in flush
            $nouveauNom = $evRectificationdunom->getNouveauNom();
            $nouveauPrenom = $evRectificationdunom->getNouveauPrenom();
            $user->setNom($nouveauNom)
                ->setPrenom($nouveauPrenom);
             $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evRectificationdunom->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_rectificationdunom_index');
            }
        }

        return $this->render('evenements/ev_rectificationdunom/edit.html.twig', [
            'ev_rectificationdunom' => $evRectificationdunom,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'users' => $users = $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_rectificationdunom_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvRectificationdunom $evRectificationdunom): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evRectificationdunom->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //Puis dernierement suppression de la ligne de Rectificationdunom (evRectificationdunom) concerné
            $entityManager->remove($evRectificationdunom);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_rectificationdunom_index');
    }
}
