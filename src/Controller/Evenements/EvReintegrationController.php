<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvReintegration;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvReintegrationType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvRetraiteRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/reintegration")
 */
class EvReintegrationController extends AbstractController
{
    /**
     * @Route("/", name="evenements_reintegration_index", methods={"GET"})
     */
    public function index(EvReintegrationRepository $evReintegrationRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_reintegration/index.html.twig', [
            'ev_reintegrations' => $evReintegrationRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_reintegration_new", methods={"GET","POST"})
     */
    public function new(Request $request, PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvPromotioninterneRepository $evPromotioninterneRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvAffectationRepository $evAffectationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        PosteRepository $posteRepository,
                        DepartementRepository $departementRepository,
                        EvDemissionRepository $evDemissionRepository,
                        EvLicenciementRepository $evLicenciementRepository,
                        EvRetraiteRepository $evRetraiteRepository,
                        EvMiseapiedRepository $evMiseapiedRepository,
                        EvSuspensionRepository $evSuspensionRepository,
                        UserRepository $userRepository,
                        DirectionRepository $directionRepository): Response
    {
        $evReintegration = new EvReintegration();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');

        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        //Récupération de PosteUser
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);

        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);
        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());
                $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());
                $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                $service = $evNominationRepository->find($idEvenement);
                $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());
                $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Reintegration'){
                $event_name = 'Reintegration';
                $user_evenement = 1;
                $service = $evReintegrationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                    $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());

                    $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());

                    $evReintegration = $evReintegration->setDepartement($service ? $service->getDepartement() : null);

                    $evReintegration = $evReintegration->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Promotioninterne'){
                $event_name = 'Promotioninterne';
                $user_evenement = 1;
                $service = $evPromotioninterneRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                    $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());

                    $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());

                    $evReintegration = $evReintegration->setDepartement($service ? $service->getDepartement() : null);

                    $evReintegration = $evReintegration->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());

                $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());

                $evReintegration = $evReintegration->setDepartement($service ? $service->getDepartement() : null);

                $evReintegration = $evReintegration->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                $evReintegration = $evReintegration->setServiceDepartement($service->getServiceDepartement());
                $evReintegration = $evReintegration->setServiceDirection($service->getServiceDirection());
            }
        }
        $evReintegration = $evReintegration->setUser($user)
            ->setIndiceGroupe($user->getIndiceGroupe() ? $user->getIndiceGroupe() : '')
            ->setIndiceCategorie($user->getIndiceCategorie() ? $user->getIndiceCategorie() : '')
            ->setIndiceEchelon($user->getIndiceEchelon() ? $user->getIndiceEchelon() : '')
            ->setCategorie($user->getCategorie())
            ->addPoste($poste_user ? $poste_user->getPoste() : $aucun);

        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evReintegration = $evReintegration->setValide(1);
        }else{
            $evReintegration = $evReintegration->setValide(0);
        }
        /*
         * Vérifications d'une valeur de suspension ou d'arret de travail avant réintégration
         */
        //Vérification de démission
        $demission = $evDemissionRepository->findByLastUserField($user);
        $info_demission = 0;
        if ($demission){
            $info_demission = 1;
        }
        //Vérification de licencicement
        $licenciement = $evLicenciementRepository->findByLastUserField($user);
        $info_licenciement = 0;
        if ($licenciement){
            $info_licenciement = 1;
        }
        //Vérification de mise à pied
        $miseapied = $evMiseapiedRepository->findByLastUserField($user);
        $info_miseapied = 0;
        if ($miseapied){
            $info_miseapied = 1;
        }
        //Vérification de retraite
        $retraite = $evRetraiteRepository->findByLastUserField($user);
        $info_retraite = 0;
        if ($retraite){
            $info_retraite = 1;
        }
        //Vérification de suspension
        $suspension = $evSuspensionRepository->findByLastUserField($user);
        $info_suspension = 0;
        if ($suspension){
            $info_suspension = 1;
        }

        $form = $this->createForm(EvReintegrationType::class, $evReintegration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'agent via son id
            $user = $evReintegration->getUser();

            //on récupère l'indiceGroupe, indiceCategorie, indiceEchelon pour les insérer dans USER
            $indiceGroupe = $evReintegration->getIndiceGroupe();
            $indiceCategorie = $evReintegration->getIndiceCategorie();
            $indiceEchelon = $evReintegration->getIndiceEchelon();

            $indices_pro = $user->setIndiceGroupe($indiceGroupe)
                                ->setIndiceCategorie($indiceCategorie)
                                ->setIndiceEchelon($indiceEchelon);

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evReintegration);
            //objet2 Les indices dans fos_user
            $entityManager->persist($indices_pro);

            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvReintegration pour pouvoir récupérer son dernier id
             */
            $posteUser = new PosteUser();
            $poste = $evReintegration->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }

            $posteUser->setUser($user)
                ->setNomEvenement('Reintegration')
                ->setIdEvenement($evReintegration->getId())
                ->setDateEvenementAt($evReintegration->getDateReintegrationAt())
            ;

            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance PosteUser avant Reintegration des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant insertion des données
            if ($evReintegration->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evReintegration->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evReintegration->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evReintegration->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evReintegration->getDepartement() != null){
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection($evReintegration->getDepartement());
                $entityManager->persist($posteuserDepartement);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evReintegration->getDirection() != null){
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evReintegration->getDirection());
                $entityManager->persist($posteuserDirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_reintegration_index');
            }
        }

        return $this->render('evenements/ev_reintegration/new.html.twig', [
            'ev_reintegration' => $evReintegration,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'user_evenement' => $user_evenement,
            'info_demission' => $info_demission,
            'info_licenciement' =>$info_licenciement,
            'info_miseapied' => $info_miseapied,
            'info_retraite' => $info_retraite,
            'info_suspension' => $info_suspension,
            'event_name' => $event_name,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_reintegration_show", methods={"GET"})
     */
    public function show(EvReintegration $evReintegration,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_reintegration/show.html.twig', [
            'ev_reintegration' => $evReintegration,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_reintegration_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvReintegration $evReintegration): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evReintegration->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evReintegration->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_reintegration_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_reintegration_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvReintegration $evReintegration,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvReintegrationType::class, $evReintegration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $user = $evReintegration->getUser();
            //On récupère les indices pour les injecter à l'utilisateur (fos_user)
            $indiceGroupe = $evReintegration->getIndiceGroupe();
            $indiceCategorie = $evReintegration->getIndiceCategorie();
            $indiceEchelon = $evReintegration->getIndiceEchelon();
            $user->setIndiceGroupe($indiceGroupe)
                 ->setIndiceCategorie($indiceCategorie)
                 ->setIndiceEchelon($indiceEchelon);

            //On flush tous les objets
            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvReintegration pour pouvoir récupérer son id
             */
            $id_evenement = $evReintegration->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Reintegration');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            //Récupération de la valeur persistentCollection POSTE
            $poste = $evReintegration->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evReintegration->getDateReintegrationAt());
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant Reintegration des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evReintegration->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evReintegration->getServiceDepartement());

                //Au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evReintegration->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evReintegration->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evReintegration->getDepartement() != null){
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection($evReintegration->getDepartement());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evReintegration->getDirection());
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evReintegration->getDirection() != null){
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evReintegration->getDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evReintegration->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_reintegration_index');
            }
        }

        return $this->render('evenements/ev_reintegration/edit.html.twig', [
            'ev_reintegration' => $evReintegration,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_reintegration_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvReintegration $evReintegration,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                           PosteuserDirectionRepository $posteuserDirectionRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evReintegration->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evReintegration->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Reintegration');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
            $posteUser_departement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_direction = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
            /**
             * @var PosteuserServicedepartementdirection $posteUser_servicedpt
             */
            //Supression du posteUser_service_département associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedpt);
            //Supression du posteUser_service_direction associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedir);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_departement);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_direction);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser);

            //Puis dernierement suppression de la ligne de Reintegration (evReintegration) concerné
            $entityManager->remove($evReintegration);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_reintegration_index');
    }
}
