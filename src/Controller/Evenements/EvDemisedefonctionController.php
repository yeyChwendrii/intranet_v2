<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvDemisedefonction;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserDepartementdirection;
use App\Entity\PosteuserDirection;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\PosteuserServicedirection;
use App\Entity\User;
use App\Form\Evenements\EvDemisedefonctionType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\PosteRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/demisedefonction")
 */
class EvDemisedefonctionController extends AbstractController
{
    /**
     * @Route("/", name="evenements_demisedefonction_index", methods={"GET"})
     */
    public function index(EvDemisedefonctionRepository $evDemisedefonctionRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_demisedefonction/index.html.twig', [
            'ev_demisedefonctions' => $evDemisedefonctionRepository->findSomeFields(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_demisedefonction_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository,
                        UserRepository $userRepository,
                        EvTitularisationRepository $evTitularisationRepository,
                        EvNominationRepository $evNominationRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        EvReintegrationRepository $evReintegrationRepository,
                        EvDemisedefonctionRepository $evDemisedefonctionRepository,
                        EvAffectationRepository $evAffectationRepository,
                        PosteRepository $posteRepository,
                        PosteUserRepository $posteUserRepository): Response
    {
        $evDemisedefonction = new EvDemisedefonction();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);

        //Récupération de PosteUser
        /** @var PosteUser $poste_user */
        $poste_user = $posteUserRepository->findLastByUser($user);
        //récupération de  "aucun' dans poste
        $aucun = $posteRepository->find(13);

        $user_evenement = 0;
        $event_name = '';
        if ($poste_user && $poste_user->getIdEvenement() != null){
            $nomEvenement = $poste_user->getNomEvenement();
            $idEvenement = $poste_user->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $user_evenement = 1;
                $event_name = 'Recrutement';
                $service = $evRecrutementRepository->find($idEvenement);
                $evDemisedefonction = $evDemisedefonction->setServiceDepartement($service->getServiceDepartement());
                $evDemisedefonction = $evDemisedefonction->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Titularisation'){
                $event_name = 'Titularisation';
                $user_evenement = 1;
                $service = $evTitularisationRepository->find($idEvenement);
                $evDemisedefonction = $evDemisedefonction->setServiceDepartement($service->getServiceDepartement());
                $evDemisedefonction = $evDemisedefonction->setServiceDirection($service->getServiceDirection());
            }elseif ($nomEvenement == 'Nomination'){
                $event_name = 'Nomination';
                $user_evenement = 1;
                $service = $evNominationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evDemisedefonction = $evDemisedefonction->setServiceDepartement($service->getServiceDepartement());

                $evDemisedefonction = $evDemisedefonction->setServiceDirection($service->getServiceDirection());

                $evDemisedefonction = $evDemisedefonction->setDepartement($service ? $service->getDepartement() : null);

                $evDemisedefonction = $evDemisedefonction->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Affectation'){
                $event_name = 'Affectation';
                $user_evenement = 1;
                $service = $evAffectationRepository->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evDemisedefonction = $evDemisedefonction->setServiceDepartement($service->getServiceDepartement());

                $evDemisedefonction = $evDemisedefonction->setServiceDirection($service->getServiceDirection());

                $evDemisedefonction = $evDemisedefonction->setDepartement($service ? $service->getDepartement() : null);

                $evDemisedefonction = $evDemisedefonction->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Demisedefonction'){
                $event_name = 'Demisedefonction';
                $user_evenement = 1;
                $service = $evDemisedefonction->find($idEvenement);
                //Test conditionnel pour savoir si c un serviceDept | serviceDir | dept | dir, qui est concerné

                $evDemisedefonction = $evDemisedefonction->setServiceDepartement($service->getServiceDepartement());

                $evDemisedefonction = $evDemisedefonction->setServiceDirection($service->getServiceDirection());

                $evDemisedefonction = $evDemisedefonction->setDepartement($service ? $service->getDepartement() : null);

                $evDemisedefonction = $evDemisedefonction->setDirection($service->getDirection());
            }elseif ($nomEvenement == 'Reintegration'){
                $user_evenement = 1;
                $event_name = 'Reintegration';
                $service = $evReintegrationRepository->find($idEvenement);
                $evDemisedefonction = $evDemisedefonction->setServiceDepartement($service->getServiceDepartement());
                $evDemisedefonction = $evDemisedefonction->setServiceDirection($service->getServiceDirection());
            }
        }
        $evDemisedefonction = $evDemisedefonction->setUser($user)
            //->addPoste($poste_user ? $poste_user->getPoste() : $aucun)
            ->setAncienposte($poste_user ? $poste_user->getPoste()->getDesignation() : $aucun->getDesignation())
        ;

        $evDemisedefonction = $evDemisedefonction->setUser($user);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evDemisedefonction = $evDemisedefonction->setValide(1);
        }else{
            $evDemisedefonction = $evDemisedefonction->setValide(0);
        }

        $form = $this->createForm(EvDemisedefonctionType::class, $evDemisedefonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'agent via son id

            $user = $evDemisedefonction->getUser();

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReecrutement
            $entityManager->persist($evDemisedefonction);

            $entityManager->flush();

            /*
             * Ajout de poste dans PosteUser
             * après le flush de l'entité EvNomination pour pouvoir récupérer son dernier id
             */
            $posteUser = new PosteUser();
            $poste = $evDemisedefonction->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value);
            }

            $posteUser->setUser($user)
                ->setNomEvenement('Demisedefonction')
                ->setIdEvenement($evDemisedefonction->getId())
                ->setDateEvenementAt($evDemisedefonction->getDateDemisedefonctionAt());
            ;
            ;
            $entityManager->persist($posteUser);
            $entityManager->flush();

            /*
             * Ajout dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance PosteUser avant demisedefonction des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant insertion des données
            if ($evDemisedefonction->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evDemisedefonction->getServiceDepartement());
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evDemisedefonction->getServiceDirection() != null){
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evDemisedefonction->getServiceDirection());
                $entityManager->persist($posteuserServicedirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evDemisedefonction->getDepartement() != null){
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection($evDemisedefonction->getDepartement());
                $entityManager->persist($posteuserDepartement);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                $entityManager->persist($posteuserDirection);

                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evDemisedefonction->getDirection() != null){
                $posteuserDirection = new PosteuserDirection();
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection($evDemisedefonction->getDirection());
                $entityManager->persist($posteuserDirection);

                //Au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = new PosteuserServicedepartementdirection();
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);
                $entityManager->persist($posteuserServicedepartementdirection);

                //Au PosteUserServicedirection on met un null au champs servicedirection_id
                $posteuserServicedirection = new PosteuserServicedirection();
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);
                $entityManager->persist($posteuserServicedirection);

                //Au PosteUserDepartement on met un null au champs departementdirection_id
                $posteuserDepartement = new PosteuserDepartementdirection();
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                $entityManager->persist($posteuserDepartement);

                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_demisedefonction_index');
            }
        }

        return $this->render('evenements/ev_demisedefonction/new.html.twig', [
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'ev_demisedefonction' => $evDemisedefonction,
            'form' => $form->createView(),
            'event_name' => $event_name,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_demisedefonction_show", methods={"GET"})
     */
    public function show(EvDemisedefonction $evDemisedefonction): Response
    {
        return $this->render('evenements/ev_demisedefonction/show.html.twig', [
            'ev_demisedefonction' => $evDemisedefonction,
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_demisedefonction_validate", methods={"VALIDATE"})
     */
    public function validate(Request $request,
                             EvDemisedefonction $evDemisedefonction): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evDemisedefonction->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evDemisedefonction->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_demisedefonction_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_demisedefonction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvDemisedefonction $evDemisedefonction,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                         PosteuserDirectionRepository $posteuserDirectionRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        $form = $this->createForm(EvDemisedefonctionType::class, $evDemisedefonction);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On flush tous les objets
            $entityManager->flush();

            /*
             * Modification de poste dans PosteUser
             * après le flush de l'entité EvDemisedefonction pour pouvoir récupérer son id
             */
            $id_evenement = $evDemisedefonction->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Demisedefonction');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            //Récupération de la valeur persistentCollection POSTE
            $poste = $evDemisedefonction->getPoste();
            foreach ($poste as $value){
                /** @var Poste $value */
                $posteUser->setPoste($value)
                ->setDateEvenementAt($evDemisedefonction->getDateDemisedefonctionAt());
            }

            $entityManager->flush();

            /*
             * Modification dans PosteuserServicedepartementdirection OU PosteuserServicedirection
             * après le flush de l'entité PosteUser pour pouvoir récupérer son dernier id
             */
            //Récupération de l'instance avant demisedefonction des valeurs
            $posteuser = $posteUserRepository->find($posteUser->getId());

            //Vérification du service qui n'est pas NULL avant d'insérer
            if ($evDemisedefonction->getServiceDepartement() != null){
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection($evDemisedefonction->getServiceDepartement());

                //Au PosteUserServicedirection on met un null au champs service
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de donner
                $entityManager->flush();

            }elseif ($evDemisedefonction->getServiceDirection() != null){
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection($evDemisedefonction->getServiceDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evDemisedefonction->getDepartement() != null){
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setServicedirection($evDemisedefonction->getDepartement());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDirection on met un null au champs direction_id
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setDirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }elseif ($evDemisedefonction->getDirection() != null){
                $posteuserDirection = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserDirection->setPosteuser($posteuser)
                    ->setServicedirection($evDemisedefonction->getDirection());

                //puis au PosteuserServiceDepartementdirection on met un null au champs service
                $posteuserServicedepartementdirection = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedepartementdirection->setPosteuser($posteuser)
                    ->setServicedepartementdirection(null);

                //Au PosteUserServicedirection on met un null au champs serviceDirection_id
                $posteuserServicedirection = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
                $posteuserServicedirection->setPosteuser($posteuser)
                    ->setServicedirection(null);

                //Au PosteUserDepartement on met un null au champs departement_id
                $posteuserDepartement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
                $posteuserDepartement->setPosteuser($posteuser)
                    ->setDepartementdirection(null);
                //puis on flush dans la base de données
                $entityManager->flush();
            }

            $this->addFlash('success', 'Opération effectuée avec succès!');
            //définition des url pour les redirections
            $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
            $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
            $entite = $request->get('entite') ? $request->get('entite'):null;

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evDemisedefonction->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_demisedefonction_index');
            }
        }

        return $this->render('evenements/ev_demisedefonction/edit.html.twig', [
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'ev_demisedefonction' => $evDemisedefonction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_demisedefonction_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvDemisedefonction $evDemisedefonction,
                           PosteUserRepository $posteUserRepository,
                           PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                           PosteuserDepartementdirectionRepository $posteuserDepartementRepository,
                           PosteuserDirectionRepository $posteuserDirectionRepository,
                           PosteuserServicedirectionRepository $posteuserServicedirectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evDemisedefonction->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $id_evenement = $evDemisedefonction->getId();

            $posteUser = $posteUserRepository->findByIdEvenement($id_evenement, 'Demisedefonction');
            /** @var PosteUser $posteUser */
            $id_posteUser = $posteUser->getId();

            $posteUser_servicedpt = $posteuserServicedepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_servicedir = $posteuserServicedirectionRepository->findByIdPosteUser($id_posteUser);
            $posteUser_departement = $posteuserDepartementRepository->findByIdPosteUser($id_posteUser);
            $posteUser_direction = $posteuserDirectionRepository->findByIdPosteUser($id_posteUser);
            /**
             * @var PosteuserServicedepartementdirection $posteUser_servicedpt
             */
            //Supression du posteUser_service_département associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedpt);
            //Supression du posteUser_service_direction associé (reconnu par l'id_posteUSer)
            $entityManager->remove($posteUser_servicedir);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_departement);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser_direction);
            //suppression du poste user associé (recconnu par l'id_evenement)
            $entityManager->remove($posteUser);

            //Puis dernierement suppression de la ligne de Demisedefonction (evDemisedefonction) concerné
            $entityManager->remove($evDemisedefonction);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_demisedefonction_index');
    }
}
