<?php

namespace App\Controller\Evenements;

use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvStage;
use App\Entity\User;
use App\Form\Evenements\EvStageType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvStageRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatutRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gestion/evenements/stage")
 */
class EvStageController extends AbstractController
{
    /**
     * @Route("/", name="evenements_stage_index", methods={"GET"})
     */
    public function index(EvStageRepository $evStageRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_stage/index.html.twig', [
            'ev_stages' => $evStageRepository->findSomeFields(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evenements_stage_new", methods={"GET","POST"})
     */
    public function new(Request $request, StatutRepository $statutRepository, PosteUserRepository $posteUserRepository,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        UserRepository $userRepository,
                        EvStageRepository $evStageRepository,
                        EvRecrutementRepository $evRecrutementRepository,
                        DirectionRepository $directionRepository): Response
    {
        //recherche de la valeur contractuel dans le StatutRepository
        /*
             titulaire = 1
             contractuel = 2
             stagiaire = 3
         */
        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $statut_contractuel = $statutRepository->find(2);
        $statut_stagiaire = $statutRepository->find(3);

        $evStage = new EvStage();
        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        //Récupération des données de recrutement si elles existent
        /** @var EvRecrutement $user_recrutement */
        $user_recrutement = $evRecrutementRepository->findLastByUserField($url_id);
        $user_stage = $evStageRepository->findLastByUserField($url_id);

        //récupération de l'utilisateur en question via son id envoyé via url
        $user = $userRepository->find($url_id);
        $evStage = $evStage->setUser($user);
        if ($this->isGranted('ROLE_ADMIN_GCAR') === true){
            $evStage = $evStage->setValide(1);
        }else{
            $evStage = $evStage->setValide(0);
        }

        $form = $this->createForm(EvStageType::class, $evStage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'agent via son id
            $user = $evStage->getUser();

            //On récupère le matricule pour l'injecter à l'utilisateur
            $nouveau_matricule = $evStage->getMatricule();
            if (!$user_recrutement){
                $donnees_user = $user->setMatricule($nouveau_matricule)
                    ->setStatut($statut_stagiaire);
            }else{
                $donnees_user = $user->setMatricule($user_recrutement->getNouveauMatricule())
                    ->setStatut($statut_contractuel);
            }


            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //On persite tous les objets
            //objet1: objet principal evReenouvellementdecontrat
            $entityManager->persist($evStage);
            //objet2: le statut et le matricule de l'agent dans fos_user
            $entityManager->persist($donnees_user);

            $entityManager->flush();


            $this->addFlash('success', 'Opération effectuée avec succès!');
            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'info_even') {
                return $this->redirectToRoute('info_evenement_agent', ['id' => $user->getId()]);
            }elseif ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $user->getId(), 'dpt_id' => $dpt_id, 'svc_id' => $svc_id, 'entite'=> $entite ]);
            }else{
                return $this->redirectToRoute('evenements_stage_index');
            }

        }

        return $this->render('evenements/ev_stage/new.html.twig', [
            'ev_stage' => $evStage,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),

        ]);
    }

    /**
     * @Route("/{id}", name="evenements_stage_show", methods={"GET"})
     */
    public function show(EvStage $evStage,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('evenements/ev_stage/show.html.twig', [
            'ev_stage' => $evStage,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_stage_validate", methods={"VALIDATE"})
     * @param Request $request
     * @param EvStage $evStage
     * @return Response
     */
    public function validate(Request $request,
                           EvStage $evStage): Response
    {
        if ($this->isCsrfTokenValid('validate'.$evStage->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();
            //on valide l'evenement en mettant  1 au champs "valide"
            $evStage->setValide(1);

            $entityManager->flush();
        }
        $this->addFlash('success', "Opération effectuée avec succès, cet évenement est validé!");

        return $this->redirectToRoute('evenements_stage_index');
    }

    /**
     * @Route("/{id}/edit", name="evenements_stage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvStage $evStage,
                         PosteUserRepository $posteUserRepository,
                         PosteuserServicedepartementdirectionRepository $posteuserServicedepartementRepository,
                         PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {

        //définition des url
        $dpt_id = $request->get('dpt_id')? $request->get('dpt_id'):null;
        $svc_id = $request->get('svc_id')? $request->get('svc_id'):null;
        $entite = $request->get('entite') ? $request->get('entite'):null;

        $form = $this->createForm(EvStageType::class, $evStage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            //objet1: objet principal evReenouvellementdecontrat
            $entityManager->persist($evStage);
            //récupération de l'agent via son id
            $user = $evStage->getUser();
            //On récupère le matricule pour l'injecter à l'utilisateur
            $matricule = $evStage->getMatricule();
            $user->setMatricule($matricule);
            $entityManager->flush();


            $this->addFlash('success', 'Stage modifié avec succès!');
            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'moncompte_event_agent'){
                return $this->redirectToRoute('moncompte_event_agent', ['id' => $evStage->getUser()->getId(), 'dpt_id' => $dpt_id, 'svc_id'=>$svc_id, 'entite'=>$entite]);
            }else {
                return $this->redirectToRoute('evenements_stage_index');
            }
        }

        return $this->render('evenements/ev_stage/edit.html.twig', [
            'ev_stage' => $evStage,
            'form' => $form->createView(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="evenements_stage_delete", methods={"DELETE"})
     */
    public function delete(Request $request,
                           EvStage $evStage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$evStage->getId(), $request->request->get('_token'))) {
            //récupération de l'entité manager
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($evStage);

            $entityManager->flush();
        }

        return $this->redirectToRoute('evenements_stage_index');
    }
}
