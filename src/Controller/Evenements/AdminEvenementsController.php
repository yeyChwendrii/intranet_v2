<?php

namespace App\Controller\Evenements;

use App\Entity\UserSearch;
use App\Form\UserSearchType;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvAnnulationdedecisionRepository;
use App\Repository\Evenements\EvAvancementnormalRepository;
use App\Repository\Evenements\EvAvertissementRepository;
use App\Repository\Evenements\EvCongeRepository;
use App\Repository\Evenements\EvDecesRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvDemissionRepository;
use App\Repository\Evenements\EvDisponibiliteRepository;
use App\Repository\Evenements\EvIndemniteforfaitaireRepository;
use App\Repository\Evenements\EvInterimRepository;
use App\Repository\Evenements\EvLevedesuspensionRepository;
use App\Repository\Evenements\EvLicenciementRepository;
use App\Repository\Evenements\EvMiseapiedRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvReclassementRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvRectificationdunomRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\Evenements\EvReprisedefonctionRepository;
use App\Repository\Evenements\EvRetraiteRepository;
use App\Repository\Evenements\EvStageRepository;
use App\Repository\Evenements\EvSuspensiondesalaireRepository;
use App\Repository\Evenements\EvSuspensionRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminEvenementsController extends AbstractController
{
    /**
     * @Route("/gestion/evenements", name="admin_evenements")
     */
    public function index(
        EvStageRepository $evStageRepository,
        EvRecrutementRepository $evRecrutementRepository,
        EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
        EvTitularisationRepository $evTitularisationRepository,
        EvReclassementRepository $evReclassementRepository,
        EvAffectationRepository $evAffectationRepository,
        EvNominationRepository $evNominationRepository,
        EvAvancementnormalRepository $evAvancementnormalRepository,
        EvRectificationdunomRepository $evRectificationdunomRepository,
        EvInterimRepository $evInterimRepository,
        EvReintegrationRepository $evReintegrationRepository,
        EvDecesRepository $evDecesRepository,
        EvRetraiteRepository $evRetraiteRepository,
        EvLicenciementRepository $evLicenciementRepository,
        EvDemissionRepository $evDemissionRepository,
        EvAvertissementRepository $evAvertissementRepository,
        EvMiseapiedRepository $evMiseapiedRepository,
        EvSuspensionRepository $evSuspensionRepository,
        EvAnnulationdedecisionRepository $evAnnulationdedecisionRepository,
        EvReprisedefonctionRepository $evReprisedefonctionRepository,
        EvIndemniteforfaitaireRepository $evIndemniteforfaitaireRepository,
        EvLevedesuspensionRepository $evLevedesuspensionRepository,
        EvSuspensiondesalaireRepository $evSuspensiondesalaireRepository,
        EvCongeRepository $evCongeRepository,
        EvDisponibiliteRepository $evDisponibiliteRepository,
        EvPromotioninterneRepository $evPromotioninterneRepository,
        EvDemisedefonctionRepository $evDemisedefonctionRepository
    )
    {

        return $this->render('evenements/index.html.twig', [
            'allow_to_see_notif'               => $this->isGranted('ROLE_ADMIN_GCAR'),
            'stages' => $evStageRepository->countEvenement(),
            'recrutements' => $evRecrutementRepository->countEvenement(),
            'renouvellementdecontrats' => $evRenouvellementdecontratRepository->countEvenement(),
            'titularisations' => $evTitularisationRepository->countEvenement(),
            //2eme stage
            'reclassements' => $evReclassementRepository->countEvenement(),
            'affectations' => $evAffectationRepository->countEvenement(),
            'nominations' => $evNominationRepository->countEvenement(),
            'promotioninternes' => $evPromotioninterneRepository->countEvenement(),
            'avancementnormals' => $evAvancementnormalRepository->countEvenement(),
            'rectificationdunoms' => $evRectificationdunomRepository->countEvenement(),
            'interims' => $evInterimRepository->countEvenement(),
            'reintegrations' => $evReintegrationRepository->countEvenement(),
            'deces' => $evDecesRepository->countEvenement(),
            'licenciements' => $evLicenciementRepository->countEvenement(),
            'retraites' => $evRetraiteRepository->countEvenement(),
            'demissions' => $evDemissionRepository->countEvenement(),
            'demisedefonctions' => $evDemisedefonctionRepository->countEvenement(),
            'avertissements' => $evAvertissementRepository->countEvenement(),
            'miseapieds' => $evMiseapiedRepository->countEvenement(),
            'suspensions' => $evSuspensionRepository->countEvenement(),
            'annulationdedecisions' => $evAnnulationdedecisionRepository->countEvenement(),
            'reprisedefonctions' => $evReprisedefonctionRepository->countEvenement(),
            'indemniteforfaitaires' => $evIndemniteforfaitaireRepository->countEvenement(),
            'levedesuspensions' => $evLevedesuspensionRepository->countEvenement(),
            'suspensiondesalaires' => $evSuspensiondesalaireRepository->countEvenement(),
            'disponibilites' => $evDisponibiliteRepository->countEvenement(),
            'conges' => $evCongeRepository->countEvenement(),
            'conge_maladie' => $evCongeRepository->countCongeByType('maladie'),
            'conge_longue_duree' => $evCongeRepository->countCongeByType('longue durée'),
            'conge_maternite' => $evCongeRepository->countCongeByType('maternité'),
            'conge_de_viduite' => $evCongeRepository->countCongeByType('de viduité'),
            'conge_sans_solde' => $evCongeRepository->countCongeByType('sans solde'),
            //comptage des evenements invalides
            'nbr_affectation'               => $evAffectationRepository->countNotValideEvent(),
            'nbr_avancementnormal'          => $evAvancementnormalRepository->countNotValideEvent(),
            'nbr_avertissement'             => $evAvertissementRepository->countNotValideEvent(),
            'nbr_conge'                     => $evCongeRepository->countNotValideEvent(),
            'nbr_deces'                     => $evDecesRepository->countNotValideEvent(),
            'nbr_demisedefonction'          => $evDemisedefonctionRepository->countNotValideEvent(),
            'nbr_demission'                 => $evDemissionRepository->countNotValideEvent(),
            'nbr_disponibilite'             => $evDisponibiliteRepository->countNotValideEvent(),
            'nbr_indemniteforfaitaire'      => $evIndemniteforfaitaireRepository->countNotValideEvent(),
            'nbr_interim'                   => $evInterimRepository->countNotValideEvent(),
            'nbr_levedesuspension'          => $evLevedesuspensionRepository->countNotValideEvent(),
            'nbr_licenciement'              => $evLicenciementRepository->countNotValideEvent(),
            'nbr_miseapied'                 => $evMiseapiedRepository->countNotValideEvent(),
            'nbr_nomination'                => $evNominationRepository->countNotValideEvent(),
            'nbr_promotioninterne'          => $evPromotioninterneRepository->countNotValideEvent(),
            'nbr_reclassement'              => $evReclassementRepository->countNotValideEvent(),
            'nbr_recrutement'               => $evRecrutementRepository->countNotValideEvent(),
            'nbr_rectificationdunom'        => $evRectificationdunomRepository->countNotValideEvent(),
            'nbr_reintegration'             => $evReintegrationRepository->countNotValideEvent(),
            'nbr_renouvellementdecontrat'   => $evRenouvellementdecontratRepository->countNotValideEvent(),
            'nbr_reprisedefonction'         => $evReprisedefonctionRepository->countNotValideEvent(),
            'nbr_retraite'                  => $evRetraiteRepository->countNotValideEvent(),
            'nbr_stage'                     => $evStageRepository->countNotValideEvent(),
            'nbr_suspension'                => $evSuspensionRepository->countNotValideEvent(),
            'nbr_suspensiondesalaire'       => $evSuspensiondesalaireRepository->countNotValideEvent(),
            'nbr_reclasssement'             => $evReclassementRepository->countNotValideEvent(),
            'nbr_titularisation'            => $evTitularisationRepository->countNotValideEvent(),
        ]);
    }

    /**
     * @Route("gestion/evenements/{evenement}/choisir_agent", name="choisir_agent")
     */
    public function choisirAgent(
        PaginatorInterface $paginator,
        Request $request,
        UserRepository $userRepository
    )
    {

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $users = $paginator->paginate(
            $userRepository->findAllOrderBy($search),
            $request->query->getInt('page', 1),
            11
        );


        return $this->render('evenements/choisir_agent.html.twig', [
            'users'         => $users,
            'formSearch'    => $formSearch->createView(),
        ]);
    }
}
