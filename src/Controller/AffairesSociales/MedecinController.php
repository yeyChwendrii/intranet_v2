<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\Medecin;
use App\Form\AffairesSociales\MedecinType;
use App\Repository\AffairesSociales\MedecinRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/medecin")
 */
class MedecinController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_medecin_index", methods={"GET"})
     */
    public function index(MedecinRepository $medecinRepository): Response
    {
        return $this->render('affaires_sociales/medecin/index.html.twig', [
            'medecins' => $medecinRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_medecin_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $medecin = new Medecin();
        $form = $this->createForm(MedecinType::class, $medecin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($medecin);
            $entityManager->flush();
            if ($request->get('pg_prcdnt') and $request->get('id')){
                return $this->redirectToRoute('afsoc_reposmaladie_new', ['id'=>$request->get('id')]);
            }else{
                return $this->redirectToRoute('afsoc_medecin_index');
            }

        }

        return $this->render('affaires_sociales/medecin/new.html.twig', [
            'medecin' => $medecin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_medecin_show", methods={"GET"})
     */
    public function show(Medecin $medecin): Response
    {
        return $this->render('affaires_sociales/medecin/show.html.twig', [
            'medecin' => $medecin,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="afsoc_medecin_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Medecin $medecin): Response
    {
        $form = $this->createForm(MedecinType::class, $medecin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('afsoc_medecin_index');
        }

        return $this->render('affaires_sociales/medecin/edit.html.twig', [
            'medecin' => $medecin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_medecin_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Medecin $medecin): Response
    {
        if ($this->isCsrfTokenValid('delete'.$medecin->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($medecin);
            $entityManager->flush();
        }

        return $this->redirectToRoute('afsoc_medecin_index');
    }
}
