<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\SituationDesRepos;
use App\Form\AffairesSociales\SituationDesReposType;
use App\Repository\AffairesSociales\SituationDesReposRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/repos_maladie")
 */
class SituationDesReposController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_reposmaladie_index", methods={"GET"})
     */
    public function index(SituationDesReposRepository $situationDesReposRepository): Response
    {
        return $this->render('affaires_sociales/situation_des_repos/index.html.twig', [
            'situation_des_repos' => $situationDesReposRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_reposmaladie_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $situationDesRepo = new SituationDesRepos();

        if ($request->get('id')){
            $user  = $userRepository->find($request->get('id'));
        }else{
            $user = null;
        }
        $situationDesRepo = $situationDesRepo->setUser($user);

        $form = $this->createForm(SituationDesReposType::class, $situationDesRepo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($situationDesRepo);
            $entityManager->flush();

            return $this->redirectToRoute('afsoc_reposmaladie_index');
        }

        return $this->render('affaires_sociales/situation_des_repos/new.html.twig', [
            'situation_des_repo' => $situationDesRepo,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="afsoc_reposmaladie_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SituationDesRepos $situationDesRepo): Response
    {
        $form = $this->createForm(SituationDesReposType::class, $situationDesRepo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('afsoc_reposmaladie_index');
        }

        return $this->render('affaires_sociales/situation_des_repos/edit.html.twig', [
            'situation_des_repo' => $situationDesRepo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_reposmaladie_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SituationDesRepos $situationDesRepo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$situationDesRepo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($situationDesRepo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('afsoc_reposmaladie_index');
    }
}
