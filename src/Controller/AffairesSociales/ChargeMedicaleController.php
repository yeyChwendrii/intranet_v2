<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\ChargeMedicale;
use App\Entity\AffairesSociales\EntiteMedicale;
use App\Form\AffairesSociales\ChargeMedicaleType;
use App\Repository\AffairesSociales\ChargeMedicaleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/charge_medicale")
 */
class ChargeMedicaleController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_chargemedicale_index", methods={"GET"})
     */
    public function index(ChargeMedicaleRepository $chargeMedicaleRepository): Response
    {
        return $this->render('affaires_sociales/charge_medicale/index.html.twig', [
            'charge_medicales' => $chargeMedicaleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_chargemedicale_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $chargeMedicale = new ChargeMedicale();
        $form = $this->createForm(ChargeMedicaleType::class, $chargeMedicale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $new_date = $chargeMedicale->getDateAt()->format('Y-m-01');
            $chargeMedicale = $chargeMedicale->setDateAt(\DateTime::createFromFormat('Y-m-d', $new_date));

            $entityManager->persist($chargeMedicale);
            $entityManager->flush();

            $this->addFlash('success','Enregistrement effectué avec succès');
            return $this->redirectToRoute('afsoc_chargemedicale_index');
        }

        return $this->render('affaires_sociales/charge_medicale/new.html.twig', [
            'charge_medicale' => $chargeMedicale,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/infoCharges", name="afsoc_chargemedicale_show", methods={"GET"})
     */
    public function chargeByentiteMedicale(ChargeMedicale $chargeMedicale): Response
    {
        return $this->render('affaires_sociales/charge_medicale/show.html.twig', [
            'charge_medicale' => $chargeMedicale,
        ]);
    }



    /**
     * @Route("/{id}/edit", name="afsoc_chargemedicale_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ChargeMedicale $chargeMedicale): Response
    {
        $form = $this->createForm(ChargeMedicaleType::class, $chargeMedicale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Opération effectué avec succès');
            return $this->redirectToRoute('afsoc_chargemedicale_index');
        }

        return $this->render('affaires_sociales/charge_medicale/edit.html.twig', [
            'charge_medicale' => $chargeMedicale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_chargemedicale_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ChargeMedicale $chargeMedicale): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chargeMedicale->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($chargeMedicale);
            $entityManager->flush();
        }

        return $this->redirectToRoute('afsoc_chargemedicale_index');
    }
}
