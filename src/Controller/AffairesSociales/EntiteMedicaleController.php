<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\EntiteMedicale;
use App\Form\AffairesSociales\EntiteMedicaleType;
use App\Repository\AffairesSociales\ChargeMedicaleRepository;
use App\Repository\AffairesSociales\EntiteMedicaleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/entite_medicale")
 */
class EntiteMedicaleController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_entitemedicale_index", methods={"GET"})
     */
    public function index(EntiteMedicaleRepository $entiteMedicaleRepository): Response
    {
        return $this->render('affaires_sociales/entite_medicale/index.html.twig', [
            'entite_medicales' => $entiteMedicaleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_entitemedicale_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entiteMedicale = new EntiteMedicale();
        $form = $this->createForm(EntiteMedicaleType::class, $entiteMedicale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entiteMedicale);
            $entityManager->flush();

            $this->addFlash('success','Opération effectué avec succès');

            return $this->redirectToRoute('afsoc_entitemedicale_index');
        }

        return $this->render('affaires_sociales/entite_medicale/new.html.twig', [
            'entite_medicale' => $entiteMedicale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_entitemedicale_show", methods={"GET"})
     */
    public function show(EntiteMedicale $entiteMedicale, ChargeMedicaleRepository $chargeMedicaleRepository): Response
    {
        $chargesMedicales = $chargeMedicaleRepository->findByEntiteMedicale($entiteMedicale);
        $total_par_an = $chargeMedicaleRepository->getSumByYear($entiteMedicale);
        //dd($chargeMedicaleRepository->getAllSum($entiteMedicale));
        return $this->render('affaires_sociales/entite_medicale/show.html.twig', [
            'entite_medicale'       => $entiteMedicale,
            'charges_medicales'     => $chargesMedicales,
            //'somme'                 => $chargeMedicaleRepository->getAllSum($entiteMedicale),
            'total_par_an'          => $total_par_an,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="afsoc_entitemedicale_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EntiteMedicale $entiteMedicale): Response
    {
        $form = $this->createForm(EntiteMedicaleType::class, $entiteMedicale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Opération effectué avec succès');

            return $this->redirectToRoute('afsoc_entitemedicale_index');
        }

        return $this->render('affaires_sociales/entite_medicale/edit.html.twig', [
            'entite_medicale' => $entiteMedicale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_entitemedicale_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EntiteMedicale $entiteMedicale): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entiteMedicale->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entiteMedicale);
            $entityManager->flush();
        }

        return $this->redirectToRoute('afsoc_entitemedicale_index');
    }
}
