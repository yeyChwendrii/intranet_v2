<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\EpouxSe;
use App\Form\AffairesSociales\EpouxSeType;
use App\Repository\AffairesSociales\EpouxSeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/epouxse")
 */
class EpouxSeController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_epouxse_index", methods={"GET"})
     */
    public function index(EpouxSeRepository $epouxSeRepository): Response
    {
        return $this->render('affaires_sociales/epouxse/index.html.twig', [
            'epouxses' => $epouxSeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_epouxse_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $epouxSe = new EpouxSe();
        $epouxSe = $epouxSe->setUser($userRepository->find($request->get('id')));
        $form = $this->createForm(EpouxSeType::class, $epouxSe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($epouxSe);
            $entityManager->flush();

            $this->addFlash('success','Enregistrement effectué avec succès');
            if ($request->get('pg_prcdnt')){
                return $this->redirectToRoute($request->get('pg_prcdnt'), ['id'=>$epouxSe->getUser()->getSituationFamiliale()->getId()]);
            }else{
                return $this->redirectToRoute('afsoc_epouxse_index');
            }
        }

        return $this->render('affaires_sociales/epouxse/new.html.twig', [
            'epouxse' => $epouxSe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_epouxse_show", methods={"GET"})
     */
    public function show(EpouxSe $epouxSe): Response
    {
        return $this->render('affaires_sociales/epouxse/show.html.twig', [
            'epouxse' => $epouxSe,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="afsoc_epouxse_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EpouxSe $epouxSe): Response
    {
        $form = $this->createForm(EpouxSeType::class, $epouxSe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'afsoc_situation_familiale_show'){
                return $this->redirectToRoute('afsoc_situation_familiale_show',['id'=>$epouxSe->getUser()->getSituationFamiliale()->getId()]);
            }else{
                return $this->redirectToRoute('afsoc_epouxse_index');
            }
        }

        return $this->render('affaires_sociales/epouxse/edit.html.twig', [
            'epouxse' => $epouxSe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_epouxse_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EpouxSe $epouxSe): Response
    {
        if ($this->isCsrfTokenValid('delete'.$epouxSe->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($epouxSe);
            $entityManager->flush();
        }

        $this->addFlash('success','Suppression effectué avec succès');
        if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'afsoc_situation_familiale_show'){
            return $this->redirectToRoute('afsoc_situation_familiale_show',['id'=>$epouxSe->getUser()->getSituationFamiliale()->getId()]);
        }else{
            return $this->redirectToRoute('afsoc_epouxse_index');
        }

    }
}
