<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\Enfant;
use App\Form\AffairesSociales\EnfantType;
use App\Repository\AffairesSociales\EnfantRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/enfant")
 */
class EnfantController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_enfant_index", methods={"GET"})
     */
    public function index(EnfantRepository $enfantRepository): Response
    {
        return $this->render('affaires_sociales/enfant/index.html.twig', [
            'enfants' => $enfantRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_enfant_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $enfant = new Enfant();
        $enfant = $enfant->setUser($userRepository->find($request->get('id')));
        $form = $this->createForm(EnfantType::class, $enfant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enfant);
            $entityManager->flush();

            $this->addFlash('success','Enregistrement effectué avec succès');
            if ($request->get('pg_prcdnt')){
                return $this->redirectToRoute($request->get('pg_prcdnt'), ['id'=>$enfant->getUser()->getSituationFamiliale()->getId()]);
            }else{
                return $this->redirectToRoute('afsoc_enfant_index');
            }
        }

        return $this->render('affaires_sociales/enfant/new.html.twig', [
            'enfant' => $enfant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_enfant_show", methods={"GET"})
     */
    public function show(Enfant $enfant): Response
    {
        return $this->render('affaires_sociales/enfant/show.html.twig', [
            'enfant' => $enfant,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="afsoc_enfant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Enfant $enfant): Response
    {
        $form = $this->createForm(EnfantType::class, $enfant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Enregistrement effectué avec succès');

            if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'afsoc_situation_familiale_show'){
                return $this->redirectToRoute('afsoc_situation_familiale_show',['id'=>$enfant->getUser()->getSituationFamiliale()->getId()]);
            }else{
                return $this->redirectToRoute('afsoc_enfant_index');
            }
        }

        return $this->render('affaires_sociales/enfant/edit.html.twig', [
            'enfant' => $enfant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_enfant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Enfant $enfant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$enfant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($enfant);
            $entityManager->flush();
        }
        $this->addFlash('success','Suppression effectué avec succès');
        if ($request->get('pg_prcdnt') and $request->get('pg_prcdnt') == 'afsoc_situation_familiale_show'){
            return $this->redirectToRoute('afsoc_situation_familiale_show',['id'=>$enfant->getUser()->getSituationFamiliale()->getId()]);
        }else{
            return $this->redirectToRoute('afsoc_enfant_index');
        }
    }
}
