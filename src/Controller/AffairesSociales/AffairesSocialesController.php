<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\SituationFamiliale;
use App\Entity\User;
use App\Form\AffairesSociales\SituationFamilialeType;
use App\Form\UserType;
use App\Repository\AffairesSociales\SituationFamilialeRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Secur;

/**
 * @Route("/affaires_sociales")
 */
class AffairesSocialesController extends AbstractController
{
    /**
     * @Route("/", name="home_afsoc", methods={"GET"})
     * @Secur("has_role('ROLE_ADMIN_GAFSOC')")
     */
    public function homeBds(Security $security, UserRepository $userRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();

        return $this->render('affaires_sociales/home_afsoc.html.twig', [
            'roles' => $roles,
        ]);
    }


}
