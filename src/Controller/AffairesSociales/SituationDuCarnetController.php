<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\SituationDuCarnet;
use App\Form\AffairesSociales\SituationDuCarnetType;
use App\Repository\AffairesSociales\SituationDuCarnetRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/carnet")
 */
class SituationDuCarnetController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_carnet_index", methods={"GET"})
     */
    public function index(SituationDuCarnetRepository $situationDuCarnetRepository): Response
    {
        return $this->render('affaires_sociales/situation_du_carnet/index.html.twig', [
            'situation_du_carnets' => $situationDuCarnetRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="afsoc_carnet_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $situationDuCarnet = new SituationDuCarnet();
        if ($request->get('id')){
            $user = $userRepository->find($request->get('id'));
            $statut = $user->getStatut();
        }else{
            $user = null;
            $statut = '';
        }
        $situationDuCarnet = $situationDuCarnet->setUser($user)->setStatutDuBeneficiaire($statut);

        $form = $this->createForm(SituationDuCarnetType::class, $situationDuCarnet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($situationDuCarnet);
            $entityManager->flush();

            return $this->redirectToRoute('afsoc_carnet_index');
        }

        return $this->render('affaires_sociales/situation_du_carnet/new.html.twig', [
            'situation_du_carnet' => $situationDuCarnet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_carnet_show", methods={"GET"})
     */
    public function show(SituationDuCarnet $situationDuCarnet): Response
    {
        return $this->render('affaires_sociales/situation_du_carnet/show.html.twig', [
            'situation_du_carnet' => $situationDuCarnet,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="afsoc_carnet_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SituationDuCarnet $situationDuCarnet): Response
    {
        $form = $this->createForm(SituationDuCarnetType::class, $situationDuCarnet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('afsoc_carnet_index');
        }

        return $this->render('affaires_sociales/situation_du_carnet/edit.html.twig', [
            'situation_du_carnet' => $situationDuCarnet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_carnet_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SituationDuCarnet $situationDuCarnet): Response
    {
        if ($this->isCsrfTokenValid('delete'.$situationDuCarnet->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($situationDuCarnet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('afsoc_carnet_index');
    }
}
