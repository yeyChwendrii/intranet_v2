<?php

namespace App\Controller\AffairesSociales;

use App\Entity\AffairesSociales\SituationFamiliale;
use App\Entity\User;
use App\Form\AffairesSociales\SituationFamilialeType;
use App\Form\UserType;
use App\Repository\AffairesSociales\SituationFamilialeRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/affaires_sociales/situationfamiliale")
 */
class SituationFamilialeController extends AbstractController
{
    /**
     * @Route("/", name="afsoc_situation_familiale_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, EvRecrutementRepository $evRecrutementRepository): Response
    {
        //dd($userRepository->showSituationFamiliale());

        return $this->render('affaires_sociales/situation_familiale/index.html.twig', [
            'agents'            => $userRepository->showSituationFamiliale(),
            'recrutements'       => $evRecrutementRepository->findSomeFields(),
        ]);
    }

    /**
     * @Route("/add_situation/{id}", name="afsoc_situation_familiale_add", methods={"GET","POST"})
     */
    public function add(Request $request, UserRepository $userRepository, User $user): Response
    {
        $situation_familale = new SituationFamiliale();
        $situation_familale = $situation_familale->setUser($userRepository->find($request->get('id')));
        $form = $this->createForm(SituationFamilialeType::class, $situation_familale);

        //dd($situation_familale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($situation_familale);
            $entityManager->flush();

            return $this->redirectToRoute('afsoc_situation_familiale_index');
        }

        return $this->render('affaires_sociales/situation_familiale/add.html.twig', [
            'situation_familiale' => $situation_familale,
            'users' => $userRepository->selectMatriculeNomPrenom(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afsoc_situation_familiale_show", methods={"GET"})
     */
    public function show(SituationFamiliale $situationFamiliale, UserRepository $userRepository, Request $request): Response
    {
        $agent = $userRepository->find($situationFamiliale->getUser()->getId());
        $evRecrutements = $agent->getEvRecrutements();
        $evTitularisations = $agent->getEvTitularisations();
        if ($evRecrutements->getValues() != []){
            foreach ($evRecrutements as $k=>$recrutement){
                if ($recrutement != null and $recrutement->getValide() == true){
                    $dateRecrutementAt = $recrutement->getDateRecrutementAt();
                }

            }
        }else{
            $dateRecrutementAt = '';
        }

        if ($evTitularisations->getValues()!= []){
            foreach ($evTitularisations as $k=>$titularisation){
                if ($titularisation->getValide() == true){
                    $dateTitularisationAt = $titularisation->getDateTitularisationAt();
                }

            }
        }else{
            $dateTitularisationAt = '';
        }

        //agent et enfants

        if($agent->getEnfants()->getValues() != []){
            $enfants = $agent->getEnfants()->getValues();
            //dd($enfants->getValues());
        }else{
            $enfants = [];
        }
        //agents Epoux epouses
        if($agent->getEnfants()->getValues() != []){

            $epouxses = $agent->getEpouxSes()->getValues();
            //dd($epouxses);
        }else{
            $epouxses = [];
        }
        return $this->render('affaires_sociales/situation_familiale/show.html.twig', [
            'situation_familiale'                   => $situationFamiliale,
            'agent'                                 => $agent,
            'dateRecrutementAt'                     => $dateRecrutementAt,
            'dateTitularisationAt'                  => $dateTitularisationAt,
            'enfants'                               => $enfants,
            'epouxses'                              => $epouxses,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="afsoc_situation_familiale_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SituationFamiliale $situationFamiliale): Response
    {
        $form = $this->createForm(SituationFamilialeType::class, $situationFamiliale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('afsoc_situation_familiale_index');
        }

        return $this->render('affaires_sociales/situation_familiale/edit.html.twig', [
            'situation_familiale' => $situationFamiliale,
            'form' => $form->createView(),
        ]);
    }

}
