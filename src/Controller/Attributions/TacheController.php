<?php

namespace App\Controller\Attributions;

use App\Entity\Tache;
use App\Form\TacheType;
use App\Repository\TacheRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/attributions/tache")
 */
class TacheController extends AbstractController
{
    /**
     * @Route("/", name="attributions_tache_index", methods={"GET"})
     */
    public function index(TacheRepository $tacheRepository): Response
    {
        return $this->render('attributions/tache/index.html.twig', [
            'taches' => $tacheRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="attributions_tache_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tache = new Tache();
        $form = $this->createForm(TacheType::class, $tache);
        $form->handleRequest($request);

        $pg_precedent = $request->get('pg_precedent');

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tache);
            $entityManager->flush();

            if ($pg_precedent && $pg_precedent = 'fonction_new'){
                $url_id = $request->get('id');
                return $this->redirectToRoute('attributions_fonction_new', ['id' => $url_id]);
            }else{
                return $this->redirectToRoute('attributions_tache_index');
            }
        }

        return $this->render('attributions/tache/new.html.twig', [
            'tache' => $tache,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attributions_tache_show", methods={"GET"})
     */
    public function show(Tache $tache): Response
    {
        return $this->render('attributions/tache/show.html.twig', [
            'tache' => $tache,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="attributions_tache_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tache $tache): Response
    {
        $form = $this->createForm(TacheType::class, $tache);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('attributions_tache_index', [
                'id' => $tache->getId(),
            ]);
        }

        return $this->render('attributions/tache/edit.html.twig', [
            'tache' => $tache,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attributions_tache_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tache $tache): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tache->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tache);
            $entityManager->flush();
        }

        return $this->redirectToRoute('attributions_tache_index');
    }
}
