<?php

namespace App\Controller\Attributions;

use App\Entity\Fonction;
use App\Form\FonctionType;
use App\Repository\FonctionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("attributions/fonction")
 */
class FonctionController extends AbstractController
{
    /**
     * @Route("/", name="attributions_fonction_index", methods={"GET"})
     */
    public function index(FonctionRepository $fonctionRepository): Response
    {
        return $this->render('attributions/fonction/index.html.twig', [
            'fonctions' => $fonctionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="attributions_fonction_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        PosteUserRepository $posteUserRepository,
                        UserRepository $userRepository
                        ): Response
    {
        $fonction = new Fonction();

        //récupération de l'id de l'URL
        $url_id = $request->get('id');
        $user = $userRepository->find($url_id);
        $posteUser = $posteUserRepository->findLastByUser($user);

        $fonction = $fonction
            ->setPosteConcerne($posteUser->getPoste()->getDesignation())
            ->addPosteUser($posteUser)
        ;
        $form = $this->createForm(FonctionType::class, $fonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fonction);
            $entityManager->flush();

            return $this->redirectToRoute('attributions_fonction_index');
        }

        $this->addFlash('success', 'Tâches attribuées avec succès!');
        return $this->render('attributions/fonction/new.html.twig', [
            'fonction' => $fonction,
            'form' => $form->createView(),
            'url_id' => $url_id,
        ]);
    }

    /**
     * @Route("/{id}", name="attributions_fonction_show", methods={"GET"})
     */
    /*
    public function show(Fonction $fonction): Response
    {
        return $this->render('attributions/fonction/show.html.twig', [
            'fonction' => $fonction,
        ]);
    }
    */

    /**
     * @Route("/{id}/edit", name="attributions_fonction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Fonction $fonction): Response
    {
        $form = $this->createForm(FonctionType::class, $fonction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('attributions_fonction_index', [
                'id' => $fonction->getId(),
            ]);
        }

        $this->addFlash('success', 'Tâches modifiées avec succès!');
        return $this->render('attributions/fonction/edit.html.twig', [
            'fonction' => $fonction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attributions_fonction_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Fonction $fonction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$fonction->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($fonction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('attributions_fonction_index');
    }
}
