<?php

namespace App\Controller\Personnel;

use App\Entity\Personnel\AgentDroitConge;
use App\Entity\Personnel\CategorieConge;
use App\Entity\Personnel\DemandeConge;
use App\Entity\Personnel\ModeleConge;
use App\Form\Personnel\DemandeCongeType;
use App\Repository\Personnel\AgentDroitCongeRepository;
use App\Repository\Personnel\CategorieCongeRepository;
use App\Repository\Personnel\DemandeCongeRepository;
use App\Repository\Personnel\ModeleCongeRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/personnel")
 */
class DemandeCongeController extends AbstractController
{
    /**
     * Affiche la liste des demandes de congés
     *
     * @Route("/demandes/conges", name="demandes_show")
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function showDemandes(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                                 ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/demande_conge/show_demandes.html.twig', [
            'roles' => $roles,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Affiche la liste des demandes de congés de l'agent connecté
     *
     * @Route("/mes/demandes/conges", name="mes_demandes_show")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showMesDemandes(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                                   ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $mesDemandes = $demandeCongeRepository->findByUserDemandesConges($user);
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/demande_conge/show_demandes.html.twig', [
            'roles' => $roles,
            'mesDemandes' => $mesDemandes,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Envoie/Edition d'une demande de congé
     *
     * @Route("/nouvelle/demande/conge", name="demande_conge_create")
     * @Route("/demande/conge/{id}/editer", name="demande_conge_edit")
     */
    public function form(Security $security, UserRepository $userRepository, ObjectManager $manager,
                         Request $request, DemandeConge $demandeConge = null, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository,
                         AgentDroitCongeRepository $agentDroitCongeRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        if (!$demandeConge) {
            $demandeConge = new DemandeConge();
        }
        $dateDebutAt_DB = $demandeConge->getDateDebutAt();
        $dateFinAt_DB = $demandeConge->getDateFinAt();
        $motif_DB = $demandeConge->getMotif();
        $justificatif_DB = $demandeConge->getJustificatif();
        $modeleConge_DB = $demandeConge->getModeleConge();
        $agent_DB = '';
        if ($demandeConge->getAgent()) $agent_DB = $demandeConge->getAgent()->getId();

        $form = $this->createForm(DemandeCongeType::class, $demandeConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateDebut_POST = $demandeConge->getDateDebutAt();
            $datefin_POST = $demandeConge->getDateFinAt();
            $nbrJrCongeDemande = $dateDebut_POST->diff($datefin_POST);
            $nbrJrCongeDemande = $nbrJrCongeDemande->days+1; // nombre de jours (congé) demandé

            $anneeEncour = date('Y');
            $anneeEncour_1 = $anneeEncour+1;
            $anneDeBase = $anneeEncour -3;
            $anneDeBase_1 = $anneDeBase +1;
            $droitCongeAnnuel = 30;

            if ($demandeConge->getModeleConge()->getCategorieConge()->getId() == 1)
            {
                $droitCongeAgents = $agentDroitCongeRepository->findOneByAgent($userRepository->find($user)->getId());
                $agentDroitConge = new AgentDroitConge();

                if (!$droitCongeAgents) {
                    $droitCongeAnnuel = 30;

                    $agentDroitConge->setAnnee(2017);
                    $agentDroitConge->setAnnuel($droitCongeAnnuel);
                    $agentDroitConge->setCongePris($droitCongeAnnuel);
                    $agentDroitConge->setCongeRestant(0);
                    $agentDroitConge->setAgent($userRepository->find($user));

                    $manager->persist($agentDroitConge);
                    $manager->flush();
                } else $droitCongeAgents = $agentDroitCongeRepository->findByAgent($userRepository->find($user)->getId());

                for ($a = $anneDeBase_1; $a <= $anneeEncour_1; $a++) {
                    foreach ($droitCongeAgents as $dca) {
                        // On ajoute la demande de congé puis
                        // On mets à jour le droit de congé en modifiant les valeurs conge_pris/conge_restant
                        if ($dca->getCongePris() < $droitCongeAnnuel) {
                            $totalNbrJrConge = $nbrJrCongeDemande + $dca->getCongePris();
                            $totalNbrJrCongeRestant = $droitCongeAnnuel - $totalNbrJrConge;
                            if ($totalNbrJrCongeRestant >= 0) {

                                // MAJ dans la table DemandeConge
                                if ($agent_DB != '') $demandeConge->setNumDemande($agent_DB . '-' . date('dmY') . '-' . date('Hi'));
                                else $demandeConge->setNumDemande($userRepository->find($user)->getMatricule() . '-' . date('dmY') . '-' . date('Hi'));
                                $demandeConge->setNombreJour($nbrJrCongeDemande);
                                if ($agent_DB) {} else $demandeConge->setAgent($this->getUser());
                                $demandeConge->setCreatedAt(new \DateTime());
                                $demandeConge->setAnnee($a);
                                $demandeConge->setAuteur($userRepository->find($user)->getId());
                                $manager->persist($demandeConge);

                                // MAJ dans la table AgentDroitConge
                                $agentDroitConge->setAnnee($a);
                                $agentDroitConge->setAnnuel($droitCongeAnnuel);
                                $agentDroitConge->setCongePris($totalNbrJrConge);
                                $agentDroitConge->setCongeRestant($totalNbrJrCongeRestant);
                                if ($agent_DB) {} else $agentDroitConge->setAgent($this->getUser());
                                $manager->persist($agentDroitConge);

//                                break;
                                $manager->flush();
                            }
                        }
                        // On ajoute la demande de congé puis
                        // On inserre une nouvelle ligne (droit de congé) dans la table AgentDroitConge
                        else {
                            $totalNbrJrCongeRestant = $droitCongeAnnuel - $nbrJrCongeDemande;
                            if ($nbrJrCongeDemande > $droitCongeAnnuel) {

                                // MAJ dans la table DemandeConge
                                if ($agent_DB != '') $demandeConge->setNumDemande($agent_DB . '-' . date('dmY') . '-' . date('Hi'));
                                else $demandeConge->setNumDemande($userRepository->find($user)->getMatricule() . '-' . date('dmY') . '-' . date('Hi'));
                                $demandeConge->setNombreJour($nbrJrCongeDemande);
                                if ($agent_DB) {} else $demandeConge->setAgent($this->getUser());
                                $demandeConge->setCreatedAt(new \DateTime());
                                $b = $a+1;
                                $c = $a+2;
                                if (($nbrJrCongeDemande / $droitCongeAnnuel) > 1 && ($nbrJrCongeDemande / $droitCongeAnnuel) <=2) {
                                    $stockAnnees = $a . '/' . $b;
                                } else {
                                    $stockAnnees = $a . '/' . $b . '/' . $c;
                                }
                                $demandeConge->setAnnee($stockAnnees);
                                $demandeConge->setAuteur($userRepository->find($user)->getId());
                                $manager->persist($demandeConge);

                                // MAJ dans la table AgentDroitConge
                                $agentDroitConge->setAnnee($a);
                                $agentDroitConge->setAnnuel($droitCongeAnnuel);
                                $agentDroitConge->setCongePris($droitCongeAnnuel);
                                $agentDroitConge->setCongeRestant(0);
                                if ($agent_DB) {} else $agentDroitConge->setAgent($this->getUser());
                                $manager->persist($agentDroitConge);

//                                if ($stockAnnees == $a . '/' . $b) break 1; else break 2;

                                $manager->flush();
                            // Si le nombre de jour de congé demandé est inferieur au droit de congé annuel
                            } else {
                                // MAJ dans la table DemandeConge
                                if ($agent_DB != '') $demandeConge->setNumDemande($agent_DB . '-' . date('dmY') . '-' . date('Hi'));
                                else $demandeConge->setNumDemande($userRepository->find($user)->getMatricule() . '-' . date('dmY') . '-' . date('Hi'));
                                $demandeConge->setNombreJour($nbrJrCongeDemande);
                                if ($agent_DB) {} else $demandeConge->setAgent($this->getUser());
                                $demandeConge->setCreatedAt(new \DateTime());
                                $demandeConge->setAnnee($a);
                                $demandeConge->setAuteur($userRepository->find($user)->getId());
                                $manager->persist($demandeConge);

                                // MAJ dans la table AgentDroitConge
                                $agentDroitConge->setAnnee($a);
                                $agentDroitConge->setAnnuel($droitCongeAnnuel);
                                $agentDroitConge->setCongePris($nbrJrCongeDemande);
                                $agentDroitConge->setCongeRestant($totalNbrJrCongeRestant);
                                if ($agent_DB) {} else $agentDroitConge->setAgent($this->getUser());
                                $manager->persist($agentDroitConge);

//                                break;

                                $manager->flush();
                            }
                        }
                    }
                    /*if ($val == 'stop') {
                        break;    /* Vous pourriez aussi utiliser 'break 1;' ici. *
                    }*/
                    break;
                }
            }

            /*if ($agent_DB != '') $demandeConge->setNumDemande($agent_DB . '-' . date('dmY') . '-' . date('Hi'));
            else $demandeConge->setNumDemande($userRepository->find($user)->getMatricule() . '-' . date('dmY') . '-' . date('Hi'));
            $demandeConge->setNombreJour($nbrJrCongeDemande);
            if ($agent_DB) {} else $demandeConge->setAgent($this->getUser());
            $demandeConge->setCreatedAt(new \DateTime());
            $demandeConge->setAuteur($userRepository->find($user)->getId());

            $manager->persist($demandeConge);
            $manager->flush();*/

            if($dateDebutAt_DB === $demandeConge->getDateDebutAt() && $dateFinAt_DB === $demandeConge->getDateFinAt() && $motif_DB === $demandeConge->getMotif() &&
                $justificatif_DB === $demandeConge->getJustificatif() && $modeleConge_DB === $demandeConge->getModeleConge()) {}
            else {
                $this->addFlash('success', 'Opération effectuée avec succès!');
            }

            return $this->redirectToRoute('demande_conge_show',[
                'id' => $demandeConge->getId(),
            ]);
        }

        return $this->render('personnel/demande_conge/create.html.twig', [
            'roles' => $roles,
            'formDemandeConge' => $form->createView(),
            'editMode' => $demandeConge->getId() !== null,
            'demandeConge' => $demandeConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Affichage d'une demande de congé à partir de son 'id'
     *
     * @Route("/demande/conge/{id}", name="demande_conge_show")
     */
    public function show(Security $security, UserRepository $userRepository, DemandeConge $demandeConge, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $agent = $userRepository->findAgentEnFonction($demandeConge->getAgent()->getId());
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/demande_conge/show.html.twig', [
            'roles' => $roles,
            'demandeConge' => $demandeConge,
            'agent' => $agent,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Suppression d'une demande de congé à partir de son 'id'
     *
     * @Route("/demande/conge/{id}/supprimer", name="demande_conge_delete", methods={"DELETE"})
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function delete(ObjectManager $manager, Request $request, DemandeConge $demandeConge): Response
    {
        if ($this->isCsrfTokenValid('delete'.$demandeConge->getId(), $request->request->get('_token'))) {
            $manager->remove($demandeConge);
            $manager->flush();
        }
        $this->addFlash('success', 'Suppression effectuée avec succès!');

        return $this->redirectToRoute('demandes_show');
    }
}