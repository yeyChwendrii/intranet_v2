<?php

namespace App\Controller\Personnel;

use App\Entity\Personnel\CategorieConge;
use App\Form\Personnel\CategorieCongeType;
use App\Repository\Personnel\CategorieCongeRepository;
use App\Repository\Personnel\DemandeCongeRepository;
use App\Repository\Personnel\ModeleCongeRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/personnel")
 */
class CategorieCongeController extends AbstractController
{
    /**
     * Affiche la liste des catégories
     *
     * @Route("/categories/conges", name="categories_show")
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function showCategories(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                                   ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/categorie_conge/show_categories.html.twig', [
            'roles' => $roles,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Ajout/Edition d'une catégorie
     *
     * @Route("/nouvelle/categorie/conge", name="category_conge_create")
     * @Route("/categorie/conge/{id}/editer", name="category_conge_edit")
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function form(Security $security, UserRepository $userRepository, ObjectManager $manager,
                         Request $request, CategorieConge $categorieConge = null, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        if (!$categorieConge) {
            $categorieConge = new CategorieConge();
        }
        $titre_DB = $categorieConge->getTitre();
        $form = $this->createForm(CategorieCongeType::class, $categorieConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categorieConge->setCreatedAt(new \DateTime());
            $categorieConge->setEnabled(true);

            $manager->persist($categorieConge);
            $manager->flush();

            if($titre_DB !== $categorieConge->getTitre()) {
                $this->addFlash('success', 'Opération effectuée avec succès!');
            }

            return $this->redirectToRoute('category_conge_show',[
                'id' => $categorieConge->getId(),
            ]);
        }

        return $this->render('personnel/categorie_conge/create.html.twig', [
            'roles' => $roles,
            'formCategorieConge' => $form->createView(),
            'editMode' => $categorieConge->getId() !== null,
            'categorieConge' => $categorieConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Affichage d'une catégorie à partir de son 'id'
     *
     * @Route("/categorie/conge/{id}", name="category_conge_show")
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function show(Security $security, UserRepository $userRepository,CategorieConge $categorieConge, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/categorie_conge/show.html.twig', [
            'roles' => $roles,
            'categorieConge' => $categorieConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Suppression d'une catégorie à partir de son 'id'
     *
     * @Route("/categorie/conge/{id}/supprimer", name="category_conge_delete", methods={"DELETE"})
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function delete(ObjectManager $manager, Request $request, CategorieConge $categorieConge): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorieConge->getId(), $request->request->get('_token'))) {
            $manager->remove($categorieConge);
            $manager->flush();
        }
        $this->addFlash('success', 'Suppression effectuée avec succès!');

        return $this->redirectToRoute('categories_show');
    }
}