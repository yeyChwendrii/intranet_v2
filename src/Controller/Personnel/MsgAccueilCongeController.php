<?php

namespace App\Controller\Personnel;

use App\Entity\Personnel\MsgAccueilConge;
use App\Form\Personnel\MsgAccueilCongeType;
use App\Repository\Personnel\MsgAccueilCongeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\Personnel\CategorieCongeRepository;
use App\Repository\Personnel\DemandeCongeRepository;
use App\Repository\Personnel\ModeleCongeRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/personnel")
 */
class MsgAccueilCongeController extends AbstractController
{
    /**
     * Affiche la liste des messages
     *
     * @Route("/messages/accueil/conge", name="messages_accueil_conge")
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function index(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                          ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository,
                          MsgAccueilCongeRepository $msgAccueilCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();
        $msgs = $msgAccueilCongeRepository->findAll();

        return $this->render('personnel/msg_accueil_conge/index.html.twig', [
            'roles' => $roles,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
            'msgs' => $msgs,
        ]);
    }

    /**
     * Ajout/Edition d'un message
     *
     * @Route("/message/accueil/conge/nouvelle", name="msg_conge_create")
     * @Route("/message/accueil//conge/{id}/editer", name="msg_conge_edit")
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function form(Security $security, UserRepository $userRepository, ObjectManager $manager,
                         Request $request, MsgAccueilConge $msgAccueilConge = null, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        if (!$msgAccueilConge) {
            $msgAccueilConge = new MsgAccueilConge();
        }
        $description_DB = $msgAccueilConge->getDescription();
        $enabled_DB = $msgAccueilConge->getEnabled();

        $form = $this->createForm(MsgAccueilCongeType::class, $msgAccueilConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $msgAccueilConge->setCreatedAt(new \DateTime());
            $msgAccueilConge->setAuteur($userRepository->find($user));
//            $msgAccueilConge->setEnabled(true);

            $manager->persist($msgAccueilConge);
            $manager->flush();

            if($description_DB === $msgAccueilConge->getDescription() && $enabled_DB === $msgAccueilConge->getEnabled()) {}
            else {
                $this->addFlash('success', 'Opération effectuée avec succès!');
            }

            return $this->redirectToRoute('msg_conge_show',[
                'id' => $msgAccueilConge->getId(),
            ]);
        }

        return $this->render('personnel/msg_accueil_conge/create.html.twig', [
            'roles' => $roles,
            'formMsgAccueilConge' => $form->createView(),
            'editMode' => $msgAccueilConge->getId() !== null,
            'msgAccueilConge' => $msgAccueilConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Affichage d'un message à partir de son 'id'
     *
     * @Route("/message/accueil/conge/{id}", name="msg_conge_show")
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function show(Security $security, UserRepository $userRepository, MsgAccueilConge $msgAccueilConge, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/msg_accueil_conge/show.html.twig', [
            'roles' => $roles,
            'msgAccueilConge' => $msgAccueilConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Suppression d'un message à partir de son 'id'
     *
     * @Route("/message/accueil/conge/{id}/supprimer", name="msg_conge_delete", methods={"DELETE"})
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function delete(ObjectManager $manager, Request $request, MsgAccueilConge $msgAccueilConge): Response
    {
        if ($this->isCsrfTokenValid('delete'.$msgAccueilConge->getId(), $request->request->get('_token'))) {
            $manager->remove($msgAccueilConge);
            $manager->flush();
        }
        $this->addFlash('success', 'Suppression effectuée avec succès!');

        return $this->redirectToRoute('messages_accueil_conge');
    }
}
