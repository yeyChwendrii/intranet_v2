<?php

namespace App\Controller\Personnel;

use App\Entity\Personnel\DemandeConge;
use App\Entity\ServiceDepartementdirection;
use App\Repository\Evenements\EvAffectationRepository;
use App\Repository\Evenements\EvDemisedefonctionRepository;
use App\Repository\Evenements\EvNominationRepository;
use App\Repository\Evenements\EvPromotioninterneRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\Evenements\EvReintegrationRepository;
use App\Repository\Evenements\EvRenouvellementdecontratRepository;
use App\Repository\Evenements\EvTitularisationRepository;
use App\Repository\Personnel\CategorieCongeRepository;
use App\Repository\Personnel\DemandeCongeRepository;
use App\Repository\Personnel\ModeleCongeRepository;
use App\Repository\PosteUserRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/reprise")
 */
class RepriseServiceController extends AbstractController
{
    /**
     * Affichage d'une reprise de service à partir de l'"id" de la demande de congé
     *
     * @Route("/service/{id}", name="reprise_service", methods={"GET"})
     */
    public function index(Security $security, UserRepository $userRepository, DemandeConge $demandeConge,
                          DemandeCongeRepository $demandeCongeRepository, ModeleCongeRepository $modeleCongeRepository,
                          CategorieCongeRepository $categorieCongeRepository, PosteUserRepository $posteUserRepository,
                          EvNominationRepository $evNominationRepository,
                          EvDemisedefonctionRepository $evDemisedefonctionRepository,
                          EvRecrutementRepository $evRecrutementRepository,
                          EvRenouvellementdecontratRepository $evRenouvellementdecontratRepository,
                          EvReintegrationRepository $evReintegrationRepository,
                          EvAffectationRepository $evAffectationRepository,
                          EvTitularisationRepository $evTitularisationRepository,
                          EvPromotioninterneRepository $evPromotioninterneRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $agent = $userRepository->findAgentEnFonction($demandeConge->getAgent()->getId());
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        $direction = ''; $departement = ''; $poste = '';
        $dernierPosteUser = $posteUserRepository->findLastByUser($agent);

        $nomEvenement = $dernierPosteUser ? $dernierPosteUser->getNomEvenement() : '';

        if ($nomEvenement!= ''){
            $idEvenement = $dernierPosteUser->getIdEvenement();
            if ($nomEvenement == 'Recrutement'){
                $evenement = $evRecrutementRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
            }
            elseif ($nomEvenement == 'Renouvellementdecontrat'){
                $evenement = $evRenouvellementdecontratRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();
                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
            }
            elseif ($nomEvenement == 'Titularisation'){
                $evenement = $evTitularisationRepository->find($idEvenement);
                $service = $evenement ? $evenement->getServiceDepartement() : $evenement->getServiceDirection();

                if ($service instanceof ServiceDepartementdirection){
                    $departement = $service->getDepartementdirection();
                    $direction = $departement->getDirection();
                }else{
                    $departement = '';
                    $direction = $service ? $service->getDirection() : $service->getDepartementdirection()->getDirection();
                }
            }
            elseif ($nomEvenement == 'Affectation'){
                $evenement = $evAffectationRepository->find($idEvenement);
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Promotioninterne'){
                $evenement = $evPromotioninterneRepository->find($idEvenement);
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Reintegration'){
                $evenement = $evReintegrationRepository->find($idEvenement);
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Nomination'){
                $evenement = $evNominationRepository->find($idEvenement);
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
            elseif ($nomEvenement == 'Demisedefonction'){
                $evenement = $evDemisedefonctionRepository->find($idEvenement);
                $departement = $evenement->getDepartement();
                $direction = $evenement->getDirection();
            }
        }
        else{
            $direction = 'Non-renseignée';
        }

        return $this->render('personnel/demande_conge/reprise_service.html.twig', [
            'roles' => $roles,
            'demandeConge' => $demandeConge,
            'departement' => $departement,
            'direction' => $direction,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }
}