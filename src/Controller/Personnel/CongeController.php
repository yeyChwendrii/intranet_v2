<?php

namespace App\Controller\Personnel;

use App\Entity\Personnel\AgentDroitConge;
use App\Entity\Personnel\CongeDataSearch;
use App\Entity\Personnel\DemandeConge;
use App\Entity\User;
use App\Form\Personnel\CongeDataSearchType;
use App\Form\Personnel\DemandeCongeType;
use App\Repository\Personnel\AgentDroitCongeRepository;
use App\Repository\Personnel\CategorieCongeRepository;
use App\Repository\Personnel\DemandeCongeRepository;
use App\Repository\Personnel\ModeleCongeRepository;
use App\Repository\Personnel\MsgAccueilCongeRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/personnel")
 */
class CongeController extends AbstractController
{
    /**
     * Affiche la page d'accueil du personnel
     *
     * @Route("/", name="conge_index", methods={"GET"})
     */
    public function index(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                          ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository,
                          MsgAccueilCongeRepository $msgAccueilCongeRepository,
                          AgentDroitCongeRepository $agentDroitCongeRepository, ObjectManager $manager): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();
        $msgs = $msgAccueilCongeRepository->findAll();
        $droitCongeAgents = $agentDroitCongeRepository->findOneByAgent($userRepository->find($user)->getId());

        if (!$droitCongeAgents) {
            $droitCongeAnnuel = 30;
            $agentDroitConge = new AgentDroitConge();
            $agentDroitConge->setAnnee(2017);
            $agentDroitConge->setAnnuel($droitCongeAnnuel);
            $agentDroitConge->setCongePris($droitCongeAnnuel);
            $agentDroitConge->setCongeRestant(0);
            $agentDroitConge->setAgent($userRepository->find($user));

            $manager->persist($agentDroitConge);
            $manager->flush();
        } else $droitCongeAgents = $agentDroitCongeRepository->findByAgent($userRepository->find($user)->getId());

        return $this->render('personnel/home_conge.html.twig', [
            'roles' => $roles,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
            'msgs' => $msgs,
            'droitCongeAgents' => $droitCongeAgents,
        ]);
    }

    /**
     * Affiche les derniers congés des agents
     *
     * @Route("/agent/dernier/conge/", name="agent_dernier_conge", methods={"GET"})
     */
    public function dernierCongeAgents(Security $security, UserRepository $userRepository,
                                             DemandeCongeRepository $demandeCongeRepository,
                                             ModeleCongeRepository $modeleCongeRepository,
                                             CategorieCongeRepository $categorieCongeRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/dernier_conge/index.html.twig', [
            'roles'      => $roles,
            'agents'     => $userRepository->showDernierConge(),
            'demandes'   => $demandes,
            'modeles'    => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Affiche les situations administrativies d'etats de congés des agents
     *
     * @Route("/agent/situation/conge/", name="agent_situation_conge", methods={"GET"})
     */
    public function situationCongeAgents(Security $security, UserRepository $userRepository,
                                         DemandeCongeRepository $demandeCongeRepository,
                                         Request $request): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();

        $search = new CongeDataSearch();
        $form = $this->createForm(CongeDataSearchType::class, $search);
        $form->handleRequest($request);
        $demandes = $demandeCongeRepository->findAllCongeQuery($search);

        return $this->render('personnel/situation_conge/index.html.twig', [
            'roles'    => $roles,
            'demandes' => $demandes,
            'form'     => $form->createView(),
        ]);
    }

    /**
     * Affiche la liste des dernières demandes de congés d' un agent
     *
     * @Route("/demandes/conge/agent/{id}", name="demandes_agent_show")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDemandesAgent(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                                      ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository,
                                      User $utilisateur)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $userId = $utilisateur->getId();
        $nomPrenom = $utilisateur->getNomPrenom();
        $sesDemandes = $demandeCongeRepository->findByUserDemandesConges($userId);
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/demande_conge/show_demandes.html.twig', [
            'roles' => $roles,
            'sesDemandes' => $sesDemandes,
            'nomPrenom'   => $nomPrenom,
            'demandes'    => $demandes,
            'modeles'     => $modeles,
            'categories'  => $categories,
        ]);
    }

    /**
     * Envoie d'une demande de congé d'un agent
     *
     * @Route("/nouvelle/demande/conge/{id}", name="demande_conge_agent_create")
     */
    public function form(Security $security, UserRepository $userRepository, ObjectManager $manager,
                         Request $request, DemandeConge $demandeConge = null, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository,
                         User $utilisateur): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        $demandeConge = new DemandeConge();

        $form = $this->createForm(DemandeCongeType::class, $demandeConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateDebut_POST = $demandeConge->getDateDebutAt();
            $datefin_POST = $demandeConge->getDateFinAt();
            $nbrJr = $dateDebut_POST->diff($datefin_POST);
            $nbrJr = $nbrJr->days+1;

            $demandeConge->setNumDemande($utilisateur->getMatricule() . '-' . date('dmY') . '-' . date('Hi'));
            $demandeConge->setNombreJour($nbrJr);
            $demandeConge->setAgent($utilisateur);
            $demandeConge->setCreatedAt(new \DateTime());
            $demandeConge->setAuteur($userRepository->find($user)->getId());
//            $demandeConge->setAnnee('2020');

            $manager->persist($demandeConge);
            $manager->flush();

            $this->addFlash('success', 'Demande effectuée avec succès!');

            return $this->redirectToRoute('demande_conge_show',[
                'id' => $demandeConge->getId(),
            ]);
        }

        return $this->render('personnel/demande_conge/create.html.twig', [
            'roles' => $roles,
            'formDemandeConge' => $form->createView(),
            'editMode' => null,
            'demandeConge' => $demandeConge,
            'nomPrenom' => $utilisateur->getNomPrenom(),
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }
}