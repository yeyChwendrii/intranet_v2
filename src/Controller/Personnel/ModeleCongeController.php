<?php

namespace App\Controller\Personnel;

use App\Entity\Personnel\CategorieConge;
use App\Entity\Personnel\ModeleConge;
use App\Form\Personnel\CategorieCongeType;
use App\Form\Personnel\ModeleCongeType;
use App\Repository\Personnel\CategorieCongeRepository;
use App\Repository\Personnel\DemandeCongeRepository;
use App\Repository\Personnel\ModeleCongeRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/personnel")
 */
class ModeleCongeController extends AbstractController
{
    /**
     * Affiche la liste des modèles
     *
     * @Route("/modeles/conge", name="modeles_show")
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function showModeles(Security $security, UserRepository $userRepository, DemandeCongeRepository $demandeCongeRepository,
                                   ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/modele_conge/show_modeles.html.twig', [
            'roles' => $roles,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Ajout/Edition d'un modèle
     *
     * @Route("/modele/conge/nouvelle", name="modele_conge_create")
     * @Route("/modele/conge/{id}/editer", name="modele_conge_edit")
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function form(Security $security, UserRepository $userRepository, ObjectManager $manager,
                         Request $request, ModeleConge $modeleConge = null, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        if (!$modeleConge) {
            $modeleConge = new ModeleConge();
        }
        $titre_DB = $modeleConge->getTitre();
        $delai_DB = $modeleConge->getDelai();
        $motif_DB = $modeleConge->getMotif();
        $justificatif_DB = $modeleConge->getJustificatif();
//        $categorieConge_DB = $modeleConge->getCategorieConge()->getId();
        $avisResponsables_DB = $modeleConge->getAvisResponsables()->count();

        $form = $this->createForm(ModeleCongeType::class, $modeleConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modeleConge->setCreatedAt(new \DateTime());
            $modeleConge->setEnabled(true);

            $manager->persist($modeleConge);
            $manager->flush();

            if($titre_DB === $modeleConge->getTitre() && $delai_DB === $modeleConge->getDelai() && $motif_DB === $modeleConge->getMotif() &&
                $justificatif_DB === $modeleConge->getJustificatif() && $avisResponsables_DB === $modeleConge->getAvisResponsables()->count()) {}
            else {
                $this->addFlash('success', 'Opération effectuée avec succès!');
            }

            return $this->redirectToRoute('modele_conge_show',[
                'id' => $modeleConge->getId(),
            ]);
        }

        return $this->render('personnel/modele_conge/create.html.twig', [
            'roles' => $roles,
            'formModeleConge' => $form->createView(),
            'editMode' => $modeleConge->getId() !== null,
            'modeleConge' => $modeleConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Affichage d'un modèle à partir de son 'id'
     *
     * @Route("/modele/conge/{id}", name="modele_conge_show")
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function show(Security $security, UserRepository $userRepository, ModeleConge $modeleConge, DemandeCongeRepository $demandeCongeRepository,
                         ModeleCongeRepository $modeleCongeRepository, CategorieCongeRepository $categorieCongeRepository)
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();
        $demandes = $demandeCongeRepository->findAll();
        $modeles = $modeleCongeRepository->findAll();
        $categories = $categorieCongeRepository->findAll();

        return $this->render('personnel/modele_conge/show.html.twig', [
            'roles' => $roles,
            'modeleConge' => $modeleConge,
            'demandes' => $demandes,
            'modeles' => $modeles,
            'categories' => $categories,
        ]);
    }

    /**
     * Suppression d'un modèle à partir de son 'id'
     *
     * @Route("/modele/conge/{id}/supprimer", name="modele_conge_delete", methods={"DELETE"})
     *
     * @IsGranted("ROLE_ADMIN_GCONGE")
     */
    public function delete(ObjectManager $manager, Request $request, ModeleConge $modeleConge): Response
    {
        if ($this->isCsrfTokenValid('delete'.$modeleConge->getId(), $request->request->get('_token'))) {
            $manager->remove($modeleConge);
            $manager->flush();
        }
        $this->addFlash('success', 'Suppression effectuée avec succès!');

        return $this->redirectToRoute('modeles_show');
    }
}