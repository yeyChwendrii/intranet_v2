<?php

namespace App\Controller\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\BdsParametresdecotisation;
use App\Form\BanqueDeSolidarite\BdsParametresdecotisationType;
use App\Repository\BanqueDeSolidarite\BdsParametresdecotisationRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bds/parametresdecotisation")
 */
class BdsParametresdecotisationController extends AbstractController
{
    /**
     * @Route("/", name="bds_parametresdecotisation_index", methods={"GET"})
     */
    public function index(BdsParametresdecotisationRepository $bdsParametresdecotisationRepository): Response
    {
        return $this->render('banque_de_solidarite/parametresdecotisation/index.html.twig', [
            'bds_parametresdecotisations' => $bdsParametresdecotisationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bds_parametresdecotisation_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $bdsParametresdecotisation = new BdsParametresdecotisation();

        $url_agent = $request->get('agent');
        $user = $userRepository->find($url_agent);

        $recrutement = $user->getEvRecrutements() ? $user->getEvRecrutements()->getValues() : null;
        $titularisation = $user->getEvTitularisations() ? $user->getEvTitularisations()->getValues() : null;

        $bdsParametresdecotisation = $bdsParametresdecotisation->setUser($user);

        $form = $this->createForm(BdsParametresdecotisationType::class, $bdsParametresdecotisation);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bdsParametresdecotisation);
            $entityManager->flush();

            return $this->redirectToRoute('bds_agents');
        }

        return $this->render('banque_de_solidarite/parametresdecotisation/new.html.twig', [
            'bds_parametresdecotisation'    => $bdsParametresdecotisation,
            'form'                          => $form->createView(),
            'recrutement'                   => $recrutement,
            'titularisation'                => $titularisation,
        ]);
    }

    /**
     * @Route("/{id}", name="bds_parametresdecotisation_show", methods={"GET"})
     */
    public function show(BdsParametresdecotisation $bdsParametresdecotisation): Response
    {
        return $this->render('banque_de_solidarite/parametresdecotisation/show.html.twig', [
            'bds_parametresdecotisation' => $bdsParametresdecotisation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bds_parametresdecotisation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BdsParametresdecotisation $bdsParametresdecotisation): Response
    {
        //test de l'url si la variable pg_prcdnt est présente
        if ($request->get('pg_prcdnt')){
            $pg_prcdnt = $request->get('pg_prcdnt');
        }else{
            $pg_prcdnt = null;
        }
        $id_cotisation = $request->get('idcotisation') ? $request->get('idcotisation') : null;


        $form = $this->createForm(BdsParametresdecotisationType::class, $bdsParametresdecotisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            if ($pg_prcdnt!= null and $pg_prcdnt == 'bds_cotisation_edit'){
                return $this->redirectToRoute('bds_cotisation_edit', ['id' => $id_cotisation]);
            }elseif ($pg_prcdnt!= null and $pg_prcdnt == 'bds_agents'){
                return $this->redirectToRoute('bds_agents');
            }else{
                return $this->redirectToRoute('bds_parametresdecotisation_index');
            }
        }

        return $this->render('banque_de_solidarite/parametresdecotisation/edit.html.twig', [
            'bds_parametresdecotisation' => $bdsParametresdecotisation,
            'pg_prcdnt' => $pg_prcdnt,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bds_parametresdecotisation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, BdsParametresdecotisation $bdsParametresdecotisation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bdsParametresdecotisation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bdsParametresdecotisation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bds_parametresdecotisation_index');
    }
}
