<?php

namespace App\Controller\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\Cotisation;
use App\Entity\UserSearch;
use App\Form\BanqueDeSolidarite\CotisationType;
use App\Form\UserSearchType;
use App\Repository\BanqueDeSolidarite\BdsCotisationRepository;
use App\Repository\BanqueDeSolidarite\BdsParametresdecotisationRepository;
use App\Repository\BanqueDeSolidarite\CotisationRepository;
use App\Repository\PosteUserRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as Secur;

/**
 * @Route("/bds")
 */
class BanquedesolidariteController extends AbstractController
{
    /**
     * @Route("/", name="home_bds", methods={"GET"})
     * @Secur("has_role('ROLE_ADMIN_GBDS')")
     */
    public function homeBds(BdsCotisationRepository $bdsCotisationRepository,Security $security, UserRepository $userRepository): Response
    {
        $user = $security->getUser();
        $roles = $userRepository->find($user)->getRoles();

        return $this->render('banque_de_solidarite/home_bds.html.twig', [
            'cotisations' => $bdsCotisationRepository->findAll(),
            'roles' => $roles,
        ]);
    }


    /**
     * @Route("/agents", name="bds_agents")
     *
     */
    public function agents(
        PaginatorInterface $paginator,
        Request $request,
        UserRepository $userRepository,
        PosteUserRepository $posteUserRepository,
        BdsParametresdecotisationRepository $bdsParametresdecotisationRepository
    )
    {

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $users = $paginator->paginate(
            $userRepository->findAllOrderBy($search),
            $request->query->getInt('page', 1),
            10
        );

        $parametresdecotisation = $bdsParametresdecotisationRepository->findAll();

        return $this->render('banque_de_solidarite/agents.html.twig', [
            'users'                                     => $users,
            'parametresdecotisation'                    => $parametresdecotisation,
            'formSearch'                                => $formSearch->createView(),
        ]);
    }
}
