<?php

namespace App\Controller\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\BdsCotisation;
use App\Entity\User;
use App\Form\BanqueDeSolidarite\BdsCotisationType;
use App\Repository\BanqueDeSolidarite\BdsCotisationRepository;
use App\Repository\BanqueDeSolidarite\BdsParametresdecotisationRepository;
use App\Repository\Evenements\EvRecrutementRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bds/cotisation")
 */
class BdsCotisationController extends AbstractController
{
    /**
     * @Route("/", name="bds_cotisation_index", methods={"GET"})
     */
    public function index(BdsCotisationRepository $bdsCotisationRepository): Response
    {
        return $this->render('banque_de_solidarite/cotisation/index.html.twig', [
            'bds_cotisations' => $bdsCotisationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bds_cotisation_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        UserRepository $userRepository, BdsCotisationRepository $bdsCotisationRepository,
                        BdsParametresdecotisationRepository $bdsParametresdecotisationRepository): Response
    {
        //récupération de la page précédente dans l'URL
        if ($request->get('pg_prcdnt')){
            $url_pg_prcdnt = $request->get('pg_prcdnt');
        }
        if ($request->get('id')){
            $user = $request->get('id') ? $userRepository->find($request->get('id')) : null;
        }
        if ($user !== null){
            $parametres = $bdsParametresdecotisationRepository->findOneByAgent($user);
        }

        $bdsCotisation = new BdsCotisation();
        $bdsCotisation = $bdsCotisation->setBdsParametresdecotisation($parametres)
        ->setCotisationappliquee($parametres->getCotisationappliquee())
        ;

        $form = $this->createForm(BdsCotisationType::class, $bdsCotisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $new_date = $bdsCotisation->getCotisationAt()->format('Y-m-01');
            $bdsCotisation = $bdsCotisation->setCotisationAt(\DateTime::createFromFormat('Y-m-d', $new_date));

            $entityManager->persist($bdsCotisation);
            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            return $this->redirectToRoute($url_pg_prcdnt);
        }

        return $this->render('banque_de_solidarite/cotisation/new.html.twig', [
            'bds_cotisation' => $bdsCotisation,
            'form'                  => $form->createView(),
            'url_pg_prcdnt'         => $url_pg_prcdnt,
            'parametres'            => $parametres,
            'user'                  => $user,
        ]);
    }

    /**
     * @Route("/new/plusieurscotisations", name="bds_cotisation_new_plusieurscotisations", methods={"GET","POST"})
     */
    public function newPlusieursCotisation(Request $request,
                        UserRepository $userRepository, BdsCotisationRepository $bdsCotisationRepository,
                        BdsParametresdecotisationRepository $bdsParametresdecotisationRepository): Response
    {
        //récupération de la page précédente dans l'URL
        if ($request->get('pg_prcdnt')){
            $url_pg_prcdnt = $request->get('pg_prcdnt');
        }
        if ($request->get('id')){
            $user = $request->get('id') ? $userRepository->find($request->get('id')) : null;
        }
        if ($user !== null){
            $parametres = $bdsParametresdecotisationRepository->findOneByAgent($user);
        }

        $bdsCotisation = new BdsCotisation();
        $bdsCotisation = $bdsCotisation->setBdsParametresdecotisation($parametres)
            ->setCotisationappliquee($parametres->getCotisationappliquee())
        ;

        $form = $this->createForm(BdsCotisationType::class, $bdsCotisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $month = substr($bdsCotisation->getCotisationAt()->format('Y-m-01'), -5, 2);
            $year = substr($bdsCotisation->getCotisationAt()->format('Y-m-01'), 0, 4);


            //dd($total_execution);

            for ($i=$month; $i<13; $i++){

                $new_date = $bdsCotisation->getCotisationAt()->format($year.'-'.$i.'-01');
                $bdsCotisation = new BdsCotisation();
                $bdsCotisation = $bdsCotisation->setCotisationAt(\DateTime::createFromFormat('Y-m-d', $new_date))
                                ->setCotisationappliquee($parametres->getCotisationappliquee())
                                ->setBdsParametresdecotisation($parametres);
                $entityManager->persist($bdsCotisation);
            }
            //dd($total_execution);



            $entityManager->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            return $this->redirectToRoute('bds_agents');
        }

        return $this->render('banque_de_solidarite/cotisation/new_plusieurscotisations.html.twig', [
            'bds_cotisation' => $bdsCotisation,
            'form'                  => $form->createView(),
            'url_pg_prcdnt'         => $url_pg_prcdnt,
            'parametres'            => $parametres,
            'user'                  => $user,
        ]);
    }

    /**
     * @Route("/{id}/infocotisations", name="bds_cotisation_showagentcotisation", methods={"GET"})
     */
    public function showAgentCotisations(User $user,
                                         EvRecrutementRepository $evRecrutementRepository,
                                         BdsCotisationRepository $bdsCotisationRepository): Response
    {
        $cotisations_agent = $bdsCotisationRepository->findByAgent($user);
        $total_par_an = $bdsCotisationRepository->getSumByYear($user);
        //dd($bdsCotisationRepository->getAllSum($user));
        return $this->render('banque_de_solidarite/cotisation/showagentcotisations.html.twig', [
            'user'                  => $user,
            'retraite'              => $user->getRetraite(),
            'cotisations_agent'     => $cotisations_agent,
            'total_par_an'          => $total_par_an,
            'somme'                 => $bdsCotisationRepository->getAllSum($user),
            'recrutement'           => $evRecrutementRepository->findLastByUserField($user),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bds_cotisation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BdsCotisation $bdsCotisation, BdsParametresdecotisationRepository $bdsParametresdecotisationRepository): Response
    {
        $form = $this->createForm(BdsCotisationType::class, $bdsCotisation);
        $form->handleRequest($request);

        $bds_param = $bdsParametresdecotisationRepository->findOneByAgent($bdsCotisation->getBdsParametresdecotisation()->getUser());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Opération effectuée avec succès!');

            return $this->redirectToRoute('bds_cotisation_index');
        }

        return $this->render('banque_de_solidarite/cotisation/edit.html.twig', [
            'bds_cotisation' => $bdsCotisation,
            'bds_param' => $bds_param,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bds_cotisation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, BdsCotisation $bdsCotisation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bdsCotisation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bdsCotisation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bds_cotisation_index');
    }
}
