<?php

namespace App\Controller;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\DirectionSearch;
use App\Entity\Ile;
use App\Entity\Poste;
use App\Entity\PosteUser;
use App\Entity\PosteuserServicedepartementdirection;
use App\Entity\Service;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use App\Entity\UserOptionVisibilite;
use App\Entity\UserSearch;
use App\Entity\Ville;
use App\Form\DepartementDirectionType;
use App\Form\DirectionSearchType;
use App\Form\DirectionType;
use App\Form\Filtres\DepartementDirectionFiltreAgenceType;
use App\Form\Filtres\DepartementDirectionFiltreDeptType;
use App\Form\Filtres\DepartementDirectionFiltreType;
use App\Form\Filtres\IleFiltreType;
use App\Form\Filtres\PosteUserFiltrePosteType;
use App\Form\Filtres\ServiceDepartementdirectionFiltreAgenceType;
use App\Form\Filtres\ServiceDepartementdirectionFiltreType;
use App\Form\Filtres\ServiceDirectionFiltreAgenceType;
use App\Form\Filtres\ServiceDirectionFiltreServType;
use App\Form\Filtres\ServiceDirectionFiltreType;
use App\Form\Filtres\VilleFiltreIleType;
use App\Form\Filtres\VilleFiltreType;
use App\Form\PosteuserServicedepartementdirectionType;
use App\Form\ServiceDirectionType;
use App\Form\UserSearchType;
use App\Form\UserType;
use App\Repository\AgenceRepository;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\PosteuserDepartementdirectionRepository;
use App\Repository\PosteuserDirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\PosteuserServicedepartementdirectionRepository;
use App\Repository\PosteuserServicedirectionRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\ServiceRepository;
use App\Repository\UserOptionVisibiliteRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CTController extends AbstractController
{
    /**
     * @Route("/ct/services", name="ct_service")
     */
    /*
    public function showService(
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository,
        UserRepository $userRepository
    )
    {
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $serviceDirections = $serviceDirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        $searchDeptDir = new DepartementDirection();
        $formDeptDirSearch = $this->createForm(DepartementDirectionFiltreType::class, $searchDeptDir);
        $formDeptDirSearch->handleRequest($request);
        $_POSTDirService = null;

        $searchDept = new DepartementDirection();
        $formDeptSearch = $this->createForm(DepartementDirectionFiltreDeptType::class, $searchDept);
        $formDeptSearch->handleRequest($request);
        $_POSTDeptService = null;

        $searchServDeptDirAgence = new ServiceDepartementdirection();
        $formServDeptDirAgenceSearch = $this->createForm(ServiceDepartementdirectionFiltreAgenceType::class, $searchServDeptDirAgence);
        $formServDeptDirAgenceSearch->handleRequest($request);
        $_POSTAgenceService = null;

        $searchIle = new Ville();
        $formIleSearch = $this->createForm(VilleFiltreIleType::class, $searchIle);
        $formIleSearch->handleRequest($request);
        $_POSTIleService = null;

        $searchAgentServ = new UserSearch();
        $formAgentServSearch = $this->createForm(UserSearchType::class, $searchAgentServ);
        $formAgentServSearch->handleRequest($request);
        $_POSTAgentService = null;

        if ($formDeptDirSearch->isSubmitted()) {
            $services = $serviceRepository->findServDeptDirByDir($searchDeptDir);
            $_POSTDirService = $searchDeptDir->getDirection()->getDesignation();
        } elseif ($formDeptSearch->isSubmitted()) {
            $services = $serviceRepository->findServDeptDirByDept($searchDept);
            $_POSTDeptService = $searchDept->getDepartement()->getDesignation();
        } elseif ($formServDeptDirAgenceSearch->isSubmitted()) {
            $services = $serviceRepository->findServDeptDirByAgence($searchServDeptDirAgence);
            $_POSTAgenceService = $searchServDeptDirAgence->getAgence()->getDesignation();
        } elseif ($formIleSearch->isSubmitted()) {
            $services = $serviceRepository->findServiceByIle($searchIle);
            $_POSTIleService = $searchIle->getIle()->getDesignation();
        } elseif ($formAgentServSearch->isSubmitted()) {
            $services = $serviceRepository->findServiceByAgent($searchAgentServ);
            $_POSTAgentService = $searchAgentServ->getMatricule();
        } elseif ($request->get('ile_service')) {
            $searchIle = $request->get('ile_service');
            if ($searchIle == 'Ngazidja') $services = $serviceRepository->findServiceByNgz();
            elseif ($searchIle == 'Ndzuani') $services = $serviceRepository->findServiceByNdz();
            elseif ($searchIle == 'Mwali') $services = $serviceRepository->findServiceByMwa();
        } else $services = $serviceRepository->findAll();

        return $this->render('ct/service.html.twig', [
            'services' => $services,
            'total_services' => $serviceRepository->findAll(),
            'departements' => $departements,
            'directions' => $directions,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirections,
            'departement_directions' => $departementDirections,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'formDeptDirSearch' => $formDeptDirSearch->createView(),
            'formDeptSearch' => $formDeptSearch->createView(),
            'formServDeptDirAgenceSearch' => $formServDeptDirAgenceSearch->createView(),
            'formIleSearch' => $formIleSearch->createView(),
            'formAgentServSearch' => $formAgentServSearch->createView(),
            '_POST_dir_service' => $_POSTDirService,
            '_POST_dept_service' => $_POSTDeptService,
            '_POST_agence_service' => $_POSTAgenceService,
            '_POST_ile_service' => $_POSTIleService,
            '_POST_agent_serv' => $_POSTAgentService,
        ]);
    }
    */

    /**
     * @Route("/ct/departements", name="ct_departement")
     */
    /*
    public function showDepartement(
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository
    )
    {
        $services = $serviceRepository->findAll();
        $directions = $directionRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        $searchDeptDir = new DepartementDirection();
        $formDeptDirSearch = $this->createForm(DepartementDirectionFiltreType::class, $searchDeptDir);
        $formDeptDirSearch->handleRequest($request);
        $_POSTDirDept = null;

        $searchDeptDirAgence = new DepartementDirection();
        $formDeptDirAgenceSearch = $this->createForm(DepartementDirectionFiltreAgenceType::class, $searchDeptDirAgence);
        $formDeptDirAgenceSearch->handleRequest($request);
        $_POSTAgenceDept = null;

        $searchIle = new Ville();
        $formIleSearch = $this->createForm(VilleFiltreIleType::class, $searchIle);
        $formIleSearch->handleRequest($request);
        $_POSTIleDept = null;

        $searchAgentDept = new UserSearch();
        $formAgentDeptSearch = $this->createForm(UserSearchType::class, $searchAgentDept);
        $formAgentDeptSearch->handleRequest($request);
        $_POSTAgentDept = null;

        if ($formDeptDirSearch->isSubmitted()) {
            $departements = $departementRepository->findDeptDirByDir($searchDeptDir);
            $_POSTDirDept = $searchDeptDir->getDirection()->getDesignation();
        } elseif ($formDeptDirAgenceSearch->isSubmitted()) {
            $departements = $departementRepository->findDeptDirByAgence($searchDeptDirAgence);
            $_POSTAgenceDept = $searchDeptDirAgence->getAgence()->getDesignation();
        } elseif ($formIleSearch->isSubmitted()) {
            $departements = $departementRepository->findDeptByIle($searchIle);
            $_POSTIleDept = $searchIle->getIle()->getDesignation();
        } elseif ($formAgentDeptSearch->isSubmitted()) {
            $departements = $departementRepository->findDeptByAgent($searchAgentDept);
            $_POSTAgentDept = $searchAgentDept->getMatricule();
        } elseif (isset($_GET['ile_departement'])) {
            $searchIle = $_GET['ile_departement'];
            if ($searchIle == 'Ngazidja') $departements = $departementRepository->findDeptByNgz();
            elseif ($searchIle == 'Ndzuani') $departements = $departementRepository->findDeptByNdz();
            else $departements = $departementRepository->findDeptByMwa();
        } else{
            $departements = $departementRepository->findAll();
        }

        return $this->render('ct/departement.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'total_departements' => $departementRepository->findAll(),
            'directions' => $directions,
            'departement_directions' => $departementDirections,
            'service_departementdirections' => $serviceDepartementdirections,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'formDeptDirSearch' => $formDeptDirSearch->createView(),
            'formDeptDirAgenceSearch' => $formDeptDirAgenceSearch->createView(),
            'formIleSearch' => $formIleSearch->createView(),
            'formAgentDeptSearch' => $formAgentDeptSearch->createView(),
            '_POST_dir_dept' => $_POSTDirDept,
            '_POST_agence_dept' => $_POSTAgenceDept,
            '_POST_ile_dept' => $_POSTIleDept,
            '_POST_agent_dept' => $_POSTAgentDept,
        ]);
    }
    */
    /**
     * @Route("/ct/directions", name="ct_direction")
     */
    /*
    public function showDirection(
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        $searchDeptDir = new DepartementDirection();
        $formDeptDirSearch = $this->createForm(DepartementDirectionFiltreDeptType::class, $searchDeptDir);
        $formDeptDirSearch->handleRequest($request);
        $_POSTDeptDir = null;

        $searchServDeptDir = new ServiceDepartementdirection();
        $formServDeptDirSearch = $this->createForm(ServiceDepartementdirectionFiltreType::class, $searchServDeptDir);
        $formServDeptDirSearch->handleRequest($request);
        $_POSTServDir = null;

        $searchServDirAgence = new ServiceDirection();
        $formServDirAgenceSearch = $this->createForm(ServiceDirectionFiltreAgenceType::class, $searchServDirAgence);
        $formServDirAgenceSearch->handleRequest($request);
        $_POSTAgenceDir = null;

        $searchIle = new Ville();
        $formIleSearch = $this->createForm(VilleFiltreIleType::class, $searchIle);
        $formIleSearch->handleRequest($request);
        $_POSTIleDir = null;

        $searchAgentDir = new UserSearch();
        $formAgentDirSearch = $this->createForm(UserSearchType::class, $searchAgentDir);
        $formAgentDirSearch->handleRequest($request);
        $_POSTAgentDir = null;

        if ($formDeptDirSearch->isSubmitted()) {
            $directions = $directionRepository->findDeptDirByDept($searchDeptDir);
            $_POSTDeptDir = $searchDeptDir->getDepartement()->getDesignation();
        } elseif ($formServDeptDirSearch->isSubmitted()) {
            $directions = $directionRepository->findServDeptDirByServ($searchServDeptDir);
            $_POSTServDir = $searchServDeptDir->getService()->getDesignation();
        } elseif ($formServDirAgenceSearch->isSubmitted()) {
            $directions = $directionRepository->findServDirByAgence($searchServDirAgence);
            $_POSTAgenceDir = $searchServDirAgence->getAgence()->getDesignation();
        } elseif ($formIleSearch->isSubmitted()) {
            $directions = $directionRepository->findDirByIle($searchIle);
            $_POSTIleDir = $searchIle->getIle()->getDesignation();
        } elseif ($formAgentDirSearch->isSubmitted()) {
            $directions = $directionRepository->findDirByAgent($searchAgentDir);
            $_POSTAgentDir = $searchAgentDir->getMatricule();
        } elseif (isset($_GET['ile_direction'])) {
            $searchIle = $_GET['ile_direction'];
            if ($searchIle == 'Ngazidja') $directions = $directionRepository->findDeptByNgz();
            elseif ($searchIle == 'Ndzuani') $directions = $directionRepository->findDeptByNdz();
            elseif ($searchIle == 'Mwali') $directions = $directionRepository->findDeptByMwa();
            else $directions = $directionRepository->findAll();
        } else {
            $directions = $directionRepository->findAll();
        }

        return $this->render('ct/direction.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'total_directions' => $directionRepository->findAll(),
            'departement_directions' => $departementDirections,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirectionRepository,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'formDeptDirSearch' => $formDeptDirSearch->createView(),
            'formServDeptDirSearch' => $formServDeptDirSearch->createView(),
            'formServDirAgenceSearch' => $formServDirAgenceSearch->createView(),
            'formIleSearch' => $formIleSearch->createView(),
            'formAgentDirSearch' => $formAgentDirSearch->createView(),
            '_POST_dept_dir' => $_POSTDeptDir,
            '_POST_serv_dir' => $_POSTServDir,
            '_POST_agence_dir' => $_POSTAgenceDir,
            '_POST_ile_direction' => $_POSTIleDir,
            '_POST_agent_dir' => $_POSTAgentDir,
        ]);
    }
    */

    /**
     * @Route("/ct/agences", name="ct_agence")
     */
    /*
    public function showAgence(
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository,
        AgenceRepository $agenceRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $serviceDirections = $serviceDirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        $searchServ = new ServiceDepartementdirection();
        $formServSearch = $this->createForm(ServiceDepartementdirectionFiltreType::class, $searchServ);
        $formServSearch->handleRequest($request);
        $_POSTServAgence = null;

        $searchDept = new DepartementDirection();
        $formDeptSearch = $this->createForm(DepartementDirectionFiltreDeptType::class, $searchDept);
        $formDeptSearch->handleRequest($request);
        $_POSTDeptAgence = null;

        $searchDir = new DepartementDirection();
        $formDirSearch = $this->createForm(DepartementDirectionFiltreType::class, $searchDir);
        $formDirSearch->handleRequest($request);
        $_POSTDirAgence = null;

        $searchIle = new Ville();
        $formIleSearch = $this->createForm(VilleFiltreIleType::class, $searchIle);
        $formIleSearch->handleRequest($request);
        $_POSTIleAgence = null;

        if ($formServSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByServ($searchServ);
            $_POSTServAgence = $searchServ->getService()->getDesignation();
        } elseif ($formDeptSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByDept($searchDept);
            $_POSTDeptAgence = $searchDept->getDepartement()->getDesignation();
        } elseif ($formDirSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByDir($searchDir);
            $_POSTDirAgence = $searchDir->getDirection()->getDesignation();
        } elseif ($formIleSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByIle($searchIle);
            $_POSTIleAgence = $searchIle->getIle()->getDesignation();
        } elseif (isset($_GET['ile_agence'])) {
            $searchIle = $_GET['ile_agence'];
            $agences = $agenceRepository->findAgenceByGetIle($searchIle);
        } else {
            $agences = $agenceRepository->findAll();
        }

        return $this->render('ct/agence.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirections,
            'departement_directions' => $departementDirections,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'agences' => $agences,
            'total_agences' => $agenceRepository->findAll(),
            'formServSearch' => $formServSearch->createView(),
            'formDeptSearch' => $formDeptSearch->createView(),
            'formDirSearch' => $formDirSearch->createView(),
            'formIleSearch' => $formIleSearch->createView(),
            '_POST_serv_agence' => $_POSTServAgence,
            '_POST_dept_agence' => $_POSTDeptAgence,
            '_POST_dir_agence' => $_POSTDirAgence,
            '_POST_ile_agence' => $_POSTIleAgence,
        ]);
    }
    */

    /**
     * @Route("/ct/filtres", name="ct_filtre")
     */
    public function showFiltre(
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository,
        AgenceRepository $agenceRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $serviceDirections = $serviceDirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        $searchServ = new ServiceDepartementdirection();
        $formServSearch = $this->createForm(ServiceDepartementdirectionFiltreType::class, $searchServ);
        $formServSearch->handleRequest($request);

        $searchDept = new DepartementDirection();
        $formDeptSearch = $this->createForm(DepartementDirectionFiltreDeptType::class, $searchDept);
        $formDeptSearch->handleRequest($request);

        $searchDir = new DepartementDirection();
        $formDirSearch = $this->createForm(DepartementDirectionFiltreType::class, $searchDir);
        $formDirSearch->handleRequest($request);

        if ($formServSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByServ($searchServ);
        } elseif ($formDeptSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByDept($searchDept);
        } elseif ($formDirSearch->isSubmitted()) {
            $agences = $agenceRepository->findAgenceByDir($searchDir);
        } else {
            $agences = $agenceRepository->findAll();
        }

        return $this->render('ct/filtre.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirections,
            'departement_directions' => $departementDirections,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'agences' => $agences,
            'formServSearch' => $formServSearch->createView(),
            'formDeptSearch' => $formDeptSearch->createView(),
            'formDirSearch' => $formDirSearch->createView(),
        ]);
    }

    /**
     * @Route("/ct/agent/service/agence", name="ct_agent_service_agence")
     */
    public function showAgentServiceAgence(
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository,
        AgenceRepository $agenceRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $serviceDirections = $serviceDirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();
        $agences = $agenceRepository->findAll();

        return $this->render('ct/agent_service_agence.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirections,
            'departement_directions' => $departementDirections,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'agences' => $agences,
        ]);
    }

    /**
     * @Route("/ct/agent", name="ct_agent")
     */
    /*
    public function showAgent(
        PaginatorInterface $paginator,
        Request $request,
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
        ServiceDirectionRepository $serviceDirectionRepository,
        DepartementDirectionRepository $departementDirectionRepository,
        PosteUserRepository $posteUserRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository,
        UserRepository $userRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();
        $serviceDepartementdirections = $serviceDepartementdirectionRepository->findAll();
        $serviceDirections = $serviceDirectionRepository->findAll();
        $departementDirections = $departementDirectionRepository->findAll();
        $posteUsers = $posteUserRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        $total_users = $userRepository->findAll();
        $total_users_filtre = 0;

        $searchServDeptDir = new ServiceDepartementdirection();
        $formServDeptDirSearch = $this->createForm(ServiceDepartementdirectionFiltreType::class, $searchServDeptDir);
        $formServDeptDirSearch->handleRequest($request);
        $_POSTServAgent = null;

        $searchDeptDir = new DepartementDirection();
        $formDeptDirSearch = $this->createForm(DepartementDirectionFiltreDeptType::class, $searchDeptDir);
        $formDeptDirSearch->handleRequest($request);
        $_POSTDeptAgent = null;

        $searchDir = new DepartementDirection();
        $formDirSearch = $this->createForm(DepartementDirectionFiltreType::class, $searchDir);
        $formDirSearch->handleRequest($request);
        $_POSTDirAgent = null;

        $search = new UserSearch();
        $formSearch = $this->createForm(UserSearchType::class, $search);
        $formSearch->handleRequest($request);

        $searchIle = new Ville();
        $formIleSearch = $this->createForm(VilleFiltreIleType::class, $searchIle);
        $formIleSearch->handleRequest($request);
        $_POSTIleAgent = null;

        $searchPoste = new PosteUser();
        $formPosteSearch = $this->createForm(PosteUserFiltrePosteType::class, $searchPoste);
        $formPosteSearch->handleRequest($request);
        $_POSTPosteAgent = null;

        $searchPosteServDeptDir = new PosteuserServicedepartementdirection();
        $formPosteServDeptDirSearch = $this->createForm(PosteuserServicedepartementdirectionType::class, $searchPosteServDeptDir);
        $formPosteServDeptDirSearch->handleRequest($request);
        $_POSTPosteServDeptDirAgent = null;
        $_POSTServDeptDirAgent = null;

        if ($request->get('filtre_sexe')) {
            $searchSexe = $request->get('filtre_sexe');
            $users = $paginator->paginate(
                $userRepository->findAllBySexe($searchSexe),
                $request->query->getInt('page', 1),
                20
            );
            $total_users_filtre = $userRepository->findAllBySexe($searchSexe)->getResult();
        } elseif ($request->get('filtre_categorie')) {
            $searchCategorie = $request->get('filtre_categorie');
            $users = $paginator->paginate(
                $userRepository->findAllByCategorie($searchCategorie),
                $request->query->getInt('page', 1),
                20
            );
            $total_users_filtre = $userRepository->findAllByCategorie($searchCategorie)->getResult();
        } elseif ($request->get('filtre_statut')) {
            $searchStatut = $request->get('filtre_statut');
            $users = $paginator->paginate(
                $userRepository->findAllByStatut($searchStatut),
                $request->query->getInt('page', 1),
                20
            );
            $total_users_filtre = $userRepository->findAllByStatut($searchStatut)->getResult();
        } elseif ($formIleSearch->isSubmitted()) {
            $users = $userRepository->findAgentByIle($searchIle);
            $_POSTIleAgent = $searchIle->getIle()->getDesignation();
        } elseif ($request->get('ile_agent')) {
            $searchIle = $request->get('ile_agent');
            $users = $userRepository->findByIle($searchIle);
        } elseif ($formServDeptDirSearch->isSubmitted()) {
            $users = $userRepository->findAgentsByServ($searchServDeptDir);
            $_POSTServAgent = $searchServDeptDir->getService()->getDesignation();
        } elseif ($formDeptDirSearch->isSubmitted()) {
            $users = $userRepository->findAgentsByDept($searchDeptDir);
            $_POSTDeptAgent = $searchDeptDir->getDepartement()->getDesignation();
        } elseif ($formDirSearch->isSubmitted()) {
            $users = $userRepository->findAgentsByDir($searchDir);
            $_POSTDirAgent = $searchDir->getDirection()->getDesignation();
        } elseif ($formPosteSearch->isSubmitted()) {
            $users = $userRepository->findAgentsByPoste($searchPoste);
            $_POSTPosteAgent = $searchPoste->getPoste()->getDesignation();
        } elseif ($formPosteServDeptDirSearch->isSubmitted()) {
            $users = $userRepository->findAgentsByPosteServDeptDir($searchPosteServDeptDir);
            $_POSTPosteServDeptDirAgent = $searchPosteServDeptDir->getPosteuser()->getPoste()->getDesignation();
            $_POSTServDeptDirAgent = $searchPosteServDeptDir->getServicedepartementdirection()->getDepartementdirection()->getDirection()->getDesignation();
        } else {
            $users = $paginator->paginate(
                $userRepository->findAllOrderBy($search),
                $request->query->getInt('page', 1),
                10
            );
        }

        $usersOnline = $userRepository->getActive();

        return $this->render('ct/agent.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'service_departementdirections' => $serviceDepartementdirections,
            'service_directions' => $serviceDirections,
            'departement_directions' => $departementDirections,
            'poste_users' => $posteUsers,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            'users' => $users,
            'users_online' => $usersOnline,
            'total_users' => $total_users,
            'total_users_filtre' => $total_users_filtre,
            'formSearch' => $formSearch->createView(),
            'formServDeptDirSearch' => $formServDeptDirSearch->createView(),
            'formDeptDirSearch' => $formDeptDirSearch->createView(),
            'formDirSearch' => $formDirSearch->createView(),
            'formIleSearch' => $formIleSearch->createView(),
            'formPosteSearch' => $formPosteSearch->createView(),
            'formPosteServDeptDirSearch' => $formPosteServDeptDirSearch->createView(),
            '_POST_serv_agent' => $_POSTServAgent,
            '_POST_dept_agent' => $_POSTDeptAgent,
            '_POST_dir_agent' => $_POSTDirAgent,
            '_POST_ile_agent' => $_POSTIleAgent,
            '_POST_poste_agent' => $_POSTPosteAgent,
            '_POST_poste_serv_dept_dir_agent' => $_POSTPosteServDeptDirAgent,
            '_POST_serv_dept_dir_agent' => $_POSTServDeptDirAgent,
        ]);
    }

    */

    /**
     * @Route("/ct/postes/service", name="ct_poste_service")
     */
    public function showPosteFonctionService(
        Request $request,
        ServiceRepository $serviceRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository
    )
    {
        $_GETService = null;
        if ($request->get('id_service')) {
            $searchServ = $request->get('id_service');
            $_GETService = $serviceRepository->findServiceById($searchServ);
        }

        $services = $serviceRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        // Récupération de la valeur du poste actuel
        /*foreach ($posteuserServicedepartementdirections as $value){
            $posteActuel = $value;
        }
        dd($posteActuel);*/
        return $this->render('ct/posteuser_service.html.twig', [
            'services' => $services,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections,
            '_GET_service' => $_GETService
        ]);
    }

    /**
     * @Route("/ct/postes/departement", name="ct_poste_departement")
     */
    public function showPosteFonctionDepartement(
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        return $this->render('ct/posteuser_departement.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections
        ]);
    }

    /**
     * @Route("/ct/postes/direction", name="ct_poste_direction")
     */
    public function showPosteFonctionDirection(
        ServiceRepository $serviceRepository,
        DepartementRepository $departementRepository,
        DirectionRepository $directionRepository,
        PosteuserDirectionRepository $posteuserDirectionRepository,
        PosteuserServicedirectionRepository $posteuserServicedirectionRepository,
        PosteuserServicedepartementdirectionRepository $posteuserServicedepartementdirectionRepository,
        PosteuserDepartementdirectionRepository $posteuserDepartementdirectionRepository
    )
    {
        $services = $serviceRepository->findAll();
        $departements = $departementRepository->findAll();
        $directions = $directionRepository->findAll();
        $posteuserDirections = $posteuserDirectionRepository->findAll();
        $posteuserServicedirections = $posteuserServicedirectionRepository->findAll();
        $posteuserServicedepartementdirections = $posteuserServicedepartementdirectionRepository->findAll();
        $posteuserDepartementdirections = $posteuserDepartementdirectionRepository->findAll();

        return $this->render('ct/posteuser_direction.html.twig', [
            'services' => $services,
            'departements' => $departements,
            'directions' => $directions,
            'posteuser_directions' => $posteuserDirections,
            'posteuser_servicedirections' => $posteuserServicedirections,
            'posteuser_servicedepartementdirections' => $posteuserServicedepartementdirections,
            'posteuser_departements' => $posteuserDepartementdirections
        ]);
    }
}
