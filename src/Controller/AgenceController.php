<?php

namespace App\Controller;

use App\Entity\Agence;
use App\Form\AgenceType;
use App\Repository\AgenceRepository;
use App\Repository\DepartementDirectionRepository;
use App\Repository\DepartementRepository;
use App\Repository\DirectionRepository;
use App\Repository\PosteUserRepository;
use App\Repository\ServiceDepartementdirectionRepository;
use App\Repository\ServiceDirectionRepository;
use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AgenceController extends AbstractController
{
    /**
     * @Route("/gestion/ct/agences/", name="agence_index", methods={"GET"})
     */
    public function index(AgenceRepository $agenceRepository,
                          ServiceRepository $serviceRepository,
                          DepartementRepository $departementRepository,
                          DirectionRepository $directionRepository): Response
    {
        return $this->render('agence/index.html.twig', [
            'agences' => $agenceRepository->findAll(),
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/gestion/ct/agences/new", name="agence_new", methods={"GET","POST"})
     */
    public function new(Request $request,
                        ServiceRepository $serviceRepository,
                        DepartementRepository $departementRepository,
                        DirectionRepository $directionRepository): Response
    {
        $agence = new Agence();
        $form = $this->createForm(AgenceType::class, $agence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($agence);
            $entityManager->flush();

            return $this->redirectToRoute('agence_index');
        }

        return $this->render('agence/new.html.twig', [
            'agence' => $agence,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gestion/ct/agences/{id}", name="agence_show", methods={"GET"})
     */
    public function show(Agence $agence,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        return $this->render('agence/show.html.twig', [
            'agence' => $agence,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/gestion/ct/agences/{id}/edit", name="agence_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         Agence $agence,
                         ServiceRepository $serviceRepository,
                         DepartementRepository $departementRepository,
                         DirectionRepository $directionRepository): Response
    {
        $form = $this->createForm(AgenceType::class, $agence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('agence_index', [
                'id' => $agence->getId(),
            ]);
        }

        return $this->render('agence/edit.html.twig', [
            'agence' => $agence,
            'services' => $serviceRepository->findAll(),
            'departements' => $departementRepository->findAll(),
            'directions' => $directionRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gestion/ct/agences/{id}", name="agence_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Agence $agence): Response
    {
        if ($this->isCsrfTokenValid('delete'.$agence->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($agence);
            $entityManager->flush();
        }

        return $this->redirectToRoute('agence_index');
    }

    /*
     * ============================
     *  AGENCES INFO
     * ============================
     */

    /**
     * @Route("societe/agc/info", name="agence_info", methods={"GET"})
     */
    public function infoDir(DepartementDirectionRepository $departementDirectionRepository,
                            DirectionRepository $directionRepository,
                            AgenceRepository $agenceRepository,
                            ServiceDepartementdirectionRepository $serviceDepartementdirectionRepository,
                            ServiceDirectionRepository $serviceDirectionRepository,
                            PosteUserRepository $posteUserRepository
    ): Response
    {
        //$agent = $posteUserRepository->showAgentOfdirSansParam();
        //dd($agent);
        //dd( $directionRepository->findSomeFields());
        //$departements_lies = $departementDirectionRepository->findDepartementOfDir();
        //dd($serviceDepartementdirectionRepository->findSomeFields());
        return $this->render('agence/info.html.twig', [
            'agences'                    => $agenceRepository->findSomeFields(),
            'service_dpt_dir'            => $serviceDepartementdirectionRepository->findSomeFields(),
            'service_dir'            => $serviceDirectionRepository->findSomeFields(),
            'departement_dir'            => $departementDirectionRepository->findSomeFields(),
            'direction'            => $directionRepository->findSomeFields(),
        ]);
    }
}
