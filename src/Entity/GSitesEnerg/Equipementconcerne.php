<?php

namespace App\Entity\GSitesEnerg;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GSitesEnerg\EquipementconcerneRepository")
 */
class Equipementconcerne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $caracteristiques;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GSitesEnerg\Baieenergetique", inversedBy="equipementconcernes")
     */
    private $baieenergetique;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getCaracteristiques(): ?string
    {
        return $this->caracteristiques;
    }

    public function setCaracteristiques(?string $caracteristiques): self
    {
        $this->caracteristiques = $caracteristiques;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getBaieenergetique(): ?Baieenergetique
    {
        return $this->baieenergetique;
    }

    public function setBaieenergetique(?Baieenergetique $baieenergetique): self
    {
        $this->baieenergetique = $baieenergetique;

        return $this;
    }
}
