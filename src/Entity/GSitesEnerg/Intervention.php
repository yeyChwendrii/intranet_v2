<?php

namespace App\Entity\GSitesEnerg;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GSitesEnerg\InterventionRepository")
 */
class Intervention
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateInterventionAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="interventions")
     * @Assert\NotBlank
     */
    private $intervenant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $saisipar;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank
     */
    private $problemeconstate;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank
     */
    private $solutionproposee;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GSitesEnerg\Materiel", inversedBy="interventions")
     */
    private $materiels;



    public function __construct()
    {
        $this->intervenant = new ArrayCollection();
        $this->materiels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateInterventionAt(): ?\DateTimeInterface
    {
        return $this->dateInterventionAt;
    }

    public function setDateInterventionAt(\DateTimeInterface $dateInterventionAt): self
    {
        $this->dateInterventionAt = $dateInterventionAt;

        return $this;
    }


    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getIntervenant(): Collection
    {
        return $this->intervenant;
    }

    public function addIntervenant(User $intervenant): self
    {
        if (!$this->intervenant->contains($intervenant)) {
            $this->intervenant[] = $intervenant;
        }

        return $this;
    }

    public function removeIntervenant(User $intervenant): self
    {
        if ($this->intervenant->contains($intervenant)) {
            $this->intervenant->removeElement($intervenant);
        }

        return $this;
    }

    public function getSaisipar(): ?string
    {
        return $this->saisipar;
    }

    public function setSaisipar(string $saisipar): self
    {
        $this->saisipar = $saisipar;

        return $this;
    }

    public function getProblemeconstate(): ?string
    {
        return $this->problemeconstate;
    }

    public function setProblemeconstate(?string $problemeconstate): self
    {
        $this->problemeconstate = $problemeconstate;

        return $this;
    }

    public function getSolutionproposee(): ?string
    {
        return $this->solutionproposee;
    }

    public function setSolutionproposee(?string $solutionproposee): self
    {
        $this->solutionproposee = $solutionproposee;

        return $this;
    }

    /**
     * @return Collection|Materiel[]
     */
    public function getMateriels(): Collection
    {
        return $this->materiels;
    }

    public function addMateriel(Materiel $materiel): self
    {
        if (!$this->materiels->contains($materiel)) {
            $this->materiels[] = $materiel;
        }

        return $this;
    }

    public function removeMateriel(Materiel $materiel): self
    {
        if ($this->materiels->contains($materiel)) {
            $this->materiels->removeElement($materiel);
        }

        return $this;
    }

}
