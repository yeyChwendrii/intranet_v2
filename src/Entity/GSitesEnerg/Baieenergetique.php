<?php

namespace App\Entity\GSitesEnerg;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GSitesEnerg\BaieenergetiqueRepository")
 */
class Baieenergetique
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GSitesEnerg\Materiel", mappedBy="baieenergetique")
     */
    private $materiels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GSitesEnerg\Equipementconcerne", mappedBy="baieenergetique")
     */
    private $equipementconcernes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GSitesEnerg\SitesEnergetiques", inversedBy="baieenergetiques")
     */
    private $siteenergetique;

    public function __construct()
    {
        $this->materiels = new ArrayCollection();
        $this->equipementconcernes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getSiteenergetique();
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @return Collection|Materiel[]
     */
    public function getMateriels(): Collection
    {
        return $this->materiels;
    }

    public function addMateriel(Materiel $materiel): self
    {
        if (!$this->materiels->contains($materiel)) {
            $this->materiels[] = $materiel;
            $materiel->setBaieenergetique($this);
        }

        return $this;
    }

    public function removeMateriel(Materiel $materiel): self
    {
        if ($this->materiels->contains($materiel)) {
            $this->materiels->removeElement($materiel);
            // set the owning side to null (unless already changed)
            if ($materiel->getBaieenergetique() === $this) {
                $materiel->setBaieenergetique(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Equipementconcerne[]
     */
    public function getEquipementconcernes(): Collection
    {
        return $this->equipementconcernes;
    }

    public function addEquipementconcerne(Equipementconcerne $equipementconcerne): self
    {
        if (!$this->equipementconcernes->contains($equipementconcerne)) {
            $this->equipementconcernes[] = $equipementconcerne;
            $equipementconcerne->setBaieenergetique($this);
        }

        return $this;
    }

    public function removeEquipementconcerne(Equipementconcerne $equipementconcerne): self
    {
        if ($this->equipementconcernes->contains($equipementconcerne)) {
            $this->equipementconcernes->removeElement($equipementconcerne);
            // set the owning side to null (unless already changed)
            if ($equipementconcerne->getBaieenergetique() === $this) {
                $equipementconcerne->setBaieenergetique(null);
            }
        }

        return $this;
    }

    public function getSiteenergetique(): ?SitesEnergetiques
    {
        return $this->siteenergetique;
    }

    public function setSiteenergetique(?SitesEnergetiques $siteenergetique): self
    {
        $this->siteenergetique = $siteenergetique;

        return $this;
    }
}
