<?php

namespace App\Entity\GSitesEnerg;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GSitesEnerg\SitesEnergetiquesRepository")
 * @UniqueEntity(fields={"designation"}, message="Cette désignation est déjà enregistrée !")
 */
class SitesEnergetiques
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Assert\NotBlank
     */
    private $designation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * --Liaison unidirectionnelle
     * @ORM\OneToOne(targetEntity="App\Entity\GSitesEnerg\Contact1", cascade={"persist", "remove"})
     * --Validation de l'entité Contact1 | Constraint de validation
     * @Assert\Valid
     *
     */
    private $contact1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GSitesEnerg\Baieenergetique", mappedBy="siteenergetique")
     */
    private $baieenergetiques;

    public function __construct()
    {
        $this->baieenergetiques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getContact1(): ?Contact1
    {
        return $this->contact1;
    }

    public function setContact1(?Contact1 $contact1): self
    {
        $this->contact1 = $contact1;

        return $this;
    }

    /**
     * @return Collection|Baieenergetique[]
     */
    public function getBaieenergetiques(): Collection
    {
        return $this->baieenergetiques;
    }

    public function addBaieenergetique(Baieenergetique $baieenergetique): self
    {
        if (!$this->baieenergetiques->contains($baieenergetique)) {
            $this->baieenergetiques[] = $baieenergetique;
            $baieenergetique->setSiteenergetique($this);
        }

        return $this;
    }

    public function removeBaieenergetique(Baieenergetique $baieenergetique): self
    {
        if ($this->baieenergetiques->contains($baieenergetique)) {
            $this->baieenergetiques->removeElement($baieenergetique);
            // set the owning side to null (unless already changed)
            if ($baieenergetique->getSiteenergetique() === $this) {
                $baieenergetique->setSiteenergetique(null);
            }
        }

        return $this;
    }
}
