<?php

namespace App\Entity\GSitesEnerg;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GSitesEnerg\MaterielRepository")
 * @UniqueEntity(
 *     fields={"designation", "baieenergetique"},
 *     message="Le matériel choisi est déjà attribué à cette baie"
 * )
 */
class Materiel
{

    const TYPE_MATERIEL = [
        '' => 'Choisir le type',
        'Redresseur' => 'Redresseur',
        'Batterie' => 'Batterie',
        'Autres' => 'Autres'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caracteristiques;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GSitesEnerg\Baieenergetique", inversedBy="materiels")
     */
    private $baieenergetique;

    /**
     * @ORM\Column(type="integer")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $typemateriel;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GSitesEnerg\Intervention", mappedBy="materiels")
     */
    private $interventions;

    public function __construct()
    {
        $this->interventions = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getBaieenergetique().' - '.$this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getCaracteristiques(): ?string
    {
        return $this->caracteristiques;
    }

    public function setCaracteristiques(?string $caracteristiques): self
    {
        $this->caracteristiques = $caracteristiques;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getBaieenergetique(): ?Baieenergetique
    {
        return $this->baieenergetique;
    }

    public function setBaieenergetique(?Baieenergetique $baieenergetique): self
    {
        $this->baieenergetique = $baieenergetique;

        return $this;
    }

    public function getNombre(): ?int
    {
        return $this->nombre;
    }

    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTypemateriel(): ?string
    {
        return $this->typemateriel;
    }

    public function setTypemateriel(string $typemateriel): self
    {
        $this->typemateriel = $typemateriel;

        return $this;
    }

    /**
     * @return Collection|Intervention[]
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions[] = $intervention;
            $intervention->addMateriel($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->contains($intervention)) {
            $this->interventions->removeElement($intervention);
            $intervention->removeMateriel($this);
        }

        return $this;
    }

}
