<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartementRepository")
 * @UniqueEntity(fields={"designation"}, message="Le département que vous avez saisi est déjà enregistré !")
 */
class Departement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DepartementDirection", mappedBy="departement")
     */
    private $departementDirections;

    public function __construct()
    {
        $this->departementDirections = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|DepartementDirection[]
     */
    public function getDepartementDirections(): Collection
    {
        return $this->departementDirections;
    }

    public function addDepartementDirection(DepartementDirection $departementDirection): self
    {
        if (!$this->departementDirections->contains($departementDirection)) {
            $this->departementDirections[] = $departementDirection;
            $departementDirection->setDepartement($this);
        }

        return $this;
    }

    public function removeDepartementDirection(DepartementDirection $departementDirection): self
    {
        if ($this->departementDirections->contains($departementDirection)) {
            $this->departementDirections->removeElement($departementDirection);
            // set the owning side to null (unless already changed)
            if ($departementDirection->getDepartement() === $this) {
                $departementDirection->setDepartement(null);
            }
        }

        return $this;
    }
}
