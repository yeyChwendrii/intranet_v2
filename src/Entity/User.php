<?php
// src/Entity/User.php

namespace App\Entity;

use App\Entity\Admin\EcFormationuser;
use App\Entity\Admin\EcExperienceAilleurs;
use App\Entity\AffairesSociales\Enfant;
use App\Entity\AffairesSociales\EpouxSe;
use App\Entity\AffairesSociales\SituationDesRepos;
use App\Entity\AffairesSociales\SituationDuCarnet;
use App\Entity\AffairesSociales\SituationFamiliale;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvRenouvellementdecontrat;
use App\Entity\Evenements\EvStage;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvAvancementnormal;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvReclassement;
use App\Entity\Evenements\EvAvertissement;
use App\Entity\Evenements\EvLicenciement;
use App\Entity\Evenements\EvDeces;
use App\Entity\Evenements\EvRetraite;
use App\Entity\Evenements\EvMiseapied;
use App\Entity\Evenements\EvDemission;
use App\Entity\Evenements\EvSuspension;
use App\Entity\Evenements\EvReintegration;
use App\Entity\Evenements\EvReprisedefonction;
use App\Entity\Evenements\EvLevedesuspension;
use App\Entity\Evenements\EvInterim;
use App\Entity\Evenements\EvAnnulationdedecision;
use App\Entity\Evenements\EvRectificationdunom;
use App\Entity\Evenements\EvIndemniteforfaitaire;
use App\Entity\Evenements\EvSuspensiondesalaire;
use App\Entity\Evenements\EvDisponibilite;
use App\Entity\Evenements\EvConge;
use App\Entity\Evenements\EvAffectation;
use App\Entity\Evenements\EvDemisedefonction;
use App\Entity\GSitesEnerg\Intervention;
use App\Entity\Personnel\AgentDroitConge;
use App\Entity\Personnel\DemandeConge;
use App\Entity\Personnel\MsgAccueilConge;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Cocur\Slugify\Slugify;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Table(name="fos_user")
 * @UniqueEntity(fields={"email"}, message="L'email que vous avez indiqué est déjà utilisé !")
 * @UniqueEntity(fields={"matricule"}, message="Le matricule que vous avez indiqué est déjà utilisé !")
 * @UniqueEntity(fields={"username"}, message="Le nom d'utilisateur est déjà utilisé !")
 */
class User extends BaseUser
{
    const SEXE = [
        '' => 'Choisir le genre',
        'F' => 'F',
        'M' => 'M'
    ];

    const IND_GROUPE = [
        '' => 'Choisir le groupe',
        'I' => 'I',
        'II' => 'II',
        'III' => 'III',
        'IV' => 'IV'
    ];

    const IND_CATEGORIE = [
        '' => 'Choisir la catégorie',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5'
    ];

    const IND_ECHELON = [
        '' => 'Choisir L\'échelon',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10'
    ];


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Date/Time of the last activity
     *
     * @var \Datetime
     * @ORM\Column(name="last_activity_at", type="datetime", nullable=true)
     */
    protected $lastActivityAt;

    /**
     * @ORM\Column(type="integer", length=6, options={"default":0})
     */
    protected $loginCount = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $firstLogin;

    /**
     * @ORM\Column(type="integer", length=6, options={"default":0})
     */
    protected $matricule;

    /**
     * @ORM\Column(type="string", length=180)
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", length=180)
     */
    protected $prenom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $adresse;

    /**
     * @ORM\Column(type="string", length=2)
     */
    protected $sexe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    protected $dateNaissanceAt;

    /**
     * @ORM\Column(type="string", length=15)
     */
    protected $tel;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    protected $indiceGroupe;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $indiceCategorie;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $indiceEchelon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="user_images", fileNameProperty="image", size="imageSize")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Statut", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActionUser", mappedBy="user", orphanRemoval=true)
     */
    private $actionUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteUser", mappedBy="user", orphanRemoval=true)
     */
    private $posteUsers;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enligne;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserOptionVisibilite", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userOptionVisibilite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lieuDeNaissance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="userSender")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvTitularisation", mappedBy="user")
     */
    private $evTitularisations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvNomination", mappedBy="user")
     */
    private $evNominations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAvancementnormal", mappedBy="user")
     */
    private $evAvancementnormals;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvPromotioninterne", mappedBy="user")
     */
    private $evPromotioninternes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReclassement", mappedBy="user")
     */
    private $evReclassements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAvertissement", mappedBy="user")
     */
    private $evAvertissements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvLicenciement", mappedBy="user")
     */
    private $evLicenciements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDeces", mappedBy="user")
     */
    private $evDecess;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRetraite", mappedBy="user")
     */
    private $evRetraites;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvMiseapied", mappedBy="user")
     */
    private $evMiseapieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemission", mappedBy="user")
     */
    private $evDemissions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvSuspension", mappedBy="user")
     */
    private $evSuspensions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAffectation", mappedBy="user")
     */
    private $evAffectations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemisedefonction", mappedBy="user")
     */
    private $evDemisedefonctions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvStage", mappedBy="user")
     */
    private $evStages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRecrutement", mappedBy="user")
     */
    private $evRecrutements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRenouvellementdecontrat", mappedBy="user")
     */
    private $evRenouvellementdecontrats;

     /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReintegration", mappedBy="user")
     */
    private $evReintegrations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReprisedefonction", mappedBy="user")
     */
    private $evReprisedefonctions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvLevedesuspension", mappedBy="user")
     */
    private $evLevedesuspensions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvInterim", mappedBy="user")
     */
    private $evInterims;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAnnulationdedecision", mappedBy="user")
     */
    private $evAnnulationdedecisions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRectificationdunom", mappedBy="user")
     */
    private $evRectificationdunoms;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvIndemniteforfaitaire", mappedBy="user")
     */
    private $evIndemniteforfaitaires;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvSuspensiondesalaire", mappedBy="user")
     */
    private $evSuspensiondesalaires;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDisponibilite", mappedBy="user")
     */
    private $evDisponibilites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvConge", mappedBy="user")
     */
    private $evConges;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcFormationuser", mappedBy="user")
     */
    private $ecFormationusers;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enfonction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enfonctionData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcExperienceAilleurs", mappedBy="user")
     */
    private $ecExperienceAilleurs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GSitesEnerg\Intervention", mappedBy="intervenant")
     */
    private $interventions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Message", mappedBy="receivers")
     */
    private $receiversMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messagesvus", mappedBy="user")
     */
    private $messagesvuses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messagesupprime", mappedBy="user")
     */
    private $messagesupprimes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AffairesSociales\Enfant", mappedBy="user")
     */
    private $enfants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AffairesSociales\EpouxSe", mappedBy="user")
     */
    private $epouxSes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AffairesSociales\SituationFamiliale", mappedBy="user", cascade={"persist", "remove"})
     */
    private $situationFamiliale;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AffairesSociales\SituationDuCarnet", mappedBy="user")
     */
    private $carnets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AffairesSociales\SituationDesRepos", mappedBy="user")
     */
    private $reposMaladies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Personnel\DemandeConge", mappedBy="agent")
     */
    private $demandeConges;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Personnel\MsgAccueilConge", mappedBy="auteur", orphanRemoval=true)
     */
    private $msgAccueilConges;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Personnel\AgentDroitConge", mappedBy="agent", orphanRemoval=true)
     */
    private $agentDroitConges;


    public function __construct()
    {
        parent::__construct();
        $this->actionUsers = new ArrayCollection();
        $this->posteUsers = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->evNominations = new ArrayCollection();
        $this->evAvancementnormals = new ArrayCollection();
        $this->evPromotioninternes = new ArrayCollection();
        $this->evReclassements = new ArrayCollection();
        $this->evAvertissements = new ArrayCollection();
        $this->evLicenciements = new ArrayCollection();
        $this->evDecess = new ArrayCollection();
        $this->evRetraites = new ArrayCollection();
        $this->evMiseapieds = new ArrayCollection();
        $this->evDemissions = new ArrayCollection();
        $this->evSuspensions = new ArrayCollection();
        $this->evReintegrations = new ArrayCollection();
        $this->evReprisedefonctions = new ArrayCollection();
        $this->evLevedesuspensions = new ArrayCollection();
        $this->evInterims = new ArrayCollection();
        $this->evAnnulationdedecisions = new ArrayCollection();
        $this->evRectificationdunoms = new ArrayCollection();
        $this->evIndemniteforfaitaires = new ArrayCollection();
        $this->evSuspensiondesalaires = new ArrayCollection();
        $this->evDisponibilites = new ArrayCollection();
        $this->evConges = new ArrayCollection();
        $this->evTitularisations = new ArrayCollection();
        $this->evAffectations = new ArrayCollection();
        $this->evDemisedefonctions = new ArrayCollection();
        $this->evStages = new ArrayCollection();
        $this->evRecrutements = new ArrayCollection();
        $this->evRenouvellementdecontrats = new ArrayCollection();
        $this->ecFormations = new ArrayCollection();
        $this->ecFormationusers = new ArrayCollection();
        $this->ecExperienceAilleurs = new ArrayCollection();
        $this->interventions = new ArrayCollection();
        $this->receiversMessages = new ArrayCollection();
        $this->messagesvuses = new ArrayCollection();
        $this->messagesupprimes = new ArrayCollection();
        $this->enfants = new ArrayCollection();
        $this->epouxSes = new ArrayCollection();
        $this->carnets = new ArrayCollection();
        $this->reposMaladies = new ArrayCollection();
        $this->demandeConges = new ArrayCollection();
        $this->msgAccueilConges = new ArrayCollection();
        $this->agentDroitConges = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->matricule.'  '.$this->getNomPrenom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->nom.''.$this->prenom);
    }

    /**
     * @param \Datetime $lastActivityAt
     */
    public function setLastActivityAt($lastActivityAt)
    {
        $this->lastActivityAt = $lastActivityAt;
    }

    /**
     * @return \Datetime
     */
    public function getLastActivityAt()
    {
        return $this->lastActivityAt;
    }

    /**
     * @return Bool Whether the user is active or not
     */
    public function isActiveNow()
    {
        // Delay during wich the user will be considered as still active
        $delay = new \DateTime('2 minutes ago');

        return ( $this->getLastActivityAt() > $delay );
    }

    public function getLoginCount(): ?int
    {
        return $this->loginCount;
    }

    public function setLoginCount(int $loginCount): self
    {
        $this->loginCount = $loginCount;

        return $this;
    }

    public function getFirstLogin(): ?\DateTimeInterface
    {
        return $this->firstLogin;
    }

    public function setFirstLogin(?\DateTimeInterface $firstLogin): self
    {
        $this->firstLogin = $firstLogin;

        return $this;
    }

    public function getMatricule(): ?int
    {
        return $this->matricule;
    }

    public function setMatricule(int $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getIndiceGroupe(): ?string
    {
        return $this->indiceGroupe;
    }

    public function setIndiceGroupe(string $indiceGroupe): self
    {
        $this->indiceGroupe = $indiceGroupe;

        return $this;
    }

    public function getIndiceCategorie(): ?string
    {
        return $this->indiceCategorie;
    }

    public function setIndiceCategorie(string $indiceCategorie): self
    {
        $this->indiceCategorie = $indiceCategorie;

        return $this;
    }

    public function getIndiceEchelon(): ?string
    {
        return $this->indiceEchelon;
    }

    public function setIndiceEchelon(string $indiceEchelon): self
    {
        $this->indiceEchelon = $indiceEchelon;

        return $this;
    }

    public function getDateNaissanceAt(): ?\DateTimeInterface
    {
        return $this->dateNaissanceAt;
    }

    public function setDateNaissanceAt(\DateTimeInterface $dateNaissanceAt): self
    {
        $this->dateNaissanceAt = $dateNaissanceAt;

        return $this;
    }

    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if (null !== $image) {
            // if 'updatedAt' is not defined in your entity, use another property
            //$this->updatedAt = new \DateTime();
            $this->updatedAt = new \DateTimeImmutable();
            //$this->setUpdatedAt(new \DateTime());
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function getStatut(): ?Statut
    {
        return $this->statut;
    }

    public function setStatut(?Statut $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getNomPrenom(): ?string
    {
        return $this->nom . ' ' . $this->prenom;
    }

    /**
     * @return Collection|ActionUser[]
     */
    public function getActionUsers(): Collection
    {
        return $this->actionUsers;
    }

    public function addActionUser(ActionUser $actionUser): self
    {
        if (!$this->actionUsers->contains($actionUser)) {
            $this->actionUsers[] = $actionUser;
            $actionUser->setUser($this);
        }

        return $this;
    }

    public function removeActionUser(ActionUser $actionUser): self
    {
        if ($this->actionUsers->contains($actionUser)) {
            $this->actionUsers->removeElement($actionUser);
            // set the owning side to null (unless already changed)
            if ($actionUser->getUser() === $this) {
                $actionUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PosteUser[]
     */
    public function getPosteUsers(): Collection
    {
        return $this->posteUsers;
    }

    public function addPosteUser(PosteUser $posteUser): self
    {
        if (!$this->posteUsers->contains($posteUser)) {
            $this->posteUsers[] = $posteUser;
            $posteUser->setUser($this);
        }

        return $this;
    }

    public function removePosteUser(PosteUser $posteUser): self
    {
        if ($this->posteUsers->contains($posteUser)) {
            $this->posteUsers->removeElement($posteUser);
            // set the owning side to null (unless already changed)
            if ($posteUser->getUser() === $this) {
                $posteUser->setUser(null);
            }
        }

        return $this;
    }

    public function getEnligne(): ?bool
    {
        return $this->enligne;
    }

    public function setEnligne(bool $enligne): self
    {
        $this->enligne = $enligne;

        return $this;
    }

    /**
     * Permet de savoir si le statut de l'agent est en ligne ou pas
     *
     * @return bool
     */
    public function isEnligne(): bool {
        return $this->enligne;
    }

    public function getUserOptionVisibilite(): ?UserOptionVisibilite
    {
        return $this->userOptionVisibilite;
    }

    public function setUserOptionVisibilite(UserOptionVisibilite $userOptionVisibilite): self
    {
        $this->userOptionVisibilite = $userOptionVisibilite;

        // set the owning side of the relation if necessary
        if ($this !== $userOptionVisibilite->getUser()) {
            $userOptionVisibilite->setUser($this);
        }

        return $this;
    }

    /*
     * CALCULER L'AGE
     */

    public function getAge()
    {
        $from = new \DateTime($this->dateNaissanceAt->format(('Y-m-d')));
        $to   = new \DateTime('today');
        return $from->diff($to)->y;
    }

    /**
     * Calcul RETRAITE
     */

    public function getRetraite()
    {
        $indiceGroupe = $this->indiceGroupe;
        $indiceCategorie = $this->indiceCategorie;
        //$indiceEchelon = $this->indiceEchelon;
        $ageDeprtRetraite = '';
        if($indiceGroupe == 'I'){
            if($indiceCategorie == '1' || $indiceCategorie=='2'){
                $ageDeprtRetraite = "58 ans";
            }
            if($indiceCategorie == '3' || $indiceCategorie=='4'){
                $ageDeprtRetraite = "60 ans";
            }
        }elseif($indiceGroupe == 'II'){
            $ageDeprtRetraite = "62 ans";
        }elseif($indiceGroupe == 'III'){
            $ageDeprtRetraite = "65 ans";
        }  else {
            $ageDeprtRetraite = "Classement de base éronné";
        }
        return $ageDeprtRetraite;
    }

    public function getLieuDeNaissance(): ?string
    {
        return $this->lieuDeNaissance;
    }

    public function setLieuDeNaissance(?string $lieuDeNaissance): self
    {
        $this->lieuDeNaissance = $lieuDeNaissance;

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUserSender($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getUserSender() === $this) {
                $message->setUserSender(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|EvTitularisation[]
     */
    public function getEvTitularisations(): Collection
    {
        return $this->evTitularisations;
    }

    public function addEvTitularisation(EvTitularisation $evTitularisation): self
    {
        if (!$this->evTitularisations->contains($evTitularisation)) {
            $this->evTitularisations[] = $evTitularisation;
            $evTitularisation->setUser($this);
        }

        return $this;
    }

    public function removeEvTitularisation(EvTitularisation $evTitularisation): self
    {
        if ($this->evTitularisations->contains($evTitularisation)) {
            $this->evTitularisations->removeElement($evTitularisation);
            // set the owning side to null (unless already changed)
            if ($evTitularisation->getUser() === $this) {
                $evTitularisation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvNomination[]
     */
    public function getEvNominations(): Collection
    {
        return $this->evNominations;
    }

    public function addEvNomination(EvNomination $evNomination): self
    {
        if (!$this->evNominations->contains($evNomination)) {
            $this->evNominations[] = $evNomination;
            $evNomination->setUser($this);
        }

        return $this;
    }

    public function removeEvNomination(EvNomination $evNomination): self
    {
        if ($this->evNominations->contains($evNomination)) {
            $this->evNominations->removeElement($evNomination);
            // set the owning side to null (unless already changed)
            if ($evNomination->getUser() === $this) {
                $evNomination->setUser(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection|EvAvancementnormal[]
     */
    public function getEvAvancementnormals(): Collection
    {
        return $this->evAvancementnormals;
    }

    public function addEvAvancementnormal(EvAvancementnormal $evAvancementnormal): self
    {
        if (!$this->evAvancementnormals->contains($evAvancementnormal)) {
            $this->evAvancementnormals[] = $evAvancementnormal;
            $evAvancementnormal->setUser($this);
        }

        return $this;
    }

    public function removeEvAvancementnormal(EvAvancementnormal $evAvancementnormal): self
    {
        if ($this->evAvancementnormals->contains($evAvancementnormal)) {
            $this->evAvancementnormals->removeElement($evAvancementnormal);
            // set the owning side to null (unless already changed)
            if ($evAvancementnormal->getUser() === $this) {
                $evAvancementnormal->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvPromotioninterne[]
     */
    public function getEvPromotioninternes(): Collection
    {
        return $this->evPromotioninternes;
    }

    public function addEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if (!$this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes[] = $evPromotioninterne;
            $evPromotioninterne->setUser($this);
        }

        return $this;
    }

    public function removeEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if ($this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes->removeElement($evPromotioninterne);
            // set the owning side to null (unless already changed)
            if ($evPromotioninterne->getUser() === $this) {
                $evPromotioninterne->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReclassement[]
     */
    public function getEvReclassements(): Collection
    {
        return $this->evReclassements;
    }

    public function addEvReclassement(EvReclassement $evReclassement): self
    {
        if (!$this->evReclassements->contains($evReclassement)) {
            $this->evReclassements[] = $evReclassement;
            $evReclassement->setUser($this);
        }

        return $this;
    }

    public function removeEvReclassement(EvReclassement $evReclassement): self
    {
        if ($this->evReclassements->contains($evReclassement)) {
            $this->evReclassements->removeElement($evReclassement);
            // set the owning side to null (unless already changed)
            if ($evReclassement->getUser() === $this) {
                $evReclassement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvAvertissement[]
     */
    public function getEvAvertissements(): Collection
    {
        return $this->evAvertissements;
    }

    public function addEvAvertissement(EvAvertissement $evAvertissement): self
    {
        if (!$this->evAvertissements->contains($evAvertissement)) {
            $this->evAvertissements[] = $evAvertissement;
            $evAvertissement->setUser($this);
        }

        return $this;
    }

    public function removeEvAvertissement(EvAvertissement $evAvertissement): self
    {
        if ($this->evAvertissements->contains($evAvertissement)) {
            $this->evAvertissements->removeElement($evAvertissement);
            // set the owning side to null (unless already changed)
            if ($evAvertissement->getUser() === $this) {
                $evAvertissement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvLicenciement[]
     */
    public function getEvLicenciements(): Collection
    {
        return $this->evLicenciements;
    }

    public function addEvLicenciement(EvLicenciement $evLicenciement): self
    {
        if (!$this->evLicenciements->contains($evLicenciement)) {
            $this->evLicenciements[] = $evLicenciement;
            $evLicenciement->setUser($this);
        }

        return $this;
    }

    public function removeEvLicenciement(EvLicenciement $evLicenciement): self
    {
        if ($this->evLicenciements->contains($evLicenciement)) {
            $this->evLicenciements->removeElement($evLicenciement);
            // set the owning side to null (unless already changed)
            if ($evLicenciement->getUser() === $this) {
                $evLicenciement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDeces[]
     */
    public function getEvDecess(): Collection
    {
        return $this->evDecess;
    }

    public function addEvDeces(EvDeces $evDeces): self
    {
        if (!$this->evDecess->contains($evDeces)) {
            $this->evDecess[] = $evDeces;
            $evDeces->setUser($this);
        }

        return $this;
    }

    public function removeEvDeces(EvDeces $evDeces): self
    {
        if ($this->evDecess->contains($evDeces)) {
            $this->evDecess->removeElement($evDeces);
            // set the owning side to null (unless already changed)
            if ($evDeces->getUser() === $this) {
                $evDeces->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRetraite[]
     */
    public function getEvRetraites(): Collection
    {
        return $this->evRetraites;
    }

    public function addEvRetraite(EvRetraite $evRetraite): self
    {
        if (!$this->evRetraites->contains($evRetraite)) {
            $this->evRetraites[] = $evRetraite;
            $evRetraite->setUser($this);
        }

        return $this;
    }

    public function removeEvRetraite(EvRetraite $evRetraite): self
    {
        if ($this->evRetraites->contains($evRetraite)) {
            $this->evRetraites->removeElement($evRetraite);
            // set the owning side to null (unless already changed)
            if ($evRetraite->getUser() === $this) {
                $evRetraite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvMiseapied[]
     */
    public function getEvMiseapieds(): Collection
    {
        return $this->evMiseapieds;
    }

    public function addEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if (!$this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds[] = $evMiseapied;
            $evMiseapied->setUser($this);
        }

        return $this;
    }

    public function removeEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if ($this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds->removeElement($evMiseapied);
            // set the owning side to null (unless already changed)
            if ($evMiseapied->getUser() === $this) {
                $evMiseapied->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemission[]
     */
    public function getEvDemissions(): Collection
    {
        return $this->evDemissions;
    }

    public function addEvDemission(EvDemission $evDemission): self
    {
        if (!$this->evDemissions->contains($evDemission)) {
            $this->evDemissions[] = $evDemission;
            $evDemission->setUser($this);
        }

        return $this;
    }

    public function removeEvDemission(EvDemission $evDemission): self
    {
        if ($this->evDemissions->contains($evDemission)) {
            $this->evDemissions->removeElement($evDemission);
            // set the owning side to null (unless already changed)
            if ($evDemission->getUser() === $this) {
                $evDemission->setUser(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection|EvSuspension[]
     */
    public function getEvSuspensions(): Collection
    {
        return $this->evSuspensions;
    }

    public function addEvSuspension(EvSuspension $evSuspension): self
    {
        if (!$this->evSuspensions->contains($evSuspension)) {
            $this->evSuspensions[] = $evSuspension;
            $evSuspension->setUser($this);
        }

        return $this;
    }

    public function removeEvSuspension(EvSuspension $evSuspension): self
    {
        if ($this->evSuspensions->contains($evSuspension)) {
            $this->evSuspensions->removeElement($evSuspension);
            // set the owning side to null (unless already changed)
            if ($evSuspension->getUser() === $this) {
                $evSuspension->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReintegration[]
     */
    public function getEvReintegrations(): Collection
    {
        return $this->evReintegrations;
    }

    public function addEvReintegration(EvReintegration $evReintegration): self
    {
        if (!$this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations[] = $evReintegration;
            $evReintegration->setUser($this);
        }

        return $this;
    }

    public function removeEvReintegration(EvReintegration $evReintegration): self
    {
        if ($this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations->removeElement($evReintegration);
            // set the owning side to null (unless already changed)
            if ($evReintegration->getUser() === $this) {
                $evReintegration->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReprisedefonction[]
     */
    public function getEvReprisedefonctions(): Collection
    {
        return $this->evReprisedefonctions;
    }

    public function addEvReprisedefonction(EvReprisedefonction $evReprisedefonction): self
    {
        if (!$this->evReprisedefonctions->contains($evReprisedefonction)) {
            $this->evReprisedefonctions[] = $evReprisedefonction;
            $evReprisedefonction->setUser($this);
        }

        return $this;
    }

    public function removeEvReprisedefonction(EvReprisedefonction $evReprisedefonction): self
    {
        if ($this->evReprisedefonctions->contains($evReprisedefonction)) {
            $this->evReprisedefonctions->removeElement($evReprisedefonction);
            // set the owning side to null (unless already changed)
            if ($evReprisedefonction->getUser() === $this) {
                $evReprisedefonction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvLevedesuspension[]
     */
    public function getEvLevedesuspensions(): Collection
    {
        return $this->evLevedesuspensions;
    }

    public function addEvLevedesuspension(EvLevedesuspension $evLevedesuspension): self
    {
        if (!$this->evLevedesuspensions->contains($evLevedesuspension)) {
            $this->evLevedesuspensions[] = $evLevedesuspension;
            $evLevedesuspension->setUser($this);
        }

        return $this;
    }

    public function removeEvLevedesuspension(EvLevedesuspension $evLevedesuspension): self
    {
        if ($this->evLevedesuspensions->contains($evLevedesuspension)) {
            $this->evLevedesuspensions->removeElement($evLevedesuspension);
            // set the owning side to null (unless already changed)
            if ($evLevedesuspension->getUser() === $this) {
                $evLevedesuspension->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvInterim[]
     */
    public function getEvInterims(): Collection
    {
        return $this->evInterims;
    }

    public function addEvInterim(EvInterim $evInterim): self
    {
        if (!$this->evInterims->contains($evInterim)) {
            $this->evInterims[] = $evInterim;
            $evInterim->setUser($this);
        }

        return $this;
    }

    public function removeEvInterim(EvInterim $evInterim): self
    {
        if ($this->evInterims->contains($evInterim)) {
            $this->evInterims->removeElement($evInterim);
            // set the owning side to null (unless already changed)
            if ($evInterim->getUser() === $this) {
                $evInterim->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvAnnulationdedecision[]
     */
    public function getEvAnnulationdedecisions(): Collection
    {
        return $this->evAnnulationdedecisions;
    }

    public function addEvAnnulationdedecision(EvAnnulationdedecision $evAnnulationdedecision): self
    {
        if (!$this->evAnnulationdedecisions->contains($evAnnulationdedecision)) {
            $this->evAnnulationdedecisions[] = $evAnnulationdedecision;
            $evAnnulationdedecision->setUser($this);
        }

        return $this;
    }

    public function removeEvAnnulationdedecision(EvAnnulationdedecision $evAnnulationdedecision): self
    {
        if ($this->evAnnulationdedecisions->contains($evAnnulationdedecision)) {
            $this->evAnnulationdedecisions->removeElement($evAnnulationdedecision);
            // set the owning side to null (unless already changed)
            if ($evAnnulationdedecision->getUser() === $this) {
                $evAnnulationdedecision->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRectificationdunom[]
     */
    public function getEvRectificationdunoms(): Collection
    {
        return $this->evRectificationdunoms;
    }

    public function addEvRectificationdunom(EvRectificationdunom $evRectificationdunom): self
    {
        if (!$this->evRectificationdunoms->contains($evRectificationdunom)) {
            $this->evRectificationdunoms[] = $evRectificationdunom;
            $evRectificationdunom->setUser($this);
        }

        return $this;
    }

    public function removeEvRectificationdunom(EvRectificationdunom $evRectificationdunom): self
    {
        if ($this->evRectificationdunoms->contains($evRectificationdunom)) {
            $this->evRectificationdunoms->removeElement($evRectificationdunom);
            // set the owning side to null (unless already changed)
            if ($evRectificationdunom->getUser() === $this) {
                $evRectificationdunom->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvIndemniteforfaitaire[]
     */
    public function getEvIndemniteforfaitaires(): Collection
    {
        return $this->evIndemniteforfaitaires;
    }

    public function addEvIndemniteforfaitaire(EvIndemniteforfaitaire $evIndemniteforfaitaire): self
    {
        if (!$this->evIndemniteforfaitaires->contains($evIndemniteforfaitaire)) {
            $this->evIndemniteforfaitaires[] = $evIndemniteforfaitaire;
            $evIndemniteforfaitaire->setUser($this);
        }

        return $this;
    }

    public function removeEvIndemniteforfaitaire(EvIndemniteforfaitaire $evIndemniteforfaitaire): self
    {
        if ($this->evIndemniteforfaitaires->contains($evIndemniteforfaitaire)) {
            $this->evIndemniteforfaitaires->removeElement($evIndemniteforfaitaire);
            // set the owning side to null (unless already changed)
            if ($evIndemniteforfaitaire->getUser() === $this) {
                $evIndemniteforfaitaire->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvSuspensiondesalaire[]
     */
    public function getEvSuspensiondesalaires(): Collection
    {
        return $this->evSuspensiondesalaires;
    }

    public function addEvSuspensiondesalaire(EvSuspensiondesalaire $evSuspensiondesalaire): self
    {
        if (!$this->evSuspensiondesalaires->contains($evSuspensiondesalaire)) {
            $this->evSuspensiondesalaires[] = $evSuspensiondesalaire;
            $evSuspensiondesalaire->setUser($this);
        }

        return $this;
    }

    public function removeEvSuspensiondesalaire(EvSuspensiondesalaire $evSuspensiondesalaire): self
    {
        if ($this->evSuspensiondesalaires->contains($evSuspensiondesalaire)) {
            $this->evSuspensiondesalaires->removeElement($evSuspensiondesalaire);
            // set the owning side to null (unless already changed)
            if ($evSuspensiondesalaire->getUser() === $this) {
                $evSuspensiondesalaire->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDisponibilite[]
     */
    public function getEvDisponibilites(): Collection
    {
        return $this->evDisponibilites;
    }

    public function addEvDisponibilite(EvDisponibilite $evDisponibilite): self
    {
        if (!$this->evDisponibilites->contains($evDisponibilite)) {
            $this->evDisponibilites[] = $evDisponibilite;
            $evDisponibilite->setUser($this);
        }

        return $this;
    }

    public function removeEvDisponibilite(EvDisponibilite $evDisponibilite): self
    {
        if ($this->evDisponibilites->contains($evDisponibilite)) {
            $this->evDisponibilites->removeElement($evDisponibilite);
            // set the owning side to null (unless already changed)
            if ($evDisponibilite->getUser() === $this) {
                $evDisponibilite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvConge[]
     */
    public function getEvConges(): Collection
    {
        return $this->evConges;
    }

    public function addEvConge(EvConge $evConge): self
    {
        if (!$this->evConges->contains($evConge)) {
            $this->evConges[] = $evConge;
            $evConge->setUser($this);
        }

        return $this;
    }

    public function removeEvConge(EvConge $evConge): self
    {
        if ($this->evConges->contains($evConge)) {
            $this->evConges->removeElement($evConge);
            // set the owning side to null (unless already changed)
            if ($evConge->getUser() === $this) {
                $evConge->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvAffectation[]
     */
    public function getEvAffectations(): Collection
    {
        return $this->evAffectations;
    }

    public function addEvAffectation(EvAffectation $evAffectation): self
    {
        if (!$this->evAffectations->contains($evAffectation)) {
            $this->evAffectations[] = $evAffectation;
            $evAffectation->setUser($this);
        }

        return $this;
    }

    public function removeEvAffectation(EvAffectation $evAffectation): self
    {
        if ($this->evAffectations->contains($evAffectation)) {
            $this->evAffectations->removeElement($evAffectation);
            // set the owning side to null (unless already changed)
            if ($evAffectation->getUser() === $this) {
                $evAffectation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemisedefonction[]
     */
    public function getEvDemisedefonctions(): Collection
    {
        return $this->evDemisedefonctions;
    }

    public function addEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if (!$this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions[] = $evDemisedefonction;
            $evDemisedefonction->setUser($this);
        }

        return $this;
    }

    public function removeEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if ($this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions->removeElement($evDemisedefonction);
            // set the owning side to null (unless already changed)
            if ($evDemisedefonction->getUser() === $this) {
                $evDemisedefonction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvStage[]
     */
    public function getEvStages(): Collection
    {
        return $this->evStages;
    }

    public function addEvStage(EvStage $evStage): self
    {
        if (!$this->evStages->contains($evStage)) {
            $this->evStages[] = $evStage;
            $evStage->setUser($this);
        }

        return $this;
    }

    public function removeEvStage(EvStage $evStage): self
    {
        if ($this->evStages->contains($evStage)) {
            $this->evStages->removeElement($evStage);
            // set the owning side to null (unless already changed)
            if ($evStage->getUser() === $this) {
                $evStage->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRecrutement[]
     */
    public function getEvRecrutements(): Collection
    {
        return $this->evRecrutements;
    }

    public function addEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if (!$this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements[] = $evRecrutement;
            $evRecrutement->setUser($this);
        }

        return $this;
    }

    public function removeEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if ($this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements->removeElement($evRecrutement);
            // set the owning side to null (unless already changed)
            if ($evRecrutement->getUser() === $this) {
                $evRecrutement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRenouvellementdecontrat[]
     */
    public function getEvRenouvellementdecontrats(): Collection
    {
        return $this->evRenouvellementdecontrats;
    }

    public function addEvRenouvellementdecontrat(EvRenouvellementdecontrat $evRenouvellementdecontrat): self
    {
        if (!$this->evRenouvellementdecontrats->contains($evRenouvellementdecontrat)) {
            $this->evRenouvellementdecontrats[] = $evRenouvellementdecontrat;
            $evRenouvellementdecontrat->setUser($this);
        }

        return $this;
    }

    public function removeEvRenouvellementdecontrat(EvRenouvellementdecontrat $evRenouvellementdecontrat): self
    {
        if ($this->evRenouvellementdecontrats->contains($evRenouvellementdecontrat)) {
            $this->evRenouvellementdecontrats->removeElement($evRenouvellementdecontrat);
            // set the owning side to null (unless already changed)
            if ($evRenouvellementdecontrat->getUser() === $this) {
                $evRenouvellementdecontrat->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EcFormationuser[]
     */
    public function getEcFormationusers(): Collection
    {
        return $this->ecFormationusers;
    }

    public function addEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if (!$this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers[] = $ecFormationuser;
            $ecFormationuser->setUser($this);
        }

        return $this;
    }

    public function removeEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if ($this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers->removeElement($ecFormationuser);
            // set the owning side to null (unless already changed)
            if ($ecFormationuser->getUser() === $this) {
                $ecFormationuser->setUser(null);
            }
        }

        return $this;
    }

    public function getEnfonction(): ?bool
    {
        return $this->enfonction;
    }

    public function setEnfonction(bool $enfonction): self
    {
        $this->enfonction = $enfonction;

        return $this;
    }

    public function getEnfonctionData(): ?string
    {
        return $this->enfonctionData;
    }

    public function setEnfonctionData(?string $enfonctionData): self
    {
        $this->enfonctionData = $enfonctionData;

        return $this;
    }

    /**
     * @return Collection|EcExperienceAilleurs[]
     */
    public function getEcExperienceAilleurs(): Collection
    {
        return $this->ecExperienceAilleurs;
    }

    public function addEcExperienceAilleur(EcExperienceAilleurs $ecExperienceAilleur): self
    {
        if (!$this->ecExperienceAilleurs->contains($ecExperienceAilleur)) {
            $this->ecExperienceAilleurs[] = $ecExperienceAilleur;
            $ecExperienceAilleur->setUser($this);
        }

        return $this;
    }

    public function removeEcExperienceAilleur(EcExperienceAilleurs $ecExperienceAilleur): self
    {
        if ($this->ecExperienceAilleurs->contains($ecExperienceAilleur)) {
            $this->ecExperienceAilleurs->removeElement($ecExperienceAilleur);
            // set the owning side to null (unless already changed)
            if ($ecExperienceAilleur->getUser() === $this) {
                $ecExperienceAilleur->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Intervention[]
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions[] = $intervention;
            $intervention->addIntervenant($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->contains($intervention)) {
            $this->interventions->removeElement($intervention);
            $intervention->removeIntervenant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getReceiversMessages(): Collection
    {
        return $this->receiversMessages;
    }

    public function addReceiversMessage(Message $receiversMessage): self
    {
        if (!$this->receiversMessages->contains($receiversMessage)) {
            $this->receiversMessages[] = $receiversMessage;
            $receiversMessage->addReceiver($this);
        }

        return $this;
    }

    public function removeReceiversMessage(Message $receiversMessage): self
    {
        if ($this->receiversMessages->contains($receiversMessage)) {
            $this->receiversMessages->removeElement($receiversMessage);
            $receiversMessage->removeReceiver($this);
        }

        return $this;
    }

    /**
     * @return Collection|Messagesvus[]
     */
    public function getMessagesvuses(): Collection
    {
        return $this->messagesvuses;
    }

    public function addMessagesvus(Messagesvus $messagesvus): self
    {
        if (!$this->messagesvuses->contains($messagesvus)) {
            $this->messagesvuses[] = $messagesvus;
            $messagesvus->setUser($this);
        }

        return $this;
    }

    public function removeMessagesvus(Messagesvus $messagesvus): self
    {
        if ($this->messagesvuses->contains($messagesvus)) {
            $this->messagesvuses->removeElement($messagesvus);
            // set the owning side to null (unless already changed)
            if ($messagesvus->getUser() === $this) {
                $messagesvus->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Messagesupprime[]
     */
    public function getMessagesupprimes(): Collection
    {
        return $this->messagesupprimes;
    }

    public function addMessagesupprime(Messagesupprime $messagesupprime): self
    {
        if (!$this->messagesupprimes->contains($messagesupprime)) {
            $this->messagesupprimes[] = $messagesupprime;
            $messagesupprime->setUser($this);
        }

        return $this;
    }

    public function removeMessagesupprime(Messagesupprime $messagesupprime): self
    {
        if ($this->messagesupprimes->contains($messagesupprime)) {
            $this->messagesupprimes->removeElement($messagesupprime);
            // set the owning side to null (unless already changed)
            if ($messagesupprime->getUser() === $this) {
                $messagesupprime->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Enfant[]
     */
    public function getEnfants(): Collection
    {
        return $this->enfants;
    }

    public function addEnfant(Enfant $enfant): self
    {
        if (!$this->enfants->contains($enfant)) {
            $this->enfants[] = $enfant;
            $enfant->setUser($this);
        }

        return $this;
    }

    public function removeEnfant(Enfant $enfant): self
    {
        if ($this->enfants->contains($enfant)) {
            $this->enfants->removeElement($enfant);
            // set the owning side to null (unless already changed)
            if ($enfant->getUser() === $this) {
                $enfant->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EpouxSe[]
     */
    public function getEpouxSes(): Collection
    {
        return $this->epouxSes;
    }

    public function addEpouxSe(EpouxSe $epouxSe): self
    {
        if (!$this->epouxSes->contains($epouxSe)) {
            $this->epouxSes[] = $epouxSe;
            $epouxSe->setUser($this);
        }

        return $this;
    }

    public function removeEpouxSe(EpouxSe $epouxSe): self
    {
        if ($this->epouxSes->contains($epouxSe)) {
            $this->epouxSes->removeElement($epouxSe);
            // set the owning side to null (unless already changed)
            if ($epouxSe->getUser() === $this) {
                $epouxSe->setUser(null);
            }
        }

        return $this;
    }

    public function getSituationFamiliale(): ?SituationFamiliale
    {
        return $this->situationFamiliale;
    }

    public function setSituationFamiliale(?SituationFamiliale $situationFamiliale): self
    {
        $this->situationFamiliale = $situationFamiliale;

        return $this;
    }

    /**
     * @return Collection|SituationDuCarnet[]
     */
    public function getCarnets(): Collection
    {
        return $this->carnets;
    }

    public function addCarnet(SituationDuCarnet $carnet): self
    {
        if (!$this->carnets->contains($carnet)) {
            $this->carnets[] = $carnet;
            $carnet->setUser($this);
        }

        return $this;
    }

    public function removeCarnet(SituationDuCarnet $carnet): self
    {
        if ($this->carnets->contains($carnet)) {
            $this->carnets->removeElement($carnet);
            // set the owning side to null (unless already changed)
            if ($carnet->getUser() === $this) {
                $carnet->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SituationDesRepos[]
     */
    public function getReposMaladies(): Collection
    {
        return $this->reposMaladies;
    }

    public function addReposMalady(SituationDesRepos $reposMalady): self
    {
        if (!$this->reposMaladies->contains($reposMalady)) {
            $this->reposMaladies[] = $reposMalady;
            $reposMalady->setUser($this);
        }

        return $this;
    }

    public function removeReposMalady(SituationDesRepos $reposMalady): self
    {
        if ($this->reposMaladies->contains($reposMalady)) {
            $this->reposMaladies->removeElement($reposMalady);
            // set the owning side to null (unless already changed)
            if ($reposMalady->getUser() === $this) {
                $reposMalady->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DemandeConge[]
     */
    public function getDemandeConges(): Collection
    {
        return $this->demandeConges;
    }

    public function addDemandeConge(DemandeConge $demandeConge): self
    {
        if (!$this->demandeConges->contains($demandeConge)) {
            $this->demandeConges[] = $demandeConge;
            $demandeConge->setAgent($this);
        }

        return $this;
    }

    public function removeDemandeConge(DemandeConge $demandeConge): self
    {
        if ($this->demandeConges->contains($demandeConge)) {
            $this->demandeConges->removeElement($demandeConge);
            // set the owning side to null (unless already changed)
            if ($demandeConge->getAgent() === $this) {
                $demandeConge->setAgent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MsgAccueilConge[]
     */
    public function getMsgAccueilConges(): Collection
    {
        return $this->msgAccueilConges;
    }

    public function addMsgAccueilConge(MsgAccueilConge $msgAccueilConge): self
    {
        if (!$this->msgAccueilConges->contains($msgAccueilConge)) {
            $this->msgAccueilConges[] = $msgAccueilConge;
            $msgAccueilConge->setAuteur($this);
        }

        return $this;
    }

    public function removeMsgAccueilConge(MsgAccueilConge $msgAccueilConge): self
    {
        if ($this->msgAccueilConges->contains($msgAccueilConge)) {
            $this->msgAccueilConges->removeElement($msgAccueilConge);
            // set the owning side to null (unless already changed)
            if ($msgAccueilConge->getAuteur() === $this) {
                $msgAccueilConge->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AgentDroitConge[]
     */
    public function getAgentDroitConges(): Collection
    {
        return $this->agentDroitConges;
    }

    public function addAgentDroitConge(AgentDroitConge $agentDroitConge): self
    {
        if (!$this->agentDroitConges->contains($agentDroitConge)) {
            $this->agentDroitConges[] = $agentDroitConge;
            $agentDroitConge->setAgent($this);
        }

        return $this;
    }

    public function removeAgentDroitConge(AgentDroitConge $agentDroitConge): self
    {
        if ($this->agentDroitConges->contains($agentDroitConge)) {
            $this->agentDroitConges->removeElement($agentDroitConge);
            // set the owning side to null (unless already changed)
            if ($agentDroitConge->getAgent() === $this) {
                $agentDroitConge->setAgent(null);
            }
        }

        return $this;
    }




}