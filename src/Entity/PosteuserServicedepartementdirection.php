<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosteuserServicedepartementdirectionRepository")
 */
class PosteuserServicedepartementdirection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PosteUser", inversedBy="posteuserServicedepartementdirections")
     */
    private $posteuser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDepartementdirection", inversedBy="posteuserServicedepartementdirections")
     */
    private $servicedepartementdirection;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosteuser(): ?PosteUser
    {
        return $this->posteuser;
    }

    public function setPosteuser(?PosteUser $posteuser): self
    {
        $this->posteuser = $posteuser;

        return $this;
    }

    public function getServicedepartementdirection(): ?ServiceDepartementdirection
    {
        return $this->servicedepartementdirection;
    }

    public function setServicedepartementdirection(?ServiceDepartementdirection $servicedepartementdirection): self
    {
        $this->servicedepartementdirection = $servicedepartementdirection;

        return $this;
    }
}
