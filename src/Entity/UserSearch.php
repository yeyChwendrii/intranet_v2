<?php

namespace App\Entity;

class UserSearch
{
    /**
     * @var string
     */
    private $matricule;

    /**
     * @return string
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * @param string $matricule
     * @return UserSearch
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;
        return $this;
    }
}