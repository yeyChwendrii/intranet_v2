<?php

namespace App\Entity;

use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvRenouvellementdecontrat;
use App\Entity\Evenements\EvStage;
use App\Entity\Evenements\EvTitularisation;
use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvReintegration;
use App\Entity\Evenements\EvDeces;
use App\Entity\Evenements\EvMiseapied;
use App\Entity\Evenements\EvDemission;
use App\Entity\Evenements\EvAffectation;
use App\Entity\Evenements\EvDemisedefonction;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServiceDepartementdirectionRepository")
 * @UniqueEntity(
 *     fields={"service", "departementdirection"},
 *     message="Le service choisi est déjà attribué à ce département"
 * )
 */
class ServiceDepartementdirection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Service", inversedBy="serviceDepartementdirections")
     */
    private $service;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartementDirection", inversedBy="serviceDepartementdirections")
     */
    private $departementdirection;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agence", inversedBy="serviceDepartementdirections")
     */
    private $agence;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserServicedepartementdirection", mappedBy="servicedepartementdirection")
     */
    private $posteuserServicedepartementdirections;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tel;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $fonctionnel = true;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvTitularisation", mappedBy="serviceDepartement")
     */
    private $evTitularisations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvNomination", mappedBy="serviceDepartement")
     */
    private $evNominations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvPromotioninterne", mappedBy="serviceDepartement")
     */
    private $evPromotioninternes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDeces", mappedBy="serviceDepartement")
     */
    private $evDecess;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvMiseapied", mappedBy="serviceDepartement")
     */
    private $evMiseapieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemission", mappedBy="serviceDepartement")
     */
    private $evDemissions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReintegration", mappedBy="serviceDepartement")
     */
    private $evReintegrations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAffectation", mappedBy="serviceDepartement")
     */
    private $evAffectations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemisedefonction", mappedBy="serviceDepartement")
     */
    private $evDemisedefonctions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvStage", mappedBy="serviceDepartement")
     */
    private $evStages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRecrutement", mappedBy="serviceDepartement")
     */
    private $evRecrutements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRenouvellementdecontrat", mappedBy="serviceDepartement")
     */
    private $evRenouvellementdecontrats;

    public function __construct()
    {
        $this->posteuserServicedepartementdirections = new ArrayCollection();
        $this->evTitularisations = new ArrayCollection();
        $this->evNominations = new ArrayCollection();
        $this->evPromotioninternes = new ArrayCollection();
        $this->evReintegrations = new ArrayCollection();
        $this->evDecess = new ArrayCollection();
        $this->evMiseapieds = new ArrayCollection();
        $this->evDemissions = new ArrayCollection();
        $this->evAffectations = new ArrayCollection();
        $this->evDemisedefonctions = new ArrayCollection();
        $this->evStages = new ArrayCollection();
        $this->evRecrutements = new ArrayCollection();
        $this->evRenouvellementdecontrats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getService(). ' (' .$this->getDepartementdirection() . ') ';
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getDepartementdirection(): ?DepartementDirection
    {
        return $this->departementdirection;
    }

    public function setDepartementdirection(?DepartementDirection $departementdirection): self
    {
        $this->departementdirection = $departementdirection;

        return $this;
    }

    public function getAgence(): ?Agence
    {
        return $this->agence;
    }

    public function setAgence(?Agence $agence): self
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * @return Collection|PosteuserServicedepartementdirection[]
     */
    public function getPosteuserServicedepartementdirections(): Collection
    {
        return $this->posteuserServicedepartementdirections;
    }

    public function addPosteuserServicedepartementdirection(PosteuserServicedepartementdirection $posteuserServicedepartementdirection): self
    {
        if (!$this->posteuserServicedepartementdirections->contains($posteuserServicedepartementdirection)) {
            $this->posteuserServicedepartementdirections[] = $posteuserServicedepartementdirection;
            $posteuserServicedepartementdirection->setServicedepartementdirection($this);
        }

        return $this;
    }

    public function removePosteuserServicedepartementdirection(PosteuserServicedepartementdirection $posteuserServicedepartementdirection): self
    {
        if ($this->posteuserServicedepartementdirections->contains($posteuserServicedepartementdirection)) {
            $this->posteuserServicedepartementdirections->removeElement($posteuserServicedepartementdirection);
            // set the owning side to null (unless already changed)
            if ($posteuserServicedepartementdirection->getServicedepartementdirection() === $this) {
                $posteuserServicedepartementdirection->setServicedepartementdirection(null);
            }
        }

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|EvTitularisation[]
     */
    public function getEvTitularisations(): Collection
    {
        return $this->evTitularisations;
    }

    public function addEvTitularisation(EvTitularisation $evTitularisation): self
    {
        if (!$this->evTitularisations->contains($evTitularisation)) {
            $this->evTitularisations[] = $evTitularisation;
            $evTitularisation->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvTitularisation(EvTitularisation $evTitularisation): self
    {
        if ($this->evTitularisations->contains($evTitularisation)) {
            $this->evTitularisations->removeElement($evTitularisation);
            // set the owning side to null (unless already changed)
            if ($evTitularisation->getServiceDepartement() === $this) {
                $evTitularisation->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvNomination[]
     */
    public function getEvNominations(): Collection
    {
        return $this->evNominations;
    }

    public function addEvNomination(EvNomination $evNomination): self
    {
        if (!$this->evNominations->contains($evNomination)) {
            $this->evNominations[] = $evNomination;
            $evNomination->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvNomination(EvNomination $evNomination): self
    {
        if ($this->evNominations->contains($evNomination)) {
            $this->evNominations->removeElement($evNomination);
            // set the owning side to null (unless already changed)
            if ($evNomination->getServiceDepartement() === $this) {
                $evNomination->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvPromotioninterne[]
     */
    public function getEvPromotioninternes(): Collection
    {
        return $this->evPromotioninternes;
    }

    public function addEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if (!$this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes[] = $evPromotioninterne;
            $evPromotioninterne->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if ($this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes->removeElement($evPromotioninterne);
            // set the owning side to null (unless already changed)
            if ($evPromotioninterne->getServiceDepartement() === $this) {
                $evPromotioninterne->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReintegration[]
     */
    public function getEvReintegrations(): Collection
    {
        return $this->evReintegrations;
    }

    public function addEvReintegration(EvReintegration $evReintegration): self
    {
        if (!$this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations[] = $evReintegration;
            $evReintegration->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvReintegration(EvReintegration $evReintegration): self
    {
        if ($this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations->removeElement($evReintegration);
            // set the owning side to null (unless already changed)
            if ($evReintegration->getServiceDepartement() === $this) {
                $evReintegration->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDeces[]
     */
    public function getEvDecess(): Collection
    {
        return $this->evDecess;
    }

    public function addEvDeces(EvDeces $evDeces): self
    {
        if (!$this->evDecess->contains($evDeces)) {
            $this->evDecess[] = $evDeces;
            $evDeces->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvDeces(EvDeces $evDeces): self
    {
        if ($this->evDecess->contains($evDeces)) {
            $this->evDecess->removeElement($evDeces);
            // set the owning side to null (unless already changed)
            if ($evDeces->getServiceDepartement() === $this) {
                $evDeces->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvMiseapied[]
     */
    public function getEvMiseapieds(): Collection
    {
        return $this->evMiseapieds;
    }

    public function addEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if (!$this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds[] = $evMiseapied;
            $evMiseapied->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if ($this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds->removeElement($evMiseapied);
            // set the owning side to null (unless already changed)
            if ($evMiseapied->getServiceDepartement() === $this) {
                $evMiseapied->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemission[]
     */
    public function getEvDemissions(): Collection
    {
        return $this->evDemissions;
    }

    public function addEvDemission(EvDemission $evDemission): self
    {
        if (!$this->evDemissions->contains($evDemission)) {
            $this->evDemissions[] = $evDemission;
            $evDemission->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvDemission(EvDemission $evDemission): self
    {
        if ($this->evDemissions->contains($evDemission)) {
            $this->evDemissions->removeElement($evDemission);
            // set the owning side to null (unless already changed)
            if ($evDemission->getServiceDepartement() === $this) {
                $evDemission->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvAffectation[]
     */
    public function getEvAffectations(): Collection
    {
        return $this->evAffectations;
    }

    public function addEvAffectation(EvAffectation $evAffectation): self
    {
        if (!$this->evAffectations->contains($evAffectation)) {
            $this->evAffectations[] = $evAffectation;
            $evAffectation->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvAffectation(EvAffectation $evAffectation): self
    {
        if ($this->evAffectations->contains($evAffectation)) {
            $this->evAffectations->removeElement($evAffectation);
            // set the owning side to null (unless already changed)
            if ($evAffectation->getServiceDepartement() === $this) {
                $evAffectation->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemisedefonction[]
     */
    public function getEvDemisedefonctions(): Collection
    {
        return $this->evDemisedefonctions;
    }

    public function addEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if (!$this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions[] = $evDemisedefonction;
            $evDemisedefonction->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if ($this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions->removeElement($evDemisedefonction);
            // set the owning side to null (unless already changed)
            if ($evDemisedefonction->getServiceDepartement() === $this) {
                $evDemisedefonction->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvStage[]
     */
    public function getEvStages(): Collection
    {
        return $this->evStages;
    }

    public function addEvStage(EvStage $evStage): self
    {
        if (!$this->evStages->contains($evStage)) {
            $this->evStages[] = $evStage;
            $evStage->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvStage(EvStage $evStage): self
    {
        if ($this->evStages->contains($evStage)) {
            $this->evStages->removeElement($evStage);
            // set the owning side to null (unless already changed)
            if ($evStage->getServiceDepartement() === $this) {
                $evStage->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRecrutement[]
     */
    public function getEvRecrutements(): Collection
    {
        return $this->evRecrutements;
    }

    public function addEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if (!$this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements[] = $evRecrutement;
            $evRecrutement->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if ($this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements->removeElement($evRecrutement);
            // set the owning side to null (unless already changed)
            if ($evRecrutement->getServiceDepartement() === $this) {
                $evRecrutement->setServiceDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRenouvellementdecontrat[]
     */
    public function getEvRenouvellementdecontrats(): Collection
    {
        return $this->evRenouvellementdecontrats;
    }

    public function addEvRenouvellementdecontrat(EvRenouvellementdecontrat $evRenouvellementdecontrat): self
    {
        if (!$this->evRenouvellementdecontrats->contains($evRenouvellementdecontrat)) {
            $this->evRenouvellementdecontrats[] = $evRenouvellementdecontrat;
            $evRenouvellementdecontrat->setServiceDepartement($this);
        }

        return $this;
    }

    public function removeEvRenouvellementdecontrat(EvRenouvellementdecontrat $evRenouvellementdecontrat): self
    {
        if ($this->evRenouvellementdecontrats->contains($evRenouvellementdecontrat)) {
            $this->evRenouvellementdecontrats->removeElement($evRenouvellementdecontrat);
            // set the owning side to null (unless already changed)
            if ($evRenouvellementdecontrat->getServiceDepartement() === $this) {
                $evRenouvellementdecontrat->setServiceDepartement(null);
            }
        }

        return $this;
    }

    public function getFonctionnel(): ?bool
    {
        return $this->fonctionnel;
    }

    public function setFonctionnel(?bool $fonctionnel): self
    {
        $this->fonctionnel = $fonctionnel;

        return $this;
    }

}
