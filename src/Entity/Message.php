<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sujet;

    /**
     * @ORM\Column(type="text")
     */
    private $contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnvoiAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeMessage", inversedBy="messages")
     */
    private $typeMessage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messages")
     */
    private $userSender;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="receiversMessages")
     */
    private $receivers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messagesvus", mappedBy="message")
     */
    private $messagesvuses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messagesupprime", mappedBy="message")
     */
    private $messagesupprimes;

    public function __construct()
    {
        $this->receivers = new ArrayCollection();
        $this->messagesvuses = new ArrayCollection();
        $this->messagesupprimes = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->sujet);
    }

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(string $sujet): self
    {
        $this->sujet = $sujet;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getDateEnvoiAt(): ?\DateTimeInterface
    {
        return $this->dateEnvoiAt;
    }

    public function setDateEnvoiAt(\DateTimeInterface $dateEnvoiAt): self
    {
        $this->dateEnvoiAt = $dateEnvoiAt;

        return $this;
    }

    public function getTypeMessage(): ?TypeMessage
    {
        return $this->typeMessage;
    }

    public function setTypeMessage(?TypeMessage $typeMessage): self
    {
        $this->typeMessage = $typeMessage;

        return $this;
    }

    public function getUserSender(): ?User
    {
        return $this->userSender;
    }

    public function setUserSender(?User $userSender): self
    {
        $this->userSender = $userSender;

        return $this;
    }

    /*
     * CALCULE DU TEMPS
     */

    function getTime_elapsed_string( $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($this->dateEnvoiAt->format(('Y-m-d')));
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'année',
            'm' => 'mois',
            'w' => 'semaine',
            'd' => 'jr',
            'h' => 'hr',
            'i' => 'min',
            's' => 'scd',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ' : 'à l\'instant';
    }

    /**
     * @return Collection|User[]
     */
    public function getReceivers(): Collection
    {
        return $this->receivers;
    }

    public function addReceiver(User $receiver): self
    {
        if (!$this->receivers->contains($receiver)) {
            $this->receivers[] = $receiver;
        }

        return $this;
    }

    public function removeReceiver(User $receiver): self
    {
        if ($this->receivers->contains($receiver)) {
            $this->receivers->removeElement($receiver);
        }

        return $this;
    }

    /**
     * @return Collection|Messagesvus[]
     */
    public function getMessagesvuses(): Collection
    {
        return $this->messagesvuses;
    }

    public function addMessagesvus(Messagesvus $messagesvus): self
    {
        if (!$this->messagesvuses->contains($messagesvus)) {
            $this->messagesvuses[] = $messagesvus;
            $messagesvus->setMessage($this);
        }

        return $this;
    }

    public function removeMessagesvus(Messagesvus $messagesvus): self
    {
        if ($this->messagesvuses->contains($messagesvus)) {
            $this->messagesvuses->removeElement($messagesvus);
            // set the owning side to null (unless already changed)
            if ($messagesvus->getMessage() === $this) {
                $messagesvus->setMessage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Messagesupprime[]
     */
    public function getMessagesupprimes(): Collection
    {
        return $this->messagesupprimes;
    }

    public function addMessagesupprime(Messagesupprime $messagesupprime): self
    {
        if (!$this->messagesupprimes->contains($messagesupprime)) {
            $this->messagesupprimes[] = $messagesupprime;
            $messagesupprime->setMessage($this);
        }

        return $this;
    }

    public function removeMessagesupprime(Messagesupprime $messagesupprime): self
    {
        if ($this->messagesupprimes->contains($messagesupprime)) {
            $this->messagesupprimes->removeElement($messagesupprime);
            // set the owning side to null (unless already changed)
            if ($messagesupprime->getMessage() === $this) {
                $messagesupprime->setMessage(null);
            }
        }

        return $this;
    }
}
