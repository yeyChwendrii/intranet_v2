<?php

namespace App\Entity;

use App\Entity\Evenements\EvAffectation;
use App\Entity\Evenements\EvDemisedefonction;
use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvDeces;
use App\Entity\Evenements\EvMiseapied;
use App\Entity\Evenements\EvDemission;
use App\Entity\Evenements\EvSuspension;
use App\Entity\Evenements\EvReintegration;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartementDirectionRepository")
 * @UniqueEntity(
 *     fields={"departement", "direction"},
 *     message="Le departement choisi est déjà attribué à cette direction"
 * )
 */
class DepartementDirection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Departement", inversedBy="departementDirections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $departement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direction", inversedBy="departementDirections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $direction;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotNull
     */
    private $tel;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $fonctionnel = true;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServiceDepartementdirection", mappedBy="departementdirection")
     */
    private $serviceDepartementdirections;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agence", inversedBy="departementDirections")
     */
    private $agence;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserDepartementdirection", mappedBy="departementdirection")
     */
    private $posteuserDepartementdirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAffectation", mappedBy="departement")
     */
    private $evAffectations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemisedefonction", mappedBy="departement")
     */
    private $evDemisedefonctions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvNomination", mappedBy="departement")
     */
    private $evNominations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvPromotioninterne", mappedBy="departement")
     */
    private $evPromotioninternes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDeces", mappedBy="departement")
     */
    private $evDecess;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvMiseapied", mappedBy="departement")
     */
    private $evMiseapieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemission", mappedBy="departement")
     */
    private $evDemissions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvSuspension", mappedBy="departement")
     */
    private $evSuspensions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReintegration", mappedBy="departement")
     */
    private $evReintegrations;

    public function __construct()
    {
        $this->serviceDepartementdirections = new ArrayCollection();
        $this->posteuserDepartementdirections = new ArrayCollection();
        $this->evAffectations = new ArrayCollection();
        $this->evDemisedefonctions = new ArrayCollection();
        $this->evNominations = new ArrayCollection();
        $this->evPromotioninternes = new ArrayCollection();
        $this->evDecess = new ArrayCollection();
        $this->evMiseapieds = new ArrayCollection();
        $this->evDemissions = new ArrayCollection();
        $this->evSuspensions = new ArrayCollection();
        $this->evReintegrations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDepartement().' - '.$this->getDirection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getDirection(): ?Direction
    {
        return $this->direction;
    }

    public function setDirection(?Direction $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|ServiceDepartementdirection[]
     */
    public function getServiceDepartementdirections(): Collection
    {
        return $this->serviceDepartementdirections;
    }

    public function addServiceDepartementdirection(ServiceDepartementdirection $serviceDepartementdirection): self
    {
        if (!$this->serviceDepartementdirections->contains($serviceDepartementdirection)) {
            $this->serviceDepartementdirections[] = $serviceDepartementdirection;
            $serviceDepartementdirection->setDepartementdirection($this);
        }

        return $this;
    }

    public function removeServiceDepartementdirection(ServiceDepartementdirection $serviceDepartementdirection): self
    {
        if ($this->serviceDepartementdirections->contains($serviceDepartementdirection)) {
            $this->serviceDepartementdirections->removeElement($serviceDepartementdirection);
            // set the owning side to null (unless already changed)
            if ($serviceDepartementdirection->getDepartementdirection() === $this) {
                $serviceDepartementdirection->setDepartementdirection(null);
            }
        }

        return $this;
    }

    public function getAgence(): ?Agence
    {
        return $this->agence;
    }

    public function setAgence(?Agence $agence): self
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * @return Collection|PosteuserDepartementdirection[]
     */
    public function getPosteuserDepartementdirections(): Collection
    {
        return $this->posteuserDepartementdirections;
    }

    public function addPosteuserDepartementdirection(PosteuserDepartementdirection $posteuserDepartementdirection): self
    {
        if (!$this->posteuserDepartementdirections->contains($posteuserDepartementdirection)) {
            $this->posteuserDepartementdirections[] = $posteuserDepartementdirection;
            $posteuserDepartementdirection->setDepartementdirection($this);
        }

        return $this;
    }

    public function removePosteuserDepartementdirection(PosteuserDepartementdirection $posteuserDepartementdirection): self
    {
        if ($this->posteuserDepartementdirections->contains($posteuserDepartementdirection)) {
            $this->posteuserDepartementdirections->removeElement($posteuserDepartementdirection);
            // set the owning side to null (unless already changed)
            if ($posteuserDepartementdirection->getDepartementdirection() === $this) {
                $posteuserDepartementdirection->setDepartementdirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvAffectation[]
     */
    public function getEvAffectations(): Collection
    {
        return $this->evAffectations;
    }

    public function addEvAffectation(EvAffectation $evAffectation): self
    {
        if (!$this->evAffectations->contains($evAffectation)) {
            $this->evAffectations[] = $evAffectation;
            $evAffectation->setDepartement($this);
        }

        return $this;
    }

    public function removeEvAffectation(EvAffectation $evAffectation): self
    {
        if ($this->evAffectations->contains($evAffectation)) {
            $this->evAffectations->removeElement($evAffectation);
            // set the owning side to null (unless already changed)
            if ($evAffectation->getDepartement() === $this) {
                $evAffectation->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemisedefonction[]
     */
    public function getEvDemisedefonctions(): Collection
    {
        return $this->evDemisedefonctions;
    }

    public function addEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if (!$this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions[] = $evDemisedefonction;
            $evDemisedefonction->setDepartement($this);
        }

        return $this;
    }

    public function removeEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if ($this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions->removeElement($evDemisedefonction);
            // set the owning side to null (unless already changed)
            if ($evDemisedefonction->getDepartement() === $this) {
                $evDemisedefonction->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvNomination[]
     */
    public function getEvNominations(): Collection
    {
        return $this->evNominations;
    }

    public function addEvNomination(EvNomination $evNomination): self
    {
        if (!$this->evNominations->contains($evNomination)) {
            $this->evNominations[] = $evNomination;
            $evNomination->setDepartement($this);
        }

        return $this;
    }

    public function removeEvNomination(EvNomination $evNomination): self
    {
        if ($this->evNominations->contains($evNomination)) {
            $this->evNominations->removeElement($evNomination);
            // set the owning side to null (unless already changed)
            if ($evNomination->getDepartement() === $this) {
                $evNomination->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvPromotioninterne[]
     */
    public function getEvPromotioninternes(): Collection
    {
        return $this->evPromotioninternes;
    }

    public function addEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if (!$this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes[] = $evPromotioninterne;
            $evPromotioninterne->setDepartement($this);
        }

        return $this;
    }

    public function removeEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if ($this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes->removeElement($evPromotioninterne);
            // set the owning side to null (unless already changed)
            if ($evPromotioninterne->getDepartement() === $this) {
                $evPromotioninterne->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDeces[]
     */
    public function getEvDecess(): Collection
    {
        return $this->evDecess;
    }

    public function addEvDeces(EvDeces $evDeces): self
    {
        if (!$this->evDecess->contains($evDeces)) {
            $this->evDecess[] = $evDeces;
            $evDeces->setDepartement($this);
        }

        return $this;
    }

    public function removeEvDeces(EvDeces $evDeces): self
    {
        if ($this->evDecess->contains($evDeces)) {
            $this->evDecess->removeElement($evDeces);
            // set the owning side to null (unless already changed)
            if ($evDeces->getDepartement() === $this) {
                $evDeces->setDepartement(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|EvMiseapied[]
     */
    public function getEvMiseapieds(): Collection
    {
        return $this->evMiseapieds;
    }

    public function addEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if (!$this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds[] = $evMiseapied;
            $evMiseapied->setDepartement($this);
        }

        return $this;
    }

    public function removeEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if ($this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds->removeElement($evMiseapied);
            // set the owning side to null (unless already changed)
            if ($evMiseapied->getDepartement() === $this) {
                $evMiseapied->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemission[]
     */
    public function getEvDemissions(): Collection
    {
        return $this->evDemissions;
    }

    public function addEvDemission(EvDemission $evDemission): self
    {
        if (!$this->evDemissions->contains($evDemission)) {
            $this->evDemissions[] = $evDemission;
            $evDemission->setDepartement($this);
        }

        return $this;
    }

    public function removeEvDemission(EvDemission $evDemission): self
    {
        if ($this->evDemissions->contains($evDemission)) {
            $this->evDemissions->removeElement($evDemission);
            // set the owning side to null (unless already changed)
            if ($evDemission->getDepartement() === $this) {
                $evDemission->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvSuspension[]
     */
    public function getEvSuspensions(): Collection
    {
        return $this->evSuspensions;
    }

    public function addEvSuspension(EvSuspension $evSuspension): self
    {
        if (!$this->evSuspensions->contains($evSuspension)) {
            $this->evSuspensions[] = $evSuspension;
            $evSuspension->setDepartement($this);
        }

        return $this;
    }

    public function removeEvSuspension(EvSuspension $evSuspension): self
    {
        if ($this->evSuspensions->contains($evSuspension)) {
            $this->evSuspensions->removeElement($evSuspension);
            // set the owning side to null (unless already changed)
            if ($evSuspension->getDepartement() === $this) {
                $evSuspension->setDepartement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReintegration[]
     */
    public function getEvReintegrations(): Collection
    {
        return $this->evReintegrations;
    }

    public function addEvReintegration(EvReintegration $evReintegration): self
    {
        if (!$this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations[] = $evReintegration;
            $evReintegration->setDepartement($this);
        }

        return $this;
    }

    public function removeEvReintegration(EvReintegration $evReintegration): self
    {
        if ($this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations->removeElement($evReintegration);
            // set the owning side to null (unless already changed)
            if ($evReintegration->getDepartement() === $this) {
                $evReintegration->setDepartement(null);
            }
        }

        return $this;
    }

    public function getFonctionnel(): ?bool
    {
        return $this->fonctionnel;
    }

    public function setFonctionnel(bool $fonctionnel): self
    {
        $this->fonctionnel = $fonctionnel;

        return $this;
    }
}
