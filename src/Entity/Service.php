<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 * @UniqueEntity(fields={"designation"}, message="Le service que vous avez saisi est déjà enregistré !")
 */
class Service
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServiceDirection", mappedBy="service")
     */
    private $serviceDirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServiceDepartementdirection", mappedBy="service")
     */
    private $serviceDepartementdirections;


    public function __construct()
    {
        $this->serviceDirections = new ArrayCollection();
        $this->serviceDepartements = new ArrayCollection();
        $this->serviceDepartementdirections = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|ServiceDirection[]
     */
    public function getServiceDirections(): Collection
    {
        return $this->serviceDirections;
    }

    public function addServiceDirection(ServiceDirection $serviceDirection): self
    {
        if (!$this->serviceDirections->contains($serviceDirection)) {
            $this->serviceDirections[] = $serviceDirection;
            $serviceDirection->setService($this);
        }

        return $this;
    }

    public function removeServiceDirection(ServiceDirection $serviceDirection): self
    {
        if ($this->serviceDirections->contains($serviceDirection)) {
            $this->serviceDirections->removeElement($serviceDirection);
            // set the owning side to null (unless already changed)
            if ($serviceDirection->getService() === $this) {
                $serviceDirection->setService(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ServiceDepartementdirection[]
     */
    public function getServiceDepartementdirections(): Collection
    {
        return $this->serviceDepartementdirections;
    }

    public function addServiceDepartementdirection(ServiceDepartementdirection $serviceDepartementdirection): self
    {
        if (!$this->serviceDepartementdirections->contains($serviceDepartementdirection)) {
            $this->serviceDepartementdirections[] = $serviceDepartementdirection;
            $serviceDepartementdirection->setService($this);
        }

        return $this;
    }

    public function removeServiceDepartementdirection(ServiceDepartementdirection $serviceDepartementdirection): self
    {
        if ($this->serviceDepartementdirections->contains($serviceDepartementdirection)) {
            $this->serviceDepartementdirections->removeElement($serviceDepartementdirection);
            // set the owning side to null (unless already changed)
            if ($serviceDepartementdirection->getService() === $this) {
                $serviceDepartementdirection->setService(null);
            }
        }

        return $this;
    }

}
