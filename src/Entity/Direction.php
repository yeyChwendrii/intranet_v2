<?php

namespace App\Entity;

use App\Entity\Evenements\EvAffectation;
use App\Entity\Evenements\EvDemisedefonction;
use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvDeces;
use App\Entity\Evenements\EvMiseapied;
use App\Entity\Evenements\EvDemission;
use App\Entity\Evenements\EvSuspension;
use App\Entity\Evenements\EvReintegration;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DirectionRepository")
 * @UniqueEntity(fields={"designation"}, message="La direction que vous avez saisi est déjà enregistrée !")
 */
class Direction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotNull
     */
    private $tel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServiceDirection", mappedBy="direction")
     */
    private $serviceDirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DepartementDirection", mappedBy="direction")
     */
    private $departementDirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserDirection", mappedBy="direction")
     */
    private $posteuserDirections;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agence", inversedBy="directions")
     */
    private $agence;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAffectation", mappedBy="direction")
     */
    private $evAffectations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemisedefonction", mappedBy="direction")
     */
    private $evDemisedefonctions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvNomination", mappedBy="direction")
     */
    private $evNominations;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvPromotioninterne", mappedBy="direction")
     */
    private $evPromotioninternes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDeces", mappedBy="direction")
     */
    private $evDecess;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvMiseapied", mappedBy="direction")
     */
    private $evMiseapieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvDemission", mappedBy="direction")
     */
    private $evDemissions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvSuspension", mappedBy="direction")
     */
    private $evSuspensions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReintegration", mappedBy="direction")
     */
    private $evReintegrations;

    public function __construct()
    {
        $this->serviceDirections = new ArrayCollection();
        $this->departementDirections = new ArrayCollection();
        $this->posteuserDirections = new ArrayCollection();
        $this->evAffectations = new ArrayCollection();
        $this->evDemisedefonctions = new ArrayCollection();
        $this->evNominations = new ArrayCollection();
        $this->evPromotioninternes = new ArrayCollection();
        $this->evDecess = new ArrayCollection();
        $this->evMiseapieds = new ArrayCollection();
        $this->evDemissions = new ArrayCollection();
        $this->evSuspensions = new ArrayCollection();
        $this->evReintegrations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|ServiceDirection[]
     */
    public function getServiceDirections(): Collection
    {
        return $this->serviceDirections;
    }

    public function addServiceDirection(ServiceDirection $serviceDirection): self
    {
        if (!$this->serviceDirections->contains($serviceDirection)) {
            $this->serviceDirections[] = $serviceDirection;
            $serviceDirection->setDirection($this);
        }

        return $this;
    }

    public function removeServiceDirection(ServiceDirection $serviceDirection): self
    {
        if ($this->serviceDirections->contains($serviceDirection)) {
            $this->serviceDirections->removeElement($serviceDirection);
            // set the owning side to null (unless already changed)
            if ($serviceDirection->getDirection() === $this) {
                $serviceDirection->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DepartementDirection[]
     */
    public function getDepartementDirections(): Collection
    {
        return $this->departementDirections;
    }

    public function addDepartementDirection(DepartementDirection $departementDirection): self
    {
        if (!$this->departementDirections->contains($departementDirection)) {
            $this->departementDirections[] = $departementDirection;
            $departementDirection->setDirection($this);
        }

        return $this;
    }

    public function removeDepartementDirection(DepartementDirection $departementDirection): self
    {
        if ($this->departementDirections->contains($departementDirection)) {
            $this->departementDirections->removeElement($departementDirection);
            // set the owning side to null (unless already changed)
            if ($departementDirection->getDirection() === $this) {
                $departementDirection->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PosteuserDirection[]
     */
    public function getPosteuserDirections(): Collection
    {
        return $this->posteuserDirections;
    }

    public function addPosteuserDirection(PosteuserDirection $posteuserDirection): self
    {
        if (!$this->posteuserDirections->contains($posteuserDirection)) {
            $this->posteuserDirections[] = $posteuserDirection;
            $posteuserDirection->setDirection($this);
        }

        return $this;
    }

    public function removePosteuserDirection(PosteuserDirection $posteuserDirection): self
    {
        if ($this->posteuserDirections->contains($posteuserDirection)) {
            $this->posteuserDirections->removeElement($posteuserDirection);
            // set the owning side to null (unless already changed)
            if ($posteuserDirection->getDirection() === $this) {
                $posteuserDirection->setDirection(null);
            }
        }

        return $this;
    }

    public function getAgence(): ?Agence
    {
        return $this->agence;
    }

    public function setAgence(?Agence $agence): self
    {
        $this->agence = $agence;

        return $this;
    }

    /**
     * @return Collection|EvAffectation[]
     */
    public function getEvAffectations(): Collection
    {
        return $this->evAffectations;
    }

    public function addEvAffectation(EvAffectation $evAffectation): self
    {
        if (!$this->evAffectations->contains($evAffectation)) {
            $this->evAffectations[] = $evAffectation;
            $evAffectation->setDirection($this);
        }

        return $this;
    }

    public function removeEvAffectation(EvAffectation $evAffectation): self
    {
        if ($this->evAffectations->contains($evAffectation)) {
            $this->evAffectations->removeElement($evAffectation);
            // set the owning side to null (unless already changed)
            if ($evAffectation->getDirection() === $this) {
                $evAffectation->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemisedefonction[]
     */
    public function getEvDemisedefonctions(): Collection
    {
        return $this->evDemisedefonctions;
    }

    public function addEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if (!$this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions[] = $evDemisedefonction;
            $evDemisedefonction->setDirection($this);
        }

        return $this;
    }

    public function removeEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if ($this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions->removeElement($evDemisedefonction);
            // set the owning side to null (unless already changed)
            if ($evDemisedefonction->getDirection() === $this) {
                $evDemisedefonction->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvNomination[]
     */
    public function getEvNominations(): Collection
    {
        return $this->evNominations;
    }

    public function addEvNomination(EvNomination $evNomination): self
    {
        if (!$this->evNominations->contains($evNomination)) {
            $this->evNominations[] = $evNomination;
            $evNomination->setDirection($this);
        }

        return $this;
    }

    public function removeEvNomination(EvNomination $evNomination): self
    {
        if ($this->evNominations->contains($evNomination)) {
            $this->evNominations->removeElement($evNomination);
            // set the owning side to null (unless already changed)
            if ($evNomination->getDirection() === $this) {
                $evNomination->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvPromotioninterne[]
     */
    public function getEvPromotioninternes(): Collection
    {
        return $this->evPromotioninternes;
    }

    public function addEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if (!$this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes[] = $evPromotioninterne;
            $evPromotioninterne->setDirection($this);
        }

        return $this;
    }

    public function removeEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if ($this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes->removeElement($evPromotioninterne);
            // set the owning side to null (unless already changed)
            if ($evPromotioninterne->getDirection() === $this) {
                $evPromotioninterne->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDeces[]
     */
    public function getEvDecess(): Collection
    {
        return $this->evDecess;
    }

    public function addEvDeces(EvDeces $evDeces): self
    {
        if (!$this->evDecess->contains($evDeces)) {
            $this->evDecess[] = $evDeces;
            $evDeces->setDirection($this);
        }

        return $this;
    }

    public function removeEvDeces(EvDeces $evDeces): self
    {
        if ($this->evDecess->contains($evDeces)) {
            $this->evDecess->removeElement($evDeces);
            // set the owning side to null (unless already changed)
            if ($evDeces->getDirection() === $this) {
                $evDeces->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvMiseapied[]
     */
    public function getEvMiseapieds(): Collection
    {
        return $this->evMiseapieds;
    }

    public function addEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if (!$this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds[] = $evMiseapied;
            $evMiseapied->setDirection($this);
        }

        return $this;
    }

    public function removeEvMiseapied(EvMiseapied $evMiseapied): self
    {
        if ($this->evMiseapieds->contains($evMiseapied)) {
            $this->evMiseapieds->removeElement($evMiseapied);
            // set the owning side to null (unless already changed)
            if ($evMiseapied->getDirection() === $this) {
                $evMiseapied->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvDemission[]
     */
    public function getEvDemissions(): Collection
    {
        return $this->evDemissions;
    }

    public function addEvDemission(EvDemission $evDemission): self
    {
        if (!$this->evDemissions->contains($evDemission)) {
            $this->evDemissions[] = $evDemission;
            $evDemission->setDirection($this);
        }

        return $this;
    }

    public function removeEvDemission(EvDemission $evDemission): self
    {
        if ($this->evDemissions->contains($evDemission)) {
            $this->evDemissions->removeElement($evDemission);
            // set the owning side to null (unless already changed)
            if ($evDemission->getDirection() === $this) {
                $evDemission->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvSuspension[]
     */
    public function getEvSuspensions(): Collection
    {
        return $this->evSuspensions;
    }

    public function addEvSuspension(EvSuspension $evSuspension): self
    {
        if (!$this->evSuspensions->contains($evSuspension)) {
            $this->evSuspensions[] = $evSuspension;
            $evSuspension->setDirection($this);
        }

        return $this;
    }

    public function removeEvSuspension(EvSuspension $evSuspension): self
    {
        if ($this->evSuspensions->contains($evSuspension)) {
            $this->evSuspensions->removeElement($evSuspension);
            // set the owning side to null (unless already changed)
            if ($evSuspension->getDirection() === $this) {
                $evSuspension->setDirection(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReintegration[]
     */
    public function getEvReintegrations(): Collection
    {
        return $this->evReintegrations;
    }

    public function addEvReintegration(EvReintegration $evReintegration): self
    {
        if (!$this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations[] = $evReintegration;
            $evReintegration->setDirection($this);
        }

        return $this;
    }

    public function removeEvReintegration(EvReintegration $evReintegration): self
    {
        if ($this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations->removeElement($evReintegration);
            // set the owning side to null (unless already changed)
            if ($evReintegration->getDirection() === $this) {
                $evReintegration->setDirection(null);
            }
        }

        return $this;
    }
}
