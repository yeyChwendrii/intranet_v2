<?php

namespace App\Entity\BanqueDeSolidarite;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BanqueDeSolidarite\CotisationRepository")
 */
class Cotisation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cotisationappliquee;

    /**
     * @ORM\Column(type="date")
     */
    private $cotisationAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BanqueDeSolidarite\Parametredecotisation", inversedBy="cotisations")
     */
    private $parametredecotisation;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCotisationappliquee(): ?int
    {
        return $this->cotisationappliquee;
    }

    public function setCotisationappliquee(int $cotisationappliquee): self
    {
        $this->cotisationappliquee = $cotisationappliquee;

        return $this;
    }


    public function getCotisationAt(): ?\DateTimeInterface
    {
        return $this->cotisationAt;
    }

    public function setCotisationAt(\DateTimeInterface $cotisationAt): self
    {
        $this->cotisationAt = $cotisationAt;

        return $this;
    }

    public function getParametredecotisation(): ?Parametredecotisation
    {
        return $this->parametredecotisation;
    }

    public function setParametredecotisation(?Parametredecotisation $parametredecotisation): self
    {
        $this->parametredecotisation = $parametredecotisation;

        return $this;
    }
}
