<?php

namespace App\Entity\BanqueDeSolidarite;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BanqueDeSolidarite\ParametredecotisationRepository")
 */
class Parametredecotisation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $salairedebase;

    /**
     * @ORM\Column(type="integer")
     */
    private $cotisationpardefaut;

    /**
     * @ORM\Column(type="integer")
     */
    private $cotisationappliquee;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BanqueDeSolidarite\Cotisation", mappedBy="parametredecotisation")
     */
    private $cotisations;

    /**
     * @ORM\Column(type="integer")
     */
    private $concerne;

    public function __construct()
    {
        $this->cotisations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalairedebase(): ?int
    {
        return $this->salairedebase;
    }

    public function setSalairedebase(?int $salairedebase): self
    {
        $this->salairedebase = $salairedebase;

        return $this;
    }

    public function getCotisationpardefaut(): ?int
    {
        return $this->cotisationpardefaut;
    }

    public function setCotisationpardefaut(int $cotisationpardefaut): self
    {
        $this->cotisationpardefaut = $cotisationpardefaut;

        return $this;
    }

    public function getCotisationappliquee(): ?int
    {
        return $this->cotisationappliquee;
    }

    public function setCotisationappliquee(int $cotisationappliquee): self
    {
        $this->cotisationappliquee = $cotisationappliquee;

        return $this;
    }

    /**
     * @return Collection|Cotisation[]
     */
    public function getCotisations(): Collection
    {
        return $this->cotisations;
    }

    public function addCotisation(Cotisation $cotisation): self
    {
        if (!$this->cotisations->contains($cotisation)) {
            $this->cotisations[] = $cotisation;
            $cotisation->setParametredecotisation($this);
        }

        return $this;
    }

    public function removeCotisation(Cotisation $cotisation): self
    {
        if ($this->cotisations->contains($cotisation)) {
            $this->cotisations->removeElement($cotisation);
            // set the owning side to null (unless already changed)
            if ($cotisation->getParametredecotisation() === $this) {
                $cotisation->setParametredecotisation(null);
            }
        }

        return $this;
    }

    public function getConcerne(): ?int
    {
        return $this->concerne;
    }

    public function setConcerne(int $concerne): self
    {
        $this->concerne = $concerne;

        return $this;
    }
}
