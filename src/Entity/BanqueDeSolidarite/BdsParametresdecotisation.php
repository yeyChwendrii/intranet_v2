<?php

namespace App\Entity\BanqueDeSolidarite;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BanqueDeSolidarite\BdsParametresdecotisationRepository")
 */
class BdsParametresdecotisation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $salairedebase;

    /**
     * @ORM\Column(type="integer")
     */
    private $cotisationpardefaut;

    /**
     * @ORM\Column(type="integer")
     */
    private $cotisationappliquee;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BanqueDeSolidarite\BdsCotisation", mappedBy="BdsParametresdecotisation")
     */
    private $bdsCotisations;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    public function __construct()
    {
        $this->bdsCotisations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUser();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalairedebase(): ?int
    {
        return $this->salairedebase;
    }

    public function setSalairedebase(?int $salairedebase): self
    {
        $this->salairedebase = $salairedebase;

        return $this;
    }

    public function getCotisationpardefaut(): ?int
    {
        return $this->cotisationpardefaut;
    }

    public function setCotisationpardefaut(int $cotisationpardefaut): self
    {
        $this->cotisationpardefaut = $cotisationpardefaut;

        return $this;
    }

    public function getCotisationappliquee(): ?int
    {
        return $this->cotisationappliquee;
    }

    public function setCotisationappliquee(int $cotisationappliquee): self
    {
        $this->cotisationappliquee = $cotisationappliquee;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|BdsCotisation[]
     */
    public function getBdsCotisations(): Collection
    {
        return $this->bdsCotisations;
    }

    public function addBdsCotisation(BdsCotisation $bdsCotisation): self
    {
        if (!$this->bdsCotisations->contains($bdsCotisation)) {
            $this->bdsCotisations[] = $bdsCotisation;
            $bdsCotisation->setBdsParametresdecotisation($this);
        }

        return $this;
    }

    public function removeBdsCotisation(BdsCotisation $bdsCotisation): self
    {
        if ($this->bdsCotisations->contains($bdsCotisation)) {
            $this->bdsCotisations->removeElement($bdsCotisation);
            // set the owning side to null (unless already changed)
            if ($bdsCotisation->getBdsParametresdecotisation() === $this) {
                $bdsCotisation->setBdsParametresdecotisation(null);
            }
        }

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }
}
