<?php

namespace App\Entity\BanqueDeSolidarite;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BanqueDeSolidarite\BdsCotisationRepository")
 * @UniqueEntity(
 *     fields={"BdsParametresdecotisation", "cotisationAt"},
 *     errorPath="BdsParametresdecotisation",
 *     message="Une cotisation à cette date est déjà enregistrée (pour cet agent)!"
 * )
 */
class BdsCotisation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $cotisationappliquee;

    /**
     * @ORM\Column(type="date")
     */
    private $cotisationAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BanqueDeSolidarite\BdsParametresdecotisation", inversedBy="bdsCotisations")
     */
    private $BdsParametresdecotisation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCotisationappliquee(): ?int
    {
        return $this->cotisationappliquee;
    }

    public function setCotisationappliquee(int $cotisationappliquee): self
    {
        $this->cotisationappliquee = $cotisationappliquee;

        return $this;
    }

    public function getCotisationAt(): ?\DateTimeInterface
    {
        return $this->cotisationAt;
    }

    public function setCotisationAt(\DateTimeInterface $cotisationAt): self
    {
        $this->cotisationAt = $cotisationAt;

        return $this;
    }

    public function getBdsParametresdecotisation(): ?BdsParametresdecotisation
    {
        return $this->BdsParametresdecotisation;
    }

    public function setBdsParametresdecotisation(?BdsParametresdecotisation $BdsParametresdecotisation): self
    {
        $this->BdsParametresdecotisation = $BdsParametresdecotisation;

        return $this;
    }

    public function getFormatedCotisation(): string
    {
        return number_format($this->cotisationappliquee, 0, '', ' ');
    }
}