<?php

namespace App\Entity\Personnel;

use Symfony\Component\Validator\Constraints\Date;
use Doctrine\Common\Collections\ArrayCollection;

/*
 * La recherche se fera dans l'entité DemandeConge
 */
class CongeDataSearch{

    /**
     * @var Date|null
     */
    private $dateDebutAt;

    /**
     * @var Date|null
     */
    private $dateFinAt;

    /**
     * @var ModeleConge|null
     */
    private $modeleConge;

    /**
     * @return Date|null
     */
    public function getDateDebutAt(): ?\DateTime
    {
        return $this->dateDebutAt;
    }

    /**
     * @param Date|null $dateDebutAt
     * @return CongeDataSearch
     */
    public function setDateDebutAt($dateDebutAt): CongeDataSearch
    {
        $this->dateDebutAt = $dateDebutAt;
        return $this;
    }

    /**
     * @return Date|null
     */
    public function getDateFinAt(): ?\DateTime
    {
        return $this->dateFinAt;
    }

    /**
     * @param Date|null $dateFinAt
     * @return CongeDataSearch
     */
    public function setDateFinAt($dateFinAt): CongeDataSearch
    {
        $this->dateFinAt = $dateFinAt;
        return $this;
    }

    public function getModeleConge(): ?ModeleConge
    {
        return $this->modeleConge;
    }

    public function setModeleConge(?ModeleConge $modeleConge): self
    {
        $this->modeleConge = $modeleConge;

        return $this;
    }
}