<?php

namespace App\Entity\Personnel;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Personnel\CategorieCongeRepository")
 * @UniqueEntity(fields={"titre"}, message="Le titre que vous avez saisi est déjà utilisé !")
 */
class CategorieConge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Assert\Length(
     *      min = 3,
     *      max = 180,
     *      minMessage = "Le titre doit avoir au moins {{ limit }} caractères",
     *      maxMessage = "Le titre ne peut pas dépasser {{ limit }} caractères"
     * )
     */
    private $titre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Personnel\ModeleConge", mappedBy="categorieConge")
     */
    private $modeleConges;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $enabled;

    public function __construct()
    {
        $this->modeleConges = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitre();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * @return Collection|ModeleConge[]
     */
    public function getModeleConges(): Collection
    {
        return $this->modeleConges;
    }

    public function addModeleConge(ModeleConge $modeleConge): self
    {
        if (!$this->modeleConges->contains($modeleConge)) {
            $this->modeleConges[] = $modeleConge;
            $modeleConge->setCategorieConge($this);
        }

        return $this;
    }

    public function removeModeleConge(ModeleConge $modeleConge): self
    {
        if ($this->modeleConges->contains($modeleConge)) {
            $this->modeleConges->removeElement($modeleConge);
            // set the owning side to null (unless already changed)
            if ($modeleConge->getCategorieConge() === $this) {
                $modeleConge->setCategorieConge(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }
}
