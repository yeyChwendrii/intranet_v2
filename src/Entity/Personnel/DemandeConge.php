<?php

namespace App\Entity\Personnel;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Personnel\DemandeCongeRepository")
 * @Vich\Uploadable
 */
class DemandeConge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $numDemande;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     */
    private $dateDebutAt;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotNull()
     */
    private $dateFinAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $nombreJour;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Le motif ne peut pas dépasser {{ limit }} caractères!"
     * )
     */
    private $motif;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avisChefService;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avisChefDept;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avisDirConcerne;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avisChefDeptAdminPersonnel;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $justificatif;

    /**
     * @Vich\UploadableField(mapping="conge_docs", fileNameProperty="justificatif")
     * @var File
     */
    private $docFile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personnel\ModeleConge", inversedBy="demandeConges")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $modeleConge;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="demandeConges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agent;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Personnel\RepriseService", mappedBy="demandeConge", cascade={"persist", "remove"})
     */
    private $repriseService;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $annee;

    /**
     * @ORM\Column(type="integer")
     */
    private $auteur;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lieuJouissance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumDemande(): ?string
    {
        return $this->numDemande;
    }

    public function setNumDemande(string $numDemande): self
    {
        $this->numDemande = $numDemande;

        return $this;
    }

    public function getDateDebutAt(): ?\DateTimeInterface
    {
        return $this->dateDebutAt;
    }

    public function setDateDebutAt(\DateTimeInterface $dateDebutAt): self
    {
        $this->dateDebutAt = $dateDebutAt;

        return $this;
    }

    public function getDateFinAt(): ?\DateTimeInterface
    {
        return $this->dateFinAt;
    }

    public function setDateFinAt(\DateTimeInterface $dateFinAt): self
    {
        $this->dateFinAt = $dateFinAt;

        return $this;
    }

    public function getNombreJour(): ?int
    {
        return $this->nombreJour;
    }

    public function setNombreJour(int $nombreJour): self
    {
        $this->nombreJour = $nombreJour;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(?string $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getAvisChefService(): ?bool
    {
        return $this->avisChefService;
    }

    public function setAvisChefService(?bool $avisChefService): self
    {
        $this->avisChefService = $avisChefService;

        return $this;
    }

    public function getAvisChefDept(): ?bool
    {
        return $this->avisChefDept;
    }

    public function setAvisChefDept(?bool $avisChefDept): self
    {
        $this->avisChefDept = $avisChefDept;

        return $this;
    }

    public function getAvisDirConcerne(): ?bool
    {
        return $this->avisDirConcerne;
    }

    public function setAvisDirConcerne(?bool $avisDirConcerne): self
    {
        $this->avisDirConcerne = $avisDirConcerne;

        return $this;
    }

    public function getAvisChefDeptAdminPersonnel(): ?bool
    {
        return $this->avisChefDeptAdminPersonnel;
    }

    public function setAvisChefDeptAdminPersonnel(?bool $avisChefDeptAdminPersonnel): self
    {
        $this->avisChefDeptAdminPersonnel = $avisChefDeptAdminPersonnel;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(?bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getJustificatif(): ?string
    {
        return $this->justificatif;
    }

    public function setJustificatif(?string $justificatif): self
    {
        $this->justificatif = $justificatif;

        return $this;
    }

    /**
     * @return File
     */
    public function getDocFile(): ?File
    {
        return $this->docFile;
    }

    /**
     * @param null|File $doc
     */
    public function setDocFile(?File $doc = null): void
    {
        $this->docFile = $doc;

        if (null !== $doc) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModeleConge(): ?ModeleConge
    {
        return $this->modeleConge;
    }

    public function setModeleConge(?ModeleConge $modeleConge): self
    {
        $this->modeleConge = $modeleConge;

        return $this;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getRepriseService(): ?RepriseService
    {
        return $this->repriseService;
    }

    public function setRepriseService(RepriseService $repriseService): self
    {
        $this->repriseService = $repriseService;

        // set the owning side of the relation if necessary
        if ($this !== $repriseService->getDemandeConge()) {
            $repriseService->setDemandeConge($this);
        }

        return $this;
    }

    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    public function setAnnee(?string $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getAuteur(): ?int
    {
        return $this->auteur;
    }

    public function setAuteur(int $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getLieuJouissance(): ?string
    {
        return $this->lieuJouissance;
    }

    public function setLieuJouissance(?string $lieuJouissance): self
    {
        $this->lieuJouissance = $lieuJouissance;

        return $this;
    }
}
