<?php

namespace App\Entity\Personnel;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Personnel\AgentDroitCongeRepository")
 */
class AgentDroitConge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;

    /**
     * @ORM\Column(type="integer")
     */
    private $annuel;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $congePris;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $congeRestant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="agentDroitConges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getAnnuel(): ?int
    {
        return $this->annuel;
    }

    public function setAnnuel(int $annuel): self
    {
        $this->annuel = $annuel;

        return $this;
    }

    public function getCongePris(): ?int
    {
        return $this->congePris;
    }

    public function setCongePris(?int $congePris): self
    {
        $this->congePris = $congePris;

        return $this;
    }

    public function getCongeRestant(): ?int
    {
        return $this->congeRestant;
    }

    public function setCongeRestant(?int $congeRestant): self
    {
        $this->congeRestant = $congeRestant;

        return $this;
    }

    public function getAgent(): ?User
    {
        return $this->agent;
    }

    public function setAgent(?User $agent): self
    {
        $this->agent = $agent;

        return $this;
    }
}
