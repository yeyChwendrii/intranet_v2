<?php

namespace App\Entity\Personnel;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Personnel\AvisResponsableRepository")
 */
class AvisResponsable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $designation;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Personnel\ModeleConge", mappedBy="avisResponsables")
     */
    private $modeleConges;

    public function __construct()
    {
        $this->modeleConges = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|ModeleConge[]
     */
    public function getModeleConges(): Collection
    {
        return $this->modeleConges;
    }

    public function addModeleConge(ModeleConge $modeleConge): self
    {
        if (!$this->modeleConges->contains($modeleConge)) {
            $this->modeleConges[] = $modeleConge;
        }

        return $this;
    }

    public function removeModeleConge(ModeleConge $modeleConge): self
    {
        if ($this->modeleConges->contains($modeleConge)) {
            $this->modeleConges->removeElement($modeleConge);
        }

        return $this;
    }
}
