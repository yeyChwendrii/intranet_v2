<?php

namespace App\Entity\Personnel;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Personnel\ModeleCongeRepository")
 * @UniqueEntity(fields={"titre"}, message="Le titre que vous avez saisi est déjà utilisé !")
 */
class ModeleConge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Assert\Length(
     *      min = 3,
     *      max = 180,
     *      minMessage = "Le titre doit avoir au moins {{ limit }} caractères",
     *      maxMessage = "Le titre ne peut pas dépasser {{ limit }} caractères"
     * )
     */
    private $titre;

    /**
     * @ORM\Column(type="integer", length=6, options={"default":0})
     */
    private $delai;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $motif;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $justificatif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personnel\CategorieConge", inversedBy="modeleConges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorieConge;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Personnel\AvisResponsable", inversedBy="modeleConges")
     */
    private $avisResponsables;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Personnel\DemandeConge", mappedBy="modeleConge")
     */
    private $demandeConges;

    public function __construct()
    {
        $this->avisResponsables = new ArrayCollection();
        $this->demandeConges = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitre() . ' (' . $this->getCategorieConge()->getTitre() . ')';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDelai(): ?string
    {
        return $this->delai;
    }

    public function setDelai(string $delai): self
    {
        $this->delai = $delai;

        return $this;
    }

    public function getMotif(): ?bool
    {
        return $this->motif;
    }

    public function setMotif(?bool $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getJustificatif(): ?bool
    {
        return $this->justificatif;
    }

    public function setJustificatif(?bool $justificatif): self
    {
        $this->justificatif = $justificatif;

        return $this;
    }

    public function getCategorieConge(): ?CategorieConge
    {
        return $this->categorieConge;
    }

    public function setCategorieConge(?CategorieConge $categorieConge): self
    {
        $this->categorieConge = $categorieConge;

        return $this;
    }

    /**
     * @return Collection|AvisResponsable[]
     */
    public function getAvisResponsables(): Collection
    {
        return $this->avisResponsables;
    }

    public function addAvisResponsable(AvisResponsable $avisResponsable): self
    {
        if (!$this->avisResponsables->contains($avisResponsable)) {
            $this->avisResponsables[] = $avisResponsable;
            $avisResponsable->addModeleConge($this);
        }

        return $this;
    }

    public function removeAvisResponsable(AvisResponsable $avisResponsable): self
    {
        if ($this->avisResponsables->contains($avisResponsable)) {
            $this->avisResponsables->removeElement($avisResponsable);
            $avisResponsable->removeModeleConge($this);
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Collection|DemandeConge[]
     */
    public function getDemandeConges(): Collection
    {
        return $this->demandeConges;
    }

    public function addDemandeConge(DemandeConge $demandeConge): self
    {
        if (!$this->demandeConges->contains($demandeConge)) {
            $this->demandeConges[] = $demandeConge;
            $demandeConge->setModeleConge($this);
        }

        return $this;
    }

    public function removeDemandeConge(DemandeConge $demandeConge): self
    {
        if ($this->demandeConges->contains($demandeConge)) {
            $this->demandeConges->removeElement($demandeConge);
            // set the owning side to null (unless already changed)
            if ($demandeConge->getModeleConge() === $this) {
                $demandeConge->setModeleConge(null);
            }
        }

        return $this;
    }
}
