<?php

namespace App\Entity\Personnel;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Personnel\RepriseServiceRepository")
 */
class RepriseService
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $repriseServiceAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avisChefService;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avisChefDepartement;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Personnel\DemandeConge", inversedBy="repriseService", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $demandeConge;

    // Ajout du demande de congé à la méthode __toString
    public function __toString()
    {
        $format = "RepriseService (id: %s, repriseServiceAt: %s, createdAt: %s, avisChefService: %s, avisChefDepartemen: %s, demandeConge: %s)\n";
        return sprintf($format, $this->id, $this->repriseServiceAt, $this->createdAt, $this->avisChefService, $this->avisChefDepartement, $this->demandeConge);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRepriseServiceAt(): ?\DateTimeInterface
    {
        return $this->repriseServiceAt;
    }

    public function setRepriseServiceAt(\DateTimeInterface $repriseServiceAt): self
    {
        $this->repriseServiceAt = $repriseServiceAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAvisChefService(): ?bool
    {
        return $this->avisChefService;
    }

    public function setAvisChefService(?bool $avisChefService): self
    {
        $this->avisChefService = $avisChefService;

        return $this;
    }

    public function getAvisChefDepartement(): ?bool
    {
        return $this->avisChefDepartement;
    }

    public function setAvisChefDepartement(?bool $avisChefDepartement): self
    {
        $this->avisChefDepartement = $avisChefDepartement;

        return $this;
    }

    public function getDemandeConge(): ?DemandeConge
    {
        return $this->demandeConge;
    }

    public function setDemandeConge(DemandeConge $demandeConge): self
    {
        $this->demandeConge = $demandeConge;

        return $this;
    }
}
