<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessagesvusRepository")
 */
class Messagesvus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $vuAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Message", inversedBy="messagesvuses")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messagesvuses")
     */
    private $user;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVuAt(): ?\DateTimeInterface
    {
        return $this->vuAt;
    }

    public function setVuAt(\DateTimeInterface $vuAt): self
    {
        $this->vuAt = $vuAt;

        return $this;
    }


    public function getMessage(): ?Message
    {
        return $this->message;
    }

    public function setMessage(?Message $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
