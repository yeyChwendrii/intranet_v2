<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosteuserDirectionRepository")
 */
class PosteuserDirection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PosteUser", inversedBy="posteuserDirections")
     */
    private $posteuser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direction", inversedBy="posteuserDirections")
     */
    private $direction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosteuser(): ?PosteUser
    {
        return $this->posteuser;
    }

    public function setPosteuser(?PosteUser $posteuser): self
    {
        $this->posteuser = $posteuser;

        return $this;
    }

    public function getDirection(): ?Direction
    {
        return $this->direction;
    }

    public function setDirection(?Direction $direction): self
    {
        $this->direction = $direction;

        return $this;
    }
}
