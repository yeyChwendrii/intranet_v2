<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgenceRepository")
 */
class Agence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServiceDepartementdirection", mappedBy="agence")
     */
    private $serviceDepartementdirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ServiceDirection", mappedBy="agence")
     */
    private $serviceDirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DepartementDirection", mappedBy="agence")
     */
    private $departementDirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Direction", mappedBy="agence")
     */
    private $directions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="agences")
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $designation;

    public function __construct()
    {
        $this->serviceDepartementdirections = new ArrayCollection();
        $this->serviceDirections = new ArrayCollection();
        $this->departementDirections = new ArrayCollection();
        $this->directions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|ServiceDepartementdirection[]
     */
    public function getServiceDepartementdirections(): Collection
    {
        return $this->serviceDepartementdirections;
    }

    public function addServiceDepartementdirection(ServiceDepartementdirection $serviceDepartementdirection): self
    {
        if (!$this->serviceDepartementdirections->contains($serviceDepartementdirection)) {
            $this->serviceDepartementdirections[] = $serviceDepartementdirection;
            $serviceDepartementdirection->setAgence($this);
        }

        return $this;
    }

    public function removeServiceDepartementdirection(ServiceDepartementdirection $serviceDepartementdirection): self
    {
        if ($this->serviceDepartementdirections->contains($serviceDepartementdirection)) {
            $this->serviceDepartementdirections->removeElement($serviceDepartementdirection);
            // set the owning side to null (unless already changed)
            if ($serviceDepartementdirection->getAgence() === $this) {
                $serviceDepartementdirection->setAgence(null);
            }
        }

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|ServiceDirection[]
     */
    public function getServiceDirections(): Collection
    {
        return $this->serviceDirections;
    }

    public function addServiceDirection(ServiceDirection $serviceDirection): self
    {
        if (!$this->serviceDirections->contains($serviceDirection)) {
            $this->serviceDirections[] = $serviceDirection;
            $serviceDirection->setAgence($this);
        }

        return $this;
    }

    public function removeServiceDirection(ServiceDirection $serviceDirection): self
    {
        if ($this->serviceDirections->contains($serviceDirection)) {
            $this->serviceDirections->removeElement($serviceDirection);
            // set the owning side to null (unless already changed)
            if ($serviceDirection->getAgence() === $this) {
                $serviceDirection->setAgence(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DepartementDirection[]
     */
    public function getDepartementDirections(): Collection
    {
        return $this->departementDirections;
    }

    public function addDepartementDirection(DepartementDirection $departementDirection): self
    {
        if (!$this->departementDirections->contains($departementDirection)) {
            $this->departementDirections[] = $departementDirection;
            $departementDirection->setAgence($this);
        }

        return $this;
    }

    public function removeDepartementDirection(DepartementDirection $departementDirection): self
    {
        if ($this->departementDirections->contains($departementDirection)) {
            $this->departementDirections->removeElement($departementDirection);
            // set the owning side to null (unless already changed)
            if ($departementDirection->getAgence() === $this) {
                $departementDirection->setAgence(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Direction[]
     */
    public function getDirections(): Collection
    {
        return $this->directions;
    }

    public function addDirection(Direction $direction): self
    {
        if (!$this->directions->contains($direction)) {
            $this->directions[] = $direction;
            $direction->setAgence($this);
        }

        return $this;
    }

    public function removeDirection(Direction $direction): self
    {
        if ($this->directions->contains($direction)) {
            $this->directions->removeElement($direction);
            // set the owning side to null (unless already changed)
            if ($direction->getAgence() === $this) {
                $direction->setAgence(null);
            }
        }

        return $this;
    }
}
