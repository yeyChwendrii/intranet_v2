<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosteuserServicedirectionRepository")
 */
class PosteuserServicedirection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PosteUser", inversedBy="posteuserServicedirections")
     */
    private $posteuser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDirection", inversedBy="posteuserServicedirections")
     */
    private $servicedirection;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosteuser(): ?PosteUser
    {
        return $this->posteuser;
    }

    public function setPosteuser(?PosteUser $posteuser): self
    {
        $this->posteuser = $posteuser;

        return $this;
    }

    public function getServicedirection(): ?ServiceDirection
    {
        return $this->servicedirection;
    }

    public function setServicedirection(?ServiceDirection $servicedirection): self
    {
        $this->servicedirection = $servicedirection;

        return $this;
    }
}
