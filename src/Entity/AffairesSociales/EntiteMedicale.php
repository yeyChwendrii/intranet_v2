<?php

namespace App\Entity\AffairesSociales;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AffairesSociales\EntiteMedicaleRepository")
 */
class EntiteMedicale
{

    const TYPE = [
        '' => 'Choisir',
        'Hôpital' => 'Hôpital',
        'Clinique' => 'Clinique',
        'Autres' => 'Autres'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AffairesSociales\ChargeMedicale", mappedBy="entiteMedicale")
     */
    private $chargeMedicales;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function __construct()
    {
        $this->chargeMedicales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|ChargeMedicale[]
     */
    public function getChargeMedicales(): Collection
    {
        return $this->chargeMedicales;
    }

    public function addChargeMedicale(ChargeMedicale $chargeMedicale): self
    {
        if (!$this->chargeMedicales->contains($chargeMedicale)) {
            $this->chargeMedicales[] = $chargeMedicale;
            $chargeMedicale->setEntiteMedicale($this);
        }

        return $this;
    }

    public function removeChargeMedicale(ChargeMedicale $chargeMedicale): self
    {
        if ($this->chargeMedicales->contains($chargeMedicale)) {
            $this->chargeMedicales->removeElement($chargeMedicale);
            // set the owning side to null (unless already changed)
            if ($chargeMedicale->getEntiteMedicale() === $this) {
                $chargeMedicale->setEntiteMedicale(null);
            }
        }

        return $this;
    }
}
