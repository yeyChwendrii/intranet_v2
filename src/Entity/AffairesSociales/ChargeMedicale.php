<?php

namespace App\Entity\AffairesSociales;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AffairesSociales\ChargeMedicaleRepository")
 * @UniqueEntity(
 *     fields={"entiteMedicale", "dateAt"},
 *     message="Charges mensuelles déjà enregistré pour cette entité médicale"
 * )
 */
class ChargeMedicale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $couts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AffairesSociales\EntiteMedicale", inversedBy="chargeMedicales")
     */
    private $entiteMedicale;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateAt(): ?\DateTimeInterface
    {
        return $this->dateAt;
    }

    public function setDateAt(\DateTimeInterface $dateAt): self
    {
        $this->dateAt = $dateAt;

        return $this;
    }

    public function getCouts(): ?int
    {
        return $this->couts;
    }

    public function setCouts(int $couts): self
    {
        $this->couts = $couts;

        return $this;
    }

    public function getEntiteMedicale(): ?EntiteMedicale
    {
        return $this->entiteMedicale;
    }

    public function setEntiteMedicale(?EntiteMedicale $entiteMedicale): self
    {
        $this->entiteMedicale = $entiteMedicale;

        return $this;
    }
}
