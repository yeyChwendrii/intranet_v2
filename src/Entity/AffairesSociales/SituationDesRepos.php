<?php

namespace App\Entity\AffairesSociales;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AffairesSociales\SituationDesReposRepository")
 *
 */
class SituationDesRepos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reposMaladies")
     */
    private $user;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getDateFinAt() <= this.getDateDebutAt() and this.getDateDebutAt() >= this.getDateFinAt() )",
     *     message="La date 'debut' ne peut pas dépasser la date 'fin' de stage"
     * )
     */
    private $dateDebutAt;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getDateFinAt() <= this.getDateDebutAt() and this.getDateDebutAt() >= this.getDateFinAt() )",
     *     message="Veuillez vérifier la date debut"
     * )
     */
    private $dateFinAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AffairesSociales\Medecin", inversedBy="situationDesRepos")
     */
    private $medecin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateDebutAt(): ?\DateTimeInterface
    {
        return $this->dateDebutAt;
    }

    public function setDateDebutAt(\DateTimeInterface $dateDebutAt): self
    {
        $this->dateDebutAt = $dateDebutAt;

        return $this;
    }

    public function getDateFinAt(): ?\DateTimeInterface
    {
        return $this->dateFinAt;
    }

    public function setDateFinAt(\DateTimeInterface $dateFinAt): self
    {
        $this->dateFinAt = $dateFinAt;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getMedecin(): ?Medecin
    {
        return $this->medecin;
    }

    public function setMedecin(?Medecin $medecin): self
    {
        $this->medecin = $medecin;

        return $this;
    }
}
