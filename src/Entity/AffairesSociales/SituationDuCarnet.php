<?php

namespace App\Entity\AffairesSociales;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AffairesSociales\SituationDuCarnetRepository")
 */
class SituationDuCarnet
{

    const LIBELLE = [
        '' => 'Choisir',
        'Nouveau' => 'Nouveau',
        'Renouveller' => 'Renouveller'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAttributionAt;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $statutDuBeneficiaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $referenceDuCarnet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="carnets")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateAttributionAt(): ?\DateTimeInterface
    {
        return $this->dateAttributionAt;
    }

    public function setDateAttributionAt(\DateTimeInterface $dateAttributionAt): self
    {
        $this->dateAttributionAt = $dateAttributionAt;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getStatutDuBeneficiaire(): ?string
    {
        return $this->statutDuBeneficiaire;
    }

    public function setStatutDuBeneficiaire(string $statutDuBeneficiaire): self
    {
        $this->statutDuBeneficiaire = $statutDuBeneficiaire;

        return $this;
    }

    public function getReferenceDuCarnet(): ?string
    {
        return $this->referenceDuCarnet;
    }

    public function setReferenceDuCarnet(string $referenceDuCarnet): self
    {
        $this->referenceDuCarnet = $referenceDuCarnet;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
