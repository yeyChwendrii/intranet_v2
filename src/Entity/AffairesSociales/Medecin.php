<?php

namespace App\Entity\AffairesSociales;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AffairesSociales\MedecinRepository")
 */
class Medecin
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomPrenom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="boolean")
     */
    private $agree;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AffairesSociales\SituationDesRepos", mappedBy="medecin")
     */
    private $situationDesRepos;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getNomPrenom();
    }

    public function __construct()
    {
        $this->situationDesRepos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    public function setNomPrenom(string $nomPrenom): self
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    public function getTel(): ?int
    {
        return $this->tel;
    }

    public function setTel(?int $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getAgree(): ?bool
    {
        return $this->agree;
    }

    public function setAgree(bool $agree): self
    {
        $this->agree = $agree;

        return $this;
    }

    /**
     * @return Collection|SituationDesRepos[]
     */
    public function getSituationDesRepos(): Collection
    {
        return $this->situationDesRepos;
    }

    public function addSituationDesRepo(SituationDesRepos $situationDesRepo): self
    {
        if (!$this->situationDesRepos->contains($situationDesRepo)) {
            $this->situationDesRepos[] = $situationDesRepo;
            $situationDesRepo->setMedecin($this);
        }

        return $this;
    }

    public function removeSituationDesRepo(SituationDesRepos $situationDesRepo): self
    {
        if ($this->situationDesRepos->contains($situationDesRepo)) {
            $this->situationDesRepos->removeElement($situationDesRepo);
            // set the owning side to null (unless already changed)
            if ($situationDesRepo->getMedecin() === $this) {
                $situationDesRepo->setMedecin(null);
            }
        }

        return $this;
    }
}
