<?php

namespace App\Entity\Evenements;

use App\Entity\Categorie;
use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Poste;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvPromotioninterneRepository")
 * @Assert\Expression(
 *     " this.getServiceDepartement() || this.getServiceDirection() || this.getDepartement() || this.getDirection()",
 *     message="Un des champs service, departement ou direction est requis! (Service d'un département ou d'une direction )"
 * )
 */
class EvPromotioninterne
{
    const IND_GROUPE = [
        '' => 'Choisir le groupe',
        'I' => 'I',
        'II' => 'II',
        'III' => 'III',
        'IV' => 'IV'
    ];

    const IND_CATEGORIE = [
        '' => 'Choisir la catégorie',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5'
    ];

    const IND_ECHELON = [
        '' => 'Choisir L\'échelon',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10'
    ];
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evPromotioninternes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datePromotioninterneAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDepartementdirection", inversedBy="evPromotioninternes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDepartement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDirection", inversedBy="evPromotioninternes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDirection;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartementDirection", inversedBy="evPromotioninternes")
     */
    private $departement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direction", inversedBy="evPromotioninternes")
     */
    private $direction;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $indiceGroupe;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $indiceCategorie;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $indiceEchelon;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Poste", inversedBy="evPromotioninternes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $poste;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="evPromotioninternes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;

    public function __construct()
    {
        $this->poste = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDatePromotioninterneAt(): ?\DateTimeInterface
    {
        return $this->datePromotioninterneAt;
    }

    public function setDatePromotioninterneAt($datePromotioninterneAt): self
    {
        $this->datePromotioninterneAt = $datePromotioninterneAt;

        return $this;
    }

    public function getServiceDepartement(): ?ServiceDepartementdirection
    {
        return $this->serviceDepartement;
    }

    public function setServiceDepartement(?ServiceDepartementdirection $serviceDepartement): self
    {
        $this->serviceDepartement = $serviceDepartement;

        return $this;
    }

    public function getServiceDirection(): ?ServiceDirection
    {
        return $this->serviceDirection;
    }

    public function setServiceDirection(?ServiceDirection $serviceDirection): self
    {
        $this->serviceDirection = $serviceDirection;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getDepartement(): ?DepartementDirection
    {
        return $this->departement;
    }

    public function setDepartement(?DepartementDirection $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getDirection(): ?Direction
    {
        return $this->direction;
    }

    public function setDirection(?Direction $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getIndiceGroupe(): ?string
    {
        return $this->indiceGroupe;
    }

    public function setIndiceGroupe(string $indiceGroupe): self
    {
        $this->indiceGroupe = $indiceGroupe;

        return $this;
    }

    public function getIndiceCategorie(): ?string
    {
        return $this->indiceCategorie;
    }

    public function setIndiceCategorie(string $indiceCategorie): self
    {
        $this->indiceCategorie = $indiceCategorie;

        return $this;
    }

    public function getIndiceEchelon(): ?string
    {
        return $this->indiceEchelon;
    }

    public function setIndiceEchelon(string $indiceEchelon): self
    {
        $this->indiceEchelon = $indiceEchelon;

        return $this;
    }

    /**
     * @return Collection|Poste[]
     */
    public function getPoste(): Collection
    {
        return $this->poste;
    }

    public function addPoste(Poste $poste): self
    {
        if (!$this->poste->contains($poste)) {
            $this->poste[] = $poste;
        }

        return $this;
    }

    public function removePoste(Poste $poste): self
    {
        if ($this->poste->contains($poste)) {
            $this->poste->removeElement($poste);
        }

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

}
