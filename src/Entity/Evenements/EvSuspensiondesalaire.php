<?php

namespace App\Entity\Evenements;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvSuspensiondesalaireRepository")
 */
class EvSuspensiondesalaire
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evSuspensiondesalaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateSuspensiondesalaireAt;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;


    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Assert\NotBlank
     */
    private $poste;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $serviceDepartementDirection;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cause;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $moisConcerne;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateSuspensiondesalaireAt(): ?\DateTimeInterface
    {
        return $this->dateSuspensiondesalaireAt;
    }

    public function setDateSuspensiondesalaireAt($dateSuspensiondesalaireAt): self
    {
        $this->dateSuspensiondesalaireAt = $dateSuspensiondesalaireAt;

        return $this;
    }


    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }


    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(?string $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getServiceDepartementDirection(): ?string
    {
        return $this->serviceDepartementDirection;
    }

    public function setServiceDepartementDirection(string $serviceDepartementDirection): self
    {
        $this->serviceDepartementDirection = $serviceDepartementDirection;

        return $this;
    }

    public function getCause(): ?string
    {
        return $this->cause;
    }

    public function setCause(string $cause): self
    {
        $this->cause = $cause;

        return $this;
    }

    public function getMoisConcerne(): ?string
    {
        return $this->moisConcerne;
    }

    public function setMoisConcerne(string $moisConcerne): self
    {
        $this->moisConcerne = $moisConcerne;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

}
