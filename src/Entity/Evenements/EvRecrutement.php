<?php

namespace App\Entity\Evenements;

use App\Entity\Categorie;
use App\Entity\DepartementDirection;
use App\Entity\Poste;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvRecrutementRepository")
 * @Assert\Expression(
 *     " this.getServiceDepartement() || this.getServiceDirection()",
 *     message="Un des champs service est requis! (Service d'un département ou d'une direction )"
 * )
 */
class EvRecrutement
{
    const IND_GROUPE = [
        '' => 'Choisir le groupe',
        'I' => 'I',
        'II' => 'II',
        'III' => 'III',
        'IV' => 'IV'
    ];

    const IND_CATEGORIE = [
        '' => 'Choisir la catégorie',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5'
    ];

    const IND_ECHELON = [
        '' => 'Choisir L\'échelon',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10'
    ];

    const NATURE_RECRUTEMENT = [
        '' => 'Recrutement par : ',
        'Décision' => 'Décision',
        'Concours' => 'Concours'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evRecrutements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $dateRecrutementAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDepartementdirection", inversedBy="evRecrutements")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDepartement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDirection", inversedBy="evRecrutements")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDirection;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ancienMatricule;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nouveauMatricule;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Poste", inversedBy="evRecrutements")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private $poste;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $indiceGroupe;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $indiceCategorie;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $indiceEchelon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="evRecrutements")
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=90, nullable=false)
     */
    private $natureRecrutement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $decisionAnnulee;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;


    public function __construct()
    {
        $this->poste = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateRecrutementAt(): ?\DateTimeInterface
    {
        return $this->dateRecrutementAt;
    }

    public function setDateRecrutementAt($dateRecrutementAt): self
    {
        $this->dateRecrutementAt = $dateRecrutementAt;

        return $this;
    }


    public function getServiceDepartement(): ?ServiceDepartementdirection
    {
        return $this->serviceDepartement;
    }

    public function setServiceDepartement(?ServiceDepartementdirection $serviceDepartement): self
    {
        $this->serviceDepartement = $serviceDepartement;

        return $this;
    }

    public function getServiceDirection(): ?ServiceDirection
    {
        return $this->serviceDirection;
    }

    public function setServiceDirection(?ServiceDirection $serviceDirection): self
    {
        $this->serviceDirection = $serviceDirection;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getAncienMatricule(): ?int
    {
        return $this->ancienMatricule;
    }

    public function setAncienMatricule(?int $ancienMatricule): self
    {
        $this->ancienMatricule = $ancienMatricule;

        return $this;
    }

    public function getNouveauMatricule(): ?int
    {
        return $this->nouveauMatricule;
    }

    public function setNouveauMatricule(?int $nouveauMatricule): self
    {
        $this->nouveauMatricule = $nouveauMatricule;

        return $this;
    }

    public function getIndiceGroupe(): ?string
    {
        return $this->indiceGroupe;
    }

    public function setIndiceGroupe(string $indiceGroupe): self
    {
        $this->indiceGroupe = $indiceGroupe;

        return $this;
    }

    public function getIndiceCategorie(): ?string
    {
        return $this->indiceCategorie;
    }

    public function setIndiceCategorie(string $indiceCategorie): self
    {
        $this->indiceCategorie = $indiceCategorie;

        return $this;
    }

    public function getIndiceEchelon(): ?string
    {
        return $this->indiceEchelon;
    }

    public function setIndiceEchelon(string $indiceEchelon): self
    {
        $this->indiceEchelon = $indiceEchelon;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getNatureRecrutement(): ?string
    {
        return $this->natureRecrutement;
    }

    public function setNatureRecrutement(?string $natureRecrutement): self
    {
        $this->natureRecrutement = $natureRecrutement;

        return $this;
    }

    public function getDecisionAnnulee(): ?bool
    {
        return $this->decisionAnnulee;
    }

    public function setDecisionAnnulee(bool $decisionAnnulee): self
    {
        $this->decisionAnnulee = $decisionAnnulee;

        return $this;
    }

    /**
     * @return Collection|Poste[]
     */
    public function getPoste(): Collection
    {
        return $this->poste;
    }

    public function addPoste(Poste $poste): self
    {
        if (!$this->poste->contains($poste)) {
            $this->poste[] = $poste;
        }

        return $this;
    }

    public function removePoste(Poste $poste): self
    {
        if ($this->poste->contains($poste)) {
            $this->poste->removeElement($poste);
        }

        return $this;
    }

    /*
     * CALCULER Le nombre d'année de recrutement
     */

    public function getNbrAnneesRecrutement()
    {
        $from = new \DateTime($this->dateRecrutementAt->format(('Y-m-d')));
        $to   = new \DateTime('today');
        return $from->diff($to)->y;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }


}
