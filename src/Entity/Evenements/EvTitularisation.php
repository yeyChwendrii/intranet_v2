<?php

namespace App\Entity\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Poste;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvTitularisationRepository")
 * @Assert\Expression(
 *     " this.getServiceDepartement() || this.getServiceDirection()",
 *     message="Un des champs service est requis! (Service d'un département ou d'une direction )"
 * )
 */
class EvTitularisation
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evTitularisations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $dateTitularisationAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDepartementdirection", inversedBy="evTitularisations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDepartement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDirection", inversedBy="evTitularisations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDirection;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ancienMatricule;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $nouveauMatricule;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Poste", inversedBy="evTitularisations")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private $poste;

    /**
     * @ORM\Column(type="boolean")
     */
    private $decisionAnnulee;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;


    public function __construct()
    {
        $this->poste = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateTitularisationAt(): ?\DateTimeInterface
    {
        return $this->dateTitularisationAt;
    }

    public function setDateTitularisationAt($dateTitularisationAt): self
    {
        $this->dateTitularisationAt = $dateTitularisationAt;

        return $this;
    }


    public function getServiceDepartement(): ?ServiceDepartementdirection
    {
        return $this->serviceDepartement;
    }

    public function setServiceDepartement(?ServiceDepartementdirection $serviceDepartement): self
    {
        $this->serviceDepartement = $serviceDepartement;

        return $this;
    }

    public function getServiceDirection(): ?ServiceDirection
    {
        return $this->serviceDirection;
    }

    public function setServiceDirection(?ServiceDirection $serviceDirection): self
    {
        $this->serviceDirection = $serviceDirection;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getAncienMatricule(): ?int
    {
        return $this->ancienMatricule;
    }

    public function setAncienMatricule(?int $ancienMatricule): self
    {
        $this->ancienMatricule = $ancienMatricule;

        return $this;
    }

    public function getNouveauMatricule(): ?int
    {
        return $this->nouveauMatricule;
    }

    public function setNouveauMatricule(?int $nouveauMatricule): self
    {
        $this->nouveauMatricule = $nouveauMatricule;

        return $this;
    }

    public function getDecisionAnnulee(): ?bool
    {
        return $this->decisionAnnulee;
    }

    public function setDecisionAnnulee(bool $decisionAnnulee): self
    {
        $this->decisionAnnulee = $decisionAnnulee;

        return $this;
    }

    /**
     * @return Collection|Poste[]
     */
    public function getPoste(): Collection
    {
        return $this->poste;
    }

    public function addPoste(Poste $poste): self
    {
        if (!$this->poste->contains($poste)) {
            $this->poste[] = $poste;
        }

        return $this;
    }

    public function removePoste(Poste $poste): self
    {
        if ($this->poste->contains($poste)) {
            $this->poste->removeElement($poste);
        }

        return $this;
    }

    /*
     * CALCULER Le nombre d'année de titularisation
     */

    public function getNbrAnneesTitularisation()
    {
        $from = new \DateTime($this->dateTitularisationAt->format(('Y-m-d')));
        $to   = new \DateTime('today');
        return $from->diff($to)->y;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }


}
