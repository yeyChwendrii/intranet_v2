<?php

namespace App\Entity\Evenements;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvAnnulationdedecisionRepository")
 */
class EvAnnulationdedecision
{

    const DECISION_A_ANNULER = [
        '' => 'choisir la décision à annuler',
        'Recrutement' => 'Recrutement',
        'Titularisation' => 'Titularisation'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evAnnulationdedecisions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank
     */
    private $dateAnnulationdedecisionAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $decisionAAnnuler;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }


    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }


    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    public function getDateAnnulationdedecisionAt(): ?\DateTimeInterface
    {
        return $this->dateAnnulationdedecisionAt;
    }

    public function setDateAnnulationdedecisionAt(?\DateTimeInterface $dateAnnulationdedecisionAt): self
    {
        $this->dateAnnulationdedecisionAt = $dateAnnulationdedecisionAt;

        return $this;
    }

    public function getMatricule(): ?int
    {
        return $this->matricule;
    }

    public function setMatricule(int $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getDecisionAAnnuler(): ?string
    {
        return $this->decisionAAnnuler;
    }

    public function setDecisionAAnnuler(string $decisionAAnnuler): self
    {
        $this->decisionAAnnuler = $decisionAAnnuler;

        return $this;
    }
}
