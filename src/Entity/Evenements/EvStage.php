<?php

namespace App\Entity\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Poste;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvStageRepository")
 * @Assert\Expression(
 *     " this.getServiceDepartement() || this.getServiceDirection()",
 *     message="Un des champs service est requis! (Service d'un département ou d'une direction )"
 * )
 */
class EvStage
{

    const TYPE_STAGE = [
        '' => 'Type de stage',
        'Universitaire' => 'Universitaire',
        'Pré-embauche' => 'Pré-embauche'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evStages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     *@Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getDateFinAt() <= this.getDateDebutAt() and this.getDateDebutAt() >= this.getDateFinAt() )",
     *     message="La date 'debut' ne peut pas dépasser la date 'fin' de stage"
     * )
     */
    private $dateDebutAt;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getDateFinAt() <= this.getDateDebutAt() and this.getDateDebutAt() >= this.getDateFinAt() )",
     *     message="Veuillez vérifier la date debut"
     * )
     */
    private $dateFinAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDepartementdirection", inversedBy="evStages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDepartement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServiceDirection", inversedBy="evStages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serviceDirection;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $typeStage;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;

    public function __construct()
    {
        $this->poste = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateDebutAt(): ?\DateTimeInterface
    {
        return $this->dateDebutAt;
    }

    public function setDateDebutAt($dateDebutAt): self
    {
        $this->dateDebutAt = $dateDebutAt;

        return $this;
    }

    public function getDateFinAt(): ?\DateTimeInterface
    {
        return $this->dateFinAt;
    }

    public function setDateFinAt($dateFinAt): self
    {
        $this->dateFinAt = $dateFinAt;

        return $this;
    }


    public function getServiceDepartement(): ?ServiceDepartementdirection
    {
        return $this->serviceDepartement;
    }

    public function setServiceDepartement(?ServiceDepartementdirection $serviceDepartement): self
    {
        $this->serviceDepartement = $serviceDepartement;

        return $this;
    }

    public function getServiceDirection(): ?ServiceDirection
    {
        return $this->serviceDirection;
    }

    public function setServiceDirection(?ServiceDirection $serviceDirection): self
    {
        $this->serviceDirection = $serviceDirection;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getMatricule(): ?int
    {
        return $this->matricule;
    }

    public function setMatricule(?int $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getTypeStage(): ?string
    {
        return $this->typeStage;
    }

    public function setTypeStage(string $typeStage): self
    {
        $this->typeStage = $typeStage;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }


}
