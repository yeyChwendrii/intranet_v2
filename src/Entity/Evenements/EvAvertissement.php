<?php

namespace App\Entity\Evenements;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvAvertissementRepository")
 */
class EvAvertissement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evAvertissements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateAvertissementAt;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $poste;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }


    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getDateAvertissementAt(): ?\DateTimeInterface
    {
        return $this->dateAvertissementAt;
    }

    public function setDateAvertissementAt($dateAvertissementAt): self
    {
        $this->dateAvertissementAt = $dateAvertissementAt;

        return $this;
    }

    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(string $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

}
