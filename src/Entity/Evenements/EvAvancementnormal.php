<?php

namespace App\Entity\Evenements;

use App\Entity\Categorie;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvAvancementnormalRepository")
 */
class EvAvancementnormal
{
    const IND_GROUPE = [
        '' => 'Choisir le groupe',
        'I' => 'I',
        'II' => 'II',
        'III' => 'III',
        'IV' => 'IV'
    ];

    const IND_CATEGORIE = [
        '' => 'Choisir la catégorie',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5'
    ];

    const IND_ECHELON = [
        '' => 'Choisir L\'échelon',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evAvancementnormals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateAvancementnormalAt;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $indiceGroupe;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $indiceCategorie;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $indiceEchelon;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $poste;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="evAvancementnormals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getDateAvancementnormalAt(): ?\DateTimeInterface
    {
        return $this->dateAvancementnormalAt;
    }

    public function setDateAvancementnormalAt($dateAvancementnormalAt): self
    {
        $this->dateAvancementnormalAt = $dateAvancementnormalAt;

        return $this;
    }

    public function getIndiceGroupe(): ?string
    {
        return $this->indiceGroupe;
    }

    public function setIndiceGroupe(?string $indiceGroupe): self
    {
        $this->indiceGroupe = $indiceGroupe;

        return $this;
    }

    public function getIndiceCategorie(): ?string
    {
        return $this->indiceCategorie;
    }

    public function setIndiceCategorie(?string $indiceCategorie): self
    {
        $this->indiceCategorie = $indiceCategorie;

        return $this;
    }

    public function getIndiceEchelon(): ?string
    {
        return $this->indiceEchelon;
    }

    public function setIndiceEchelon(string $indiceEchelon): self
    {
        $this->indiceEchelon = $indiceEchelon;

        return $this;
    }

    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(string $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

}
