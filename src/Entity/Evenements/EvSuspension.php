<?php

namespace App\Entity\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvSuspensionRepository")
 */
class EvSuspension
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evSuspensions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateSuspensionAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartementDirection", inversedBy="evSuspensions")
     */
    private $departement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Direction", inversedBy="evSuspensions")
     */
    private $direction;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motif;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $poste;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $service_departement_direction;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateSuspensionAt(): ?\DateTimeInterface
    {
        return $this->dateSuspensionAt;
    }

    public function setDateSuspensionAt($dateSuspensionAt): self
    {
        $this->dateSuspensionAt = $dateSuspensionAt;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getDepartement(): ?DepartementDirection
    {
        return $this->departement;
    }

    public function setDepartement(?DepartementDirection $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getDirection(): ?Direction
    {
        return $this->direction;
    }

    public function setDirection(?Direction $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(string $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(string $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getServiceDepartementDirection(): ?string
    {
        return $this->service_departement_direction;
    }

    public function setServiceDepartementDirection(string $service_departement_direction): self
    {
        $this->service_departement_direction = $service_departement_direction;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

}
