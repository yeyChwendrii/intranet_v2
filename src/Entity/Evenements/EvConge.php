<?php

namespace App\Entity\Evenements;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Evenements\EvCongeRepository")
 */
class EvConge
{

    const NATURE_CONGE = [
        '' => 'Choisir le type de congé',
        'maladie' => 'maladie',
        'longue durée' => 'longue durée',
        'maternité' => 'maternité',
        'viduité' => 'viduité',
        'sans solde' => 'sans solde'
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evConges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateCongeAt;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;


    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Assert\NotBlank
     */
    private $poste;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $serviceDepartementDirection;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motif;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getFinCongeAt() <= this.getDebutCongeAt() and this.getDebutCongeAt() >= this.getFinCongeAt() )",
     *     message="La date 'debut' ne peut pas dépasser la date 'fin' de congé"
     * )
     */
    private $debutCongeAt;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getFinCongeAt() <= this.getDebutCongeAt() and this.getDebutCongeAt() >= this.getFinCongeAt() )",
     *     message="La date 'debut' ne peut pas dépasser la date 'fin' de congé"
     * )
     */
    private $finCongeAt;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $natureConge;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 1})
     */
    private $valide = true;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return DepartementDirection
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateCongeAt(): ?\DateTimeInterface
    {
        return $this->dateCongeAt;
    }

    public function setDateCongeAt($dateCongeAt): self
    {
        $this->dateCongeAt = $dateCongeAt;

        return $this;
    }


    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }


    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(?string $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getServiceDepartementDirection(): ?string
    {
        return $this->serviceDepartementDirection;
    }

    public function setServiceDepartementDirection(string $serviceDepartementDirection): self
    {
        $this->serviceDepartementDirection = $serviceDepartementDirection;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(string $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getDebutCongeAt(): ?\DateTimeInterface
    {
        return $this->debutCongeAt;
    }

    public function setDebutCongeAt(\DateTimeInterface $debutCongeAt): self
    {
        $this->debutCongeAt = $debutCongeAt;

        return $this;
    }

    public function getFinCongeAt(): ?\DateTimeInterface
    {
        return $this->finCongeAt;
    }

    public function setFinCongeAt(\DateTimeInterface $finCongeAt): self
    {
        $this->finCongeAt = $finCongeAt;

        return $this;
    }

    public function getNatureConge(): ?string
    {
        return $this->natureConge;
    }

    public function setNatureConge(string $natureConge): self
    {
        $this->natureConge = $natureConge;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

}
