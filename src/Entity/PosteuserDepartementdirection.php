<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosteuserDepartementdirectionRepository")
 */
class PosteuserDepartementdirection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PosteUser", inversedBy="posteuserDepartementdirections")
     */
    private $posteuser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DepartementDirection", inversedBy="posteuserDepartementdirections")
     */
    private $departementdirection;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosteuser(): ?PosteUser
    {
        return $this->posteuser;
    }

    public function setPosteuser(?PosteUser $posteuser): self
    {
        $this->posteuser = $posteuser;

        return $this;
    }

    public function getDepartementdirection(): ?DepartementDirection
    {
        return $this->departementdirection;
    }

    public function setDepartementdirection(?DepartementDirection $departementdirection): self
    {
        $this->departementdirection = $departementdirection;

        return $this;
    }
}
