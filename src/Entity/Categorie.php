<?php

namespace App\Entity;

use App\Entity\Evenements\EvAvancementnormal;
use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvReintegration;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 * @UniqueEntity(
 *     fields={"designation"},
 *     message="La catégorie saisie est déjà enregistrée !"
 * )
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $designation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="categorie")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvReintegration", mappedBy="categorie")
     */
    private $evReintegrations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvRecrutement", mappedBy="categorie")
     */
    private $evRecrutements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvPromotioninterne", mappedBy="categorie")
     */
    private $evPromotioninternes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvNomination", mappedBy="categorie")
     */
    private $evNominations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evenements\EvAvancementnormal", mappedBy="categorie")
     */
    private $evAvancementnormals;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->evReintegrations = new ArrayCollection();
        $this->evRecrutements = new ArrayCollection();
        $this->evPromotioninternes = new ArrayCollection();
        $this->evNominations = new ArrayCollection();
        $this->evAvancementnormals = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCategorie($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getCategorie() === $this) {
                $user->setCategorie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvReintegration[]
     */
    public function getEvReintegrations(): Collection
    {
        return $this->evReintegrations;
    }

    public function addEvReintegration(EvReintegration $evReintegration): self
    {
        if (!$this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations[] = $evReintegration;
            $evReintegration->setCategorie($this);
        }

        return $this;
    }

    public function removeEvReintegration(EvReintegration $evReintegration): self
    {
        if ($this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations->removeElement($evReintegration);
            // set the owning side to null (unless already changed)
            if ($evReintegration->getCategorie() === $this) {
                $evReintegration->setCategorie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRecrutement[]
     */
    public function getEvRecrutements(): Collection
    {
        return $this->evRecrutements;
    }

    public function addEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if (!$this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements[] = $evRecrutement;
            $evRecrutement->setCategorie($this);
        }

        return $this;
    }

    public function removeEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if ($this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements->removeElement($evRecrutement);
            // set the owning side to null (unless already changed)
            if ($evRecrutement->getCategorie() === $this) {
                $evRecrutement->setCategorie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvPromotioninterne[]
     */
    public function getEvPromotioninternes(): Collection
    {
        return $this->evPromotioninternes;
    }

    public function addEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if (!$this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes[] = $evPromotioninterne;
            $evPromotioninterne->setCategorie($this);
        }

        return $this;
    }

    public function removeEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if ($this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes->removeElement($evPromotioninterne);
            // set the owning side to null (unless already changed)
            if ($evPromotioninterne->getCategorie() === $this) {
                $evPromotioninterne->setCategorie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvNomination[]
     */
    public function getEvNominations(): Collection
    {
        return $this->evNominations;
    }

    public function addEvNomination(EvNomination $evNomination): self
    {
        if (!$this->evNominations->contains($evNomination)) {
            $this->evNominations[] = $evNomination;
            $evNomination->setCategorie($this);
        }

        return $this;
    }

    public function removeEvNomination(EvNomination $evNomination): self
    {
        if ($this->evNominations->contains($evNomination)) {
            $this->evNominations->removeElement($evNomination);
            // set the owning side to null (unless already changed)
            if ($evNomination->getCategorie() === $this) {
                $evNomination->setCategorie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvAvancementnormal[]
     */
    public function getEvAvancementnormals(): Collection
    {
        return $this->evAvancementnormals;
    }

    public function addEvAvancementnormal(EvAvancementnormal $evAvancementnormal): self
    {
        if (!$this->evAvancementnormals->contains($evAvancementnormal)) {
            $this->evAvancementnormals[] = $evAvancementnormal;
            $evAvancementnormal->setCategorie($this);
        }

        return $this;
    }

    public function removeEvAvancementnormal(EvAvancementnormal $evAvancementnormal): self
    {
        if ($this->evAvancementnormals->contains($evAvancementnormal)) {
            $this->evAvancementnormals->removeElement($evAvancementnormal);
            // set the owning side to null (unless already changed)
            if ($evAvancementnormal->getCategorie() === $this) {
                $evAvancementnormal->setCategorie(null);
            }
        }

        return $this;
    }
}
