<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserOptionVisibiliteRepository")
 */
class UserOptionVisibilite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optUsername;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optEmail;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optEnabled;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optLastLogin;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optLastActivityAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optLoginCount;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optFirstLogin;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optMatricule;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optNom;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optPrenom;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $optAdresse;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optSexe;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $optDateNaissanceAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optPhoto;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $optTel;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $optIndiceGroupe;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $optIndiceCategorieNiveau;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $optIndiceEchelon;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $optEnligne;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="userOptionVisibilite", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOptUsername(): ?bool
    {
        return $this->optUsername;
    }

    public function setOptUsername(bool $optUsername): self
    {
        $this->optUsername = $optUsername;

        return $this;
    }

    public function getOptEmail(): ?bool
    {
        return $this->optEmail;
    }

    public function setOptEmail(bool $optEmail): self
    {
        $this->optEmail = $optEmail;

        return $this;
    }

    public function getOptEnabled(): ?bool
    {
        return $this->optEnabled;
    }

    public function setOptEnabled(bool $optEnabled): self
    {
        $this->optEnabled = $optEnabled;

        return $this;
    }

    public function getOptLastLogin(): ?bool
    {
        return $this->optLastLogin;
    }

    public function setOptLastLogin(bool $optLastLogin): self
    {
        $this->optLastLogin = $optLastLogin;

        return $this;
    }

    public function getOptLastActivityAt(): ?bool
    {
        return $this->optLastActivityAt;
    }

    public function setOptLastActivityAt(bool $optLastActivityAt): self
    {
        $this->optLastActivityAt = $optLastActivityAt;

        return $this;
    }

    public function getOptLoginCount(): ?bool
    {
        return $this->optLoginCount;
    }

    public function setOptLoginCount(bool $optLoginCount): self
    {
        $this->optLoginCount = $optLoginCount;

        return $this;
    }

    public function getOptFirstLogin(): ?bool
    {
        return $this->optFirstLogin;
    }

    public function setOptFirstLogin(bool $optFirstLogin): self
    {
        $this->optFirstLogin = $optFirstLogin;

        return $this;
    }

    public function getOptMatricule(): ?bool
    {
        return $this->optMatricule;
    }

    public function setOptMatricule(bool $optMatricule): self
    {
        $this->optMatricule = $optMatricule;

        return $this;
    }

    public function getOptNom(): ?bool
    {
        return $this->optNom;
    }

    public function setOptNom(bool $optNom): self
    {
        $this->optNom = $optNom;

        return $this;
    }

    public function getOptPrenom(): ?bool
    {
        return $this->optPrenom;
    }

    public function setOptPrenom(bool $optPrenom): self
    {
        $this->optPrenom = $optPrenom;

        return $this;
    }

    public function getOptAdresse(): ?bool
    {
        return $this->optAdresse;
    }

    public function setOptAdresse(bool $optAdresse): self
    {
        $this->optAdresse = $optAdresse;

        return $this;
    }

    public function getOptSexe(): ?bool
    {
        return $this->optSexe;
    }

    public function setOptSexe(bool $optSexe): self
    {
        $this->optSexe = $optSexe;

        return $this;
    }

    public function getOptDateNaissanceAt(): ?bool
    {
        return $this->optDateNaissanceAt;
    }

    public function setOptDateNaissanceAt(bool $optDateNaissanceAt): self
    {
        $this->optDateNaissanceAt = $optDateNaissanceAt;

        return $this;
    }

    public function getOptPhoto(): ?bool
    {
        return $this->optPhoto;
    }

    public function setOptPhoto(bool $optPhoto): self
    {
        $this->optPhoto = $optPhoto;

        return $this;
    }

    public function getOptTel(): ?bool
    {
        return $this->optTel;
    }

    public function setOptTel(bool $optTel): self
    {
        $this->optTel = $optTel;

        return $this;
    }

    public function getOptIndiceGroupe(): ?bool
    {
        return $this->optIndiceGroupe;
    }

    public function setOptIndiceGroupe(bool $optIndiceGroupe): self
    {
        $this->optIndiceGroupe = $optIndiceGroupe;

        return $this;
    }

    public function getOptIndiceCategorieNiveau(): ?bool
    {
        return $this->optIndiceCategorieNiveau;
    }

    public function setOptIndiceCategorieNiveau(bool $optIndiceCategorieNiveau): self
    {
        $this->optIndiceCategorieNiveau = $optIndiceCategorieNiveau;

        return $this;
    }

    public function getOptIndiceEchelon(): ?bool
    {
        return $this->optIndiceEchelon;
    }

    public function setOptIndiceEchelon(bool $optIndiceEchelon): self
    {
        $this->optIndiceEchelon = $optIndiceEchelon;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOptEnligne(): ?bool
    {
        return $this->optEnligne;
    }

    public function setOptEnligne(bool $optEnligne): self
    {
        $this->optEnligne = $optEnligne;

        return $this;
    }
}
