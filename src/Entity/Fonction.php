<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Ici les fonctions sont perçues comme étant les tâches des agents et non comme des fonctions
 * L'entité fonction de baase a été remplacé par POSTE (fonction dans l'ancien Intranet)
 *
 *
 * @ORM\Entity(repositoryClass="App\Repository\FonctionRepository")
 * @UniqueEntity(fields={"posteConcerne"}, message="La fonction que vous avez saisi est déjà enregistrée !")
 */
class Fonction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $posteConcerne;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PosteUser", mappedBy="fonctions")
     */
    private $posteUsers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tache", inversedBy="fonctions")
     */
    private $taches;


    public function __construct()
    {
        $this->posteUsers = new ArrayCollection();
        $this->taches = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getPosteConcerne();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosteConcerne(): ?string
    {
        return $this->posteConcerne;
    }

    public function setPosteConcerne(string $posteConcerne): self
    {
        $this->posteConcerne = $posteConcerne;

        return $this;
    }

    /**
     * @return Collection|PosteUser[]
     */
    public function getPosteUsers(): Collection
    {
        return $this->posteUsers;
    }

    public function addPosteUser(PosteUser $posteUser): self
    {
        if (!$this->posteUsers->contains($posteUser)) {
            $this->posteUsers[] = $posteUser;
            $posteUser->addFonction($this);
        }

        return $this;
    }

    public function removePosteUser(PosteUser $posteUser): self
    {
        if ($this->posteUsers->contains($posteUser)) {
            $this->posteUsers->removeElement($posteUser);
            $posteUser->removeFonction($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tache[]
     */
    public function getTaches(): Collection
    {
        return $this->taches;
    }

    public function addTach(Tache $tach): self
    {
        if (!$this->taches->contains($tach)) {
            $this->taches[] = $tach;
        }

        return $this;
    }

    public function removeTach(Tache $tach): self
    {
        if ($this->taches->contains($tach)) {
            $this->taches->removeElement($tach);
        }

        return $this;
    }

}
