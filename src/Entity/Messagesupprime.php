<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessagesupprimeRepository")
 */
class Messagesupprime
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $supprimeAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Message", inversedBy="messagesupprimes")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messagesupprimes")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSupprimeAt(): ?\DateTimeInterface
    {
        return $this->supprimeAt;
    }

    public function setSupprimeAt(\DateTimeInterface $supprimeAt): self
    {
        $this->supprimeAt = $supprimeAt;

        return $this;
    }

    public function getMessage(): ?Message
    {
        return $this->message;
    }

    public function setMessage(?Message $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
