<?php

namespace App\Entity\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcMetierRepository")
 * @UniqueEntity(fields={"designation"}, message="Le métier que vous avez saisi est déjà enregistré !")
 */
class EcMetier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $designation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcSousmetier", mappedBy="ecMetier")
     */
    private $ecSousmetiers;

    public function __construct()
    {
        $this->ecSousmetiers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|EcSousmetier[]
     */
    public function getEcSousmetiers(): Collection
    {
        return $this->ecSousmetiers;
    }

    public function addEcSousmetier(EcSousmetier $ecSousmetier): self
    {
        if (!$this->ecSousmetiers->contains($ecSousmetier)) {
            $this->ecSousmetiers[] = $ecSousmetier;
            $ecSousmetier->setEcMetier($this);
        }

        return $this;
    }

    public function removeEcSousmetier(EcSousmetier $ecSousmetier): self
    {
        if ($this->ecSousmetiers->contains($ecSousmetier)) {
            $this->ecSousmetiers->removeElement($ecSousmetier);
            // set the owning side to null (unless already changed)
            if ($ecSousmetier->getEcMetier() === $this) {
                $ecSousmetier->setEcMetier(null);
            }
        }

        return $this;
    }
}
