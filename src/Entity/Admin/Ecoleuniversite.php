<?php

namespace App\Entity\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcoleuniversiteRepository")
 * @UniqueEntity(fields={"designation"}, message="L'école ou l'université saisie est déjà enregistrée !")
 */
class Ecoleuniversite
{
    const ECOLE_UNIVERSITE = [
        '' => 'Ecole ou Université',
        'Ecole' => 'Ecole',
        'Université' => 'Université'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Expression(
     *     "not ( this.getNomUniversite() !='' and this.getEcoleOuUniversite() == 'Ecole' )",
     *     message="Vérifier le type"
     * )
     * @Assert\Expression(
     *     "not ( this.getNomUniversite() =='' and this.getEcoleOuUniversite() == 'Université' )",
     *     message="Ce champs ne peut pas être vide si le type est une université"
     * )
     */
    private $ecoleOuUniversite;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $ville;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\Niveauqualification", inversedBy="ecoleuniversites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $niveauqualification;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\EcSousmetier", inversedBy="ecoleuniversites")
     */
    private $ecSousmetier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcFormationuser", mappedBy="ecoleuniversite")
     */
    private $ecFormationusers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\EcUniversite", inversedBy="ecoleuniversites")
     * @Assert\Expression(
     *     "not ( this.getNomUniversite() =='' and this.getEcoleOuUniversite() == 'Université' )",
     *     message="Ce champs ne peut pas être vide si le type est une université"
     * )
     * @Assert\Expression(
     *     "not ( this.getNomUniversite() !='' and this.getEcoleOuUniversite() == 'Ecole' )",
     *     message="Vérifier le type"
     * )
     */
    private $nomUniversite;

    public function __construct()
    {
        $this->niveauqualification = new ArrayCollection();
        $this->ecSousmetier = new ArrayCollection();
        $this->ecFormationusers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation().' '.$this->getNomUniversite();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEcoleOuUniversite(): ?string
    {
        return $this->ecoleOuUniversite;
    }

    public function setEcoleOuUniversite(string $ecoleOuUniversite): self
    {
        $this->ecoleOuUniversite = $ecoleOuUniversite;

        return $this;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @return Collection|Niveauqualification[]
     */
    public function getNiveauqualification(): Collection
    {
        return $this->niveauqualification;
    }

    public function addNiveauqualification(Niveauqualification $niveauqualification): self
    {
        if (!$this->niveauqualification->contains($niveauqualification)) {
            $this->niveauqualification[] = $niveauqualification;
        }

        return $this;
    }

    public function removeNiveauqualification(Niveauqualification $niveauqualification): self
    {
        if ($this->niveauqualification->contains($niveauqualification)) {
            $this->niveauqualification->removeElement($niveauqualification);
        }

        return $this;
    }

    /**
     * @return Collection|EcSousmetier[]
     */
    public function getEcSousmetier(): Collection
    {
        return $this->ecSousmetier;
    }

    public function addEcSousmetier(EcSousmetier $ecSousmetier): self
    {
        if (!$this->ecSousmetier->contains($ecSousmetier)) {
            $this->ecSousmetier[] = $ecSousmetier;
        }

        return $this;
    }

    public function removeEcSousmetier(EcSousmetier $ecSousmetier): self
    {
        if ($this->ecSousmetier->contains($ecSousmetier)) {
            $this->ecSousmetier->removeElement($ecSousmetier);
        }

        return $this;
    }

    /**
     * @return Collection|EcFormationuser[]
     */
    public function getEcFormationusers(): Collection
    {
        return $this->ecFormationusers;
    }

    public function addEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if (!$this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers[] = $ecFormationuser;
            $ecFormationuser->setEcoleuniversite($this);
        }

        return $this;
    }

    public function removeEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if ($this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers->removeElement($ecFormationuser);
            // set the owning side to null (unless already changed)
            if ($ecFormationuser->getEcoleuniversite() === $this) {
                $ecFormationuser->setEcoleuniversite(null);
            }
        }

        return $this;
    }

    public function getNomUniversite(): ?EcUniversite
    {
        return $this->nomUniversite;
    }

    public function setNomUniversite(?EcUniversite $nomUniversite): self
    {
        $this->nomUniversite = $nomUniversite;

        return $this;
    }
}
