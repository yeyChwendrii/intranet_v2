<?php

namespace App\Entity\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcSousmetierRepository")
 * @UniqueEntity(fields={"designation"}, message="Il existe un enregistrement portant la même désignation!")
 */
class EcSousmetier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $designation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\EcMetier", inversedBy="ecSousmetiers")
     */
    private $ecMetier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcFormation", mappedBy="ecSousmetier")
     */
    private $ecFormations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\Ecoleuniversite", mappedBy="ecSousmetier")
     */
    private $ecoleuniversites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcFormationuser", mappedBy="ecSousmetier")
     */
    private $ecFormationusers;

    public function __construct()
    {
        $this->ecFormations = new ArrayCollection();
        $this->ecoleuniversites = new ArrayCollection();
        $this->ecFormationusers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getEcMetier(): ?EcMetier
    {
        return $this->ecMetier;
    }

    public function setEcMetier(?EcMetier $ecMetier): self
    {
        $this->ecMetier = $ecMetier;

        return $this;
    }

    /**
     * @return Collection|EcFormation[]
     */
    public function getEcFormations(): Collection
    {
        return $this->ecFormations;
    }

    public function addEcFormation(EcFormation $ecFormation): self
    {
        if (!$this->ecFormations->contains($ecFormation)) {
            $this->ecFormations[] = $ecFormation;
            $ecFormation->setEcSousmetier($this);
        }

        return $this;
    }

    public function removeEcFormation(EcFormation $ecFormation): self
    {
        if ($this->ecFormations->contains($ecFormation)) {
            $this->ecFormations->removeElement($ecFormation);
            // set the owning side to null (unless already changed)
            if ($ecFormation->getEcSousmetier() === $this) {
                $ecFormation->setEcSousmetier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ecoleuniversite[]
     */
    public function getEcoleuniversites(): Collection
    {
        return $this->ecoleuniversites;
    }

    public function addEcoleuniversite(Ecoleuniversite $ecoleuniversite): self
    {
        if (!$this->ecoleuniversites->contains($ecoleuniversite)) {
            $this->ecoleuniversites[] = $ecoleuniversite;
            $ecoleuniversite->addEcSousmetier($this);
        }

        return $this;
    }

    public function removeEcoleuniversite(Ecoleuniversite $ecoleuniversite): self
    {
        if ($this->ecoleuniversites->contains($ecoleuniversite)) {
            $this->ecoleuniversites->removeElement($ecoleuniversite);
            $ecoleuniversite->removeEcSousmetier($this);
        }

        return $this;
    }

    /**
     * @return Collection|EcFormationuser[]
     */
    public function getEcFormationusers(): Collection
    {
        return $this->ecFormationusers;
    }

    public function addEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if (!$this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers[] = $ecFormationuser;
            $ecFormationuser->setEcSousmetier($this);
        }

        return $this;
    }

    public function removeEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if ($this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers->removeElement($ecFormationuser);
            // set the owning side to null (unless already changed)
            if ($ecFormationuser->getEcSousmetier() === $this) {
                $ecFormationuser->setEcSousmetier(null);
            }
        }

        return $this;
    }
}
