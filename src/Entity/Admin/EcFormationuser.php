<?php

namespace App\Entity\Admin;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcFormationuserRepository")
 */
class EcFormationuser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ecFormationusers")
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDebutAt;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateFinAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Ecoleuniversite", inversedBy="ecFormationusers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ecoleuniversite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\EcSousmetier", inversedBy="ecFormationusers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ecSousmetier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\EcFormation", inversedBy="ecFormationusers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ecFormation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Niveauqualification", inversedBy="ecFormationusers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $niveauqualification;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEcFormation(): ?EcFormation
    {
        return $this->ecFormation;
    }

    public function setEcFormation(?EcFormation $ecFormation): self
    {
        $this->ecFormation = $ecFormation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateDebutAt(): ?\DateTimeInterface
    {
        return $this->dateDebutAt;
    }

    public function setDateDebutAt($dateDebutAt): self
    {
        $this->dateDebutAt = $dateDebutAt;

        return $this;
    }

    public function getDateFinAt(): ?\DateTimeInterface
    {
        return $this->dateFinAt;
    }

    public function setDateFinAt(?\DateTimeInterface $dateFinAt): self
    {
        $this->dateFinAt = $dateFinAt;

        return $this;
    }

    /**
     * Set reference
     *
     * @param string
     *
     * @return EcSousmetier
     */
    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getEcoleuniversite(): ?Ecoleuniversite
    {
        return $this->ecoleuniversite;
    }

    public function setEcoleuniversite(?Ecoleuniversite $ecoleuniversite): self
    {
        $this->ecoleuniversite = $ecoleuniversite;

        return $this;
    }

    public function getEcSousmetier(): ?EcSousmetier
    {
        return $this->ecSousmetier;
    }

    public function setEcSousmetier(?EcSousmetier $ecSousmetier): self
    {
        $this->ecSousmetier = $ecSousmetier;

        return $this;
    }

    public function getNiveauqualification(): ?Niveauqualification
    {
        return $this->niveauqualification;
    }

    public function setNiveauqualification(?Niveauqualification $niveauqualification): self
    {
        $this->niveauqualification = $niveauqualification;

        return $this;
    }
}
