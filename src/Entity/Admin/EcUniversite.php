<?php

namespace App\Entity\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcUniversiteRepository")
 * @UniqueEntity(fields={"designation"}, message="L'université saisie est déjà enregistrée !")
 */
class EcUniversite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $tel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\Ecoleuniversite", mappedBy="nomUniversite")
     */
    private $ecoleuniversites;

    public function __construct()
    {
        $this->ecoleuniversites = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|Ecoleuniversite[]
     */
    public function getEcoleuniversites(): Collection
    {
        return $this->ecoleuniversites;
    }

    public function addEcoleuniversite(Ecoleuniversite $ecoleuniversite): self
    {
        if (!$this->ecoleuniversites->contains($ecoleuniversite)) {
            $this->ecoleuniversites[] = $ecoleuniversite;
            $ecoleuniversite->setNomUniversite($this);
        }

        return $this;
    }

    public function removeEcoleuniversite(Ecoleuniversite $ecoleuniversite): self
    {
        if ($this->ecoleuniversites->contains($ecoleuniversite)) {
            $this->ecoleuniversites->removeElement($ecoleuniversite);
            // set the owning side to null (unless already changed)
            if ($ecoleuniversite->getNomUniversite() === $this) {
                $ecoleuniversite->setNomUniversite(null);
            }
        }

        return $this;
    }
}
