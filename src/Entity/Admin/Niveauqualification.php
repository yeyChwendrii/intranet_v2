<?php

namespace App\Entity\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\NiveauqualificationRepository")
 * @UniqueEntity(fields={"designation"}, message="Le niveau de qualification saisi est déjà enregistré !")
 */
class Niveauqualification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $designation;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\Ecoleuniversite", mappedBy="niveauqualification")
     */
    private $ecoleuniversites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcFormationuser", mappedBy="niveauqualification")
     */
    private $ecFormationusers;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function __construct()
    {
        $this->ecoleuniversites = new ArrayCollection();
        $this->ecFormationusers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|Ecoleuniversite[]
     */
    public function getEcoleuniversites(): Collection
    {
        return $this->ecoleuniversites;
    }

    public function addEcoleuniversite(Ecoleuniversite $ecoleuniversite): self
    {
        if (!$this->ecoleuniversites->contains($ecoleuniversite)) {
            $this->ecoleuniversites[] = $ecoleuniversite;
            $ecoleuniversite->addNiveauqualification($this);
        }

        return $this;
    }

    public function removeEcoleuniversite(Ecoleuniversite $ecoleuniversite): self
    {
        if ($this->ecoleuniversites->contains($ecoleuniversite)) {
            $this->ecoleuniversites->removeElement($ecoleuniversite);
            $ecoleuniversite->removeNiveauqualification($this);
        }

        return $this;
    }

    /**
     * @return Collection|EcFormationuser[]
     */
    public function getEcFormationusers(): Collection
    {
        return $this->ecFormationusers;
    }

    public function addEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if (!$this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers[] = $ecFormationuser;
            $ecFormationuser->setNiveauqualification($this);
        }

        return $this;
    }

    public function removeEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if ($this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers->removeElement($ecFormationuser);
            // set the owning side to null (unless already changed)
            if ($ecFormationuser->getNiveauqualification() === $this) {
                $ecFormationuser->setNiveauqualification(null);
            }
        }

        return $this;
    }
}
