<?php

namespace App\Entity\Admin;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcExperienceAilleursRepository")
 */
class EcExperienceAilleurs
{
    const TYPE = [
        '' => 'Choisir le type de formation',
        'Stage' => 'Stage',
        'Séminaire' => 'Séminaire',
        'formation' => 'formation',
        'Autres' => 'Autres'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $type;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getDateFinAt() <= this.getDateDebutAt() and this.getDateDebutAt() >= this.getDateFinAt() )",
     *     message="La date 'debut' ne peut pas dépasser la date 'fin' de stage"
     * )
     */
    private $dateDebutAt;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Assert\Expression(
     *     "not ( this.getDateFinAt() <= this.getDateDebutAt() and this.getDateDebutAt() >= this.getDateFinAt() )",
     *     message="Veuillez vérifier la date debut"
     * )
     */
    private $dateFinAt;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $societe;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $posteOccupe;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $dirDeptServ;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ecExperienceAilleurs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observations;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $responsableEnCharge;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getPosteOccupe() . ' {' . $this->getSociete() . '}';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateDebutAt(): ?\DateTimeInterface
    {
        return $this->dateDebutAt;
    }

    public function setDateDebutAt(\DateTimeInterface $dateDebutAt): self
    {
        $this->dateDebutAt = $dateDebutAt;

        return $this;
    }

    public function getDateFinAt(): ?\DateTimeInterface
    {
        return $this->dateFinAt;
    }

    public function setDateFinAt(?\DateTimeInterface $dateFinAt): self
    {
        $this->dateFinAt = $dateFinAt;

        return $this;
    }

    public function getSociete(): ?string
    {
        return $this->societe;
    }

    public function setSociete(string $societe): self
    {
        $this->societe = $societe;

        return $this;
    }

    public function getPosteOccupe(): ?string
    {
        return $this->posteOccupe;
    }

    public function setPosteOccupe(?string $posteOccupe): self
    {
        $this->posteOccupe = $posteOccupe;

        return $this;
    }

    public function getDirDeptServ(): ?string
    {
        return $this->dirDeptServ;
    }

    public function setDirDeptServ(?string $dirDeptServ): self
    {
        $this->dirDeptServ = $dirDeptServ;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getResponsableEnCharge(): ?string
    {
        return $this->responsableEnCharge;
    }

    public function setResponsableEnCharge(?string $responsableEnCharge): self
    {
        $this->responsableEnCharge = $responsableEnCharge;

        return $this;
    }
}
