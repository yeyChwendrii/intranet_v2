<?php

namespace App\Entity\Admin;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\EcFormationRepository")
 * @UniqueEntity(fields={"designation"}, message="La formation que vous avez saisi est déjà enregistrée !")
 */
class EcFormation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $designation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\EcSousmetier", inversedBy="ecFormations")
     */
    private $ecSousmetier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admin\EcFormationuser", mappedBy="ecFormation")
     */
    private $ecFormationusers;

    public function __construct()
    {
        $this->ecFormationusers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|EcFormationuser[]
     */
    public function getEcFormationusers(): Collection
    {
        return $this->ecFormationusers;
    }

    public function addEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if (!$this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers[] = $ecFormationuser;
            $ecFormationuser->setEcFormation($this);
        }

        return $this;
    }

    public function removeEcFormationuser(EcFormationuser $ecFormationuser): self
    {
        if ($this->ecFormationusers->contains($ecFormationuser)) {
            $this->ecFormationusers->removeElement($ecFormationuser);
            // set the owning side to null (unless already changed)
            if ($ecFormationuser->getEcFormation() === $this) {
                $ecFormationuser->setEcFormation(null);
            }
        }

        return $this;
    }

    public function getEcSousmetier(): ?EcSousmetier
    {
        return $this->ecSousmetier;
    }

    public function setEcSousmetier(?EcSousmetier $ecSousmetier): self
    {
        $this->ecSousmetier = $ecSousmetier;

        return $this;
    }

}
