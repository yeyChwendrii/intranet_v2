<?php

namespace App\Entity;

use App\Entity\Evenements\EvAffectation;
use App\Entity\Evenements\EvDemisedefonction;
use App\Entity\Evenements\EvNomination;
use App\Entity\Evenements\EvPromotioninterne;
use App\Entity\Evenements\EvRecrutement;
use App\Entity\Evenements\EvReintegration;
use App\Entity\Evenements\EvRenouvellementdecontrat;
use App\Entity\Evenements\EvTitularisation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosteRepository")
 */
class Poste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $designation;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteUser", mappedBy="poste")
     */
    private $posteUsers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvRecrutement", mappedBy="poste")
     */
    private $evRecrutements;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvRenouvellementdecontrat", mappedBy="poste")
     */
    private $evRenouvellementdecontrats;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvTitularisation", mappedBy="poste")
     */
    private $evTitularisations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvNomination", mappedBy="poste")
     */
    private $evNominations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvAffectation", mappedBy="poste")
     */
    private $evAffectations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvDemisedefonction", mappedBy="poste")
     */
    private $evDemisedefonctions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvReintegration", mappedBy="poste")
     */
    private $evReintegrations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Evenements\EvPromotioninterne", mappedBy="poste")
     */
    private $evPromotioninternes;



    public function __construct()
    {
        $this->posteUsers = new ArrayCollection();
        $this->evRecrutements = new ArrayCollection();
        $this->evRenouvellementdecontrats = new ArrayCollection();
        $this->evTitularisations = new ArrayCollection();
        $this->evNominations = new ArrayCollection();
        $this->evAffectations = new ArrayCollection();
        $this->evDemisedefonctions = new ArrayCollection();
        $this->evReintegrations = new ArrayCollection();
        $this->evPromotioninternes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDesignation();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|PosteUser[]
     */
    public function getPosteUsers(): Collection
    {
        return $this->posteUsers;
    }

    public function addPosteUser(PosteUser $posteUser): self
    {
        if (!$this->posteUsers->contains($posteUser)) {
            $this->posteUsers[] = $posteUser;
            $posteUser->setPoste($this);
        }

        return $this;
    }

    public function removePosteUser(PosteUser $posteUser): self
    {
        if ($this->posteUsers->contains($posteUser)) {
            $this->posteUsers->removeElement($posteUser);
            // set the owning side to null (unless already changed)
            if ($posteUser->getPoste() === $this) {
                $posteUser->setPoste(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvRecrutement[]
     */
    public function getEvRecrutements(): Collection
    {
        return $this->evRecrutements;
    }

    public function addEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if (!$this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements[] = $evRecrutement;
            $evRecrutement->addPoste($this);
        }

        return $this;
    }

    public function removeEvRecrutement(EvRecrutement $evRecrutement): self
    {
        if ($this->evRecrutements->contains($evRecrutement)) {
            $this->evRecrutements->removeElement($evRecrutement);
            $evRecrutement->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvRenouvellementdecontrat[]
     */
    public function getEvRenouvellementdecontrats(): Collection
    {
        return $this->evRenouvellementdecontrats;
    }

    public function addEvRenouvellementdecontrat(EvRenouvellementdecontrat $evRenouvellementdecontrat): self
    {
        if (!$this->evRenouvellementdecontrats->contains($evRenouvellementdecontrat)) {
            $this->evRenouvellementdecontrats[] = $evRenouvellementdecontrat;
            $evRenouvellementdecontrat->addPoste($this);
        }

        return $this;
    }

    public function removeEvRenouvellementdecontrat(EvRenouvellementdecontrat $evRenouvellementdecontrat): self
    {
        if ($this->evRenouvellementdecontrats->contains($evRenouvellementdecontrat)) {
            $this->evRenouvellementdecontrats->removeElement($evRenouvellementdecontrat);
            $evRenouvellementdecontrat->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvTitularisation[]
     */
    public function getEvTitularisations(): Collection
    {
        return $this->evTitularisations;
    }

    public function addEvTitularisation(EvTitularisation $evTitularisation): self
    {
        if (!$this->evTitularisations->contains($evTitularisation)) {
            $this->evTitularisations[] = $evTitularisation;
            $evTitularisation->addPoste($this);
        }

        return $this;
    }

    public function removeEvTitularisation(EvTitularisation $evTitularisation): self
    {
        if ($this->evTitularisations->contains($evTitularisation)) {
            $this->evTitularisations->removeElement($evTitularisation);
            $evTitularisation->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvNomination[]
     */
    public function getEvNominations(): Collection
    {
        return $this->evNominations;
    }

    public function addEvNomination(EvNomination $evNomination): self
    {
        if (!$this->evNominations->contains($evNomination)) {
            $this->evNominations[] = $evNomination;
            $evNomination->addPoste($this);
        }

        return $this;
    }

    public function removeEvNomination(EvNomination $evNomination): self
    {
        if ($this->evNominations->contains($evNomination)) {
            $this->evNominations->removeElement($evNomination);
            $evNomination->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvAffectation[]
     */
    public function getEvAffectations(): Collection
    {
        return $this->evAffectations;
    }

    public function addEvAffectation(EvAffectation $evAffectation): self
    {
        if (!$this->evAffectations->contains($evAffectation)) {
            $this->evAffectations[] = $evAffectation;
            $evAffectation->addPoste($this);
        }

        return $this;
    }

    public function removeEvAffectation(EvAffectation $evAffectation): self
    {
        if ($this->evAffectations->contains($evAffectation)) {
            $this->evAffectations->removeElement($evAffectation);
            $evAffectation->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvDemisedefonction[]
     */
    public function getEvDemisedefonctions(): Collection
    {
        return $this->evDemisedefonctions;
    }

    public function addEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if (!$this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions[] = $evDemisedefonction;
            $evDemisedefonction->addPoste($this);
        }

        return $this;
    }

    public function removeEvDemisedefonction(EvDemisedefonction $evDemisedefonction): self
    {
        if ($this->evDemisedefonctions->contains($evDemisedefonction)) {
            $this->evDemisedefonctions->removeElement($evDemisedefonction);
            $evDemisedefonction->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvReintegration[]
     */
    public function getEvReintegrations(): Collection
    {
        return $this->evReintegrations;
    }

    public function addEvReintegration(EvReintegration $evReintegration): self
    {
        if (!$this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations[] = $evReintegration;
            $evReintegration->addPoste($this);
        }

        return $this;
    }

    public function removeEvReintegration(EvReintegration $evReintegration): self
    {
        if ($this->evReintegrations->contains($evReintegration)) {
            $this->evReintegrations->removeElement($evReintegration);
            $evReintegration->removePoste($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvPromotioninterne[]
     */
    public function getEvPromotioninternes(): Collection
    {
        return $this->evPromotioninternes;
    }

    public function addEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if (!$this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes[] = $evPromotioninterne;
            $evPromotioninterne->addPoste($this);
        }

        return $this;
    }

    public function removeEvPromotioninterne(EvPromotioninterne $evPromotioninterne): self
    {
        if ($this->evPromotioninternes->contains($evPromotioninterne)) {
            $this->evPromotioninternes->removeElement($evPromotioninterne);
            $evPromotioninterne->removePoste($this);
        }

        return $this;
    }

}
