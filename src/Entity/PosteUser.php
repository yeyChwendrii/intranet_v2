<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Cocur\Slugify\Slugify;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PosteUserRepository")
 * @UniqueEntity(
 *     fields={"poste", "user"},
 *     message="Ce poste choisi est déjà attribué à cet ville"
 * )
 */
class PosteUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Poste", inversedBy="posteUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $poste;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posteUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Fonction", inversedBy="posteUsers")
     */
    private $fonctions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserServicedepartementdirection", mappedBy="posteuser")
     */
    private $posteuserServicedepartementdirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserServicedirection", mappedBy="posteuser")
     */
    private $posteuserServicedirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserDepartementdirection", mappedBy="posteuser")
     */
    private $posteuserDepartementdirections;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PosteuserDirection", mappedBy="posteuser")
     */
    private $posteuserDirections;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $nomEvenement;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idEvenement;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEvenementAt;

    public function __construct()
    {
        $this->posteUserServices = new ArrayCollection();
        $this->posteUserDepartements = new ArrayCollection();
        $this->posteUserDirections = new ArrayCollection();
        $this->fonctions = new ArrayCollection();
        $this->posteuserServicedepartementdirections = new ArrayCollection();
        $this->posteuserServicedirections = new ArrayCollection();
        $this->posteuserDepartementdirections = new ArrayCollection();
        $this->posteuserDirections = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getPoste() . ' (' . $this->getUser() . ')';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->poste);
    }

    public function getPoste(): ?Poste
    {
        return $this->poste;
    }

    public function setPoste(?Poste $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Fonction[]
     */
    public function getFonctions(): Collection
    {
        return $this->fonctions;
    }

    public function addFonction(Fonction $fonction): self
    {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions[] = $fonction;
        }

        return $this;
    }

    public function removeFonction(Fonction $fonction): self
    {
        if ($this->fonctions->contains($fonction)) {
            $this->fonctions->removeElement($fonction);
        }

        return $this;
    }

    /**
     * @return Collection|PosteuserServicedepartementdirection[]
     */
    public function getPosteuserServicedepartementdirections(): Collection
    {
        return $this->posteuserServicedepartementdirections;
    }

    public function addPosteuserServicedepartementdirection(PosteuserServicedepartementdirection $posteuserServicedepartementdirection): self
    {
        if (!$this->posteuserServicedepartementdirections->contains($posteuserServicedepartementdirection)) {
            $this->posteuserServicedepartementdirections[] = $posteuserServicedepartementdirection;
            $posteuserServicedepartementdirection->setPosteuser($this);
        }

        return $this;
    }

    public function removePosteuserServicedepartementdirection(PosteuserServicedepartementdirection $posteuserServicedepartementdirection): self
    {
        if ($this->posteuserServicedepartementdirections->contains($posteuserServicedepartementdirection)) {
            $this->posteuserServicedepartementdirections->removeElement($posteuserServicedepartementdirection);
            // set the owning side to null (unless already changed)
            if ($posteuserServicedepartementdirection->getPosteuser() === $this) {
                $posteuserServicedepartementdirection->setPosteuser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PosteuserServicedirection[]
     */
    public function getPosteuserServicedirections(): Collection
    {
        return $this->posteuserServicedirections;
    }

    public function addPosteuserServicedirection(PosteuserServicedirection $posteuserServicedirection): self
    {
        if (!$this->posteuserServicedirections->contains($posteuserServicedirection)) {
            $this->posteuserServicedirections[] = $posteuserServicedirection;
            $posteuserServicedirection->setPosteuser($this);
        }

        return $this;
    }

    public function removePosteuserServicedirection(PosteuserServicedirection $posteuserServicedirection): self
    {
        if ($this->posteuserServicedirections->contains($posteuserServicedirection)) {
            $this->posteuserServicedirections->removeElement($posteuserServicedirection);
            // set the owning side to null (unless already changed)
            if ($posteuserServicedirection->getPosteuser() === $this) {
                $posteuserServicedirection->setPosteuser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PosteuserDepartementdirection[]
     */
    public function getPosteuserDepartementdirections(): Collection
    {
        return $this->posteuserDepartementdirections;
    }

    public function addPosteuserDepartementdirection(PosteuserDepartementdirection $posteuserDepartementdirection): self
    {
        if (!$this->posteuserDepartementdirections->contains($posteuserDepartementdirection)) {
            $this->posteuserDepartementdirections[] = $posteuserDepartementdirection;
            $posteuserDepartementdirection->setPosteuser($this);
        }

        return $this;
    }

    public function removePosteuserDepartementdirection(PosteuserDepartementdirection $posteuserDepartementdirection): self
    {
        if ($this->posteuserDepartementdirections->contains($posteuserDepartementdirection)) {
            $this->posteuserDepartementdirections->removeElement($posteuserDepartementdirection);
            // set the owning side to null (unless already changed)
            if ($posteuserDepartementdirection->getPosteuser() === $this) {
                $posteuserDepartementdirection->setPosteuser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PosteuserDirection[]
     */
    public function getPosteuserDirections(): Collection
    {
        return $this->posteuserDirections;
    }

    public function addPosteuserDirection(PosteuserDirection $posteuserDirection): self
    {
        if (!$this->posteuserDirections->contains($posteuserDirection)) {
            $this->posteuserDirections[] = $posteuserDirection;
            $posteuserDirection->setPosteuser($this);
        }

        return $this;
    }

    public function removePosteuserDirection(PosteuserDirection $posteuserDirection): self
    {
        if ($this->posteuserDirections->contains($posteuserDirection)) {
            $this->posteuserDirections->removeElement($posteuserDirection);
            // set the owning side to null (unless already changed)
            if ($posteuserDirection->getPosteuser() === $this) {
                $posteuserDirection->setPosteuser(null);
            }
        }

        return $this;
    }

    public function getNomEvenement(): ?string
    {
        return $this->nomEvenement;
    }

    public function setNomEvenement(?string $nomEvenement): self
    {
        $this->nomEvenement = $nomEvenement;

        return $this;
    }

    public function getIdEvenement(): ?int
    {
        return (int)$this->idEvenement;
    }

    public function setIdEvenement(?int $idEvenement): self
    {
        $this->idEvenement = $idEvenement;

        return $this;
    }

    public function getDateEvenementAt(): ?\DateTimeInterface
    {
        return $this->dateEvenementAt;
    }

    public function setDateEvenementAt(?\DateTimeInterface $dateEvenementAt): self
    {
        $this->dateEvenementAt = $dateEvenementAt;

        return $this;
    }

}
