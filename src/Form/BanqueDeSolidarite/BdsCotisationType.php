<?php

namespace App\Form\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\BdsCotisation;
use App\Form\DataTransformer\IncompleteDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BdsCotisationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cotisationappliquee')
            /*
            ->add('cotisationAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            */
            ->add('BdsParametresdecotisation')

            ->add(
                $builder->create('cotisationAt', DateType::class, array(
                    'format'        => 'MMMM-yyyyd', // you need to have 'y', 'M' and 'd' here
                    'years'         => range(date('Y'), date('Y') - 32, -1),
                    'required'      => true,
                    'label'         =>false
                ))
                    ->addViewTransformer(new IncompleteDateTransformer()))
        ;




    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BdsCotisation::class,
        ]);
    }
}
