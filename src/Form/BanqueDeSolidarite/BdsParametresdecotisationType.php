<?php

namespace App\Form\BanqueDeSolidarite;

use App\Entity\BanqueDeSolidarite\BdsParametresdecotisation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BdsParametresdecotisationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('salairedebase')
            ->add('cotisationpardefaut')
            ->add('cotisationappliquee')
            ->add('observations')
            //->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BdsParametresdecotisation::class,
        ]);
    }
}
