<?php

namespace App\Form;

use App\Entity\PosteuserServicedepartementdirection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PosteuserServicedepartementdirectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('posteuser')
            ->add('servicedepartementdirection')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PosteuserServicedepartementdirection::class,
        ]);
    }
}
