<?php

namespace App\Form\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Evenements\EvSuspension;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvSuspensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('user')
            ->add('reference')
            ->add('poste')
            ->add('motif')
            ->add('dateSuspensionAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('service_departement_direction')
            ->add('observations')
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Evenements\EvSuspension',
            'translation_domain' => 'forms',
            'allow_extra_fields' => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'suspension';
    }
}
