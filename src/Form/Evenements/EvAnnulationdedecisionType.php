<?php

namespace App\Form\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Evenements\EvAnnulationdedecision;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvAnnulationdedecisionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('user')
            ->add('decisionAAnnuler', ChoiceType::class, [
                'choices'=> $this->getDecisionAAnnulerChoices()
            ])
            ->add('reference')
            ->add('matricule')
            ->add('dateAnnulationdedecisionAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('observations')

        ;


    }


    private function getDecisionAAnnulerChoices()
    {
        $choices = EvAnnulationdedecision::DECISION_A_ANNULER;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Evenements\EvAnnulationdedecision',
            'translation_domain' => 'forms',
            "allow_extra_fields" => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'annulationdedecision';
    }
}
