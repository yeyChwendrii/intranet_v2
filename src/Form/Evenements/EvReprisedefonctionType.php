<?php

namespace App\Form\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Evenements\EvReprisedefonction;
use App\Entity\Poste;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvReprisedefonctionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('user')
            ->add('reference')
            ->add('poste')
            ->add('serviceDepartementDirection')
            ->add('dateReprisedefonctionAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('observations')

        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Evenements\EvReprisedefonction',
            'translation_domain' => 'forms',
            'allow_extra_fields' => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reprisedefonction';
    }
}
