<?php

namespace App\Form\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Evenements\EvReintegration;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvReintegrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('user')
            ->add('reference')
            ->add('poste', null, [
                'required' => 'true',
            ])
            ->add('dateReintegrationAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('categorie')
            ->add('indiceGroupe', ChoiceType::class, [
                'choices' => $this->getIndGroupeChoices()
            ])
            ->add('indiceCategorie', ChoiceType::class, [
                'choices' => $this->getIndCategorieChoices()
            ])
            ->add('indiceEchelon', ChoiceType::class, [
                'choices' => $this->getIndEchelonChoices()
            ])
            ->add('observations')
            ->add( 'direction', EntityType::class, [
                'class' => 'App\Entity\Direction',
                'placeholder' => 'Sélectionnez la direction',
                //'mapped' => false,
                'required' => false,
            ] )
        ;

        $builder->get( 'direction' )->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $direction = $event->getForm()->getData();
                //Ajout du département quand on aura selectionné la direction
                $this->addDepartementField($form->getParent(), $direction );
                //Ajout du service de direction quand on aura selectionné la direction
                $this->addServicedirectionField($form->getParent(), $direction );


            }
        );


        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event){
                $data = $event->getData();
                $serviceDepartement = $data->getServiceDepartement();
                $serviceDirection = $data->getServiceDirection();
                $departement   = $data->getDepartement();
                $form = $event->getForm();
                /* @var $serviceDepartement ServiceDepartementdirection
                 * @var $serviceDirection ServiceDirection
                 */
                if ($serviceDepartement or $serviceDirection) {

                    if ($serviceDepartement){
                        $departement = $serviceDepartement->getDepartementDirection();
                        $direction = $departement->getDirection();

                    }elseif($serviceDirection){
                        $direction = $serviceDirection->getDirection();
                        $departement = null;
                    }else{
                        $direction = null;
                    }
                    $this->addDepartementField($form, $direction);
                    $this->addServiceDirectionField($form, $direction);
                    $this->addServiceDepartementField($form, $departement);
                    if ($serviceDepartement){
                        $form->get('direction')->setData($direction);
                    }elseif($serviceDirection){
                        $form->get('direction')->setData($direction);
                    }
                    $form->get('departement')->setData($departement);

                }elseif($departement){
                    $direction = $departement->getDirection();
                    $this->addDepartementField($form, $direction);
                    $this->addServicedirectionField($form, null);
                    $this->addServicedepartementField($form, null);
                }else{
                    $this->addDepartementField($form, null);
                    $this->addServicedirectionField($form, null);
                    $this->addServicedepartementField($form, null);
                }
            }
        );
    }



    /** Rajoute un champ departement au formulaire
     * @param FormInterface $form
     * @param Direction $direction
     */
    private function addDepartementField(FormInterface $form, Direction $direction = null)
    {
        $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'departement',
            EntityType::class,
            null,
            [
                'class' => 'App\Entity\DepartementDirection',
                'placeholder' => $direction ? 'Sélectionnez votre département' : "Selectionner d'abord une direction",
                //'mapped' => false,
                'required' => false,
                'auto_initialize' => false,
                'empty_data' => null,
                'choices' => $direction ? $direction->getDepartementDirections(): []

            ]

        );
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm()->getParent();

                $dep = $event->getForm()->getData();

                if ($dep instanceof DepartementDirection) {

                    $this->addServicedepartementField( $form, $dep );
                }
            }
        );

        $form->add( $builder->getForm() );
    }

    /** Rajoute un champ Service dpt au formulaire
     * @param FormInterface $form
     * @param DepartementDirection $departement
     */
    private function addServicedepartementField(FormInterface $form, DepartementDirection $departement = null)
    {
        $form->add(
            'serviceDepartement',
            EntityType::class,

            [
                'class' => 'App\Entity\ServiceDepartementdirection',
                'placeholder' => $departement ? 'Sélectionnez le Service du département' : "Selectionner d'abord le département",
                'choices' => $departement? $departement->getServiceDepartementdirections() : [],
                'required' => false,

            ]
        );


    }

    /** Rajoute un champ Service de direction au formulaire
     * @param FormInterface $form
     * @param Direction $direction
     */
    private function addServicedirectionField(FormInterface $form, Direction $direction = null)
    {
        $form->add(
            'serviceDirection',
            EntityType::class,

            [
                'class' => 'App\Entity\ServiceDirection',
                'placeholder' => $direction ? 'Sélectionnez le Service de la direction' : "Selectionner d'abord la direction",
                'choices' => $direction ? $direction->getServiceDirections() : [],
                'required' => false,
            ]
        );


    }

    private function getIndGroupeChoices()
    {
        $choices = EvReintegration::IND_GROUPE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndCategorieChoices()
    {
        $choices = EvReintegration::IND_CATEGORIE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndEchelonChoices()
    {
        $choices = EvReintegration::IND_ECHELON;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Evenements\EvReintegration',
            'translation_domain' => 'forms',
            'allow_extra_fields' => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reintegration';
    }
}
