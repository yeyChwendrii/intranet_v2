<?php

namespace App\Form\Evenements;

use App\Entity\DepartementDirection;
use App\Entity\Direction;
use App\Entity\Evenements\EvAvancementnormal;
use App\Entity\ServiceDepartementdirection;
use App\Entity\ServiceDirection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvAvancementnormalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('user')
            ->add('reference')
            ->add('dateAvancementnormalAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('poste', null, [
                'required' => 'true',
            ])
            ->add('categorie')
            ->add('indiceGroupe', ChoiceType::class, [
                'choices' => $this->getIndGroupeChoices()
            ])
            ->add('indiceCategorie', ChoiceType::class, [
                'choices' => $this->getIndCategorieChoices()
            ])
            ->add('indiceEchelon', ChoiceType::class, [
                'choices' => $this->getIndEchelonChoices()
            ])
            ->add('observations')
        ;

    }

    private function getIndGroupeChoices()
    {
        $choices = EvAvancementnormal::IND_GROUPE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndCategorieChoices()
    {
        $choices = EvAvancementnormal::IND_CATEGORIE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndEchelonChoices()
    {
        $choices = EvAvancementnormal::IND_ECHELON;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Evenements\EvAvancementnormal',
            'translation_domain' => 'forms',
            'allow_extra_fields' => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'avancementnormal';
    }
}
