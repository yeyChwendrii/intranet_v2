<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label'=> false,
            ])
            //->add('usernameCanonical')
            ->add('email')
            ->add('emailCanonical')
            ->add('enabled')
            ->add('salt')
            //->add('password')
            //->add('lastLogin')
            //->add('confirmationToken')
            //->add('passwordRequestedAt')
            ->add('roles')
            //->add('lastActivityAt')
            //->add('loginCount')
            //->add('firstLogin')
            ->add('matricule')
            ->add('nom')
            ->add('prenom')
            ->add('adresse')
            ->add('sexe', ChoiceType::class, [
                'choices'=> $this->getSexeChoices()
            ])
            ->add('dateNaissanceAt', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('tel')

            ->add('indiceGroupe', ChoiceType::class, [
                'choices' => $this->getIndGroupeChoices()
            ])
            ->add('indiceCategorie', ChoiceType::class, [
                'choices' => $this->getIndCategorieChoices()
            ])
            ->add('indiceEchelon', ChoiceType::class, [
                'choices' => $this->getIndEchelonChoices()
            ])

            ->add('image')

            ->add('imageFile', FileType::class, [
                'required'  => false,
                'label'     => false
            ])
            //->add('imageSize')
            //->add('updatedAt')
            //->add('enligne')
            ->add('lieuDeNaissance')
            ->add('statut')
            ->add('categorie')
            //->add('userOptionVisibilite')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    private function getSexeChoices()
    {
        $choices = User::SEXE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }


    private function getIndGroupeChoices()
    {
        $choices = User::IND_GROUPE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndCategorieChoices()
    {
        $choices = User::IND_CATEGORIE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndEchelonChoices()
    {
        $choices = User::IND_ECHELON;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

}
