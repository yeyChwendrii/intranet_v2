<?php

namespace App\Form\AffairesSociales;

use App\Entity\AffairesSociales\Medecin;
use App\Entity\AffairesSociales\SituationDesRepos;
use App\Entity\User;
use App\Repository\AffairesSociales\MedecinRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SituationDesReposType extends AbstractType
{
    private $userRepository;
    private $medecinRepository;

    public function __construct(UserRepository $userRepository, MedecinRepository $medecinRepository)
    {
        $this->userRepository = $userRepository;
        $this->medecinRepository = $medecinRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebutAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('dateFinAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('medecin', EntityType::class, [
                'class' => Medecin::class,
                'choice_label' => function(Medecin $medecin) {
                    return sprintf('(%d) %s', $medecin->getId(), $medecin->getNomPrenom());
                },
                'choices' => $this->medecinRepository->findAll(),
                'required' => true,
                'multiple' => false
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => function(User $receivers) {
                    return sprintf('(%d) %s', $receivers->getMatricule(), $receivers->getNomPrenom());
                },
                'choices' => $this->userRepository->findAll(),
                'required' => true,
                'multiple' => false
            ])
            ->add('observations')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SituationDesRepos::class,
        ]);
    }
}
