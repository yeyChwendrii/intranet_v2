<?php

namespace App\Form\AffairesSociales;

use App\Entity\AffairesSociales\ChargeMedicale;
use App\Entity\AffairesSociales\EntiteMedicale;
use App\Form\DataTransformer\IncompleteDateTransformer;
use App\Repository\AffairesSociales\EntiteMedicaleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChargeMedicaleType extends AbstractType
{
    private $entiteMedicaleRepository;

    public function __construct(EntiteMedicaleRepository $entiteMedicaleRepository)
    {
        $this->entiteMedicaleRepository = $entiteMedicaleRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('dateAt')
            ->add('couts')
            ->add('entiteMedicale', EntityType::class, [
                'class' => EntiteMedicale::class,
                'choice_label' => function(EntiteMedicale $entiteMedicale) {
                    return sprintf('%s - %s', $entiteMedicale->getType(), $entiteMedicale->getDesignation());
                },
                'choices' => $this->entiteMedicaleRepository->findAll(),
                'required' => true,
                'multiple' => false
            ])
            ->add(
                $builder->create('dateAt', DateType::class, array(
                    'format'        => 'MMMM-yyyyd', // you need to have 'y', 'M' and 'd' here
                    'years'         => range(date('Y'), date('Y') - 32, -1),
                    'required'      => true,
                    'label'         =>false
                ))
                    ->addViewTransformer(new IncompleteDateTransformer()))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChargeMedicale::class,
        ]);
    }
}
