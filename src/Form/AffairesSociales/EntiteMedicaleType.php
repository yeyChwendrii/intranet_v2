<?php

namespace App\Form\AffairesSociales;

use App\Entity\AffairesSociales\EntiteMedicale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntiteMedicaleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('type', ChoiceType::class, [
                'choices' => $this->getTypeChoices()
            ])
            ->add('tel')
            ->add('adresse')
        ;
    }

    private function getTypeChoices()
    {
        $choices = EntiteMedicale::TYPE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EntiteMedicale::class,
        ]);
    }
}
