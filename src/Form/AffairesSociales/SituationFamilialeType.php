<?php

namespace App\Form\AffairesSociales;

use App\Entity\AffairesSociales\SituationFamiliale;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SituationFamilialeType extends AbstractType
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('situation', ChoiceType::class, [
                'choices'=> $this->getSituationChoices()
            ])
            ->add('imageFile', FileType::class, [
                'required'  => false,
                'label'     => false
            ])
            /*
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => function(User $receivers) {
                    return sprintf('(%d) %s', $receivers->getMatricule(), $receivers->getNomPrenom());
                },
                'choices' => $this->userRepository->findAll(),
                'required' => true,
                'multiple' => false
            ])
            */
        ;
    }

    private function getSituationChoices()
    {
        $choices = SituationFamiliale::SITUATION;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SituationFamiliale::class,
        ]);
    }
}
