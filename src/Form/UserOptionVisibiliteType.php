<?php

namespace App\Form;

use App\Entity\UserOptionVisibilite;
use function PHPSTORM_META\type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserOptionVisibiliteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('optLastLogin', CheckboxType::class, [
                'label' => 'Rendre visible la date de ma dernière connexion ?',
                'required' => false,
            ])
            ->add('optLastActivityAt', CheckboxType::class, [
                'label' => 'Rendre visible la date de ma dernière activité ?',
                'required' => false,
            ])
            ->add('optLoginCount', CheckboxType::class, [
                'label' => 'Rendre visible le nombre de mes connexions ?',
                'required' => false,
            ])
            ->add('optFirstLogin', CheckboxType::class, [
                'label' => 'Rendre visible la date de ma première connexion ?',
                'required' => false,
            ])
            ->add('optAdresse', CheckboxType::class, [
                'label' => 'Rendre visible mon adresse postale ?',
                'required' => false,
            ])
            ->add('optDateNaissanceAt', CheckboxType::class, [
                'label' => 'Rendre visible ma date de naissance ?',
                'required' => false,
            ])
            ->add('optTel', CheckboxType::class, [
                'label' => 'Rendre visible mon numéro de téléphonne ?',
                'required' => false,
            ])
           ->add('optIndiceGroupe', CheckboxType::class, [
               'label' => 'Rendre visible mon indice (Groupe/Catégorie/Echelon) ?',
               'required' => false,
           ])
            ->add('optEnligne', CheckboxType::class, [
                'label' => 'Rendre visible mon statut (en ligne ou hors ligne) ?',
                'required' => false,
            ])
            ->getForm()
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserOptionVisibilite::class,
        ]);
    }
}
