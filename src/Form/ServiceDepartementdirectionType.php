<?php

namespace App\Form;

use App\Entity\DepartementDirection;
use App\Entity\Service;
use App\Entity\ServiceDepartementdirection;
use App\Repository\DepartementDirectionRepository;
use App\Repository\ServiceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceDepartementdirectionType extends AbstractType
{
    private $service;
    private $departement;

    public function __construct(ServiceRepository $serviceRepository, DepartementDirectionRepository $departementDirectionRepository)
    {
        $this->service = $serviceRepository;
        $this->departement = $departementDirectionRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tel')
            ->add('service', EntityType::class, [
                'class' => Service::class,
                'choice_label' => function(Service $service) {
                    return sprintf('%s', $service->getDesignation());
                },
                'choices' => $this->service->serviceNotLinked(),
                'required' => true,
                'multiple' => false,
            ])
            ->add('departementdirection', EntityType::class, [
                'class' => DepartementDirection::class,
                'choice_label' => function(DepartementDirection $departementDirection) {
                    return sprintf('%s - %s', $departementDirection->getDepartement()->getDesignation(), $departementDirection->getDirection());
                },
                'choices' => $this->departement->showDepartement(),
                'required' => true,
                'multiple' => false,
            ])
            //->add('departementdirection')
            ->add('agence')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ServiceDepartementdirection::class,
        ]);
    }
}
