<?php

namespace App\Form\Admin;

use App\Entity\Admin\EcFormation;
use App\Entity\Admin\EcFormationuser;
use App\Entity\Admin\Ecoleuniversite;
use App\Entity\Admin\EcSousmetier;
use App\Entity\Admin\Niveauqualification;
use App\Repository\Admin\EcoleuniversiteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EcFormationuserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('user')
            ->add('dateDebutAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('dateFinAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('observations')

            ->add( 'ecoleuniversite', EntityType::class, [
                'class' => 'App\Entity\Admin\Ecoleuniversite',
                'placeholder' => 'Sélectionnez l école / l université',
                'mapped' => true,
                'required' => true,
            ] )
        ;

        $builder->get( 'ecoleuniversite' )->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $ecoleuniversite = $event->getForm()->getData();
                //Ajout du sousmetier quand on aura selectionné l'école
                $this->addEcSousmetierField($form->getParent(), $ecoleuniversite );

                $this->addNiveauqualificationField($form->getParent(), $ecoleuniversite );
            }
        );

        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event){
                $data = $event->getData();
                $ecFormation = $data->getEcFormation();
                $niveauqualification = $data->getNiveauqualification();
                $form = $event->getForm();
                /* @var $ecFormation EcFormation
                 * @var $niveauqualification Niveauqualification
                 */
                if ($ecFormation or $niveauqualification) {

                    if ($ecFormation){
                        /** @var EcSousmetier $ecSousmetier */
                        $ecSousmetier = $ecFormation->getEcSousmetier();
                        /** @var Ecoleuniversite $ecoleuniversite */
                        $ecoleuniversite = $ecSousmetier->getEcoleuniversites();

                    }elseif($niveauqualification){
                        $ecoleuniversite = $niveauqualification->getEcoleuniversites();
                        $ecSousmetier = null;
                    }else{
                        $ecoleuniversite = null;
                    }

                    if ($ecoleuniversite instanceof Ecoleuniversite){
                        $ecoleuniversite = $ecSousmetier->getEcoleuniversites();
                    }else{
                        foreach ($ecoleuniversite as $value){
                            $ecoleuniversite = $value;
                        }
                    }

                    $this->addEcSousmetierField($form, $ecoleuniversite);
                    $this->addEcFormationField($form, $ecSousmetier);
                    $this->addNiveauqualificationField($form, $ecoleuniversite);
                    if ($ecFormation){
                        $form->get('ecoleuniversite')->setData($ecoleuniversite);
                    }elseif($niveauqualification){
                        $form->get('ecoleuniversite')->setData($ecoleuniversite);
                    }
                    $form->get('ecSousmetier')->setData($ecSousmetier);

                }else{
                    $this->addEcSousmetierField($form, null);
                    $this->addEcFormationField($form, null);
                    $this->addNiveauqualificationField($form, null);
                }
            }
        );
    }


    /** Rajoute un champ EcSousmetier au formulaire
     * @param FormInterface $form
     * @param Ecoleuniversite $ecoleuniversite
     */
    private function addEcSousmetierField(FormInterface $form, Ecoleuniversite $ecoleuniversite = null)
    {

        $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'ecSousmetier',
            EntityType::class,
            null,
            [
                'class' => 'App\Entity\Admin\EcSousmetier',
                'placeholder' => $ecoleuniversite ? 'Sélectionnez le sous-metier' : "Selectionner d'abord une école/université",
                'mapped' => true,
                'required' => true,
                'auto_initialize' => false,
                'empty_data' => null,
                'choices' => $ecoleuniversite ? $ecoleuniversite->getEcSousmetier() : []

            ]

        );
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm()->getParent();

                $ecSm = $event->getForm()->getData();

                if ($ecSm instanceof EcSousmetier) {

                    $this->addEcFormationField( $form, $ecSm );
                }
            }
        );

        $form->add( $builder->getForm() );
    }

    /** Rajoute un champ EcFormation au formulaire
     * @param FormInterface $form
     * @param EcSousmetier $ecSousmetier
     */
    private function addEcFormationField(FormInterface $form, EcSousmetier $ecSousmetier = null)
    {
        $form->add(
            'ecFormation',
            EntityType::class,

            [
                'class' => 'App\Entity\Admin\EcFormation',
                'placeholder' => $ecSousmetier ? 'Sélectionnez le domaine de formation' : "Selectionner d'abord le sous-metier",
                'choices' => $ecSousmetier ? $ecSousmetier->getEcFormations() : [],
                'required' => true,

            ]
        );


    }

    /** Rajoute un champ niveauqualification au formulaire
     * @param FormInterface $form
     * @param Ecoleuniversite $ecoleuniversite
     */
    private function addNiveauqualificationField(FormInterface $form, Ecoleuniversite $ecoleuniversite = null)
    {
        $form->add(
            'niveauqualification',
            EntityType::class,
            [
                'class' => 'App\Entity\Admin\Niveauqualification',
                'placeholder' => $ecoleuniversite ? 'Sélectionnez le niveau de qualification' : "Selectionner d'abord l'école / l'université",
                'choices' => $ecoleuniversite ? $ecoleuniversite->getNiveauqualification() : [],
                'required' => true,

            ]
        );


    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EcFormationuser::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ec_formationuser';
    }
}
