<?php

namespace App\Form\Admin;

use App\Entity\Admin\EcFormation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EcFormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('ecSousmetier', EntityType::class, [
                'class' => 'App\Entity\Admin\EcSousmetier',
                'placeholder' => 'Sélectionnez le sous-metier',
                'mapped' => true,
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EcFormation::class,
        ]);
    }
}
