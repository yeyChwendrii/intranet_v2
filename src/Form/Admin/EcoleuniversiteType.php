<?php

namespace App\Form\Admin;

use App\Entity\Admin\Ecoleuniversite;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EcoleuniversiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ecoleOuUniversite',ChoiceType::class, [
                'choices' => $this->getEcoleUniversiteChoices()
            ])
            ->add('designation')
            ->add('nomUniversite')
            ->add('tel')
            ->add('email')
            ->add('pays')
            ->add('ville')
            ->add('ecSousmetier')
            ->add('observations')
            ->add('niveauqualification', null, [
                'required' => 'true',
            ])
        ;
    }

    private function getEcoleUniversiteChoices()
    {
        $choices = Ecoleuniversite::ECOLE_UNIVERSITE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ecoleuniversite::class,
        ]);
    }
}
