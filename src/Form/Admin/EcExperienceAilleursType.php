<?php

namespace App\Form\Admin;

use App\Entity\Admin\EcExperienceAilleurs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EcExperienceAilleursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => $this->getTypeChoices()
            ])
            ->add('dateDebutAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('dateFinAt', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('societe')
            ->add('responsableEnCharge')
            ->add('posteOccupe')
            ->add('dirDeptServ')
            ->add('observations')
        ;
    }

    private function getTypeChoices()
    {
        $choices = EcExperienceAilleurs::TYPE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EcExperienceAilleurs::class,
        ]);
    }
}
