<?php

namespace App\Form\Admin;

use App\Entity\Admin\EcSousmetier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EcSousmetierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('ecMetier', EntityType::class, [
                'class' => 'App\Entity\Admin\EcMetier',
                'placeholder' => 'Sélectionnez le metier',
                'mapped' => true,
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EcSousmetier::class,
        ]);
    }
}
