<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;

class RegistrationFormType extends AbstractType

{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('matricule')
            ->add('nom')
            ->add('prenom')
            ->add('sexe', ChoiceType::class, [
                'choices'=> $this->getSexeChoices()
            ])
            ->add('dateNaissanceAt', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('lieuDeNaissance')
            //->add('statut')
            //->add('categorie')
            ->add('tel')
            ->add('adresse')

            /*
            ->add('remarque')
            ->add('indiceGroupe', ChoiceType::class, [
                'choices' => $this->getIndGroupeChoices()
            ])
            ->add('indiceCategorie', ChoiceType::class, [
                'choices' => $this->getIndCategorieChoices()
            ])
            ->add('indiceEchelon', ChoiceType::class, [
                'choices' => $this->getIndEchelonChoices()
            ])
            */
            ->add('enligne')
            ->add('enfonction')

        ;
    }

    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }

    private function getSexeChoices()
    {
        $choices = User::SEXE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    /*
    private function getIndGroupeChoices()
    {
        $choices = User::IND_GROUPE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndCategorieChoices()
    {
        $choices = User::IND_CATEGORIE;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    private function getIndEchelonChoices()
    {
        $choices = User::IND_ECHELON;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }
    */
}