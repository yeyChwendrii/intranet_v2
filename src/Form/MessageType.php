<?php

namespace App\Form;

use App\Entity\Message;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sujet')
            ->add('contenu')
            ->add('receivers', EntityType::class, [
                'class' => User::class,
                'choice_label' => function(User $receivers) {
                    return sprintf('(%d) %s', $receivers->getMatricule(), $receivers->getNomPrenom());
                },
                'choices' => $this->userRepository->showAllEnfonctionWithActivAccount(),
                'required' => true,
                'multiple' => true,
                'placeholder' => 'Select'
            ])
            //->add('dateEnvoiAt')
            //->add('typeMessage')
            //Commenté parce que c'est le champs de l'agent expedireur dont agent connecté
            //->add('user')
                /*
            ->add('userSender', EntityType::class, [
                'class' => User::class,
                'choice_label' => function(User $userSender) {
                    return sprintf('(%d) %s', $userSender->getMatricule(), $userSender->getNomPrenom());
                },
                'choices' => $this->userRepository->selectAgentofDptenergie(),
                'required' => true,
                'multiple' => true
            ])
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
        ]);
    }
}
