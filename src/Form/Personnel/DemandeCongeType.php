<?php

namespace App\Form\Personnel;

use App\Entity\Personnel\DemandeConge;
use App\Entity\Personnel\ModeleConge;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Vich\UploaderBundle\Form\Type\VichFileType;

class DemandeCongeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebutAt', DateType::class, [
                'required' => true,
                'label' => false,
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ])
            ->add('dateFinAt', DateType::class, [
                'required' => true,
                'label' => false,
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control',
                ),
            ])
            ->add('motif', TextareaType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('lieuJouissance', TextType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('docFile', VichFileType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('modeleConge', EntityType::class, [
                'required' => false,
                'label' => false,
                'class' => ModeleConge::class,
                'choice_label' => 'titre',
                'multiple' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DemandeConge::class,
        ]);
    }
}
