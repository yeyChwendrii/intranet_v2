<?php

namespace App\Form\Personnel;

use App\Entity\Personnel\CongeDataSearch;
use App\Entity\Personnel\ModeleConge;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CongeDataSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebutAt', DateType::class,[
                'required' => false,
                'label' => false,
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control',
                )
            ])
            ->add('dateFinAt',DateType::class,[
                'required' => false,
                'label' => false,
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control',
                )
            ])
            ->add('modeleConge',EntityType::class,[
                'required' => false,
                'label' => false,
                'class' => ModeleConge::class,
                'choice_label' => 'titre',
                'multiple' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CongeDataSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
