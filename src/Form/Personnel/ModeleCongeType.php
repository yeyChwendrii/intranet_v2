<?php

namespace App\Form\Personnel;

use App\Entity\Personnel\ModeleConge;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModeleCongeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('delai')
            ->add('motif')
            ->add('justificatif')
//            ->add('createdAt')
//            ->add('enabled')
            ->add('categorieConge')
            ->add('avisResponsables')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ModeleConge::class,
        ]);
    }
}
