<?php

namespace App\Form\GSitesEnerg;

use App\Entity\GSitesEnerg\SitesEnergetiques;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SitesEnergetiquesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('observations')
            ->add('contact1', Contact1Type::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SitesEnergetiques::class,
        ]);
    }
}
