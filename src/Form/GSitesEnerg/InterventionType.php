<?php

namespace App\Form\GSitesEnerg;

use App\Entity\GSitesEnerg\Baieenergetique;
use App\Entity\GSitesEnerg\Intervention;
use App\Entity\GSitesEnerg\Materiel;
use App\Entity\GSitesEnerg\SitesEnergetiques;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterventionType extends AbstractType
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateInterventionAt', DateType::class, [
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('observations')
            ->add('saisipar')
            ->add('problemeconstate')
            ->add('solutionproposee')
            ->add('intervenant', EntityType::class, [
                'class' => User::class,
                'choice_label' => function(User $user) {
                    return sprintf('(%d) %s', $user->getMatricule(), $user->getNomPrenom());
                },
                'choices' => $this->userRepository->selectAgentofDptenergie(),
                'required' => true,
                'multiple' => true
            ])

            ->add('materiels')
        ;

    }
}
