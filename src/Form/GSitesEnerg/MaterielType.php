<?php

namespace App\Form\GSitesEnerg;

use App\Entity\GSitesEnerg\Materiel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('caracteristiques')
            ->add('observations')
            ->add('baieenergetique', EntityType::class, [
        'class' => 'App\Entity\GSitesEnerg\Baieenergetique',
        'placeholder' => 'Sélectionner la baie',
        'required' => true,
    ])
            ->add('nombre')
            ->add('typemateriel', ChoiceType::class, [
        'choices'=> $this->getTypematerielChoices()
    ])
        ;
    }

    private function getTypematerielChoices()
    {
        $choices = Materiel::TYPE_MATERIEL;
        $outpout = [];
        foreach ($choices as $k => $v){
            $outpout[$v] = $k;
        }
        return $outpout;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Materiel::class,
        ]);
    }
}
