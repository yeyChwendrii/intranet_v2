<?php

namespace App\Form\GSitesEnerg;

use App\Entity\GSitesEnerg\Baieenergetique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BaieenergetiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('observations')
            //->add('materiels')
            //->add('equipementconcernes')
            ->add('siteenergetique')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Baieenergetique::class,
        ]);
    }
}
