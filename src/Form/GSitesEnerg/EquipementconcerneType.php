<?php

namespace App\Form\GSitesEnerg;

use App\Entity\GSitesEnerg\Equipementconcerne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquipementconcerneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('designation')
            ->add('caracteristiques')
            ->add('observations')
            ->add('baieenergetique')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Equipementconcerne::class,
        ]);
    }
}
